<?php
/**
* Template Name: Add Roster Pages
*/
?>
<?php acf_form_head(); ?>
<?php get_header(); ?>

	<div id="primary" class="content-area">
		<div class="other-entry-header text-center">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
							<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
						</div><!-- .entry-header -->
				</div>
			</div>
		</div>
		<div class="breadcrumbs-container">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<?php dimox_breadcrumbs(); ?>
					</div>
				</div>
			</div>			
		</div>
		<main id="main" class="container site-main" role="main">
			<div class="row">
				<div class="col-xs-12">
					<div class="myarea-page-links pull-right">
						<a href="<?php echo site_url(); ?>/manager-myarea/manage-rosters/" class="btn custom-btn">Back to manage roster</a>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12">
					<?php
					while ( have_posts() ) : the_post(); ?>

						<?php get_template_part( 'template-parts/other', 'page' );?>

						<?php 
						// If comments are open or we have at least one comment, load up the comment template.
						if ( comments_open() || get_comments_number() ) :
							comments_template();
						endif;

					endwhile; // End of the loop.
					?>

					<?php
						$current_user = wp_get_current_user();
						if ($current_user->roles[1] == 'recruiter' || $current_user->roles[0] == 'recruiter') {


						// $form_id = '11'; 
						// $search_criteria = array(
						//     'status'        => 'active',					    
						// );
						// $entries = GFAPI::get_entries( $form_id, $search_criteria );
						// foreach ($entries as $value) {
						// 	$get_data = unserialize($value[1]);
						// 	foreach ($get_data as $value_) {
						// 		$date[] = $value_['Date'];
						// 	}
						// }

						// $unique = array_unique($date);
						// echo "<pre>";
						// print_r($unique);

						?>

						<!-- <table>
							<tbody>
						<?php 
							foreach ($entries as $key => $value) {
									$get_data = unserialize($value[1]);
								?>								
								<?php 
								// print_r($get_data);
								foreach ($get_data as $entrie_value) { ?>
									<tr>
										<td><?php echo $entrie_value['Date']; ?></td>
										<td>
											<?php 
												$user_info = get_userdata( $entrie_value['Staff Member'] );
												echo  $user_info->first_name;
											 	?>									 	
										 </td>
										<td><?php echo $entrie_value['Start Time']; ?></td>
										<td><?php echo $entrie_value['Finish Time']; ?></td>
									</tr>
								<?php 
								} ?>
							<?php 
							}
						?>
						</tbody>
					</table> -->
					<?php } ?>

				</div>
				<!-- <div class="col-xs-12 col-sm-3">
					<div id="sidebar" class="sidebar">
						<?php //get_sidebar(); ?>
					</div>
				</div> -->
			</div>

		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();