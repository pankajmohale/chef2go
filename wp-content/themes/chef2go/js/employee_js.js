/* employee_js */

jQuery(document).ready(function(){
	jQuery('#chefliastingcmd').click(function () {
		var timesheetfromvalue = $('#searchfrominput').attr('value');
		var timesheettovalue = $('#searchtoinput').attr('value');

		$.ajax({
	        type: 'POST',
	        url: ajaxurl,
	        data: {
	                action:'update_timesheet_value',
	                searchfromvalue : timesheetfromvalue,
	                searchtovalue : timesheettovalue
	        },
	        success:function(response){
	                if (response) {
	                    location.reload();
	                }
	        }
	    });
	});	
});