jQuery(document).ready(function($){

	//jQuery('.custom-datepicker .gfield_time_hour #input_1_420_1').timepicker({ 'timeFormat': 'H' });
	/*jQuery('.custom-datepicker .ginput_container input').pickatime({
	  // Escape any “rule” characters with an exclamation mark (!).
	  clear: '',
	  format: 'h:i A',
	});*/

	jQuery('.start-time .ginput_container input').pickatime({
		clear: 'Clear',
		interval: 15,
	});

	jQuery('.end-time .ginput_container input').pickatime({
		clear: 'Clear',
		interval: 15,
	});

	jQuery('.gfield_list_1_cell1 input').pickadate({
	  today: '',
	  clear: 'Clear selection',
	  close: 'Cancel'
	});

	jQuery('.gfield_list_1_cell3 input').pickatime({
		clear: 'Clear',
		interval: 15,
	});

	jQuery('.gfield_list_1_cell4 input').pickatime({
		clear: 'Clear',
		interval: 15,
	});

	jQuery('.gfield_list_icons .add_list_item').live('click', function(){
		jQuery('.gfield_list_1_cell1 input').pickadate({
			today: '',
			clear: 'Clear selection',
			close: 'Cancel'
		});

		jQuery('.gfield_list_1_cell3 input').pickatime({
			clear: 'Clear',
			interval: 15,
		});

		jQuery('.gfield_list_1_cell4 input').pickatime({
			clear: 'Clear',
			interval: 15,
		});
	});

	jQuery('.roster-date input').pickadate({
	  format: 'dd-m-yyyy',
	  today: '',
	  clear: 'Clear selection',
	  close: 'Cancel'
	});

	jQuery('.finish-time .ginput_container input').pickatime({
		clear: 'Clear',
		interval: 15,
	});

	jQuery('.start-time').pickatime({
		clear: 'Clear',
		interval: 15,
	});

	jQuery('.finish-time').pickatime({
		clear: 'Clear',
		interval: 15,
	});

});