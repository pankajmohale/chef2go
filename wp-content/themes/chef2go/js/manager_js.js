/* This file is for manger jquery only */

jQuery(document).ready(function($){

	/* Following functions are use for manager section to add, Delete, and edit client */
    /*$('.show-less-btn').click(function(){
        // $(this).parents().siblings('.extradata').addClass('show');
        if($(this).parents().siblings('.extradata').hasClass('show')) {
            $(this).parents().siblings('.extradata').removeClass('show');
            // $(this).removeClass('active');
        } else {
            $(this).parents().siblings('.extradata').addClass('show');
            // $(this).addClass('active');
        }
        $(this).parents().siblings('.extradata').toggle();
    });*/

   /* $('.show-less-btn').click(function(){
        // $(this).parents().parents().parents().find('.extradata').removeClass('active').removeAttr('style');
        // $(this).parents().parents().parents().find('.extradata');
        var isContentVisible = $(this).parents().siblings('.extradata').toggle().is(':visible');
        $(this).parents().siblings('.extradata').toggleClass('active', isContentVisible);
        // $(this).toggleClass('show-btn');
    });*/

    $('.show-less-btn').click(function(){
        if($(this).hasClass('active')) {
            $(this).removeClass('active');
            $(this).text('Read More');
        } else {
            $(this).text('Less More');
            $(this).addClass('active');
        }
        $(this).parents().siblings('.extradata').toggle();
    });

    $('.show-less-btn-dyanamic').live( "click", function() {
        if($(this).hasClass('active')) {
            $(this).removeClass('active');
            $(this).text('Read More');
        } else {
            $(this).text('Less More');
            $(this).addClass('active');
        }
        $(this).parents().siblings('.extradata-dyanamic').toggle();
    });

    $('.client-edit').click(function(){
        $(this).parents('.list-group-item').find('.edit-client').toggle('slow');
    });


    $( '.client-edit-live' ).live( "click", function() {
		$(this).parents('.list-group-item').find('.edit-client').toggle('slow');
    });

    // Full Time Chef listing
    $(document).on('click','.update-client',function(){
        var postid = $(this).attr('postid');
        var postslug = $(this).attr('postslug');
        var client_name = $('input#'+postslug+postid).val();
        var client_location = $('select#'+postslug+postid).val();
        
        // $('.recruiter_full_time_chef_listing').html('<div class="se-pre-con"></div>');
        $.ajax({
                type: 'POST',
                url: ajaxurl,
                data: {
                        action:'update_client_by_manager',
                        postid : postid,
                        postslug : postslug,
                        client_name : client_name,
                        client_location : client_location,
                },
                success:function(response){
                        // console.log(response);
                        location.reload();
                        // if (response) {
                        //     $('.recruiter_full_time_chef_listing').html(response); 
                        // }
                }
        });
    });

    // Full Time Chef listing
    $('.update-client-live').live('click',function(){
        var postid = $('.update-client-live').attr('postid');
        var postslug = $('.update-client-live').attr('postslug');
        var client_name = $('input#'+postslug+postid).val();
        var client_location = $('select#'+postslug+postid).val();
        
        // $('.recruiter_full_time_chef_listing').html('<div class="se-pre-con"></div>');
        $.ajax({
                type: 'POST',
                url: ajaxurl,
                data: {
                        action:'update_client_by_manager',
                        postid : postid,
                        postslug : postslug,
                        client_name : client_name,
                        client_location : client_location,
                },
                success:function(response){
                        // console.log(response);
                        location.reload();
                        // if (response) {
                        //     $('.recruiter_full_time_chef_listing').html(response); 
                        // }
                }
        });
    });

    $(document).on('click','.client-delete',function(){
        var postid = $(this).attr('postid');
        var postslug = $(this).attr('postslug');
        
        // $('.recruiter_full_time_chef_listing').html('<div class="se-pre-con"></div>');
        $.ajax({
                type: 'POST',
                url: ajaxurl,
                data: {
                        action:'delete_client_by_manager',
                        postid : postid,
                        postslug : postslug,
                },
                success:function(response){
                        // console.log(response);
                        location.reload();
                }
        });
    });
    
});

jQuery(document).ready(function($){

    // Update Roster from edit roster page 
    $(document).on('click','#edit_roster_update_post',function(){
        var post_id_value = $('.post-id-field').attr('value');
        var roster_member_list = $('#roster_member_list').attr('value');
        // Monday
        var roster_clients_1 = $('#roster_clients_1').attr('value');
        var monday_week_day = $('#monday_week_day').attr('value');
        var monday_staff_member = $('#monday_staff_member').attr('value');
        var monday_time_start = $('#monday_time_start').attr('value');
        var monday_time_finish = $('#monday_time_finish').attr('value');
        // Tuesday
        var roster_clients_2 = $('#roster_clients_2').attr('value');
        var tuesday_week_day = $('#tuesday_week_day').attr('value');
        var tuesday_staff_member = $('#tuesday_staff_member').attr('value');
        var tuesday_time_start = $('#tuesday_time_start').attr('value');
        var tuesday_time_finish = $('#tuesday_time_finish').attr('value');
        // Wednesday
        var roster_clients_3 = $('#roster_clients_3').attr('value');
        var wednesday_week_day = $('#wednesday_week_day').attr('value');
        var wednesday_staff_member = $('#wednesday_staff_member').attr('value');
        var wednesday_time_start = $('#wednesday_time_start').attr('value');
        var wednesday_time_finish = $('#wednesday_time_finish').attr('value');
        // Thursday
        var roster_clients_4 = $('#roster_clients_4').attr('value');
        var thursday_week_day = $('#thursday_week_day').attr('value');
        var thursday_staff_member = $('#thursday_staff_member').attr('value');
        var thursday_time_start = $('#thursday_time_start').attr('value');
        var thursday_time_finish = $('#thursday_time_finish').attr('value');
        // Friday
        var roster_clients_5 = $('#roster_clients_5').attr('value');
        var friday_week_day = $('#friday_week_day').attr('value');
        var friday_staff_member = $('#friday_staff_member').attr('value');
        var friday_time_start = $('#friday_time_start').attr('value');
        var friday_time_finish = $('#friday_time_finish').attr('value');
        // Saturday
        var roster_clients_6 = $('#roster_clients_6').attr('value');
        var saturday_week_day = $('#saturday_week_day').attr('value');
        var saturday_staff_member = $('#saturday_staff_member').attr('value');
        var saturday_time_start = $('#saturday_time_start').attr('value');
        var saturday_time_finish = $('#saturday_time_finish').attr('value');
        // Saturday
        var roster_clients_7 = $('#roster_clients_7').attr('value');
        var sunday_week_day = $('#sunday_week_day').attr('value');
        var sunday_staff_member = $('#sunday_staff_member').attr('value');
        var sunday_time_start = $('#sunday_time_start').attr('value');
        var sunday_time_finish = $('#sunday_time_finish').attr('value');
        // Field 8
        var roster_clients_8 = $('#roster_clients_8').attr('value');
        var field8_week_day = $('#field8_week_day').attr('value');
        var field8_staff_member = $('#field8_staff_member').attr('value');
        var field8_time_start = $('#field8_time_start').attr('value');
        var field8_time_finish = $('#field8_time_finish').attr('value');
        // Field 9
        var roster_clients_9 = $('#roster_clients_9').attr('value');
        var field9_week_day = $('#field9_week_day').attr('value');
        var field9_staff_member = $('#field9_staff_member').attr('value');
        var field9_time_start = $('#field9_time_start').attr('value');
        var field9_time_finish = $('#field9_time_finish').attr('value');
        // Field 10
        var roster_clients_10 = $('#roster_clients_10').attr('value');
        var field10_week_day = $('#field10_week_day').attr('value');
        var field10_staff_member = $('#field10_staff_member').attr('value');
        var field10_time_start = $('#field10_time_start').attr('value');
        var field10_time_finish = $('#field10_time_finish').attr('value');
        
        
        $('.edit-roster-sucess-message').html('<div class="se-pre-con"></div>');
        $.ajax({
                type: 'POST',
                url: ajaxurl,
                data: {
                    action:'editroster_data',
                    post_id_value : post_id_value,
                    
                    roster_member_list : roster_member_list,
                    
                    monday_week_day : monday_week_day,
                    monday_staff_member : monday_staff_member,
                    monday_time_start : monday_time_start,
                    monday_time_finish : monday_time_finish,
                    roster_clients_1 : roster_clients_1,
                    // Tuesday
                    tuesday_week_day : tuesday_week_day,
                    tuesday_staff_member : tuesday_staff_member,
                    tuesday_time_start : tuesday_time_start,
                    tuesday_time_finish : tuesday_time_finish,
                    roster_clients_2 : roster_clients_2,
                    // wednesday
                    wednesday_week_day : wednesday_week_day,
                    wednesday_staff_member : wednesday_staff_member,
                    wednesday_time_start : wednesday_time_start,
                    wednesday_time_finish : wednesday_time_finish,
                    roster_clients_3 : roster_clients_3,
                    // Thursday
                    thursday_week_day : thursday_week_day,
                    thursday_staff_member : thursday_staff_member,
                    thursday_time_start : thursday_time_start,
                    thursday_time_finish : thursday_time_finish,
                    roster_clients_4 : roster_clients_4,
                    // Friday
                    friday_week_day : friday_week_day,
                    friday_staff_member : friday_staff_member,
                    friday_time_start : friday_time_start,
                    friday_time_finish : friday_time_finish,
                    roster_clients_5 : roster_clients_5,
                    // Saturday
                    saturday_week_day : saturday_week_day,
                    saturday_staff_member : saturday_staff_member,
                    saturday_time_start : saturday_time_start,
                    saturday_time_finish : saturday_time_finish,
                    roster_clients_6 : roster_clients_6,
                    // Sunday
                    sunday_week_day : sunday_week_day,
                    sunday_staff_member : sunday_staff_member,
                    sunday_time_start : sunday_time_start,
                    sunday_time_finish : sunday_time_finish,
                    roster_clients_7 : roster_clients_7,
                    // field 8
                    field8_week_day : field8_week_day,
                    field8_staff_member : field8_staff_member,
                    field8_time_start : field8_time_start,
                    field8_time_finish : field8_time_finish,
                    roster_clients_8 : roster_clients_8,
                    // field 9
                    field9_week_day : field9_week_day,
                    field9_staff_member : field9_staff_member,
                    field9_time_start : field9_time_start,
                    field9_time_finish : field9_time_finish,
                    roster_clients_9 : roster_clients_9,
                    // field 10
                    field10_week_day : field10_week_day,
                    field10_staff_member : field10_staff_member,
                    field10_time_start : field10_time_start,
                    field10_time_finish : field10_time_finish,
                    roster_clients_10 : roster_clients_10,

                },
                success:function(response){
                    if (response) {
                        $('.edit-roster-sucess-message').html(response);
                        location.reload(); 
                    }
                }
        });        
    });

    // Manage roster listing according to loaction
    $(document).on('change','select#manager-client',function(){
        var client_residency = $(this).val();
        
        $('.client-listing').html('<div class="se-pre-con"></div>');
        $.ajax({
                type: 'POST',
                url: ajaxurl,
                data: {
                        action:'client_list_by_residency',
                        client_residency : client_residency,
                },
                success:function(response){
                        if (response != 'all') {
                            $('.client-listing').html(response);                            
                        } else {
                            location.reload();
                        }
                }
        });
    });

    
    // Delete Roster from recruiter list 
    $(document).on('click','.delete-roster-data',function(){
        var deleted_post_id = $(this).attr('post-id');
        console.log(deleted_post_id);
        
        $('.delete-sucess-message').html('<div class="se-pre-con"></div>');
        $.ajax({
                type: 'POST',
                url: ajaxurl,
                data: {
                    action:'deleteroster_data',
                    deleted_post_id : deleted_post_id
                },
                success:function(response){
                    if (response) {
                        $('.delete-sucess-message h4').html(response);
                        location.reload(); 
                    }
                }
        });        
    });

    // Manage roster listing according to loaction
    $(document).on('change','select#manager-roster',function(){
        var roster_residency = $(this).val();
        var roster_user = $('select#roster-user-filter').val();
        
        $('.manager-roster-listing').html('<div class="se-pre-con"></div>');
        $.ajax({
                type: 'POST',
                url: ajaxurl,
                data: {
                        action:'roster_list_by_residency',
                        roster_residency : roster_residency,
                        roster_user : roster_user,
                },
                success:function(response){
                        //console.log(response);
                        if (response) {
                            $('.manager-roster-listing').html(response);                            
                        }
                }
        });
    });

    // Manage roster listing according to user
    $(document).on('change','select#roster-user-filter',function(){
        var roster_user = $(this).val();
        var roster_residency = $('select#manager-roster').val();
        
        $('.manager-roster-listing').html('<div class="se-pre-con"></div>');
        $.ajax({
                type: 'POST',
                url: ajaxurl,
                data: {
                        action:'roster_user_list_by_residency',
                        roster_user : roster_user,
                        roster_residency : roster_residency,
                },
                success:function(response){
                        //console.log(response);
                        if (response) {
                            $('.manager-roster-listing').html(response);                            
                        }
                }
        });
    });


function myFunction() {
    jQuery(function() {
        jQuery('.equalheight').matchHeight({
            byRow: true,
        });
    });
}

/* =================================================================================================
                                    Filter Employee module in Manager
==================================================================================================== */

    // Manage roster listing according to loaction
    $(document).on('change','select#manager-employee-filter',function(){
        var employee_residency = $(this).val();
        var employee_user = $('select#employee-user-filter').val();
        
        $('.employee-listing').html('<div class="se-pre-con"></div>');
        if (employee_residency == 'all' && employee_user == 'all') {
            location.reload();
        } else {
            $.ajax({
                    type: 'POST',
                    url: ajaxurl,
                    data: {
                            action:'employee_list_by_residency',
                            employee_residency : employee_residency,
                            employee_user : employee_user,
                    },
                    success:function(response){
                            //console.log(response);
                            if (response) {
                                $('.employee-listing').html(response); 
                                myFunction();                          
                            }
                    }
            });
        }

    });

    // Manage roster listing according to user
    $(document).on('change','select#employee-user-filter',function(){
        var employee_user = $(this).val();
        var employee_residency = $('select#manager-employee-filter').val();
        
        $('.employee-listing').html('<div class="se-pre-con"></div>');

        if (employee_residency == 'all' && employee_user == 'all') {
            location.reload();
        } else {
            $.ajax({
                    type: 'POST',
                    url: ajaxurl,
                    data: {
                            action:'employee_list_by_user',
                            employee_user : employee_user,
                            employee_residency : employee_residency,
                    },
                    success:function(response){
                            //console.log(response);
                            if (response) {
                                $('.employee-listing').html(response); 
                                myFunction();                           
                            }
                    }
            });
        }
    });

/* =================================================================================================
                                    Filter Timesheet module in Manager
==================================================================================================== */

    // Manage roster listing according to loaction
    $(document).on('change','select#manager-timesheet-filter',function(){
        var timesheet_residency = $(this).val();
        var timesheet_user = $('select#timesheet-user-filter').val();
        
        $('.timesheet-listing').html('<div class="se-pre-con"></div>');
        if (timesheet_residency != 'all') {
            $.ajax({
                    type: 'POST',
                    url: ajaxurl,
                    data: {
                            action:'timesheet_list_by_residency',
                            timesheet_residency : timesheet_residency,
                            timesheet_user : timesheet_user,
                    },
                    success:function(response){
                            //console.log(response);
                            if (response) {
                                $('.timesheet-listing').html(response);                            
                            }
                    }
            });            
        } else {
            location.reload();
        }
    });

    // Manage roster listing according to user
    $(document).on('change','select#timesheet-user-filter',function(){
        var timesheet_user = $(this).val();
        var timesheet_residency = $('select#manager-timesheet-filter').val();
        
        $('.timesheet-listing').html('<div class="se-pre-con"></div>');
        $.ajax({
                type: 'POST',
                url: ajaxurl,
                data: {
                        action:'timesheet_list_by_user',
                        timesheet_user : timesheet_user,
                        timesheet_residency : timesheet_residency,
                },
                success:function(response){
                        //console.log(response);
                        if (response) {
                            $('.timesheet-listing').html(response);                            
                        }
                }
        });
    });


});