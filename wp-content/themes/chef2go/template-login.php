<?php
/**
* Template Name: Login
*/

get_header(); ?>

	<div id="primary" class="content-area">
		<div class="other-entry-header text-center">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
							<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
						</div><!-- .entry-header -->
				</div>
			</div>
		</div>
		<div class="breadcrumbs-container">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<?php dimox_breadcrumbs(); ?>
					</div>
				</div>
			</div>			
		</div>
		<main id="main" class="container site-main" role="main">
			<div class="row">
				<div class="hidden-xs col-sm-2 col-md-3"></div>
				<div class="col-xs-12 col-sm-8 col-md-6">
					<?php if ( is_active_sidebar( 'login-page-widget' ) ) : ?>
					        <?php dynamic_sidebar( 'login-page-widget' ); ?>
					<?php endif; ?>	
					<div class="user-activation-btn">
						<a href="<?php echo site_url().'/activation-and-deactivation/'; ?>" class="btn custom-btn">Account Activation and deactivation</a>
					</div>
				</div>
				<div class="hidden-xs col-sm-2 col-md-3"></div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();
