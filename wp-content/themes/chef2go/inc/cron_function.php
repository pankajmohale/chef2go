<?php
/* Setup cron function to send mail to employee on every monday */

/*if ( ! wp_next_scheduled( 'monday_reminder_mail_hook' ) ) {
  wp_schedule_event( 300 * MINUTE_IN_SECONDS, 'hourly', 'monday_reminder_mail_hook' );
}

add_action( 'monday_reminder_mail_hook', 'monday_reminder_mail_function' );

function monday_reminder_mail_function() {
  wp_mail( 'pankaj.mohale@prodigitas.com', 'Automatic email', 'Automatic scheduled email from chefs2go.');
}*/


/*

function monday_reminder_mail_function() {
  wp_mail( 'pankaj.mohale@prodigitas.com', 'Automatic email', 'Automatic scheduled email from chefs2go.');
}
*/


register_activation_hook(__FILE__, 'my_activation');

function my_activation() {
    if (! wp_next_scheduled ( 'my_hourly_event' )) {
	wp_schedule_event(time(), 'hourly', 'my_hourly_event');
    }
}

add_action('my_hourly_event', 'do_this_hourly');

function do_this_hourly() {
	// do something every hour
}


/* =====================================================================================================
						Setup cron function to send mail to employee on every monday					
======================================================================================================== */
if ( ! get_transient( 'every_day_minutes' ) ) {
    set_transient( 'every_day_minutes', true, 1 * DAY_IN_SECONDS );
    // test_function();
}

function monday_reminder_mail_function() {
	if(date('D') === 'Mon'){
		global $redux_demo;
	
		// get the chefs
		$chef_query = new WP_User_Query(
			array(
				'role'	 =>	'chef_employee',
			)
		);
		$chefs = $chef_query->get_results();

		// get the staff
		$staff_employee_query = new WP_User_Query(
			array(
				'role'	 =>	'staff_employee',
			)
		);
		$staffs = $staff_employee_query->get_results();

		// store them all as users
		$users = array_merge( $staffs, $chefs );

		// User Loop
		if ( ! empty( $users ) ) {
			foreach ( $users as $user ) { 
				$user_email_array[] = $user->user_email;
			}
			foreach ($user_email_array as $email_array) {
				$to = $email_array;
				$subject = $redux_demo['weekly-subject-title'];
				$body = $redux_demo['weekly-email-editor'];
				$headers = array('Content-Type: text/html; charset=UTF-8');
			
				wp_mail( $to, $subject, $body, $headers );
			}
		} else {
			echo 'No users found.';
		}
	} 

}

/*if ( ! get_transient( 'every_5_minutes' ) ) {
    set_transient( 'every_5_minutes', true, 1 * MINUTE_IN_SECONDS );
    monday_reminder_mail_function();
}

add_action( 'init', 'monday_reminder_mail_function' );
function monday_reminder_mail_function() {
	global $redux_demo;
	
	// get the chefs
	$chef_query = new WP_User_Query(
		array(
			'role'	 =>	'chef_employee',
		)
	);
	$chefs = $chef_query->get_results();

	// get the staff
	$staff_employee_query = new WP_User_Query(
		array(
			'role'	 =>	'staff_employee',
		)
	);
	$staffs = $staff_employee_query->get_results();

	// store them all as users
	$users = array_merge( $staffs, $chefs );

	// User Loop
	if ( ! empty( $users ) ) {
		foreach ( $users as $user ) { 
			$user_email_array[] = $user->user_email;
		}
		foreach ($user_email_array as $email_array) {
			$to = $email_array;
			$subject = $redux_demo['weekly-subject-title'];
			$body = $redux_demo['weekly-email-editor'];
			$headers = array('Content-Type: text/html; charset=UTF-8');
		
			wp_mail( $to, $subject, $body, $headers );
		}
	} else {
		echo 'No users found.';
	}
}
*/

/* =====================================================================================================
					Setup cron function to send mail to recruiter on expiration of user					
======================================================================================================== */
add_action( 'init', 'monthly_reminder_mail_function' );
function monthly_reminder_mail_function() {
	global $redux_demo;
	// get the Recruiter
	$recruiter_query = new WP_User_Query(
		array(
			'role'	 =>	'recruiter',
		)
	);
	$recruiter = $recruiter_query->get_results();

	// User Loop
	// Loop for get user registered date and add 28 days in that date.
	// for loop for check current date is same as expiredate if yes then it will send mail to
	// that user.
	if ( ! empty( $recruiter ) ) {
		foreach ( $recruiter as $user ) {
			$registered = $user->user_registered;
			$registered_expire_date = date( "d M Y", strtotime( $registered.'+28 day' ) );
			if (date( "d M Y") === $registered_expire_date ) {
				$to = $user->user_email;
				$subject = $redux_demo['monthly-mail-subject-title'];
				$body = $redux_demo['monthly-email-editor'];
				$headers = array('Content-Type: text/html; charset=UTF-8');
			
				wp_mail( $to, $subject, $body, $headers );
			}
		}
	} else {
		echo 'No recruiter found.';
	}
}