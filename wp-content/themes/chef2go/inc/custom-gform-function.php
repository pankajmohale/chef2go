<?php
/* ================================= gravity form function ========================== */
add_action( 'gform_user_registered', 'add_custom_post_meta', 10, 4 );
function add_custom_post_meta( $user_id, $feed, $entry, $user_pass ) {
	
	update_post_meta($entry['post_id'], 'employee_user_id', $user_id);
	update_user_meta( $entry['post_id'], 'recruiter_user_id', $user_id );
}
 
/* ================================= gravity Update Chef user function ========================== */
add_action( 'gform_post_submission_19', 'update_user_post_meta', 10, 2 );
function update_user_post_meta( $entry, $form ) {

	$update_user_val = $entry['created_by'];
	$uname = $entry[55].' '.$entry[56];

	$args = array(
		'post_type' => 'chef',
		'posts_per_page' => -1,
	);
	// the query
	$the_query = new WP_Query( $args ); ?>

	<?php if ( $the_query->have_posts() ) : ?>

		<!-- pagination here -->

		<!-- the loop -->
		<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>


	<?php
		$employee_user_id_value = get_post_meta( get_the_ID(), 'employee_user_id', true );
		if ($update_user_val == $employee_user_id_value) {

			// Update post 37
			$my_post = array(
			   'ID'           => get_the_ID(),
			   'post_title'   => $uname,
			   'post_content' => $entry[30],
			);

			// Update the post into the database
			wp_update_post( $my_post );
			
			update_post_meta(get_the_ID(), 'freelance_hourly_rate', $entry[22]);
			update_post_meta(get_the_ID(), 'mail_id', $entry[34]);
			update_post_meta(get_the_ID(), 'custom_address_full', $entry[32]);
			update_post_meta(get_the_ID(), 'user_phone_number', $entry[33]);
			update_post_meta(get_the_ID(), 'work_in_remote_locations', $entry[28]);
			update_post_meta(get_the_ID(), 'new_resume', $entry[31]);
			update_post_meta(get_the_ID(), 'employee_user_id', $update_user_val);

			// Document Details
			update_post_meta(get_the_ID(), 'freelance_position_details_document', $entry[49]);
			update_post_meta(get_the_ID(), 'freelance_position_engagement_letter', $entry[50]);
			update_post_meta(get_the_ID(), 'freelance_position_flexibility_agreement_12_hour', $entry[51]);
			update_post_meta(get_the_ID(), 'freelance_position_flexibility_agreement_day_worker', $entry[52]);
			update_post_meta(get_the_ID(), 'freelance_position_independent_contractor_agreement', $entry[53]);

			// Citizenship
			$residency = explode(",",$entry[42]);
			foreach ($residency as $key => $reside) {
				$reside_in[] = intval($reside);
			}
			wp_set_object_terms( get_the_ID(), $reside_in, 'location_residency', false );

			// Citizenship
			$citizenship = explode(",",$entry[38]);
			foreach ($citizenship as $key => $citizen) {
				$citi[] = intval($citizen);
			}
			wp_set_object_terms( get_the_ID(), $citi, 'citizenship_visa', false );

			// type_of_recruitment
			$recruitment = explode(",",$entry[37]);
			foreach ($recruitment as $key => $types) {
				$recruit[] = intval($types);
			}
			wp_set_object_terms( get_the_ID(), $recruit, 'type_of_recruitment', false );

			// chef_skill
			$chef_skill = explode(",",$entry[36]);
			foreach ($chef_skill as $key => $skill) {
				$chefskill[] = intval($skill);
			}
			wp_set_object_terms( get_the_ID(), $chefskill, 'chef_skill', false );

			// field_of_interest
			$field_of_interest = explode(",",$entry[41]);
			foreach ($field_of_interest as $key => $interest) {
				$fieldofinterest[] = intval($interest);
			}
			wp_set_object_terms( get_the_ID(), $fieldofinterest, 'field_of_interest', false );

			// specialising_in
			$specialising_in = explode(",",$entry[35]);
			foreach ($specialising_in as $key => $specialisingin) {
				$specialising[] = intval($specialisingin);
			}
			wp_set_object_terms( get_the_ID(), $specialising, 'specialising_in', false );


		}
    ?>

    <?php endwhile; ?>
		<!-- end of the loop -->

		<!-- pagination here -->

		<?php wp_reset_postdata(); ?>

	<?php else : ?>
		<p><?php _e( 'Sorry, No User Updated.' ); ?></p>
	<?php endif; 

}


/* ================================= gravity Update Chef user function ========================== */
add_action( 'gform_post_submission_20', 'update_staff_user_post_meta', 10, 2 );
function update_staff_user_post_meta( $entry, $form ) {

	$update_user_val = $entry['created_by'];
	$uname = $entry[54].' '.$entry[55];

	$args = array(
		'post_type' => 'hospitality_staff',
		'posts_per_page' => -1,
	);
	// the query
	$the_query = new WP_Query( $args ); ?>

	<?php if ( $the_query->have_posts() ) : ?>

		<!-- pagination here -->

		<!-- the loop -->
		<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>


	<?php
		$employee_user_id_value = get_post_meta( get_the_ID(), 'employee_user_id', true );
		if ($update_user_val == $employee_user_id_value) {

			// Update post
			$my_post = array(
			   'ID'           => get_the_ID(),
			   'post_title'   => $uname,
			   'post_content' => $entry[30],
			);

			// Update the post into the database
			wp_update_post( $my_post );
			
			update_post_meta(get_the_ID(), 'staff_hourly_rate', $entry[22]);
			update_post_meta(get_the_ID(), 'staff_mail_id', $entry[34]);
			update_post_meta(get_the_ID(), 'staff_address_full', $entry[32]);
			update_post_meta(get_the_ID(), 'staff_phone_number', $entry[33]);
			update_post_meta(get_the_ID(), 'staff_work_in_remote_locations', $entry[28]);
			update_post_meta(get_the_ID(), 'staff_new_resume', $entry[31]);
			update_post_meta(get_the_ID(), 'staff_are_you_rsa_certified', $entry[53]);
			update_post_meta(get_the_ID(), 'staff_upload_rsa_certificate', $entry[52]);
			update_post_meta(get_the_ID(), 'employee_user_id', $update_user_val);

			// location_residency
			$residency = explode(",",$entry[41]);
			foreach ($residency as $key => $location) {
				$location_residency[] = intval($location);
			}
			wp_set_object_terms( get_the_ID(), $location_residency, 'location_residency', false );
			
			// Citizenship
			$citizenship = explode(",",$entry[38]);
			foreach ($citizenship as $key => $citizen) {
				$citi[] = intval($citizen);
			}
			wp_set_object_terms( get_the_ID(), $citi, 'staff_citizenship', false );

			// type_of_recruitment
			$recruitment = explode(",",$entry[37]);
			foreach ($recruitment as $key => $types) {
				$recruit[] = intval($types);
			}
			wp_set_object_terms( get_the_ID(), $recruit, 'staff_type_of_recruitment', false );
			

			// position_type
			$position_type = explode(",",$entry[36]);
			foreach ($position_type as $key => $positiontype) {
				$posit[] = intval($positiontype);
			}
			wp_set_object_terms( get_the_ID(), $posit, 'position_type', false );

		}
    ?>

    <?php endwhile; ?>
		<!-- end of the loop -->

		<!-- pagination here -->

		<?php wp_reset_postdata(); ?>

	<?php else : ?>
		<p><?php _e( 'Sorry, No User Updated.' ); ?></p>
	<?php endif; 

}


/* ================================= gravity Update Chef user function ========================== */
add_action( 'gform_post_submission_21', 'update_recruiter_user_post_meta', 10, 2 );
function update_recruiter_user_post_meta( $entry, $form ) {

	$update_user_val = $entry['created_by'];
	$uname = $entry[18].' '.$entry[19];

	$args = array(
		'post_type' => 'recruiter',
		'posts_per_page' => -1,
	);
	// the query
	$the_query = new WP_Query( $args ); ?>

	<?php if ( $the_query->have_posts() ) : ?>

		<!-- pagination here -->

		<!-- the loop -->
		<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>


	<?php
		$recruiter_user_id_value = get_post_meta( get_the_ID(), 'recruiter_user_id', true );
		if ($update_user_val == $recruiter_user_id_value) {
			// Update post
			$my_post = array(
			   'ID'           => get_the_ID(),
			   'post_title'   => $uname,
			);

			// Update the post into the database
			wp_update_post( $my_post );
			
			update_post_meta(get_the_ID(), 'recruiter_phone_number', $entry[10]);
			update_post_meta(get_the_ID(), 'recruiter_venue_name', $entry[11]);
			update_post_meta(get_the_ID(), 'recruiter_email_address', $entry[12]);

			// location_residency
			$residency = explode(",",$entry[6]);
			foreach ($residency as $key => $location) {
				$location_residency[] = intval($location);
			}
			wp_set_object_terms( get_the_ID(), $location_residency, 'location_residency', false );

		}
    ?>

    <?php endwhile; ?>
		<!-- end of the loop -->

		<!-- pagination here -->

		<?php wp_reset_postdata(); ?>

	<?php else : ?>
		<p><?php _e( 'Sorry, No User Updated.' ); ?></p>
	<?php endif; 

}

/* ==========================================================================================
	* Employee Timesheet section *
	This function is use for employee timesheet area in that they have call ajax after change location field and this function is call exact below function for add clients as per location.				
============================================================================================= */
add_filter("gform_pre_render_24", "change_location_dropdown");
add_filter( 'gform_pre_validation_24', 'change_location_dropdown' );
add_filter( 'gform_pre_submission_filter_24', 'change_location_dropdown' );
add_filter( 'gform_admin_pre_render_24', 'change_location_dropdown' );
add_filter("gform_pre_render_4", "change_location_dropdown");
add_filter( 'gform_pre_validation_4', 'change_location_dropdown' );
add_filter( 'gform_pre_submission_filter_4', 'change_location_dropdown' );
add_filter( 'gform_admin_pre_render_4', 'change_location_dropdown' );
function change_location_dropdown($form){
?>
    <script type="text/javascript">
    jQuery(document).ready(function($){

        jQuery('#input_4_8, #input_24_8').bind('change', function() {
            //get selected value from drop down;
            var selectedValue = jQuery("#input_4_8, #input_24_8").val();

			$.ajax({
			    type: 'POST',
			    url: ajaxurl,
			    data: {
		            action:'fatch_client_by_location_in_employee',
		            selectedValue : selectedValue,
			    },
			    success:function(response){
				    // console.log(responsearray);
			    	if (response != null ) {
				        var responsearray = jQuery.parseJSON(response);
				        $('#input_4_2 option:gt(0)').remove();
				        $('#input_4_17 option:gt(0)').remove();
				        $('#input_4_24 option:gt(0)').remove();
				        $('#input_4_31 option:gt(0)').remove();
				        $('#input_4_37 option:gt(0)').remove();
				        $('#input_4_45 option:gt(0)').remove();
				        $('#input_4_49 option:gt(0)').remove();
				        $('#input_4_55 option:gt(0)').remove();
				        $('#input_4_63 option:gt(0)').remove();
				        $('#input_4_67 option:gt(0)').remove();
				        // Update Post
				        $('#input_24_2 option:gt(0)').remove();
				        $('#input_24_17 option:gt(0)').remove();
				        $('#input_24_24 option:gt(0)').remove();
				        $('#input_24_31 option:gt(0)').remove();
				        $('#input_24_37 option:gt(0)').remove();
				        $('#input_24_45 option:gt(0)').remove();
				        $('#input_24_49 option:gt(0)').remove();
				        $('#input_24_55 option:gt(0)').remove();
				        $('#input_24_63 option:gt(0)').remove();
				        $('#input_24_67 option:gt(0)').remove();
				        for (var i = responsearray.length - 1; i >= 0; i--) {
				        	$('#input_4_2').append('<option value=' + responsearray[i].value + '>' + responsearray[i].text + '</option>');
				        	$('#input_4_17').append('<option value=' + responsearray[i].value + '>' + responsearray[i].text + '</option>');
				        	$('#input_4_24').append('<option value=' + responsearray[i].value + '>' + responsearray[i].text + '</option>');
				        	$('#input_4_31').append('<option value=' + responsearray[i].value + '>' + responsearray[i].text + '</option>');
				        	$('#input_4_37').append('<option value=' + responsearray[i].value + '>' + responsearray[i].text + '</option>');
				        	$('#input_4_45').append('<option value=' + responsearray[i].value + '>' + responsearray[i].text + '</option>');
				        	$('#input_4_49').append('<option value=' + responsearray[i].value + '>' + responsearray[i].text + '</option>');
				        	$('#input_4_55').append('<option value=' + responsearray[i].value + '>' + responsearray[i].text + '</option>');
				        	$('#input_4_63').append('<option value=' + responsearray[i].value + '>' + responsearray[i].text + '</option>');
				        	$('#input_4_67').append('<option value=' + responsearray[i].value + '>' + responsearray[i].text + '</option>');
				        	// Update Post
				        	$('#input_24_2').append('<option value=' + responsearray[i].value + '>' + responsearray[i].text + '</option>');
				        	$('#input_24_17').append('<option value=' + responsearray[i].value + '>' + responsearray[i].text + '</option>');
				        	$('#input_24_24').append('<option value=' + responsearray[i].value + '>' + responsearray[i].text + '</option>');
				        	$('#input_24_31').append('<option value=' + responsearray[i].value + '>' + responsearray[i].text + '</option>');
				        	$('#input_24_37').append('<option value=' + responsearray[i].value + '>' + responsearray[i].text + '</option>');
				        	$('#input_24_45').append('<option value=' + responsearray[i].value + '>' + responsearray[i].text + '</option>');
				        	$('#input_24_49').append('<option value=' + responsearray[i].value + '>' + responsearray[i].text + '</option>');
				        	$('#input_24_55').append('<option value=' + responsearray[i].value + '>' + responsearray[i].text + '</option>');
				        	$('#input_24_63').append('<option value=' + responsearray[i].value + '>' + responsearray[i].text + '</option>');
				        	$('#input_24_67').append('<option value=' + responsearray[i].value + '>' + responsearray[i].text + '</option>');
				        }			    		
			    	}
			    }
			});
        });
    });
    </script>
<?php

return $form;
}

/* ==================================================================================================
							 fatch client for employee section  
 This call function is use for above function in that I have change client list as per location
===================================================================================================== */
add_action( 'wp_ajax_nopriv_fatch_client_by_location_in_employee', 'fatch_client_by_location_in_employee' );
add_action( 'wp_ajax_fatch_client_by_location_in_employee', 'fatch_client_by_location_in_employee' );
function fatch_client_by_location_in_employee(){
		$location_id = $_POST['selectedValue'];
	?>
	<?php 
		$clients_location = array(
			'post_type' => 'locations',
			'posts_per_page' => -1,
			'tax_query' => array(
				array(
					'taxonomy' => 'location_residency',
					'field'    => 'term_id',
					'terms'    => $location_id,
				),
			),
		); 
		// the query
		$clients_location_query = new WP_Query( $clients_location ); 
		
	        // get the chefs
			// User Loop ?>
	        <?php if ( $clients_location_query->have_posts() ) : ?>

				<?php while ( $clients_location_query->have_posts() ) : $clients_location_query->the_post(); ?>
					<?php 
						$slug = basename(get_permalink());
						$client_location = get_terms( 'client_location', array(
						    'orderby'    => 'count',
						    'hide_empty' => 0
						) );
						foreach ($client_location as $key => $value) {
							if ($slug == $value->slug) {
								$choices[] = array( 'text' => $value->name, 'value' => $value->term_id );
							}
						}

						// print_r($client_location);

						// $choices[] = array( 'text' => get_the_title(), 'value' => get_the_ID() );
					 ?>
				<?php endwhile; ?>

				<?php wp_reset_postdata(); ?>

			<?php else : ?>
				
			<?php endif; 
		echo json_encode($choices);
		die();
}

/* ===================================================================================================== 
							gravity form function for update post status of chefs
======================================================================================================== */
add_action( 'gform_user_registered', 'change_post_status', 10, 4 );
function change_post_status( $user_id, $feed, $entry, $user_pass ) {

	$activateargs = array(
		'post_type' => 'chef',
		'post_status' => array( 'pending', 'draft' ),
		'posts_per_page' => -1,
		'meta_query' => array(
			array(
				'key'     => 'mail_id',
				'value'   => $entry[34],
				'compare' => 'LIKE',
			),
		),
	);
	// the query
	$activatethe_query = new WP_Query( $activateargs ); ?>

<?php if ( $activatethe_query->have_posts() ) : ?>

	<!-- pagination here -->

	<!-- the loop -->
	<?php while ( $activatethe_query->have_posts() ) : $activatethe_query->the_post(); ?>
		
		<?php 
			$chef_id = get_the_ID();
			wp_update_post(
				array(
					'ID'=> get_the_ID(), 
					'post_status'=>'publish',
				)
			);
			update_post_meta($chef_id, 'custom_address_full', $entry[32]);
			update_post_meta($chef_id, 'mail_id', $entry[34]);
			update_post_meta($chef_id, 'user_phone_number', $entry[33]);
			update_post_meta($chef_id, 'freelance_hourly_rate', $entry[22]);
			update_post_meta($chef_id, 'work_in_remote_locations', $entry[28]);
			update_post_meta($chef_id, 'new_resume', $entry[31]);
			update_post_meta($chef_id, 'employee_user_id', $user_id);
		?>


	<?php endwhile; ?>
	<!-- end of the loop -->

	<!-- pagination here -->

	<?php wp_reset_postdata(); ?>

<?php else : ?>
	<p><?php //_e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif;

}

/* ===================================================================================================== 
							gravity form function for update post status of staff
======================================================================================================== */
add_action( 'gform_user_registered', 'change_hospitality_staff_post_status', 10, 4 );
function change_hospitality_staff_post_status( $user_id, $feed, $entry, $user_pass ) {
	
	$activateargs = array(
		'post_type' => 'hospitality_staff',
		'post_status' => array( 'pending', 'draft' ),
		'posts_per_page' => -1,
		'meta_query' => array(
			array(
				'key'     => 'staff_mail_id',
				'value'   => $entry[34],
				'compare' => 'LIKE',
			),
		),
	);
	// the query
	$activatethe_query = new WP_Query( $activateargs ); ?>

<?php if ( $activatethe_query->have_posts() ) : ?>

	<!-- pagination here -->

	<!-- the loop -->
	<?php while ( $activatethe_query->have_posts() ) : $activatethe_query->the_post(); ?>
		
		<?php 
		$staff_id = get_the_ID();
		wp_update_post(
			array(
				'ID'=> get_the_ID(), 
				'post_status'=>'publish',
			)
		); 
		update_post_meta($staff_id, 'staff_address_full', $entry[32]);
		update_post_meta($staff_id, 'staff_mail_id', $entry[34]);
		update_post_meta($staff_id, 'staff_phone_number', $entry[33]);
		update_post_meta($staff_id, 'staff_hourly_rate', $entry[22]);
		update_post_meta($staff_id, 'staff_work_in_remote_locations', $entry[28]);
		update_post_meta($staff_id, 'staff_new_resume', $entry[31]);
		update_post_meta($staff_id, 'staff_are_you_rsa_certified', $entry[53]);
		update_post_meta($staff_id, 'staff_upload_rsa_certificate', $entry[52]);
		update_post_meta($staff_id, 'employee_user_id', $user_id);
		?>


	<?php endwhile; ?>
	<!-- end of the loop -->

	<!-- pagination here -->

	<?php wp_reset_postdata(); ?>

<?php else : ?>
	<p><?php //_e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif;

}

/* ===================================================================================================== 
							gravity form function for update post status of Recruiter
======================================================================================================== */
add_action( 'gform_user_registered', 'change_recruiter_post_status', 10, 4 );
function change_recruiter_post_status( $user_id, $feed, $entry, $user_pass ) {

	$activateargs = array(
		'post_type' => 'recruiter',
		'posts_per_page' => -1,
		'post_status' => array( 'pending', 'draft' ),
		'meta_query' => array(
			array(
				'key'     => 'recruiter_email_address',
				'value'   => $entry[12],
				'compare' => 'LIKE',
			),
		),
	);
	// the query
	$activatethe_query = new WP_Query( $activateargs ); ?>

<?php if ( $activatethe_query->have_posts() ) : ?>

	<!-- pagination here -->

	<!-- the loop -->
	<?php while ( $activatethe_query->have_posts() ) : $activatethe_query->the_post(); ?>
		
		<?php 
		$recruiter_id =  get_the_ID();
		wp_update_post(
			array(
				'ID'=> get_the_ID(), 
				'post_status'=>'publish',
			)
		);

		update_post_meta($recruiter_id, 'recruiter_venue_name', $entry[11]);
		update_post_meta($recruiter_id, 'recruiter_email_address', $entry[12]);
		update_post_meta($recruiter_id, 'recruiter_phone_number', $entry[10]);
		update_post_meta($recruiter_id, 'recruiter_user_id', $user_id);
		 ?>


	<?php endwhile; ?>
	<!-- end of the loop -->

	<!-- pagination here -->

	<?php wp_reset_postdata(); ?>

<?php else : ?>
	<p><?php //_e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif;

}


/* ================================= gravity Update Chef user function ========================== */
add_action( 'gform_post_submission_16', 'add_term_in_employee_timesheet', 10, 2 );
function add_term_in_employee_timesheet( $entry, $form ) {
	$post = get_post( $entry['post_id'] );
	wp_insert_term(
	    $post->post_title,   // the term 
	    'client_location', // the taxonomy
	    array(
	        'slug' => $post->post_name,
	    )
	);

}


add_filter( 'gform_pre_render_24', 'populate_posts' );
add_filter( 'gform_pre_validation_24', 'populate_posts' );
add_filter( 'gform_pre_submission_filter_24', 'populate_posts' );
add_filter( 'gform_admin_pre_render_24', 'populate_posts' );
function populate_posts( $form ) {

    foreach ( $form['fields'] as &$field ) {

        if ( $field->type != 'select' || strpos( $field->cssClass, 'create-timesheet-client' ) === false ) {
            continue;
        }

        // you can add additional parameters here to alter the posts that are retrieved
        // more info: [http://codex.wordpress.org/Template_Tags/get_posts](http://codex.wordpress.org/Template_Tags/get_posts)
        $terms = get_terms( 'client_location', array(
		 	'orderby'    => 'count',
		 	'hide_empty' => 0,
		 ) );
        
        // isSelected
        $get_post_id = $_GET['timesheet_id'];


        $choices = array();

    	if ($field['id'] == 2) {
    		$datevalue = (int)get_post_meta( $get_post_id, 'clients_1', true );
    		foreach ( $terms as $term ) {
	    		if ($datevalue === $term->term_id) {
	        		$choices[] = array( 'text' => $term->name, 'value' => $term->term_id, 'isSelected' => true );
	    		} else {
		    	    $choices[] = array( 'text' => $term->name, 'value' => $term->term_id );
		    	}		    
		    }
    	} elseif ($field['id'] == 17) {
    		$datevalue = (int)get_post_meta( $get_post_id, 'clients_2', true );
    		foreach ( $terms as $term ) {
	    		if ($datevalue === $term->term_id) {
	        		$choices[] = array( 'text' => $term->name, 'value' => $term->term_id, 'isSelected' => true );
	    		} else {
		    	    $choices[] = array( 'text' => $term->name, 'value' => $term->term_id );
		    	}		    
		    }
    	} elseif ($field['id'] == 24) {
    		$datevalue = (int)get_post_meta( $get_post_id, 'clients_3', true );
    		foreach ( $terms as $term ) {
	    		if ($datevalue === $term->term_id) {
	        		$choices[] = array( 'text' => $term->name, 'value' => $term->term_id, 'isSelected' => true );
	    		} else {
		    	    $choices[] = array( 'text' => $term->name, 'value' => $term->term_id );
		    	}		    
		    }
    	} elseif ($field['id'] == 31) {
    		$datevalue = (int)get_post_meta( $get_post_id, 'clients_4', true );
    		foreach ( $terms as $term ) {
	    		if ($datevalue === $term->term_id) {
	        		$choices[] = array( 'text' => $term->name, 'value' => $term->term_id, 'isSelected' => true );
	    		} else {
		    	    $choices[] = array( 'text' => $term->name, 'value' => $term->term_id );
		    	}		    
		    }
    	} elseif ($field['id'] == 37) {
    		$datevalue = (int)get_post_meta( $get_post_id, 'clients_5', true );
    		foreach ( $terms as $term ) {
	    		if ($datevalue === $term->term_id) {
	        		$choices[] = array( 'text' => $term->name, 'value' => $term->term_id, 'isSelected' => true );
	    		} else {
		    	    $choices[] = array( 'text' => $term->name, 'value' => $term->term_id );
		    	}		    
		    }
    	} elseif ($field['id'] == 45) {
    		$datevalue = (int)get_post_meta( $get_post_id, 'clients_6', true );
    		foreach ( $terms as $term ) {
	    		if ($datevalue === $term->term_id) {
	        		$choices[] = array( 'text' => $term->name, 'value' => $term->term_id, 'isSelected' => true );
	    		} else {
		    	    $choices[] = array( 'text' => $term->name, 'value' => $term->term_id );
		    	}		    
		    }
    	} elseif ($field['id'] == 49) {
    		$datevalue = (int)get_post_meta( $get_post_id, 'clients_7', true );
    		foreach ( $terms as $term ) {
	    		if ($datevalue === $term->term_id) {
	        		$choices[] = array( 'text' => $term->name, 'value' => $term->term_id, 'isSelected' => true );
	    		} else {
		    	    $choices[] = array( 'text' => $term->name, 'value' => $term->term_id );
		    	}		    
		    }
    	} elseif ($field['id'] == 55) {
    		$datevalue = (int)get_post_meta( $get_post_id, 'clients_8', true );
    		foreach ( $terms as $term ) {
	    		if ($datevalue === $term->term_id) {
	        		$choices[] = array( 'text' => $term->name, 'value' => $term->term_id, 'isSelected' => true );
	    		} else {
		    	    $choices[] = array( 'text' => $term->name, 'value' => $term->term_id );
		    	}		    
		    }
    	} elseif ($field['id'] == 63) {
    		$datevalue = (int)get_post_meta( $get_post_id, 'clients_9', true );
    		foreach ( $terms as $term ) {
	    		if ($datevalue === $term->term_id) {
	        		$choices[] = array( 'text' => $term->name, 'value' => $term->term_id, 'isSelected' => true );
	    		} else {
		    	    $choices[] = array( 'text' => $term->name, 'value' => $term->term_id );
		    	}		    
		    }
    	} elseif ($field['id'] == 67) {
    		$datevalue = (int)get_post_meta( $get_post_id, 'clients_10', true );
    		foreach ( $terms as $term ) {
	    		if ($datevalue === $term->term_id) {
	        		$choices[] = array( 'text' => $term->name, 'value' => $term->term_id, 'isSelected' => true );
	    		} else {
		    	    $choices[] = array( 'text' => $term->name, 'value' => $term->term_id );
		    	}		    
		    }
    	}

        // update 'Select a Post' to whatever you'd like the instructive option to be
        $field->placeholder = 'Select a Post';
        $field->choices = $choices;

    }

    return $form;
}

add_filter( 'gform_pre_render_24', 'populate_posts_taxonomy' );
add_filter( 'gform_pre_validation_24', 'populate_posts_taxonomy' );
add_filter( 'gform_pre_submission_filter_24', 'populate_posts_taxonomy' );
add_filter( 'gform_admin_pre_render_24', 'populate_posts_taxonomy' );
function populate_posts_taxonomy( $form ) {

    foreach ( $form['fields'] as &$field ) {

        if ( $field->type != 'select' || strpos( $field->cssClass, 'select-location' ) === false ) {
            continue;
        }

        // you can add additional parameters here to alter the posts that are retrieved
        // more info: [http://codex.wordpress.org/Template_Tags/get_posts](http://codex.wordpress.org/Template_Tags/get_posts)
        $terms = get_terms( 'location_residency', array(
		 	'orderby'    => 'count',
		 	'hide_empty' => 0,
		 ) );
        
        // isSelected
        $get_post_id = $_GET['timesheet_id'];
        $term_list = wp_get_post_terms($get_post_id, 'location_residency', array("fields" => "ids"));

        $choices = array();

    	if ($field['id'] == 8) {
    		foreach ( $terms as $term ) {
	    		if ($term_list[0] === $term->term_id) {
	        		$choices[] = array( 'text' => $term->name, 'value' => $term->term_id, 'isSelected' => true );
	    		} else {
		    	    $choices[] = array( 'text' => $term->name, 'value' => $term->term_id );
		    	}    
		    }
    	}

        // update 'Select a Post' to whatever you'd like the instructive option to be
        $field->placeholder = 'Select a Post';
        $field->choices = $choices;

    }

    return $form;
}