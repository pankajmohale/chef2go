<?php
function all_chef_listing( ) {
	ob_start();
	// the query
	$cheflisting_args = array(
		'post_type' => 'chef',
		/*'tax_query' => array(
			array(
				'taxonomy' => 'people',
				'field'    => 'slug',
				'terms'    => 'bob',
			),
		),*/
	);
	$cheflisting = new WP_Query( $cheflisting_args ); ?>

	<?php if ( $cheflisting->have_posts() ) : ?>

		<!-- pagination here -->
		<div class="custom-filter-option">
			<div class="custom-filter-option-inner">
				<div class="form-group">
			      <label for="sel1">Filter by Location:</label>
					<?php 
						$terms = get_terms( 'residency', array(
						    'orderby'    => 'count',
						    'hide_empty' => 0,
						) );
						if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
						    echo '<select class="form-control residency_select" id="sel1">';
						        echo '<option value="all">All</option>';
						    foreach ( $terms as $term ) {
						        echo '<option value='.$term->slug.'>' . $term->name . '</option>';
						    }
						    echo '</select>';
						}
					?>
				</div>				
			</div>
			<div class="custom-filter-option-inner">
				<div class="form-group">
			      <label for="sel2">Filter by Level:</label>
					<?php 
						$terms = get_terms( 'field_of_interest', array(
						    'orderby'    => 'count',
						    'hide_empty' => 0,
						) );
						if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
						    echo '<select class="form-control" id="sel2">';
						        echo '<option value="all">All</option>';
						    foreach ( $terms as $term ) {
						        echo '<option value='.$term->slug.'>' . $term->name . '</option>';
						    }
						    echo '</select>';
						}
					?>
				</div>				
			</div>
		</div>

		<!-- the loop -->
		<?php while ( $cheflisting->have_posts() ) : $cheflisting->the_post(); ?>
			
			<?php 
				get_template_part( 'template-parts/chef', 'listing' );
			?>


		<?php endwhile; ?>
		<!-- end of the loop -->

		<!-- pagination here -->

		<?php wp_reset_postdata(); ?>

	<?php else : ?>
		<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
	<?php endif;
	return ob_get_clean();
}
add_shortcode( 'cheflisting', 'all_chef_listing' );