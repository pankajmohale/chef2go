<?php 
// This file is use for create custom function ajax for manager role

/* ============================= Delete Roster from Recruiter Module =============================== */
add_action( 'wp_ajax_nopriv_update_client_by_manager', 'update_client_by_manager' );
add_action( 'wp_ajax_update_client_by_manager', 'update_client_by_manager' );
function update_client_by_manager(){
		$client_post_id = $_POST['postid'];
		$client_post_slug = $_POST['postslug'];
		$client_name = $_POST['client_name'];
		$client_location = $_POST['client_location'];
	?>
	<?php 
		$clients_location = array(
			'post_type' => 'locations',
		); 
		// the query
		$the_query = new WP_Query( $clients_location ); 
		?>

		<?php if ( $the_query->have_posts() ) : ?>
			<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
				<?php

					if ($client_post_id == get_the_ID()) {

							wp_set_post_terms( get_the_ID(), $client_location, 'location_residency' );

							$update_client = array(
								'ID'           => get_the_ID(),
								'post_title'   => $client_name,
							);

							// Update the post into the database
							wp_update_post( $update_client );

							// update_post_meta(get_the_ID(), 'recruiter_selected_staff', $userinfo);
							echo "Client is Updated";
					}
				?>							
			<?php endwhile; ?>
			<?php wp_reset_postdata(); ?>

		<?php else : ?>
			<p><?php _e( 'Sorry, no client matched your criteria.' ); ?></p>
		<?php endif; ?>

		<?php 
			// update client location taxonomy
			$term = get_term_by('slug', $client_post_slug, 'client_location');
			$return_val = wp_update_term($term->term_id, 'client_location', array(
			  'name' => $client_name,
			));		
		?>

		<?php
		echo 'updated';
		die();
}

/* ============================= Delete Roster from Recruiter Module =============================== */
add_action( 'wp_ajax_nopriv_delete_client_by_manager', 'delete_client_by_manager' );
add_action( 'wp_ajax_delete_client_by_manager', 'delete_client_by_manager' );
function delete_client_by_manager(){
		$client_post_id = $_POST['postid'];
		$client_postslug = $_POST['postslug'];
	?>

	<?php 
		$clients_location = array(
			'post_type' => 'locations',
		); 
		// the query
		$the_query = new WP_Query( $clients_location ); 
		?>

		<?php if ( $the_query->have_posts() ) : ?>
			<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
				<?php

					if ($client_post_id == get_the_ID()) {

						wp_delete_post( get_the_ID(), true );

					}
				?>							
			<?php endwhile; ?>
			<?php wp_reset_postdata(); ?>

		<?php else : ?>
			<p><?php _e( 'Sorry, no client matched your criteria.' ); ?></p>
		<?php endif; ?>

		<?php 
			// Delete term from client_location taxonomy
			$term = get_term_by('slug', $client_postslug, 'client_location');
			$return_val = wp_delete_term( $term->term_id, 'client_location' ); 
		?>

		<?php
		echo 'updated';
		die();
}


/* ============================= fatch client for add roster  =============================== */
add_action( 'wp_ajax_nopriv_fatch_client_by_location', 'fatch_client_by_location' );
add_action( 'wp_ajax_fatch_client_by_location', 'fatch_client_by_location' );
function fatch_client_by_location(){
		$location_id = $_POST['selectedValue'];
	?>
	<?php 
		$clients_location = array(
			'post_type' => 'locations',
			'posts_per_page' => -1,
			'tax_query' => array(
				array(
					'taxonomy' => 'location_residency',
					'field'    => 'term_id',
					'terms'    => $location_id,
				),
			),
		); 
		// the query
		$clients_location_query = new WP_Query( $clients_location ); 
		
	        // get the chefs
			// User Loop ?>
	        <?php if ( $clients_location_query->have_posts() ) : ?>

				<?php while ( $clients_location_query->have_posts() ) : $clients_location_query->the_post(); ?>
					<?php 
						$choices[] = array( 'text' => get_the_title(), 'value' => get_the_ID() );
					 ?>
				<?php endwhile; ?>

				<?php wp_reset_postdata(); ?>

			<?php else : ?>
				
			<?php endif; 

		echo json_encode($choices);
		die();
}

/* ============================= Update Roster from Recruiter Module =============================== */
add_action( 'wp_ajax_nopriv_editroster_data', 'edit_roster_data' );
add_action( 'wp_ajax_editroster_data', 'edit_roster_data' );
function edit_roster_data(){
		$current_user = wp_get_current_user();
		$post_id_value = $_POST['post_id_value'];

		$roster_client = $_POST['roster_client'];
		$roster_member_list = $_POST['roster_member_list'];

		// Monday
		$monday_week_day = $_POST['monday_week_day'];
		$monday_staff_member = $_POST['monday_staff_member'];
		$monday_user_info = get_userdata($monday_staff_member);
		$monday_time_start = $_POST['monday_time_start'];
		$monday_time_finish = $_POST['monday_time_finish'];
		$roster_clients_1 = $_POST['roster_clients_1'];

		// Tuesday
		$tuesday_week_day = $_POST['tuesday_week_day'];
		$tuesday_staff_member = $_POST['tuesday_staff_member'];
		$tuesday_user_info = get_userdata($tuesday_staff_member);
		$tuesday_time_start = $_POST['tuesday_time_start'];
		$tuesday_time_finish = $_POST['tuesday_time_finish'];
		$roster_clients_2 = $_POST['roster_clients_2'];
		
		// Wednesday
		$wednesday_week_day = $_POST['wednesday_week_day'];
		$wednesday_staff_member = $_POST['wednesday_staff_member'];
		$wednesday_user_info = get_userdata($wednesday_staff_member);
		$wednesday_time_start = $_POST['wednesday_time_start'];
		$wednesday_time_finish = $_POST['wednesday_time_finish'];
		$roster_clients_3 = $_POST['roster_clients_3'];
		
		// Thursday
		$thursday_week_day = $_POST['thursday_week_day'];
		$thursday_staff_member = $_POST['thursday_staff_member'];
		$thursday_user_info = get_userdata($thursday_staff_member);
		$thursday_time_start = $_POST['thursday_time_start'];
		$thursday_time_finish = $_POST['thursday_time_finish'];
		$roster_clients_4 = $_POST['roster_clients_4'];
		
		// Friday
		$friday_week_day = $_POST['friday_week_day'];
		$friday_staff_member = $_POST['friday_staff_member'];
		$friday_user_info = get_userdata($friday_staff_member);
		$friday_time_start = $_POST['friday_time_start'];
		$friday_time_finish = $_POST['friday_time_finish'];
		$roster_clients_5 = $_POST['roster_clients_5'];
		
		// Saturday
		$saturday_week_day = $_POST['saturday_week_day'];
		$saturday_staff_member = $_POST['saturday_staff_member'];
		$saturday_user_info = get_userdata($saturday_staff_member);
		$saturday_time_start = $_POST['saturday_time_start'];
		$saturday_time_finish = $_POST['saturday_time_finish'];
		$roster_clients_6 = $_POST['roster_clients_6'];
		
		// Sunday
		$sunday_week_day = $_POST['sunday_week_day'];
		$sunday_staff_member = $_POST['sunday_staff_member'];
		$sunday_user_info = get_userdata($sunday_staff_member);
		$sunday_time_start = $_POST['sunday_time_start'];
		$sunday_time_finish = $_POST['sunday_time_finish'];
		$roster_clients_7 = $_POST['roster_clients_7'];

		// field 8
		$field8_week_day = $_POST['field8_week_day'];
		$field8_staff_member = $_POST['field8_staff_member'];
		$field8_user_info = get_userdata($field8_staff_member);
		$field8_time_start = $_POST['field8_time_start'];
		$field8_time_finish = $_POST['field8_time_finish'];
		$roster_clients_8 = $_POST['roster_clients_8'];

		// field 9
		$field9_week_day = $_POST['field9_week_day'];
		$field9_staff_member = $_POST['field9_staff_member'];
		$field9_user_info = get_userdata($field9_staff_member);
		$field9_time_start = $_POST['field9_time_start'];
		$field9_time_finish = $_POST['field9_time_finish'];
		$roster_clients_9 = $_POST['roster_clients_9'];
		
		// field 10
		$field10_week_day = $_POST['field10_week_day'];
		$field10_staff_member = $_POST['field10_staff_member'];
		$field10_user_info = get_userdata($field10_staff_member);
		$field10_time_start = $_POST['field10_time_start'];
		$field10_time_finish = $_POST['field10_time_finish'];
		$roster_clients_10 = $_POST['roster_clients_10'];



	?>
	<?php
	//getting post
    $post = get_post( $post_id_value );
    $post_id = $post->ID;
    $post_title = $post->post_title;

    // update_post_meta($post->ID, 'roster_time_start_2', $tuesday_time_start);
    
	update_post_meta($post_id, 'roster_member_list', $roster_member_list);
	// Monday
	update_post_meta($post_id, 'roster_week_day_1', $monday_week_day);
	update_post_meta($post_id, 'roster_staff_member_1', $monday_user_info->first_name);
	update_post_meta($post_id, 'roster_time_start_1', $monday_time_start);
	update_post_meta($post_id, 'roster_time_finish_1', $monday_time_finish);
	update_post_meta($post_id, 'roster_clients_1', $roster_clients_1);
	// Tuesday
	update_post_meta($post_id, 'roster_week_day_2', $tuesday_week_day);
	update_post_meta($post_id, 'roster_staff_member_2', $tuesday_user_info->first_name);
	update_post_meta($post_id, 'roster_time_start_2', $tuesday_time_start);
	update_post_meta($post_id, 'roster_time_finish_2', $tuesday_time_finish);
	update_post_meta($post_id, 'roster_clients_2', $roster_clients_2);
	// Wednesday
	update_post_meta($post_id, 'roster_week_day_3', $wednesday_week_day);
	update_post_meta($post_id, 'roster_staff_member_3', $wednesday_user_info->first_name);
	update_post_meta($post_id, 'roster_time_start_3', $wednesday_time_start);
	update_post_meta($post_id, 'roster_time_finish_3', $wednesday_time_finish);
	update_post_meta($post_id, 'roster_clients_3', $roster_clients_3);
	// Thursday
	update_post_meta($post_id, 'roster_week_day_4', $thursday_week_day);
	update_post_meta($post_id, 'roster_staff_member_4', $thursday_user_info->first_name);
	update_post_meta($post_id, 'roster_time_start_4', $thursday_time_start);
	update_post_meta($post_id, 'roster_time_finish_4', $thursday_time_finish);
	update_post_meta($post_id, 'roster_clients_4', $roster_clients_4);
	// Friday
	update_post_meta($post_id, 'roster_week_day_5', $friday_week_day);
	update_post_meta($post_id, 'roster_staff_member_5', $friday_user_info->first_name);
	update_post_meta($post_id, 'roster_time_start_5', $friday_time_start);
	update_post_meta($post_id, 'roster_time_finish_5', $friday_time_finish);
	update_post_meta($post_id, 'roster_clients_5', $roster_clients_5);
	// Saturday
	update_post_meta($post_id, 'roster_week_day_6', $saturday_week_day);
	update_post_meta($post_id, 'roster_staff_member_6', $saturday_user_info->first_name);
	update_post_meta($post_id, 'roster_time_start_6', $saturday_time_start);
	update_post_meta($post_id, 'roster_time_finish_6', $saturday_time_finish);
	update_post_meta($post_id, 'roster_clients_6', $roster_clients_6);
	// Sunday
	update_post_meta($post_id, 'roster_week_day_7', $sunday_week_day);
	update_post_meta($post_id, 'roster_staff_member_7', $sunday_user_info->first_name);
	update_post_meta($post_id, 'roster_time_start_7', $sunday_time_start);
	update_post_meta($post_id, 'roster_time_finish_7', $sunday_time_finish);
	update_post_meta($post_id, 'roster_clients_7', $roster_clients_7);
	// Field 8
	update_post_meta($post_id, 'roster_week_day_8', $field8_week_day);
	update_post_meta($post_id, 'roster_staff_member_8', $field8_user_info->first_name);
	update_post_meta($post_id, 'roster_time_start_8', $field8_time_start);
	update_post_meta($post_id, 'roster_time_finish_8', $field8_time_finish);
	update_post_meta($post_id, 'roster_clients_8', $roster_clients_8);
	// Field 9
	update_post_meta($post_id, 'roster_week_day_9', $field9_week_day);
	update_post_meta($post_id, 'roster_staff_member_9', $field9_user_info->first_name);
	update_post_meta($post_id, 'roster_time_start_9', $field9_time_start);
	update_post_meta($post_id, 'roster_time_finish_9', $field9_time_finish);
	update_post_meta($post_id, 'roster_clients_9', $roster_clients_9);
	// Field 10
	update_post_meta($post_id, 'roster_week_day_10', $field10_week_day);
	update_post_meta($post_id, 'roster_staff_member_10', $field10_user_info->first_name);
	update_post_meta($post_id, 'roster_time_start_10', $field10_time_start);
	update_post_meta($post_id, 'roster_time_finish_10', $field10_time_finish);
	update_post_meta($post_id, 'roster_clients_10', $roster_clients_10);
	
    //updating post
    // wp_update_post( $post );

	echo "Post Updated";
	?>

	<?php
	die();
}

/* ============================= Delete Roster from Manager Module =============================== */
add_action( 'wp_ajax_nopriv_deleteroster_data', 'deleteroster_roster_data' );
add_action( 'wp_ajax_deleteroster_data', 'deleteroster_roster_data' );
function deleteroster_roster_data(){
		$current_user = wp_get_current_user();
		$deleted_post_id = $_POST['deleted_post_id'];
	?>
		<?php 
		$recruiterargs = array(
			'post_type' => 'rosters',
		); 
		// the query
		$the_query = new WP_Query( $recruiterargs ); 
		?>

		<?php if ( $the_query->have_posts() ) : ?>

			<!-- pagination here -->

			<!-- the loop -->
			<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
				<?php
					if (get_the_ID() == $deleted_post_id) {
						wp_delete_post( get_the_ID());
						echo "Roster Deleted";
					}	
				?>							
			<?php endwhile; ?>
			<!-- end of the loop -->

			<!-- pagination here -->

			<?php wp_reset_postdata(); ?>

		<?php else : ?>
			<p><?php _e( 'Sorry, no recruiter matched your criteria.' ); ?></p>
		<?php endif; ?>

		<?php

		die();
}

/* ======================== Roster filter for search roster loctionwise  ============================ */
add_action( 'wp_ajax_nopriv_roster_user_list_by_residency', 'roster_list_by_residency' );
add_action( 'wp_ajax_roster_user_list_by_residency', 'roster_list_by_residency' );
add_action( 'wp_ajax_nopriv_roster_list_by_residency', 'roster_list_by_residency' );
add_action( 'wp_ajax_roster_list_by_residency', 'roster_list_by_residency' );
function roster_list_by_residency(){
		$roster_residency = $_POST['roster_residency'];
		$roster_user = $_POST['roster_user'];
	?>
		<?php
		if($roster_residency && $roster_residency !='all' && $roster_user && $roster_user != 'all'){
			$rosterargs = array(
				'post_type' => 'rosters',
				'posts_per_page' => -1,
				'tax_query' => array(
					array(
						'taxonomy' => 'location_residency',
						'field'    => 'slug',
						'terms'    => $roster_residency,
					),
				),
				'meta_query' => array(
					array(
						'key'     => 'roster_member_list',
						'value'   => $roster_user,
						'compare' => 'LIKE',
					),
				),
			);
		} elseif ($roster_residency && $roster_residency !='all' && $roster_user && $roster_user == 'all') {
			$rosterargs = array(
				'post_type' => 'rosters',
				'posts_per_page' => -1,
				'tax_query' => array(
					array(
						'taxonomy' => 'location_residency',
						'field'    => 'slug',
						'terms'    => $roster_residency,
					),
				),
			);
		} elseif ($roster_residency && $roster_residency =='all' && $roster_user && $roster_user != 'all') {
			$rosterargs = array(
				'post_type' => 'rosters',
				'posts_per_page' => -1,
				'meta_query' => array(
					array(
						'key'     => 'roster_member_list',
						'value'   => $roster_user,
						'compare' => 'LIKE',
					),
				),
			);			
		} else {
			$rosterargs = array(
				'post_type' => 'rosters',
				'posts_per_page' => -1,				
			);
		} 
		// the query
		$rosterthe_query = new WP_Query( $rosterargs ); 
		?>

		<?php if ( $rosterthe_query->have_posts() ) : ?>

			<!-- pagination here -->

			<!-- the loop -->
			<?php while ( $rosterthe_query->have_posts() ) : $rosterthe_query->the_post(); ?>
				<div class="col-xs-6">
					<div id="manager-roster-list" class="list-group-item">
						<span class="badge delete-roster-data" post-id="<?php echo get_the_ID(); ?>" title="Delete Roster"><i class="fa fa-trash-o" aria-hidden="true"></i></span>
						<span class="badge edit-roster" title="Edit Roster"><a href="<?php echo site_url().'/edit-roster/?post_id='.get_the_ID(); ?>" data-value="<?php echo get_the_ID(); ?>"><i class="fa fa-pencil" aria-hidden="true"></i></a></span>
						<div class="rosters-list-function">
							<?php
								$get_title = get_the_title(); 
								$date=date_create($get_title);
								echo date_format($date,"D, j F, Y"); 
							?>											
						</div>
						<div class="roster-employee-name roster-meta">
							<span>Name: </span>
							<?php
								$roster_member_list = get_post_meta( get_the_ID(), 'roster_member_list', true );
								$author_name = get_user_by('id', $roster_member_list);
								echo $author_name->first_name .' '. $author_name->last_name;
							?>
						</div>
						<div class="roster_location roster-meta">
							<span>Location: </span>
							<?php 
								$term_list = wp_get_post_terms(get_the_ID(), 'location_residency', array("fields" => "names"));
								foreach ($term_list as $term_listkey => $term_listvalue) {
										echo $term_listvalue;
									}	
							?>														
						</div>
						<div class="roster-show-more">
							<a class="show-less-btn-dyanamic">Show More</a>
						</div>
						<div class="extradata-dyanamic">
							<table>
							<?php 
								for ($metaloop = 1; $metaloop <= 10; $metaloop++) { 
									$rosterweekday = 'roster_week_day_'.$metaloop;
									$rosterweekdayval = get_post_meta( get_the_ID(), $rosterweekday, true );	
									
									$roster_time_start = 'roster_time_start_'.$metaloop;
									$roster_time_startval = get_post_meta( get_the_ID(), $roster_time_start, true );

									$roster_time_finish = 'roster_time_finish_'.$metaloop;
									$roster_time_finishval = get_post_meta( get_the_ID(), $roster_time_finish, true );

									$roster_clients = 'roster_clients_'.$metaloop;
									$roster_clientsval = get_post_meta( get_the_ID(), $roster_clients, true );

								if ($rosterweekdayval && $roster_time_startval && $roster_time_finishval && $roster_clientsval) {
								?>
								<tr class="roster-meta-details">
									<td><?php echo $rosterweekdayval; ?></td>
									<td><?php echo $roster_time_startval; ?></td>
									<td><?php echo $roster_time_finishval; ?></td>
									<td><?php echo get_the_title($roster_clientsval); ?></td>														
								</tr>
							<?php }
							}
							?>
							</table>
						</div>
					</div>											
				</div>							
			<?php endwhile; ?>
			<!-- end of the loop -->

			<!-- pagination here -->

			<?php wp_reset_postdata(); ?>

		<?php else : ?>
			<div class="col-xs-12">
				<div class="list-group">
					<a class="list-group-item" title="No Roster Added">
						<span class="badge"></span>
						<div class="no-rosters-added"><?php _e( 'Sorry, no roster found.' ); ?></div>
					</a>
				</div>				
			</div>
		<?php endif; ?>

		<?php
		echo "";
		die();
}


/* ============================= fatch client for add roster  =============================== */
add_action( 'wp_ajax_nopriv_fatch_client_by_location_in_timesheet', 'fatch_client_in_timesheet' );
add_action( 'wp_ajax_fatch_client_by_location_in_timesheet', 'fatch_client_in_timesheet' );
function fatch_client_in_timesheet(){
		$location_id = $_POST['selectedValue'];
	?>
	<?php 
		$clients_location = array(
			'post_type' => 'locations',
			'posts_per_page' => -1,
			'tax_query' => array(
				array(
					'taxonomy' => 'location_residency',
					'field'    => 'term_id',
					'terms'    => $location_id,
				),
			),
		); 
		// the query
		$clients_location_query = new WP_Query( $clients_location ); 
		
	        // get the chefs
			// User Loop ?>
	        <?php if ( $clients_location_query->have_posts() ) : ?>

				<?php while ( $clients_location_query->have_posts() ) : $clients_location_query->the_post(); ?>
					<?php 
						$choices[] = array( 'text' => get_the_title(), 'value' => get_the_ID() );
					 ?>
				<?php endwhile; ?>

				<?php wp_reset_postdata(); ?>

			<?php else : ?>
				
			<?php endif; 

		echo json_encode($choices);
		die();
}

/* =============== Employee Management filter for search employee loctionwise  ===================== */
add_action( 'wp_ajax_nopriv_employee_list_by_residency', 'employee_management_filter' );
add_action( 'wp_ajax_employee_list_by_residency', 'employee_management_filter' );
add_action( 'wp_ajax_nopriv_employee_list_by_user', 'employee_management_filter' );
add_action( 'wp_ajax_employee_list_by_user', 'employee_management_filter' );
function employee_management_filter(){


		$employee_residency = $_POST['employee_residency'];
		$employee_user = $_POST['employee_user'];
	?>
		<?php
		if($employee_residency && $employee_residency !='all' && $employee_user && $employee_user != 'all'){
			$rosterargs = array(
				'post_type' => array('chef', 'hospitality_staff'),
				'post_status' => 'publish',
				'posts_per_page' => -1,
				'tax_query' => array(
					array(
						'taxonomy' => 'location_residency',
						'field'    => 'slug',
						'terms'    => $employee_residency,
					),
				),
				'meta_query' => array(
					array(
						'key'     => 'employee_user_id',
						'value'   => $employee_user,
						'compare' => 'LIKE',
					),
				),
			);
		} elseif ($employee_residency && $employee_residency !='all' && $employee_user && $employee_user == 'all') {
			$rosterargs = array(
				'post_type' => array('chef', 'hospitality_staff'),
				'post_status' => 'publish',
				'posts_per_page' => -1,
				'tax_query' => array(
					array(
						'taxonomy' => 'location_residency',
						'field'    => 'slug',
						'terms'    => $employee_residency,
					),
				),
			);
		} elseif ($employee_residency && $employee_residency =='all' && $employee_user && $employee_user != 'all') {
			$rosterargs = array(
				'post_type' => array('chef', 'hospitality_staff'),
				'post_status' => 'publish',
				'posts_per_page' => -1,
				'meta_query' => array(
					array(
						'key'     => 'employee_user_id',
						'value'   => $employee_user,
						'compare' => 'LIKE',
					),
				),
			);			
		} else {
			$rosterargs = array(
				'post_type' => array('chef', 'hospitality_staff'),
				'posts_per_page' => -1,				
				'post_status' => 'publish',
			);
		} 
		// the query
		$rosterthe_query = new WP_Query( $rosterargs ); 
		?>

		<?php if ( $rosterthe_query->have_posts() ) : ?>

			<!-- pagination here -->

			<!-- the loop -->
			<?php while ( $rosterthe_query->have_posts() ) : $rosterthe_query->the_post(); ?>
				<div class="manager-user-listing-wrapper">
					<div class="col-xs-12 col-sm-4">
						<div class="user-listing equalheight">
							<div class="user-listing-title">
								<h2><?php the_title(); ?></h2>
							</div>
							<div class="user-listing-description">
								<?php my_excerpt(20); ?>
							</div>
							<div class="user-listing-readmore">
								<a href='<?php the_permalink(); ?>'>Read More</a>
							</div>											
						</div>
					</div>
				</div>							
			<?php endwhile; ?>
			<!-- end of the loop -->

			<!-- pagination here -->

			<?php wp_reset_postdata(); ?>

		<?php else : ?>
			<div class="col-xs-12">
				<div class="list-group">
					<a class="list-group-item" title="No Roster Added">
						<span class="badge"></span>
						<div class="no-rosters-added"><?php _e( 'Sorry, no Employee found.' ); ?></div>
					</a>
				</div>				
			</div>
		<?php endif; ?>

		<?php
		echo "";
		die();
}

/* =============== Employee Management filter for search employee loctionwise  ===================== */
add_action( 'wp_ajax_nopriv_timesheet_list_by_residency', 'timesheet_management_filter' );
add_action( 'wp_ajax_timesheet_list_by_residency', 'timesheet_management_filter' );
add_action( 'wp_ajax_nopriv_timesheet_list_by_user', 'timesheet_management_filter' );
add_action( 'wp_ajax_timesheet_list_by_user', 'timesheet_management_filter' );
function timesheet_management_filter(){
		$timesheet_residency = $_POST['timesheet_residency'];
		$timesheet_user = $_POST['timesheet_user'];
	?>
		<?php
		if ($timesheet_user != 'all'  && $timesheet_residency !='all') {
			$timesheetargs = array(
				'post_type' => 'timesheet',
				'post_status' => 'publish',
				'posts_per_page' => -1,
				'tax_query' => array(
					array(
						'taxonomy' => 'location_residency',
						'field'    => 'slug',
						'terms'    => $timesheet_residency,
					),
				),
				'meta_query' => array(
					array(
						'key'     => 'timesheet_uploaded_userid',
						'value'   => $timesheet_user,
						'compare' => 'LIKE',
					),
				),
			);
		} elseif ($timesheet_user != 'all' && $timesheet_residency =='all') {
			$timesheetargs = array(
				'post_type' => 'timesheet',
				'post_status' => 'publish',
				'posts_per_page' => -1,
				'meta_query' => array(
					array(
						'key'     => 'timesheet_uploaded_userid',
						'value'   => $timesheet_user,
						'compare' => 'LIKE',
					),
				),
			);
		} elseif ($timesheet_user == 'all' && $timesheet_residency !='all') {
			$timesheetargs = array(
				'post_type' => 'timesheet',
				'post_status' => 'publish',
				'posts_per_page' => -1,
				'tax_query' => array(
					array(
						'taxonomy' => 'location_residency',
						'field'    => 'slug',
						'terms'    => $timesheet_residency,
					),
				),				
			);
			
		} else {
			$timesheetargs = array(
				'post_type' => 'timesheet',
				'posts_per_page' => -1,				
				'post_status' => 'publish',
			);
		}

		/*if ($timesheet_residency && $timesheet_residency !='all' ) {
			$timesheetargs = array(
				'post_type' => 'timesheet',
				'post_status' => 'publish',
				'posts_per_page' => -1,
				'tax_query' => array(
					array(
						'taxonomy' => 'location_residency',
						'field'    => 'slug',
						'terms'    => $timesheet_residency,
					),
				),
			);
		} elseif ($timesheet_residency && $timesheet_residency !='all' ) {

		} else {
			$timesheetargs = array(
				'post_type' => 'timesheet',
				'posts_per_page' => -1,				
				'post_status' => 'publish',
			);
		} */
		// the query
		$rosterthe_query = new WP_Query( $timesheetargs ); 
		?>

		<?php if ( $rosterthe_query->have_posts() ) : ?>

			<!-- pagination here -->
			<div class="manager-timesheet-listing-wrapper">
				<ul class="timesheet-list list-group">
			<!-- the loop -->
			<?php while ( $rosterthe_query->have_posts() ) : $rosterthe_query->the_post(); ?>
				<li class="list-group-item clearfix">
	  				<div class="col-sm-3">
	  					<?php 
	  						$timesheet_user_id = get_post_meta( get_the_ID(), 'timesheet_uploaded_userid', true );
	  						$user = get_user_by('id', $timesheet_user_id);
	  					?>
						<?php echo $user->first_name . ' ' . $user->last_name; ?>						  					
	  				</div>
	  				<div class="col-sm-7">
	  					<?php $timesheet_date = get_the_date( 'l, F j, Y', get_the_ID() ); ?>
	  					<?php echo $timesheet_date; ?>
	  				</div>
	  				<div class="col-sm-2">
	  					<?php 
	  						$timesheet_uploaded_sheet_value = get_post_meta( get_the_ID(), 'timesheet_uploaded_sheet', true );
	  					?>
						<a href='<?php echo $timesheet_uploaded_sheet_value; ?>' download>Download</a>
	  				</div>
	  			</li>							
			<?php endwhile; ?>
			<!-- end of the loop -->
				</ul>
			</div>
			<!-- pagination here -->

			<?php wp_reset_postdata(); ?>

		<?php else : ?>
			<div class="list-group">
				<a class="list-group-item" title="No Roster Added">
					<span class="badge"></span>
					<div class="no-rosters-added"><?php _e( 'Sorry, No Timesheet Found.' ); ?></div>
				</a>
			</div>	
		<?php endif; ?>

		<?php
		echo "";
		die();
}

/* =============== Employee Management filter for search employee loctionwise  ===================== */
add_action( 'wp_ajax_nopriv_client_list_by_residency', 'client_management_filter' );
add_action( 'wp_ajax_client_list_by_residency', 'client_management_filter' );
function client_management_filter(){
	global $post;
		$client_residency = $_POST['client_residency'];
	?>
		<?php
		if ($client_residency != 'all') {
			
		$timesheetargs = array(
			'post_type' => 'locations',
			'post_status' => 'publish',
			'posts_per_page' => -1,
			'tax_query' => array(
				array(
					'taxonomy' => 'location_residency',
					'field'    => 'slug',
					'terms'    => $client_residency,
				),
			),
		);
		// the query
		$client_query = new WP_Query( $timesheetargs ); 
		?>

		<?php if ( $client_query->have_posts() ) : ?>

			<!-- pagination here -->

			<!-- the loop -->
			<?php while ( $client_query->have_posts() ) : $client_query->the_post(); ?>
				<div class="list-group-item">
					<div class="row">
						<div class="col-xs-12 col-sm-6"><?php echo get_the_title(); ?></div>
						<div class="col-xs-6 col-sm-3"><?php 
						$categories = get_the_terms( $post->ID, 'location_residency' );
						if ($categories) {
							foreach( $categories as $category ) {
							   echo $category->name;
							}												
						}
						?></div>
						<div class="col-xs-3 col-sm-2"><a class="client-edit-live">Edit</a></div>
						<div class="client-delete" postid="<?php echo get_the_ID(); ?>" postslug="<?php echo $post->post_name; ?>">
							<span class="badge"><i class="fa fa-trash-o" aria-hidden="true"></i></span>
						</div>
					</div>
					<div class="row">
						<?php 
					$term_list = wp_get_post_terms(get_the_ID(), 'location_residency', array("fields" => "names"));
					$postterm = $term_list;
					$terms = get_terms([
						'taxonomy' => 'location_residency',
						'hide_empty' => false,
					]);
						?>
						<div class="edit-client clearfix">
							<div class="form-inline">
								<div class="col-xs-12 col-md-5 form-group padding-right-0">
									<input type="text" class="form-control" id="<?php echo $post->post_name; ?><?php echo get_the_ID(); ?>" value="<?php echo get_the_title(); ?>">
								</div>
								<div class="col-xs-12 col-md-4 form-group padding-right-0">
									<select class="form-control" id="<?php echo $post->post_name; ?><?php echo get_the_ID(); ?>">
										<?php 
										if ($terms) {
										foreach ($terms as $termkey => $termvalue) { ?>
											<option value="<?php echo $termvalue->term_id; ?>" <?php if( $postterm[0] == $termvalue->name ){ echo 'selected="selected"'; }?>>
												<?php echo $termvalue->name; ?>
											</option>
										<?php } 
											
										} ?>
									</select>
								</div>
								<div class="col-xs-12 col-md-3 form-group padding-right-0">
									<button type="submit" class="btn btn-brown update-client-live" postid="<?php echo get_the_ID(); ?>" postslug="<?php echo $post->post_name; ?>">Update</button>
								</div>
							</div>
						</div>											
					</div>
				</div>							
			<?php endwhile; ?>
			<!-- end of the loop -->
				
			<!-- pagination here -->

			<?php wp_reset_postdata(); ?>

		<?php else : ?>
			<div class="list-group">
				<a class="list-group-item" title="No Roster Added">
					<span class="badge"></span>
					<div class="no-rosters-added"><?php _e( 'Sorry, No Clients Found.' ); ?></div>
				</a>
			</div>	
		<?php endif; ?>

		<?php
		echo "";
		} else {
			wp_send_json('all');
			echo "";
		}
		die();
}