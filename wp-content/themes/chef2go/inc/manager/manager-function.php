<?php
// This file is use for Custem function for manager section

// numbered pagination
function pagination($pages = '', $range = 4) {  
     $showitems = ($range * 2)+1;  
 
     global $paged;
     if(empty($paged)) $paged = 1;
 
     if($pages == '')
     {
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
     }   
 
     if(1 != $pages)
     {
         echo "<div class=\"pagination\"><span>Page ".$paged." of ".$pages."</span>";
         if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo; First</a>";
         if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."'>&lsaquo; Previous</a>";
 
         for ($i=1; $i <= $pages; $i++)
         {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             {
                 echo ($paged == $i)? "<span class=\"current\">".$i."</span>":"<a href='".get_pagenum_link($i)."' class=\"inactive\">".$i."</a>";
             }
         }
 
         if ($paged < $pages && $showitems < $pages) echo "<a href=\"".get_pagenum_link($paged + 1)."\">Next &rsaquo;</a>";  
         if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'>Last &raquo;</a>";
         echo "</div>\n";
     }
}

function get_exired_posts_to_delete() {
// Remove timesheet after 4 week automatically
$args = array(
	'post_type' => 'timesheet',
	'post_status' => 'publish',
	'posts_per_page' => -1,
);
$timesheet_query = new WP_Query( $args ); ?>

<?php if ( $timesheet_query->have_posts() ) : ?>

	<!-- pagination here -->

	<!-- the loop -->
	<?php while ( $timesheet_query->have_posts() ) : $timesheet_query->the_post(); ?>
		<?php $date = get_the_date(); ?>
		<?php $after4week_date = date('Y-m-d', strtotime($date .' +4 week')); ?>
		<?php $current_date = date('Y-m-d'); ?>
		<?php 
			// Check timesheet date after
			if ($current_date >= $after4week_date) {
				wp_delete_post( get_the_id());
				/* $query = array(
					'ID' => get_the_id(),
					'post_status' => 'draft',
				);
				wp_update_post( $query, true ); */
			}
		?>
	<?php endwhile; ?>
	<!-- end of the loop -->
	<!-- pagination here -->
	<?php wp_reset_postdata(); ?>

<?php else : ?>
<?php endif; 	
}
add_action( 'expired_post_delete', 'get_exired_posts_to_delete' );

// Add function to register event to wp
add_action( 'wp', 'register_daily_post_delete_event');
function register_daily_post_delete_event() {
    // Make sure this event hasn't been scheduled
    if( !wp_next_scheduled( 'expired_post_delete' ) ) {
        // Schedule the event
        wp_schedule_event( time(), 'daily', 'expired_post_delete' );
    }
}

function get_roster_to_delete() {
// Remove roster after 4 week automatically
$args = array(
    'post_type' => 'rosters',
    'post_status' => 'publish',
    'posts_per_page' => -1,
);
$roster_query = new WP_Query( $args ); ?>

<?php if ( $roster_query->have_posts() ) : ?>

    <!-- pagination here -->

    <!-- the loop -->
    <?php while ( $roster_query->have_posts() ) : $roster_query->the_post(); ?>
        <?php $date = get_the_date(); ?>
        <?php $after4week_date = date('Y-m-d', strtotime($date .' +4 week')); ?>
        <?php $current_date = date('Y-m-d'); ?>
        <?php 
            // Check timesheet date after
            if ($current_date >= $after4week_date) {
                wp_delete_post( get_the_id());
                // $query = array(
                //     'ID' => get_the_id(),
                //     'post_status' => 'draft',
                // );
                // wp_update_post( $query, true );
            }
        ?>
    <?php endwhile; ?>
    <!-- end of the loop -->
    <!-- pagination here -->
    <?php wp_reset_postdata(); ?>

<?php else : ?>
<?php endif;    
}
add_action( 'expired_roster_delete', 'get_roster_to_delete' );

// Add function to register event to wp
add_action( 'wp', 'register_daily_roster_delete_event');
function register_daily_roster_delete_event() {
    // Make sure this event hasn't been scheduled
    if( !wp_next_scheduled( 'expired_roster_delete' ) ) {
        // Schedule the event
        wp_schedule_event( time(), 'daily', 'expired_roster_delete' );
    }
}