<?php 
/**
 * Dimox Breadcrumbs
 * 
 */

/* ============================================================ Breadcrumbs Function ============================================================ */
function dimox_breadcrumbs(){
  /* === OPTIONS === */
	$text['home']     = 'Home'; // text for the 'Home' link
	$text['category'] = 'Archive by Category "%s"'; // text for a category page
	$text['tax'] 	  = 'Archive for "%s"'; // text for a taxonomy page
	$text['search']   = 'Search Results for "%s" Query'; // text for a search results page
	$text['tag']      = 'Posts Tagged "%s"'; // text for a tag page
	$text['author']   = 'Articles Posted by %s'; // text for an author page
	$text['404']      = 'Error 404'; // text for the 404 page
	$showCurrent = 1; // 1 - show current post/page title in breadcrumbs, 0 - don't show
	$showOnHome  = 0; // 1 - show breadcrumbs on the homepage, 0 - don't show
	$delimiter   = ' <span class="separator"><i class="fa fa-angle-right"></i></span> '; // delimiter between crumbs
	$before      = '<span class="current">'; // tag before the current crumb
	$after       = '</span>'; // tag after the current crumb
	/* === END OF OPTIONS === */
	global $post;
	$homeLink = get_bloginfo('url') . '/';
	$linkBefore = '<span typeof="v:Breadcrumb">';
	$linkAfter = '</span>';
	$linkAttr = ' rel="v:url" property="v:title"';
	$link = $linkBefore . '<a' . $linkAttr . ' href="%1$s">%2$s</a>' . $linkAfter;
	if (is_home() || is_front_page()) {
		if ($showOnHome == 1) echo '<div id="crumbs"><a href="' . $homeLink . '">' . $text['home'] . '</a></div>';
	} else {
		echo '<div id="crumbs" xmlns:v="http://rdf.data-vocabulary.org/#">' . sprintf($link, $homeLink, $text['home']) . $delimiter;
		
		if ( is_category() ) {
			$thisCat = get_category(get_query_var('cat'), false);
			if ($thisCat->parent != 0) {
				$cats = get_category_parents($thisCat->parent, TRUE, $delimiter);
				$cats = str_replace('<a', $linkBefore . '<a' . $linkAttr, $cats);
				$cats = str_replace('</a>', '</a>' . $linkAfter, $cats);
				echo $cats;
			}
			echo $before . sprintf($text['category'], single_cat_title('', false)) . $after;
		} elseif( is_tax() ){
			$thisCat = get_category(get_query_var('cat'), false);
			if ($thisCat->parent != 0) {
				$cats = get_category_parents($thisCat->parent, TRUE, $delimiter);
				$cats = str_replace('<a', $linkBefore . '<a' . $linkAttr, $cats);
				$cats = str_replace('</a>', '</a>' . $linkAfter, $cats);
				echo $cats;
			}
			echo $before . sprintf($text['tax'], single_cat_title('', false)) . $after;
		
		}elseif ( is_search() ) {
			echo $before . sprintf($text['search'], get_search_query()) . $after;
		} elseif ( is_day() ) {
			echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $delimiter;
			echo sprintf($link, get_month_link(get_the_time('Y'),get_the_time('m')), get_the_time('F')) . $delimiter;
			echo $before . get_the_time('d') . $after;
		} elseif ( is_month() ) {
			echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $delimiter;
			echo $before . get_the_time('F') . $after;
		} elseif ( is_year() ) {
			echo $before . get_the_time('Y') . $after;
		} elseif ( is_single() && !is_attachment() ) {
			if ( get_post_type() != 'post' ) {
				$post_type = get_post_type_object(get_post_type());
				$slug = $post_type->rewrite;
				printf($link, $homeLink . '/' . $slug['slug'] . '/', $post_type->labels->singular_name);
				if ($showCurrent == 1) echo $delimiter . $before . get_the_title() . $after;
			} else {
				$cat = get_the_category(); $cat = $cat[0];
				$cats = get_category_parents($cat, TRUE, $delimiter);
				if ($showCurrent == 0) $cats = preg_replace("#^(.+)$delimiter$#", "$1", $cats);
				$cats = str_replace('<a', $linkBefore . '<a' . $linkAttr, $cats);
				$cats = str_replace('</a>', '</a>' . $linkAfter, $cats);
				echo $cats;
				if ($showCurrent == 1) echo $before . get_the_title() . $after;
			}
		} elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
			$post_type = get_post_type_object(get_post_type());
			echo $before . $post_type->labels->singular_name . $after;
		} elseif ( is_attachment() ) {
			$parent = get_post($post->post_parent);
			$cat = get_the_category($parent->ID); $cat = $cat[0];
			$cats = get_category_parents($cat, TRUE, $delimiter);
			$cats = str_replace('<a', $linkBefore . '<a' . $linkAttr, $cats);
			$cats = str_replace('</a>', '</a>' . $linkAfter, $cats);
			echo $cats;
			printf($link, get_permalink($parent), $parent->post_title);
			if ($showCurrent == 1) echo $delimiter . $before . get_the_title() . $after;
		} elseif ( is_page() && !$post->post_parent ) {
			if ($showCurrent == 1) echo $before . get_the_title() . $after;
		} elseif ( is_page() && $post->post_parent ) {
			$parent_id  = $post->post_parent;
			$breadcrumbs = array();
			while ($parent_id) {
				$page = get_page($parent_id);
				$breadcrumbs[] = sprintf($link, get_permalink($page->ID), get_the_title($page->ID));
				$parent_id  = $page->post_parent;
			}
			$breadcrumbs = array_reverse($breadcrumbs);
			for ($i = 0; $i < count($breadcrumbs); $i++) {
				echo $breadcrumbs[$i];
				if ($i != count($breadcrumbs)-1) echo $delimiter;
			}
			if ($showCurrent == 1) echo $delimiter . $before . get_the_title() . $after;
		} elseif ( is_tag() ) {
			echo $before . sprintf($text['tag'], single_tag_title('', false)) . $after;
		} elseif ( is_author() ) {
	 		global $author;
			$userdata = get_userdata($author);
			echo $before . sprintf($text['author'], $userdata->display_name) . $after;
		} elseif ( is_404() ) {
			echo $before . $text['404'] . $after;
		}
		if ( get_query_var('paged') ) {
			if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
			echo __('Page') . ' ' . get_query_var('paged');
			if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
		}
		echo '</div>';
	}
}



/* ========================================= Meta box Function ======================================== */
/* Meta box */
function adding_page_meta_boxes() {
    add_meta_box( 'page_meta_fields','Slider Shortcode','slider_shortcode_metabox','page','normal','default');
}
add_action( 'add_meta_boxes', 'adding_page_meta_boxes', 10, 2 );

function slider_shortcode_metabox() {
    global $post; 
    $_slider_shortcode = get_post_meta($post->ID,'_slider_shortcode',true); 
    ?>
    <table class="form-table">
		<tbody>
			<tr>
				<th scope="row" style="width: 140px">
					<label for="shortcode-title">Paste Shortcode</label>
				</th>
				<td>
					<input id='shortcode-container' value='<?php echo $_slider_shortcode; ?>' type='text' name='_slider_shortcode' class='text large-text'>
					<span class="description">For display shortcode please select "Contact Us" template from right side.</span>
				</td>
			</tr>
		</tbody>
	</table>
<?php }
function save_page_utilities_meta($post_id) {
    global $post;
    if( 'page' == $post->post_type ){
        update_post_meta($post_id, '_slider_shortcode', $_POST['_slider_shortcode']);
    }
}
add_action( 'save_post', 'save_page_utilities_meta' );


/* ============================= Ajax for chef shortcode filter ==================================== */
add_action( 'wp_ajax_nopriv_chef_lising_query', 'chef_lising_query' );
add_action( 'wp_ajax_chef_lising_query', 'chef_lising_query' );
function chef_lising_query(){
		$residency_slug = $_POST['residency_slug'];
		$level_select = $_POST['level_select'];

		if ($residency_slug == 'all' && $level_select == 'all' ) {
			$cheflisting_args = array(
				'post_type' => 'chef',	
				'posts_per_page' => -1,
				'post_status' => array( 'publish' ),
			);			
		} elseif ($residency_slug != 'all' && $level_select == 'all') {
			$cheflisting_args = array(
				'post_type' => 'chef',
				'posts_per_page' => -1,
				'tax_query' => array(
					array(
						'taxonomy' => 'location_residency',
						'field'    => 'slug',
						'terms'    => $residency_slug,
					),
				),				
				'post_status' => array( 'publish' ),
			);
		} elseif ($residency_slug == 'all' && $level_select != 'all') {
			$cheflisting_args = array(
				'post_type' => 'chef',
				'posts_per_page' => -1,
				'tax_query' => array(
					array(
						'taxonomy' => 'field_of_interest',
						'field'    => 'slug',
						'terms'    => $level_select,
					),
				),				
				'post_status' => array( 'publish' ),
			);
		} elseif ($residency_slug != 'all' && $level_select != 'all') {
			$cheflisting_args = array(
				'post_type' => 'chef',
				'posts_per_page' => -1,
				'post_status' => array( 'publish' ),
				'tax_query' => array(
					array(
						'taxonomy' => 'location_residency',
						'field'    => 'slug',
						'terms'    => $residency_slug,
					),
					array(
						'taxonomy' => 'field_of_interest',
						'field'    => 'slug',
						'terms'    => $level_select,
					),
				),
			);
		}

		$cheflisting = new WP_Query( $cheflisting_args ); ?>

		<?php if ( $cheflisting->have_posts() ) : ?>

			<!-- pagination here -->
			
			<!-- the loop -->
			<?php while ( $cheflisting->have_posts() ) : $cheflisting->the_post(); ?>
				
				<?php 
					get_template_part( 'template-parts/chef', 'listing' );
				?>


			<?php endwhile; ?>
			<!-- end of the loop -->

			<!-- pagination here -->

			<?php wp_reset_postdata(); ?>

		<?php else : ?>
			<h3><?php _e( 'Sorry, no relief chefs are found.' ); ?></h3>
		<?php endif; 
		echo ' ';
		die();
}

/* ================ Ajax for part time chef in recruitor shortcode filter ========== */
add_action( 'wp_ajax_nopriv_part_time_chef_recruitor', 'part_time_chef_recruitor' );
add_action( 'wp_ajax_part_time_chef_recruitor', 'part_time_chef_recruitor' );
function part_time_chef_recruitor(){
		$residency_slug = $_POST['residency_slug'];

		if ($residency_slug == 'all' ) {
			$cheflisting_args = array(
				'post_type' => 'chef',
				'post_status' => array( 'publish' ),
				'posts_per_page' => -1,			
			);			
		} elseif ($residency_slug != 'all') {
			$cheflisting_args = array(
				'post_type' => 'chef',
				'posts_per_page' => -1,
				'tax_query' => array(
					array(
						'taxonomy' => 'field_of_interest',
						'field'    => 'slug',
						'terms'    => $residency_slug,
					),
				),
				'post_status' => array( 'publish' ),		
			);
		}

		$cheflisting = new WP_Query( $cheflisting_args ); ?>

		<?php if ( $cheflisting->have_posts() ) : ?>

			<!-- pagination here -->
			
			<!-- the loop -->
			<?php while ( $cheflisting->have_posts() ) : $cheflisting->the_post(); ?>
				
				<?php 
					get_template_part( 'template-parts/chef', 'listing' );
				?>


			<?php endwhile; ?>
			<!-- end of the loop -->

			<!-- pagination here -->

			<?php wp_reset_postdata(); ?>

		<?php else : ?>
			<p><?php _e( 'Sorry, no relief chefs are found.' ); ?></p>
		<?php endif; 
		echo ' ';
		die();
}


/* ======================================= Staff Listing ============================================= */
add_action( 'wp_ajax_nopriv_staff_lising_query', 'staff_lising_query' );
add_action( 'wp_ajax_staff_lising_query', 'staff_lising_query' );
function staff_lising_query(){
		$residency_slug = $_POST['staff_residency_slug'];
		$level_select = $_POST['staff_level_select'];

		if ($residency_slug == 'all' && $level_select == 'all') {
			$cheflisting_args = array(
				'post_type' => 'hospitality_staff',
				'posts_per_page' => -1,
				'post_status' => array( 'publish' ),
			);
		} elseif ($level_select != '' || $residency_slug != '') {
			if ($level_select != '' && $residency_slug == 'all') {
				$cheflisting_args = array(
					'post_type' => 'hospitality_staff',
					'posts_per_page' => -1,
					'tax_query' => array(
						array(
							'taxonomy' => 'position_type',
							'field'    => 'slug',
							'terms'    => $level_select,
						),
					),
					'post_status' => array( 'publish' ),
				);	
			} elseif ($level_select == 'all' && $residency_slug != '') {
				$cheflisting_args = array(
					'post_type' => 'hospitality_staff',
					'posts_per_page' => -1,
					'tax_query' => array(
						array(
							'taxonomy' => 'location_residency',
							'field'    => 'slug',
							'terms'    => $residency_slug,
						),						
					),
					'post_status' => array( 'publish' ),
				);
			} else {
				$cheflisting_args = array(
					'post_type' => 'hospitality_staff',
					'posts_per_page' => -1,
					'tax_query' => array(
						array(
							'taxonomy' => 'location_residency',
							'field'    => 'slug',
							'terms'    => $residency_slug,
						),
						array(
							'taxonomy' => 'position_type',
							'field'    => 'slug',
							'terms'    => $level_select,
						),
					),
					'post_status' => array( 'publish' ),
				);				
			}
		}

		$cheflisting = new WP_Query( $cheflisting_args ); ?>

		<?php if ( $cheflisting->have_posts() ) : ?>

			<!-- pagination here -->
			
			<!-- the loop -->
			<?php while ( $cheflisting->have_posts() ) : $cheflisting->the_post(); ?>
				
				<?php 
					get_template_part( 'template-parts/staff', 'listing' );
				?>


			<?php endwhile; ?>
			<!-- end of the loop -->

			<!-- pagination here -->

			<?php wp_reset_postdata(); ?>

		<?php else : ?>
			<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
		<?php endif; 
		echo ' ';
		die();
}



/* ============================= Populate Form Field =============================== */
//add_action( 'gform_pre_submission_4', 'populate_custom_hidden_field' );
function populate_custom_hidden_field( $form ) {
	$_POST['input_7'] = $_POST['input_3'];
	return $form;
}


/* ============================= Search Entries of Employee Module =============================== */
add_action( 'wp_ajax_nopriv_search_entries_listing', 'search_entries_listing_function' );
add_action( 'wp_ajax_search_entries_listing', 'search_entries_listing_function' );
function search_entries_listing_function(){
		$current_user = wp_get_current_user();
		$newfromvalue = '';
		$newtovalue = '';
		$search_criteria = array();
		$form_id = 4;
		
		$fromvalue = $_POST['fromvalue'];
		$tovalue = $_POST['tovalue'];
		
		$newfromvalue = date("Ymd", strtotime($fromvalue));
		$newtovalue = date("Ymd", strtotime($tovalue));


		?>
		<?php 		
			$timesheet_args = array(
				'post_type' => 'employee_timesheet',
				'posts_per_page' => -1,
				'author' => $current_user->ID,
			);

			// the query
			$timesheet_query = new WP_Query( $timesheet_args ); 
			
			?>

			<?php if ( $timesheet_query->have_posts() ) : ?>

		        <!-- pagination here -->

	        <div id="chefliasting">
				<!-- <img src="http://localhost/chef2go/wp-content/uploads/map.png"> -->
				<div class="timesheet-header">
					<!-- <div class="pdffields" style="width: 50%; display: inline-block; margin-bottom: 20px;"><strong>Name: </strong> _________________</div>
					<div class="pdffields" style="width: 49%; display: inline-block;"><strong>Establishment:</strong> _________________</div>
					<div class="pdffields" style="width: 50%; display: inline-block;"><strong>Week ending: </strong>_________________</div>
					<div style="text-align: center;"><h4>Australian Capital Territory</h4></div> -->
				</div>
				<div class="timesheet-content">
					<h5>This is a reference for your worked week if you would like to save this as a PDF please click “Generate Prefilled Timesheet” which will save to your computer.</h5>
					<table border="1" cellpadding="10" style="width: 100%;">
						<tbody align="center">
							<tr>
								<th style="text-align: center;">Location</th>
								<th style="text-align: center;">date</th>
								<th style="text-align: center;">Start</th>
								<th style="text-align: center;">Finish</th>
								<th style="text-align: center;">Daily Total HRS Worked</th>
								<th style="text-align: center;">Supervisor Intials</th>
							</tr>

		        <!-- the loop -->
		        <?php while ( $timesheet_query->have_posts() ) : $timesheet_query->the_post(); ?>
		        
		        <?php 

		        	$clinedet = get_post_meta( get_the_ID(), false );
					        
			        for ($timesheet_meta = 1; $timesheet_meta < 11; $timesheet_meta++) { 
					    $datekey = 'timesheet_date_'.$timesheet_meta;
				        $timesheet_date = get_post_meta( get_the_ID(), $datekey, true );
				        if ($timesheet_date >= $newfromvalue && $timesheet_date <= $newtovalue ) {
					        $clientkey = 'clients_'.$timesheet_meta;
							$clientval = get_post_meta( get_the_ID(), $clientkey, true );						        
					        if ($clientval) {
						        $startkey = 'start_time_'.$timesheet_meta;
						        $endkey = 'end_time_'.$timesheet_meta;
						        print_timesheet($clientkey, $datekey, $startkey, $endkey );
						        $totaltime += difference_calculator($startkey, $endkey );
					        }				        	
				        }
			        	
			        }

					// $client = get_post_meta( get_the_ID(), 'clients_1', true );
					// $date = get_post_meta( get_the_ID(), 'timesheet_date_1', true );

					// $start_time = get_post_meta( get_the_ID(), 'start_time_1', true );
					// $end_time = get_post_meta( get_the_ID(), 'end_time_1', true );

					// $starttime = get_post_meta( get_the_ID(), 'start_time_1', true );
					// $endtime = get_post_meta( get_the_ID(), 'end_time_1', true );

					// $start = array_filter(explode(' ',str_replace(':',' ',$starttime)));
					// $end = array_filter(explode(' ',str_replace(':',' ',$endtime)));
					// $newDate = date("m/d/Y", strtotime($date));
					
					// // convert date and time arrays into datetime formats
					// $startdate = date_create_from_format('m/d/Y@h:i a', $newDate . "@". $start[0].":".$start[1]." ".$start[2]);
					// $enddate =   date_create_from_format('m/d/Y@h:i a', $newDate . "@". $end[0].":".$end[1]." ".$end[2]);

					// //convert datetimes into seconds to compare
					// $starttime = strtotime($startdate->format('Y-m-d H:i:s'));
					// $endtime = strtotime($enddate->format('Y-m-d H:i:s'));

					// // check to see if the times span overnight
					// if($starttime > $endtime)
					// $endtime = strtotime($enddate->format('Y-m-d H:i:s') . " +1 day");

					// // perform calculation
					// $diff = floor(($endtime - $starttime)/60);
					// $totaldiff = $diff/60;
					// $totaldiff = explode(".",$totaldiff);


					// $totaltime += $diff;

					// if ($diff >= 60 ) {
					// 	$time = $diff/60 . ' Hours';
					// } else {
					// 	$time = $diff . ' Minutes';									
					// }

				?>
					<!-- <tr>
						<td> -->
						<?php 	
							// $term = get_term_by('term_id', $client, 'client_location');
							// echo $term->name;
						?><!-- </td> -->
						<!-- <td><?php echo date("d-m-Y", strtotime($date)); ?></td>
						<td><?php echo $start_time; ?></td>
						<td><?php echo $end_time; ?></td>
						<td><?php echo $totaldiff[0] . ' Hours ' . $diff%60 . ' Minutes' ; ?></td>
						<td style="width:150px;"></td> -->
					<!-- </tr> -->
				
		        <?php endwhile; ?>
		        <!-- end of the loop -->

					</tbody>
				</table>
			</div>
			<?php 
				$totalhourswork = $totaltime/60;
				$totaltime = explode(".",$totalhourswork);
				$minutes = $totaltime[1]*0.6;
				$numlength = strlen((string)$minutes);
				if ($numlength <= 1) {
					$min = $minutes.'0';
				} else {
					$min = $minutes; 
				}
			?>
			<div class="timesheet-footer">
				<div style="text-align: right; font-size: 18px; margin-bottom: 20px;"> Total: <?php echo $totaltime[0] . ' Hours ' . $min . ' Minutes' ; ?></div>
				<div style="font-size: 22px; line-height: 52px; " class="hidedata"> Employee Signature: ________________</div>
				<div style="font-size: 22px; line-height: 52px; " class="hidedata"> Supervisor Signature: ________________</div>
			</div>
		</div>

			        <!-- pagination here -->

	        <?php wp_reset_postdata(); ?>

	    <?php else : ?>
	        <h2><?php _e( 'Sorry, no timesheet matched your criteria.' ); ?></h2>
	    <?php endif; ?>

		<?php 
		die();
}

/* ============================= Search Entries of Employee Module =============================== */
add_action( 'wp_ajax_nopriv_search_entries_listing_location', 'search_entries_listing_location_function' );
add_action( 'wp_ajax_search_entries_listing_location', 'search_entries_listing_location_function' );
function search_entries_listing_location_function(){
		$current_user = wp_get_current_user();
		$newfromvalue = '';
		$newtovalue = '';
		$searchlocation = '';
		$search_criteria = array();
		$form_id = 4;
		
		$fromvalue = $_POST['fromvalue'];
		$tovalue = $_POST['tovalue'];
		$searchlocation = $_POST['searchlocation'];

		$post_id = $searchlocation;
		$post = get_post($post_id); 
		$slug = $post->post_name;
		
		$newfromvalue = date("Ymd", strtotime($fromvalue));
		$newtovalue = date("Ymd", strtotime($tovalue));?>
		
		<?php 		
			$timesheet_args = array(
				'post_type' => 'employee_timesheet',
				'posts_per_page' => -1,
				'author' => $current_user->ID,
				'meta_query' => array(
					array (
						'key'     => 'timesheet_date',
						'value'   => array( $newfromvalue, $newtovalue ),
						'compare' => 'BETWEEN',
					),
				),
			);
			// the query
			$timesheet_query = new WP_Query( $timesheet_args ); ?>

		<?php if ( $timesheet_query->have_posts() ) : ?>

	        <!-- pagination here -->
    <div id="myarea">
		<div class="timesheet-header">
			<!-- <div class="pdffields" style="width: 50%; display: inline-block; margin-bottom: 20px;"><strong>Name: </strong> _________________</div>
			<div class="pdffields" style="width: 49%; display: inline-block;"><strong>Establishment:</strong> _________________</div>
			<div class="pdffields" style="width: 50%; display: inline-block;"><strong>Week ending: </strong>_________________</div> -->
			<div style="font-size: 30px; text-align: center; margin-bottom: 20px;"><?php echo get_the_title( $searchlocation ); ?></div>
		</div>
		<div class="timesheet-content">
			<table border="1" cellpadding="10" style="width: 100%;">
				<tbody align="center">
					<tr>
						<th style="text-align: center;">Day</th>
						<th style="text-align: center;">date</th>
						<th style="text-align: center;">Start</th>
						<th style="text-align: center;">Finish</th>
						<th style="text-align: center; width: 220px; padding: 10px;">Daily Total HRS Worked</th>
						<th style="text-align: center;">Supervisor Intials</th>
					</tr>

	        <!-- the loop -->
	        <?php while ( $timesheet_query->have_posts() ) : $timesheet_query->the_post(); ?>
            	<?php 
					$client = get_post_meta( get_the_ID(), 'clients', true );
					$term = get_term_by('term_id', $client, 'client_location');
										
					if ( $slug == $term->slug ) { // It return result of selected value 
						
					$date = get_post_meta( get_the_ID(), 'timesheet_date', true );

					$start_time = get_post_meta( get_the_ID(), 'start_time', true );
					$end_time = get_post_meta( get_the_ID(), 'end_time', true );

					$starttime = get_post_meta( get_the_ID(), 'start_time', true );
					$endtime = get_post_meta( get_the_ID(), 'end_time', true );

					$start = array_filter(explode(' ',str_replace(':',' ',$starttime)));
					$end = array_filter(explode(' ',str_replace(':',' ',$endtime)));
					$newDate = date("m/d/Y", strtotime($date));
					
					// convert date and time arrays into datetime formats
					$startdate = date_create_from_format('m/d/Y@h:i a', $newDate . "@". $start[0].":".$start[1]." ".$start[2]);
					$enddate =   date_create_from_format('m/d/Y@h:i a', $newDate . "@". $end[0].":".$end[1]." ".$end[2]);

					//convert datetimes into seconds to compare
					$starttime = strtotime($startdate->format('Y-m-d H:i:s'));
					$endtime = strtotime($enddate->format('Y-m-d H:i:s'));

					// check to see if the times span overnight
					if($starttime > $endtime)
					$endtime = strtotime($enddate->format('Y-m-d H:i:s') . " +1 day");

					// perform calculation
					$diff = floor(($endtime - $starttime)/60);
					$totaldiff = $diff/60;
					$totaldiff = explode(".",$totaldiff);


					$totaltime += $diff;

					if ($diff >= 60 ) {
						$time = $diff/60 . ' Hours';
					} else {
						$time = $diff . ' Minutes';									
					}
				?>
					<tr>
						<td><?php echo date('l', strtotime($date)); ?></td>
						<td><?php echo date('d-m-Y', strtotime($date)); ?></td>
						<td><?php echo $start_time; ?></td>
						<td><?php echo $end_time; ?></td>
						<td><?php echo $totaldiff[0] . ' Hours ' . $diff%60 . ' Minutes' ; ?></td>
						<td style="width:100px;"></td>
					</tr>

				<?php } ?>	

	        <?php endwhile; ?>
	        <!-- end of the loop -->

						
				</tbody>
			</table>
		</div>
		<?php 
			$totalhourswork = $totaltime/60;
			$totaltime = explode(".",$totalhourswork);
			$minutes = $totaltime[1]*0.6;
			$numlength = strlen((string)$minutes);
			if ($numlength <= 1) {
				$min = $minutes.'0';
			} else {
				$min = $minutes; 
			}
			?>
		<div class="timesheet-footer">
			<div style="text-align: right; font-size: 24px; margin-bottom: 20px;"> Total: <?php echo $totaltime[0] . ' Hours ' . $min . ' Minutes' ; ?></div>
			<div style="font-size: 22px; line-height: 52px; " class="hidedata"> Employee Signature: ________________</div>
			<div style="font-size: 22px; line-height: 52px; " class="hidedata"> Supervisor Signature: ________________</div>
		</div>
	</div>

		 <!-- pagination here -->

	        <?php wp_reset_postdata(); ?>

	    <?php else : ?>
	        <h2><?php _e( 'Sorry, no timesheet added in your criteria.' ); ?></h2>
	    <?php endif; ?>

		<?php 
		die();
}

/* ============================= Add Staff in Recruiter Module =============================== */
add_action( 'wp_ajax_nopriv_addstaffmember_update_meta', 'addstaffmember_update_meta' );
add_action( 'wp_ajax_addstaffmember_update_meta', 'addstaffmember_update_meta' );
function addstaffmember_update_meta(){
		$current_user = wp_get_current_user();
		$staff_member = $_POST['staff_member'];
		$addstaffuserrole = $_POST['addstaffuserrole'];

		?>
		<?php 
		$recruiterargs = array(
			'post_type' => 'recruiter',
			'posts_per_page' => -1,
			'post_status' => array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash'),
		); 
		// the query
		$the_query = new WP_Query( $recruiterargs ); 
		?>

		<?php if ( $the_query->have_posts() ) : ?>

			<!-- pagination here -->

			<!-- the loop -->
			<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
				<?php

					$recruiter_user_id = get_post_meta(get_the_ID(),'recruiter_user_id',true);
					


					$recruiter_selected_staff_val = get_post_meta(get_the_ID(),'recruiter_selected_staff',true);

					if ($current_user->ID == $recruiter_user_id) {
						if ($recruiter_selected_staff_val != '') {

							$already_val = unserialize($recruiter_selected_staff_val);
							//$newvalue = array('u_id' => $staff_member, 'u_role' => $addstaffuserrole, );
							
							$already_val[] = array('u_id' => $staff_member, 'u_role' => $addstaffuserrole, );
							
							$userinfo = serialize($already_val);
							update_post_meta(get_the_ID(), 'recruiter_selected_staff', $userinfo);
							echo "Updated Staff";

						} else {
							$already_val[] = array('u_id' => $staff_member, 'u_role' => $addstaffuserrole, );
							$userinfo = serialize($already_val);
							update_post_meta(get_the_ID(), 'recruiter_selected_staff', $userinfo);
							echo "User Saved";
							
						}
					}
				?>							
			<?php endwhile; ?>
			<!-- end of the loop -->

			<!-- pagination here -->

			<?php wp_reset_postdata(); ?>

		<?php else : ?>
			<p><?php _e( 'Sorry, no recruiter matched your criteria.' ); ?></p>
		<?php endif; ?>

		<?php

		die();
}

/* ============================= Edit Staff Member in Recruiter Module =============================== */
add_action( 'wp_ajax_nopriv_editstaffmember_update_meta', 'editstaffmember_update_meta' );
add_action( 'wp_ajax_editstaffmember_update_meta', 'editstaffmember_update_meta' );
function editstaffmember_update_meta(){
		$current_user = wp_get_current_user();
		$staff_member = $_POST['edit_member_id'];
		$addstaffuserrole = $_POST['edit_member_role'];
	?>
	<?php 
		$recruiterargs = array(
			'post_type' => 'recruiter',
			'posts_per_page' => -1,
			'post_status' => array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash'),
		); 
		// the query
		$the_query = new WP_Query( $recruiterargs ); 
		?>

		<?php if ( $the_query->have_posts() ) : ?>

			<!-- pagination here -->

			<!-- the loop -->
			<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
				<?php

					$recruiter_user_id = get_post_meta(get_the_ID(),'recruiter_user_id',true);
					$recruiter_selected_staff_val = get_post_meta(get_the_ID(),'recruiter_selected_staff',true);

					if ($current_user->ID == $recruiter_user_id) {
						if ($recruiter_selected_staff_val != '') {

							$already_val = unserialize($recruiter_selected_staff_val);
							
							foreach ($already_val as $metavalue) {
								$author_info = get_userdata($metavalue['u_id']);

								if ($metavalue['u_id'] == $staff_member){
									$metavalue['u_role'] = $addstaffuserrole;
								}
								$edited_meta_value[] = $metavalue;
							}
							
							$userinfo = serialize($edited_meta_value);
							update_post_meta(get_the_ID(), 'recruiter_selected_staff', $userinfo);
							echo "New Value Saved";
						}
					}
				?>							
			<?php endwhile; ?>
			<!-- end of the loop -->

			<!-- pagination here -->

			<?php wp_reset_postdata(); ?>

		<?php else : ?>
			<p><?php _e( 'Sorry, no recruiter matched your criteria.' ); ?></p>
		<?php endif; ?>

		<?php

		die();
}

/* ============================= Delete Staff Member from Recruiter Module =============================== */
add_action( 'wp_ajax_nopriv_deletestaffmember_update_meta', 'deletestaffmember_update_meta' );
add_action( 'wp_ajax_deletestaffmember_update_meta', 'deletestaffmember_update_meta' );
function deletestaffmember_update_meta(){
		$current_user = wp_get_current_user();
		$staff_member_id = $_POST['staff_member_id'];

	?>
	<?php 
		$recruiterargs = array(
			'post_type' => 'recruiter',
			'posts_per_page' => -1,
			'post_status' => array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash'),
		); 
		// the query
		$the_query = new WP_Query( $recruiterargs ); 
		?>

		<?php if ( $the_query->have_posts() ) : ?>

			<!-- pagination here -->

			<!-- the loop -->
			<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
				<?php

					$recruiter_user_id = get_post_meta(get_the_ID(),'recruiter_user_id',true);
					$recruiter_selected_staff_val = get_post_meta(get_the_ID(),'recruiter_selected_staff',true);

					if ($current_user->ID == $recruiter_user_id) {
						if ($recruiter_selected_staff_val != '') {

							$already_val = unserialize($recruiter_selected_staff_val);

							foreach ($already_val as $key => $metaarrvalue) {
								if ($metaarrvalue['u_id'] == $staff_member_id){
									unset($already_val[$key]);
								}								
							}
							
							$userinfo = serialize($already_val);
							update_post_meta(get_the_ID(), 'recruiter_selected_staff', $userinfo);
							echo "Staff Deleted";
						}
					}
				?>							
			<?php endwhile; ?>
			<!-- end of the loop -->

			<!-- pagination here -->

			<?php wp_reset_postdata(); ?>

		<?php else : ?>
			<p><?php _e( 'Sorry, no recruiter matched your criteria.' ); ?></p>
		<?php endif; ?>

		<?php

		die();
}

/* ============================= Job Applied Module =============================== */
add_action( 'wp_ajax_nopriv_job_applied', 'job_applied_mail' );
add_action( 'wp_ajax_job_applied', 'job_applied_mail' );
function job_applied_mail(){
		$applied_post_id = $_POST['applied_post_id'];
		$applied_user_id = $_POST['applied_user_id'];
		
		$applied_job_val = get_user_meta( $applied_user_id,  'applied_job', true);

		if (!empty($applied_job_val)) {
			$applied_job_val = $applied_job_val.', '.$applied_post_id;
			update_user_meta( $applied_user_id, 'applied_job', $applied_job_val );

		} else {
			update_user_meta( $applied_user_id, 'applied_job', $applied_post_id );
		}

	?>
	<?php 
		$current_user = wp_get_current_user();
		$mailtitle = get_the_title( $applied_post_id );
		$admin_email = get_option( 'admin_email' );
		$to = $admin_email;
		$subject = 'Applied Job';
		
		$body = ''.$current_user->user_firstname .' is Applied for '. $mailtitle .' job.
		<br />
		Please follow full details of employee.
		<br />
		User Name : '. $current_user->user_firstname .'
		<br />
		User E-mail : '. $current_user->user_email .'
		';
		$headers = array('Content-Type: text/html; charset=UTF-8');
		 
		wp_mail( $to, $subject, $body, $headers );
	?>

		<?php

		die();
}


/* ============================= Delete added timesheet =============================== */
add_action( 'wp_ajax_nopriv_delete_added_timedheet', 'delete_added_timedheet' );
add_action( 'wp_ajax_delete_added_timedheet', 'delete_added_timedheet' );
function delete_added_timedheet(){
		$timedheetid = $_POST['timedheetid'];
		$clientkey = $_POST['clientkey'];
		$timesheet_datekey = $_POST['timesheet_datekey'];
		$start_time_key = $_POST['start_time_key'];
		$end_time_key = $_POST['end_time_key'];
	?>
	<?php 

    $employee_timesheet_args = array(
        'post_type' => 'employee_timesheet',
        'posts_per_page' => -1,
    );

    // the query
    $employee_timesheet_query = new WP_Query( $employee_timesheet_args ); ?>

    <?php if ( $employee_timesheet_query->have_posts() ) : ?>

        <!-- pagination here -->

        <!-- the loop -->
        <?php while ( $employee_timesheet_query->have_posts() ) : $employee_timesheet_query->the_post(); ?>
        	<?php 
        		$flag = 0;
        		$delete_post_id = get_the_ID();

				if ($timedheetid == get_the_ID() ) {
					//wp_delete_post( $timedheetid, true);
					delete_post_meta($timedheetid, $clientkey);
					delete_post_meta($timedheetid, $timesheet_datekey);
					delete_post_meta($timedheetid, $start_time_key);
					delete_post_meta($timedheetid, $end_time_key);
					
					for ($timesheet_meta_list = 1; $timesheet_meta_list < 11; $timesheet_meta_list++) { 
			        	$clientkey = 'clients_'.$timesheet_meta_list;
			        	$clientval = get_post_meta( $delete_post_id, $clientkey, true );
			        	$startkey = 'start_time_'.$timesheet_meta_list;
			        	$startval = get_post_meta( $delete_post_id, $startkey, true );						        
			        	$endkey = 'end_time_'.$timesheet_meta_list;
						$endval = get_post_meta( $delete_post_id, $endkey, true );						        
			        	if ($clientval != '' && $startval != '' && $endval != '') {
					        $flag = 1;
					    }
			        	
			        }

			        if ($flag == 0) {
			        	wp_delete_post( $delete_post_id, true);
			        }		
				}

        	?>
        <?php endwhile; ?>
        <!-- end of the loop -->

        <!-- pagination here -->

        <?php wp_reset_postdata(); ?>

    <?php else : ?>
        <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
    <?php endif; ?>
	
	<?php
	die();
}

/* ============================= Delete added Roster =============================== */
add_action( 'wp_ajax_nopriv_delete_roster', 'delete_added_roster' );
add_action( 'wp_ajax_delete_roster', 'delete_added_roster' );
function delete_added_roster(){
		$rosterid = $_POST['roster_post_id'];
	?>
	<?php 

    $rosters_args = array(
        'post_type' => 'rosters',
        'posts_per_page' => -1,
    );

    // the query
    $rosters_query = new WP_Query( $rosters_args ); ?>

    <?php if ( $rosters_query->have_posts() ) : ?>

        <!-- pagination here -->

        <!-- the loop -->
        <?php while ( $rosters_query->have_posts() ) : $rosters_query->the_post(); ?>
        	<?php 
				if ($rosterid == get_the_ID() ) {
					//  $query = array(
					// 	'ID' => get_the_id(),
					// 	'post_status' => 'draft',
					// );
					// wp_update_post( $query, true );
					wp_delete_post( $rosterid, true);
				}

        	?>
        <?php endwhile; ?>
        <!-- end of the loop -->
        <!-- pagination here -->

        <?php wp_reset_postdata(); ?>

    <?php else : ?>
    <?php endif; ?>
	
	<?php
	die();
}


function show_timesheet($clientkey, $timesheet_date_key, $start_time_key, $end_time_key){

		$client = get_post_meta( get_the_ID(), $clientkey, true );
		$date = get_post_meta( get_the_ID(), $timesheet_date_key, true );

		$start_time = get_post_meta( get_the_ID(), $start_time_key, true );
		$end_time = get_post_meta( get_the_ID(), $end_time_key, true );

		$starttime = get_post_meta( get_the_ID(), $start_time_key, true );
		$endtime = get_post_meta( get_the_ID(), $end_time_key, true );

		$start = array_filter(explode(' ',str_replace(':',' ',$starttime)));
		$end = array_filter(explode(' ',str_replace(':',' ',$endtime)));
		$newDate = date("m/d/Y", strtotime($date));
		
		// convert date and time arrays into datetime formats
		$startdate = date_create_from_format('m/d/Y@h:i a', $newDate . "@". $start[0].":".$start[1]." ".$start[2]);
		$enddate =   date_create_from_format('m/d/Y@h:i a', $newDate . "@". $end[0].":".$end[1]." ".$end[2]);

		//convert datetimes into seconds to compare
		$starttime = strtotime($startdate->format('Y-m-d H:i:s'));
		$endtime = strtotime($enddate->format('Y-m-d H:i:s'));

		// check to see if the times span overnight
		if($starttime > $endtime)
		$endtime = strtotime($enddate->format('Y-m-d H:i:s') . " +1 day");

		// perform calculation
		$diff = floor(($endtime - $starttime)/60);

		$totaldiff = $diff/60;
		$totaldiff = explode(".",$totaldiff);

		$totaltime += $diff;

		if ($diff >= 60 ) {
			$time = $diff/60 . ' Hours';
		} else {
			$time = $diff . ' Minutes';									
		}					
	?>
		<tr>
			<td><?php 
				// location/ client name
				$term = get_term_by('term_id', $client, 'client_location');
				echo $term->name;
			?></td>
			<td><?php echo date("d-m-Y", strtotime($date)); ?></td>
			<td><?php echo $start_time; ?></td>
			<td><?php echo $end_time; ?></td>
			<td><?php echo $totaldiff[0] . ' Hours ' . $diff%60 . ' Minutes' ; ?> <span class="delete-timedheet pull-right" timesheet-data="<?php echo get_the_ID(); ?>" clientkey="<?php echo $clientkey; ?>" timesheet_date="<?php echo $timesheet_date_key; ?>" start_time_key="<?php echo $start_time_key; ?>" end_time_key="<?php echo $end_time_key; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></span><a href="<?php echo site_url().'/edit-timesheet/?timesheet_id='.get_the_ID();?>" class="edit-timesheet-btn pull-right">Edit</a></td>
		</tr>
<?php }



function difference_calculator($start_time_key, $end_time_key){
	
	$starttime = get_post_meta( get_the_ID(), $start_time_key, true );
	$endtime = get_post_meta( get_the_ID(), $end_time_key, true );

	$start = array_filter(explode(' ',str_replace(':',' ',$starttime)));
	$end = array_filter(explode(' ',str_replace(':',' ',$endtime)));
	$newDate = date("m/d/Y", strtotime($date));

	// convert date and time arrays into datetime formats
	$startdate = date_create_from_format('m/d/Y@h:i a', $newDate . "@". $start[0].":".$start[1]." ".$start[2]);
	$enddate =   date_create_from_format('m/d/Y@h:i a', $newDate . "@". $end[0].":".$end[1]." ".$end[2]);

	//convert datetimes into seconds to compare
	$starttime = strtotime($startdate->format('Y-m-d H:i:s'));
	$endtime = strtotime($enddate->format('Y-m-d H:i:s'));

	// check to see if the times span overnight
	if($starttime > $endtime)
	$endtime = strtotime($enddate->format('Y-m-d H:i:s') . " +1 day");

	// perform calculation
	$diff = floor(($endtime - $starttime)/60);

	return $diff;
} 



function print_timesheet($clientkey, $timesheet_date_key, $start_time_key, $end_time_key){

		$client = get_post_meta( get_the_ID(), $clientkey, true );
		$date = get_post_meta( get_the_ID(), $timesheet_date_key, true );

		$start_time = get_post_meta( get_the_ID(), $start_time_key, true );
		$end_time = get_post_meta( get_the_ID(), $end_time_key, true );

		$starttime = get_post_meta( get_the_ID(), $start_time_key, true );
		$endtime = get_post_meta( get_the_ID(), $end_time_key, true );

		$start = array_filter(explode(' ',str_replace(':',' ',$starttime)));
		$end = array_filter(explode(' ',str_replace(':',' ',$endtime)));
		$newDate = date("m/d/Y", strtotime($date));
		
		// convert date and time arrays into datetime formats
		$startdate = date_create_from_format('m/d/Y@h:i a', $newDate . "@". $start[0].":".$start[1]." ".$start[2]);
		$enddate =   date_create_from_format('m/d/Y@h:i a', $newDate . "@". $end[0].":".$end[1]." ".$end[2]);

		//convert datetimes into seconds to compare
		$starttime = strtotime($startdate->format('Y-m-d H:i:s'));
		$endtime = strtotime($enddate->format('Y-m-d H:i:s'));

		// check to see if the times span overnight
		if($starttime > $endtime)
		$endtime = strtotime($enddate->format('Y-m-d H:i:s') . " +1 day");

		// perform calculation
		$diff = floor(($endtime - $starttime)/60);

		$totaldiff = $diff/60;
		$totaldiff = explode(".",$totaldiff);

		$totaltime += $diff;

		if ($diff >= 60 ) {
			$time = $diff/60 . ' Hours';
		} else {
			$time = $diff . ' Minutes';									
		}					
	?>

	<tr>
		<td>
		<?php 	
			$term = get_term_by('term_id', $client, 'client_location');
			echo $term->name;
		?></td>
		<td><?php echo date("d-m-Y", strtotime($date)); ?></td>
		<td><?php echo $start_time; ?></td>
		<td><?php echo $end_time; ?></td>
		<td><?php echo $totaldiff[0] . ' Hours ' . $diff%60 . ' Minutes' ; ?></td>
		<td style="width:150px;"></td>
	</tr>

<?php }

function currently_seeking_work_func( ) {
	ob_start();
	$terms = get_terms('field_of_interest'); 
	if ( !empty( $terms ) && !is_wp_error( $terms ) ){
	     echo '<ul class="tax_list_count field_of_interest_count">';
	     foreach ( $terms as $term ) {
	       echo '<li>' . $term->name . ' (' . $term->count . ')' . '</li>';
	     }
	     echo '</ul>';
	 } 
	return ob_get_clean();
}
add_shortcode( 'currently_seeking_work', 'currently_seeking_work_func' );

function currently_seeking_employment_func( ) {
	ob_start();
	$terms = get_terms('position_type'); 
	if ( !empty( $terms ) && !is_wp_error( $terms ) ){
	     echo '<ul class="tax_list_count position_type_count">';
	     foreach ( $terms as $term ) {
	       echo '<li>' . $term->name . ' (' . $term->count . ')' . '</li>';
	     }
	     echo '</ul>';
	 } 
	return ob_get_clean();
}

add_shortcode( 'currently_seeking_employment', 'currently_seeking_employment_func' );
