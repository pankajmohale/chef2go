<?php
/**
 * _s Theme Customizer.
 *
 * @package _s
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function _s_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
}
add_action( 'customize_register', '_s_customize_register' );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function _s_customize_preview_js() {
	wp_enqueue_script( '_s_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', '_s_customize_preview_js' );


/* ====================================================================================================================== */
	// Roster gravity form field populated with user listing
/* ====================================================================================================================== */

// This code is use for roster after adding roster mail will send to chef or staff employee. 
add_filter( 'gform_column_input_11_1_2', 'set_column', 10, 5 );
function set_column( $input_info, $field, $column, $value, $form_id ) {
	$user_query = new WP_User_Query( array( 'role' => 'staff_employee' ) );
	$choices = array();

	$array_val[] = array( 'text' => 'Select Staff', 'value' => '' );
	foreach ( $user_query->results as $user ) {
		$array_val[] = array( 'text' => $user->first_name, 'value' => $user->id );
	}

	return array( 'type' => 'select', 'choices' => $array_val );
}

add_action( 'gform_after_submission_11', 'post_to_third_party', 10, 2 );
function post_to_third_party( $entry, $form ) {
    $get_data = unserialize($entry[1]);
    foreach ($get_data as $value) {
    	$user_info = get_userdata( $value['Staff Member'] );
		
		$to = $user_info->user_email;
		$subject = 'Employee Time Scheduled';
		$body = 'Hello,
		Your work scheduled is as fallow:
		Date :'.$value['Date'] .'
		Start Time : '.$value['Start Time'].'
		Finish Time : '.$value['Finish Time'].'
		';
		$headers = array('Content-Type: text/html; charset=UTF-8');
		 
		wp_mail( $to, $subject, $body, $headers );

    }
}

/* **************************** add user id in recruter cpt ******************************** */

// This code is use for adding user id meta value in recruiter field.
// This is register form field after user approve this field is added in user id in recruiter post type.  
add_action( 'gform_user_registered', 'add_custom_user_meta', 10, 4 );
function add_custom_user_meta( $user_id, $feed, $entry, $user_pass ) { 
	$email_id = $entry[12];
	$post_email_id = '';

	// 
	$recruiterargs = array(
		'post_type' => 'recruiter',
		'post_status' => array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash'),
	); 
	// the query
	$the_query = new WP_Query( $recruiterargs ); 
	?>

	<?php if ( $the_query->have_posts() ) : ?>

		<!-- pagination here -->

		<!-- the loop -->
		<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
			<?php
				$post_email_id = get_post_meta(get_the_ID(),'recruiter_email_address',true); 
				if ($email_id == $post_email_id) {
					update_post_meta(get_the_ID(), 'recruiter_user_id', $user_id);
				} 
			?>
			
		<?php endwhile; ?>
		<!-- end of the loop -->

		<!-- pagination here -->

		<?php wp_reset_postdata(); ?>

	<?php else : ?>
		<p><?php _e( 'Sorry, no recruiter matched your criteria.' ); ?></p>
	<?php endif;

}

# this ajax is used to store selected date form employee my area page and store it in field.
add_action('wp_ajax_update_timesheet_value','update_timesheet_value_function');
add_action('wp_ajax_nopriv_update_timesheet_value','update_timesheet_value_function');
function update_timesheet_value_function(){
	$timesheetfromvalue = $_POST['searchfromvalue'];
	$timesheettovalue = $_POST['searchtovalue'];

	$current_user = wp_get_current_user();
	$user_id = $current_user->ID;
	
	$get_timesheet_array = get_user_meta( $user_id, 'user_uploaded_time_sheet_date', true ); 
	
	if ($get_timesheet_array) {
	
		$timesheetarray = array(
		    "from" => $timesheetfromvalue,
		    "to" => $timesheettovalue,
		);

		$taken_timesheet_date = unserialize($get_timesheet_array);
		
		array_push($taken_timesheet_date, $timesheetarray);
		update_user_meta( $user_id, 'user_uploaded_time_sheet_date', serialize($taken_timesheet_date) );		

	} else {
		$timesheetarray = array(
		    "from" => $timesheetfromvalue,
		    "to" => $timesheettovalue,
		);

		$timesheet_array_merge[] = $timesheetarray;
		update_user_meta( $user_id, 'user_uploaded_time_sheet_date', serialize($timesheet_array_merge));
	}

	echo "done";
	die();
}


// Display Selected timesheet date in field value in form 7 at the tiem of uploading timesheet 
add_filter( 'gform_pre_render_7', 'populate_date' );
add_filter( 'gform_pre_validation_7', 'populate_date' );
add_filter( 'gform_pre_submission_filter_7', 'populate_date' );
add_filter( 'gform_admin_pre_render_7', 'populate_date' );
function populate_date( $form ) {

    foreach ( $form['fields'] as &$field ) {

        if ( $field->type != 'select' || strpos( $field->cssClass, 'populate-date' ) === false ) {
            continue;
        }

        $current_user = wp_get_current_user();
		$user_id = $current_user->ID;
		
		$get_timesheet_array = get_user_meta( $user_id, 'user_uploaded_time_sheet_date', true );

		$timesheet_date = unserialize($get_timesheet_array);

        // you can add additional parameters here to alter the posts that are retrieved
        // more info: [http://codex.wordpress.org/Template_Tags/get_posts](http://codex.wordpress.org/Template_Tags/get_posts)
        $posts = get_posts( 'numberposts=-1&post_status=publish' );

        $choices = array();

        if (is_array($timesheet_date)) {
	        foreach ($timesheet_date as $timesheet_date_key => $timesheet_date_value) {
	            $choices[] = array( 'text' => $timesheet_date_value['from'].'-'.$timesheet_date_value['to'], 'value' => $timesheet_date_value['from'].'/'.$timesheet_date_value['to'] );
	        }        	
        }

        // update 'Select a Post' to whatever you'd like the instructive option to be
        $field->placeholder = 'Select a Date';
        $field->choices = $choices;
    }

    return $form;
}

add_action( 'gform_after_submission_7', 'remove_entries_from_timesheet', 10, 2 );
function remove_entries_from_timesheet( $entry, $form ) {
	$current_user = wp_get_current_user();

	$entrival = explode("/", $entry[10]);

	$fromvalue = $entrival[0];
	$tovalue = $entrival[1];
	
	$newfromvalue = date("Ymd", strtotime($fromvalue));
	$newtovalue = date("Ymd", strtotime($tovalue));

	?>
	<?php 		
		$timesheet_args = array(
			'post_type' => 'employee_timesheet',
			'posts_per_page' => -1,
			'author' => $current_user->ID,
		);

		// the query
		$timesheet_query = new WP_Query( $timesheet_args ); 
		
		?>

		<?php if ( $timesheet_query->have_posts() ) : ?>

	        <!-- pagination here -->

	        <!-- the loop -->
	        <?php while ( $timesheet_query->have_posts() ) : $timesheet_query->the_post(); ?>
	        
	        <?php 
	        	$flag = 0;
	        	$search_post_id = get_the_ID();
	        	
	        	for ($timesheet_meta = 1; $timesheet_meta < 11; $timesheet_meta++) { 
				    $datekey = 'timesheet_date_'.$timesheet_meta;
			        $timesheet_date = get_post_meta( $search_post_id, $datekey, true );
			        if ($timesheet_date >= $newfromvalue && $timesheet_date <= $newtovalue ) {
				        $clientkey = 'clients_'.$timesheet_meta;
						$clientval = get_post_meta( $search_post_id, $clientkey, true );						        
				        if ($clientval) {
					        $startkey = 'start_time_'.$timesheet_meta;
					        $endkey = 'end_time_'.$timesheet_meta;
					        delete_post_meta($search_post_id, $clientkey);
							delete_post_meta($search_post_id, $datekey);
							delete_post_meta($search_post_id, $startkey);
							delete_post_meta($search_post_id, $endkey);				        
				        }				        	
			        }
		        	
		        }

		        for ($timesheet_meta_list = 1; $timesheet_meta_list < 11; $timesheet_meta_list++) { 
		        	$clientkey = 'clients_'.$timesheet_meta_list;
		        	$clientval = get_post_meta( $search_post_id, $clientkey, true );
		        	$startkey = 'start_time_'.$timesheet_meta_list;
		        	$startval = get_post_meta( $search_post_id, $startkey, true );						        
		        	$endkey = 'end_time_'.$timesheet_meta_list;
					$endval = get_post_meta( $search_post_id, $endkey, true );						        
		        	if ($clientval != '' && $startval != '' && $endval != '') {
				        $flag = 1;
				    }
		        	
		        }

		        if ($flag == 0) {
		        	wp_delete_post( get_the_ID(), true);
		        }	        
			?>
			
			<?php endwhile; ?>
	        <!-- end of the loop -->

		    <!-- pagination here -->

        <?php wp_reset_postdata(); ?>

    <?php else : ?>
        <h2><?php _e( 'Sorry, no timesheet matched your criteria.' ); ?></h2>
    <?php endif; 

    // Remove selected option form array
    $user_id = $current_user->ID;
	$array_key = NULL;
	$optionfromvalue = date("j-n-Y", strtotime($fromvalue));
	$optiontovalue = date("j-n-Y", strtotime($tovalue));

	$get_timesheet_val = get_user_meta( $user_id, 'user_uploaded_time_sheet_date', true );
	$timesheet_option_array = unserialize($get_timesheet_val);
	foreach ($timesheet_option_array as $key => $option_array) {
		if ($option_array['from'] == $optionfromvalue && $option_array['to'] == $optiontovalue) {
			$array_key = $key;
			unset($timesheet_option_array[$array_key]);
		} 	
	}
	update_user_meta( $current_user->ID, 'user_uploaded_time_sheet_date', serialize($timesheet_option_array));

	update_post_meta($entry['post_id'], 'timesheet_uploaded_location', $entry[6]);
	update_post_meta($entry['post_id'], 'timesheet_uploaded_date', $entry[10]);
}

add_action( 'gform_after_submission_7', 'add_multiple_files', 10, 2 );
function add_multiple_files( $entry, $form ) {
	$gform_file_upload = $entry[11];
	$gform_file_upload = trim(trim($gform_file_upload, '['), ']');
	$gform_file_upload = str_replace('"', '', $gform_file_upload);
	$gform_file_upload = explode(",",$gform_file_upload);
	foreach ($gform_file_upload as $key => $files) {
		$key_val = $key + 1;
		update_post_meta($entry['post_id'], 'file_upload_'.$key_val, $files);
	}
}


add_filter("gform_pre_render_24", "edit_timesheet_pre_render");
add_filter( 'gform_pre_validation_24', 'edit_timesheet_pre_render' );
add_filter( 'gform_pre_submission_filter_24', 'edit_timesheet_pre_render' );
add_filter( 'gform_admin_pre_render_24', 'edit_timesheet_pre_render' );
function edit_timesheet_pre_render($form){

?>
    <script type="text/javascript">
    jQuery(document).ready(function($){

        /*jQuery('#input_7_6').bind('change', function() {*/
            //get selected value from drop down;
            var selectedValue = jQuery("#input_24_8").val();
            
            $.ajax({
			    type: 'POST',
			    url: ajaxurl,
			    data: {
		            action:'fatch_timesheet_pre_render',
		            selectedValue : selectedValue,
			    },
			    success:function(response){
			    	if (response != null ) {
				        var responsearray = jQuery.parseJSON(response);
				        
				        var entryval24_2 = jQuery("#input_24_2").val();
				        var entryval24_2_text = jQuery("#input_24_2 option:selected").text();
				        entryval24_2_text = entryval24_2_text.replace(/&amp;/g, '&');

				        if (entryval24_2) {
					        $('#input_24_2 option:gt(0)').remove();
					    	for (var i = responsearray.length - 1; i >= 0; i--) {
					    		var text_val = responsearray[i].text;
					    		text_val = text_val.replace(/&amp;/g, '&');

					    		if (entryval24_2_text == text_val) {
					        		$('#input_24_2').append('<option value=' + responsearray[i].value + ' selected="selected">' + responsearray[i].text + '</option>');
					    		} else {
					        		$('#input_24_2').append('<option value=' + responsearray[i].value + '>' + responsearray[i].text + '</option>');
					    		}
					        }
				        }

				        var entryval_24_17 = jQuery("#input_24_17").val();
				        var entryval24_17_text = jQuery("#input_24_17 option:selected").text();
				        entryval24_17_text = entryval24_17_text.replace(/&amp;/g, '&');

				        if (entryval_24_17) {
					        $('#input_24_17 option:gt(0)').remove();
					        for (var i = responsearray.length - 1; i >= 0; i--) {
					        	var text_val = responsearray[i].text;
					    		text_val = text_val.replace(/&amp;/g, '&');

					        	if (entryval24_17_text == text_val) {
					        		$('#input_24_17').append('<option value=' + responsearray[i].value + ' selected="selected">' + responsearray[i].text + '</option>');
					        	} else {
					        		$('#input_24_17').append('<option value=' + responsearray[i].value + '>' + responsearray[i].text + '</option>');
					        	}
					        }				        	
				        }

				        var entryval_24_24 = jQuery("#input_24_24").val();
				        var entryval24_24_text = jQuery("#input_24_24 option:selected").text();
				        entryval24_24_text = entryval24_24_text.replace(/&amp;/g, '&');

				        if (entryval_24_24) {
					        $('#input_24_24 option:gt(0)').remove();
					        for (var i = responsearray.length - 1; i >= 0; i--) {
					        	var text_val = responsearray[i].text;
					    		text_val = text_val.replace(/&amp;/g, '&');

					        	if (entryval24_24_text == text_val) {
					        		$('#input_24_24').append('<option value=' + responsearray[i].value + ' selected="selected">' + responsearray[i].text + '</option>');
					        	} else {
					        		$('#input_24_24').append('<option value=' + responsearray[i].value + '>' + responsearray[i].text + '</option>');
					        	}
					        }				        	
				        }

				        var entryval_24_31 = jQuery("#input_24_31").val();
				        var entryval24_31_text = jQuery("#input_24_31 option:selected").text();
				        entryval24_31_text = entryval24_31_text.replace(/&amp;/g, '&');

				        if (entryval_24_31) {
					        $('#input_24_31 option:gt(0)').remove();
					        for (var i = responsearray.length - 1; i >= 0; i--) {
					        	var text_val = responsearray[i].text;
					    		text_val = text_val.replace(/&amp;/g, '&');

					        	if (entryval24_31_text == text_val) {
					        		$('#input_24_31').append('<option value=' + responsearray[i].value + ' selected="selected">' + responsearray[i].text + '</option>');
					        	} else {
					        		$('#input_24_31').append('<option value=' + responsearray[i].value + '>' + responsearray[i].text + '</option>');
					        	}
					        }				        	
				        }

				        var entryval_24_37 = jQuery("#input_24_37").val();
				        var entryval24_37_text = jQuery("#input_24_37 option:selected").text();
				        entryval24_37_text = entryval24_37_text.replace(/&amp;/g, '&');

				        if (entryval_24_37) {
					        $('#input_24_37 option:gt(0)').remove();
					        for (var i = responsearray.length - 1; i >= 0; i--) {
					        	var text_val = responsearray[i].text;
					    		text_val = text_val.replace(/&amp;/g, '&');

					        	if (entryval24_37_text == text_val) {
					        		$('#input_24_37').append('<option value=' + responsearray[i].value + ' selected="selected">' + responsearray[i].text + '</option>');
					        	} else {
					        		$('#input_24_37').append('<option value=' + responsearray[i].value + '>' + responsearray[i].text + '</option>');
					        	}
					        }				        	
				        }

				        var entryval_24_45 = jQuery("#input_24_45").val();
				        var entryval24_45_text = jQuery("#input_24_45 option:selected").text();
				        entryval24_45_text = entryval24_45_text.replace(/&amp;/g, '&');

				        if (entryval_24_45) {
					        $('#input_24_45 option:gt(0)').remove();
					        for (var i = responsearray.length - 1; i >= 0; i--) {
					        	var text_val = responsearray[i].text;
					    		text_val = text_val.replace(/&amp;/g, '&');

					        	if (entryval24_45_text == text_val) {
					        		$('#input_24_45').append('<option value=' + responsearray[i].value + ' selected="selected">' + responsearray[i].text + '</option>');
					        	} else {
					        		$('#input_24_45').append('<option value=' + responsearray[i].value + '>' + responsearray[i].text + '</option>');
					        	}
					        }				        	
				        }
				        
				        var entryval_24_49 = jQuery("#input_24_49").val();
				        var entryval24_49_text = jQuery("#input_24_49 option:selected").text();
				        entryval24_49_text = entryval24_49_text.replace(/&amp;/g, '&');

				        if (entryval_24_49) {
					        $('#input_24_49 option:gt(0)').remove();
					        for (var i = responsearray.length - 1; i >= 0; i--) {
					        	var text_val = responsearray[i].text;
					    		text_val = text_val.replace(/&amp;/g, '&');

					        	if (entryval24_49_text == text_val) {
					        		$('#input_24_49').append('<option value=' + responsearray[i].value + ' selected="selected">' + responsearray[i].text + '</option>');
					        	} else {
					        		$('#input_24_49').append('<option value=' + responsearray[i].value + '>' + responsearray[i].text + '</option>');
					        	}
					        }				        	
				        }

				        var entryval_24_55 = jQuery("#input_24_55").val();
				        var entryval24_55_text = jQuery("#input_24_55 option:selected").text();
				        entryval24_55_text = entryval24_55_text.replace(/&amp;/g, '&');
				        if (entryval_24_55) {
					        $('#input_24_55 option:gt(0)').remove();
					        for (var i = responsearray.length - 1; i >= 0; i--) {
					        	var text_val = responsearray[i].text;
					    		text_val = text_val.replace(/&amp;/g, '&');

					        	if (entryval24_55_text == text_val) {
					        		$('#input_24_55').append('<option value=' + responsearray[i].value + ' selected="selected">' + responsearray[i].text + '</option>');
					        	} else {
					        		$('#input_24_55').append('<option value=' + responsearray[i].value + '>' + responsearray[i].text + '</option>');
					        	}
					        }				        	
				        }
				        
				        var entryval_24_63 = jQuery("#input_24_63").val();
				        var entryval24_63_text = jQuery("#input_24_63 option:selected").text();
				        entryval24_63_text = entryval24_63_text.replace(/&amp;/g, '&');
					    if (entryval_24_63) {
					        $('#input_24_63 option:gt(0)').remove();
					        for (var i = responsearray.length - 1; i >= 0; i--) {
					    		var text_val = responsearray[i].text;
					    		text_val = text_val.replace(/&amp;/g, '&');

					    		if (entryval24_63_text == text_val) {
					        		$('#input_24_63').append('<option value=' + responsearray[i].value + ' selected="selected">' + responsearray[i].text + '</option>');
					        	} else {
						        	$('#input_24_63').append('<option value=' + responsearray[i].value + '>' + responsearray[i].text + '</option>');
						        }
					        }				        	
				        }
				        
				        var entryval_24_67 = jQuery("#input_24_67").val();
				        var entryval24_67_text = jQuery("#input_24_67 option:selected").text();
				        entryval24_67_text = entryval24_67_text.replace(/&amp;/g, '&');
				        if (entryval_24_67) {
					        $('#input_24_67 option:gt(0)').remove();
					        for (var i = responsearray.length - 1; i >= 0; i--) {
					        	var text_val = responsearray[i].text;
					    		text_val = text_val.replace(/&amp;/g, '&');

					        	if (entryval24_67_text == text_val) {
					        		$('#input_24_67').append('<option value=' + responsearray[i].value + ' selected="selected">' + responsearray[i].text + '</option>');
					        	} else {
					        		$('#input_24_67').append('<option value=' + responsearray[i].value + '>' + responsearray[i].text + '</option>');
					        	}
					        }				        	
				        }

			    	}
			    }
			});


            //populate a text field with the selected drop down value
            // jQuery("#input_167_1").val(selectedValue);
        /*});*/
    });
    </script>
<?php

return $form;
}


add_action( 'wp_ajax_nopriv_fatch_timesheet_pre_render', 'fatch_timesheet_pre_render' );
add_action( 'wp_ajax_fatch_timesheet_pre_render', 'fatch_timesheet_pre_render' );
function fatch_timesheet_pre_render(){
		$location_id = $_POST['selectedValue'];
	?>
	<?php 
		$clients_location = array(
			'post_type' => 'locations',
			'posts_per_page' => -1,
			'tax_query' => array(
				array(
					'taxonomy' => 'location_residency',
					'field'    => 'term_id',
					'terms'    => $location_id,
				),
			),
		); 
		// the query
		$clients_location_query = new WP_Query( $clients_location ); 
		
	        // get the chefs
			// User Loop ?>
	        <?php if ( $clients_location_query->have_posts() ) : ?>

				<?php while ( $clients_location_query->have_posts() ) : $clients_location_query->the_post(); ?>
					<?php 
						$choices[] = array( 'text' => get_the_title(), 'value' => get_the_ID() );
					 ?>
				<?php endwhile; ?>

				<?php wp_reset_postdata(); ?>

			<?php else : ?>
				
			<?php endif; 

		echo json_encode($choices);
		die();
}