<?php
    // hook into the init action and call create_book_taxonomies when it fires
    add_action( 'init', 'create_custom_taxonomies', 0 );

    // create two taxonomies, Citizenship / Visa for the post type "book"
    function create_custom_taxonomies() {
        // Add new taxonomy, make it hierarchical (like categories)
        /* ================ Citizenship / Visa ================ */
        $labels = array(
            'name'              => _x( 'Citizenship / Visa', 'taxonomy general name', 'textdomain' ),
            'singular_name'     => _x( 'Citizenship / Visa', 'taxonomy singular name', 'textdomain' ),
            'search_items'      => __( 'Search Citizenship / Visa', 'textdomain' ),
            'all_items'         => __( 'All Citizenship / Visa', 'textdomain' ),
            'parent_item'       => __( 'Parent Citizenship / Visa', 'textdomain' ),
            'parent_item_colon' => __( 'Parent Citizenship / Visa:', 'textdomain' ),
            'edit_item'         => __( 'Edit Citizenship / Visa', 'textdomain' ),
            'update_item'       => __( 'Update Citizenship / Visa', 'textdomain' ),
            'add_new_item'      => __( 'Add New Citizenship / Visa', 'textdomain' ),
            'new_item_name'     => __( 'New Citizenship / Visa Name', 'textdomain' ),
            'menu_name'         => __( 'Citizenship / Visa', 'textdomain' ),
        );

        $args = array(
            'hierarchical'      => true,
            'labels'            => $labels,
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
            'rewrite'           => array( 'slug' => 'citizenship_visa' ),
        );

        register_taxonomy( 'citizenship_visa', array( 'chef' ), $args );


        /* ================ Type of Recruitment ================ */

        $recruitment = array(
            'name'              => _x( 'Type of Recruitment', 'taxonomy general name', 'textdomain' ),
            'singular_name'     => _x( 'Type of Recruitment', 'taxonomy singular name', 'textdomain' ),
            'search_items'      => __( 'Search Type of Recruitment', 'textdomain' ),
            'all_items'         => __( 'All Type of Recruitment', 'textdomain' ),
            'parent_item'       => __( 'Parent Type of Recruitment', 'textdomain' ),
            'parent_item_colon' => __( 'Parent Type of Recruitment:', 'textdomain' ),
            'edit_item'         => __( 'Edit Type of Recruitment', 'textdomain' ),
            'update_item'       => __( 'Update Type of Recruitment', 'textdomain' ),
            'add_new_item'      => __( 'Add New Type of Recruitment', 'textdomain' ),
            'new_item_name'     => __( 'New Type of Recruitment Name', 'textdomain' ),
            'menu_name'         => __( 'Type of Recruitment', 'textdomain' ),
        );

        $type_of_recruitment = array(
            'hierarchical'      => true,
            'labels'            => $recruitment,
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
            'rewrite'           => array( 'slug' => 'type_of_recruitment' ),
        );

        register_taxonomy( 'type_of_recruitment', array( 'chef' ), $type_of_recruitment );
        

        /* ================ Chef Skills ================ */
        $chef_skills = array(
            'name'              => _x( 'Chef Skills', 'taxonomy general name', 'textdomain' ),
            'singular_name'     => _x( 'Chef Skills', 'taxonomy singular name', 'textdomain' ),
            'search_items'      => __( 'Search Chef Skills', 'textdomain' ),
            'all_items'         => __( 'All Chef Skills', 'textdomain' ),
            'parent_item'       => __( 'Parent Chef Skills', 'textdomain' ),
            'parent_item_colon' => __( 'Parent Chef Skills:', 'textdomain' ),
            'edit_item'         => __( 'Edit Chef Skills', 'textdomain' ),
            'update_item'       => __( 'Update Chef Skills', 'textdomain' ),
            'add_new_item'      => __( 'Add New Chef Skills', 'textdomain' ),
            'new_item_name'     => __( 'New Chef Skills Name', 'textdomain' ),
            'menu_name'         => __( 'Chef Skills', 'textdomain' ),
        );

        $chef_skill = array(
            'hierarchical'      => true,
            'labels'            => $chef_skills,
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
            'rewrite'           => array( 'slug' => 'chef_skill' ),
        );

        register_taxonomy( 'chef_skill', array( 'chef' ), $chef_skill );

        /* ================ Specialising in ================ */

        $specialising_in = array(
            'name'              => _x( 'Specialising in', 'taxonomy general name', 'textdomain' ),
            'singular_name'     => _x( 'Specialising in', 'taxonomy singular name', 'textdomain' ),
            'search_items'      => __( 'Search Specialising in', 'textdomain' ),
            'all_items'         => __( 'All Specialising in', 'textdomain' ),
            'parent_item'       => __( 'Parent Specialising in', 'textdomain' ),
            'parent_item_colon' => __( 'Parent Specialising in:', 'textdomain' ),
            'edit_item'         => __( 'Edit Specialising in', 'textdomain' ),
            'update_item'       => __( 'Update Specialising in', 'textdomain' ),
            'add_new_item'      => __( 'Add New Specialising in', 'textdomain' ),
            'new_item_name'     => __( 'New Specialising in Name', 'textdomain' ),
            'menu_name'         => __( 'Specialising in', 'textdomain' ),
        );

        $specialising = array(
            'hierarchical'      => true,
            'labels'            => $specialising_in,
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
            'rewrite'           => array( 'slug' => 'specialising_in' ),
        );

        register_taxonomy( 'specialising_in', array( 'chef' ), $specialising );

        /* ================ Area you reside in ================ */
        $residency_in = array(
            'name'              => _x( 'Residency', 'taxonomy general name', 'textdomain' ),
            'singular_name'     => _x( 'Residency', 'taxonomy singular name', 'textdomain' ),
            'search_items'      => __( 'Search Residency', 'textdomain' ),
            'all_items'         => __( 'All Residency', 'textdomain' ),
            'parent_item'       => __( 'Parent Residency', 'textdomain' ),
            'parent_item_colon' => __( 'Parent Residency:', 'textdomain' ),
            'edit_item'         => __( 'Edit Residency', 'textdomain' ),
            'update_item'       => __( 'Update Residency', 'textdomain' ),
            'add_new_item'      => __( 'Add New Residency', 'textdomain' ),
            'new_item_name'     => __( 'New Residency Name', 'textdomain' ),
            'menu_name'         => __( 'Residency', 'textdomain' ),
        );

        $residency = array(
            'hierarchical'      => true,
            'labels'            => $residency_in,
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
            'rewrite'           => array( 'slug' => 'residency' ),
        );

        // register_taxonomy( 'residency', array( 'chef' ), $residency );

        /* ================ Area you reside in ================ */
        $field_of_interest_in = array(
            'name'              => _x( 'Field of Interest', 'taxonomy general name', 'textdomain' ),
            'singular_name'     => _x( 'Field of Interest', 'taxonomy singular name', 'textdomain' ),
            'search_items'      => __( 'Search Field of Interest', 'textdomain' ),
            'all_items'         => __( 'All Field of Interest', 'textdomain' ),
            'parent_item'       => __( 'Parent Field of Interest', 'textdomain' ),
            'parent_item_colon' => __( 'Parent Field of Interest:', 'textdomain' ),
            'edit_item'         => __( 'Edit Field of Interest', 'textdomain' ),
            'update_item'       => __( 'Update Field of Interest', 'textdomain' ),
            'add_new_item'      => __( 'Add New Field of Interest', 'textdomain' ),
            'new_item_name'     => __( 'New Field of Interest Name', 'textdomain' ),
            'menu_name'         => __( 'Field of Interest', 'textdomain' ),
        );

        $field_of_interest = array(
            'hierarchical'      => true,
            'labels'            => $field_of_interest_in,
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
            'rewrite'           => array( 'slug' => 'field_of_interest' ),
        );

        register_taxonomy( 'field_of_interest', array( 'chef' ), $field_of_interest );



        // +++++++++++++++++++++++++++ Custom Taxonomy for Hospitality Staff Post type ++++++++++++++++++++++++++++++++++
        /* ================ Citizenship / Visa ================ */
        $hospitality_labels = array(
            'name'              => _x( 'Staff Citizenship / Visa', 'taxonomy general name', 'textdomain' ),
            'singular_name'     => _x( 'Staff Citizenship / Visa', 'taxonomy singular name', 'textdomain' ),
            'search_items'      => __( 'Search Staff Citizenship / Visa', 'textdomain' ),
            'all_items'         => __( 'All Staff Citizenship / Visa', 'textdomain' ),
            'parent_item'       => __( 'Parent Staff Citizenship / Visa', 'textdomain' ),
            'parent_item_colon' => __( 'Parent Staff Citizenship / Visa:', 'textdomain' ),
            'edit_item'         => __( 'Edit Staff Citizenship / Visa', 'textdomain' ),
            'update_item'       => __( 'Update Staff Citizenship / Visa', 'textdomain' ),
            'add_new_item'      => __( 'Add New Staff Citizenship / Visa', 'textdomain' ),
            'new_item_name'     => __( 'New Staff Citizenship / Visa Name', 'textdomain' ),
            'menu_name'         => __( 'Staff Citizenship / Visa', 'textdomain' ),
        );

        $hospitality_args = array(
            'hierarchical'      => true,
            'labels'            => $hospitality_labels,
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
            'rewrite'           => array( 'slug' => 'staff_citizenship' ),
        );

        register_taxonomy( 'staff_citizenship', array( 'hospitality_staff' ), $hospitality_args );


        /* ================ Type of Recruitment ================ */

        $hospitality_recruitment = array(
            'name'              => _x( 'Staff Type of Recruitment', 'taxonomy general name', 'textdomain' ),
            'singular_name'     => _x( 'Staff Type of Recruitment', 'taxonomy singular name', 'textdomain' ),
            'search_items'      => __( 'Search Staff Type of Recruitment', 'textdomain' ),
            'all_items'         => __( 'All Staff Type of Recruitment', 'textdomain' ),
            'parent_item'       => __( 'Parent Staff Type of Recruitment', 'textdomain' ),
            'parent_item_colon' => __( 'Parent Staff Type of Recruitment:', 'textdomain' ),
            'edit_item'         => __( 'Edit Staff Type of Recruitment', 'textdomain' ),
            'update_item'       => __( 'Update Staff Type of Recruitment', 'textdomain' ),
            'add_new_item'      => __( 'Add New Staff Type of Recruitment', 'textdomain' ),
            'new_item_name'     => __( 'New Staff Type of Recruitment Name', 'textdomain' ),
            'menu_name'         => __( 'Staff Type of Recruitment', 'textdomain' ),
        );

        $hospitality_type_of_recruitment = array(
            'hierarchical'      => true,
            'labels'            => $hospitality_recruitment,
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
            'rewrite'           => array( 'slug' => 'staff_type_of_recruitment' ),
        );

        register_taxonomy( 'staff_type_of_recruitment', array( 'hospitality_staff' ), $hospitality_type_of_recruitment );
        

        /* ================ Hospitality Position Type ================ */
        $hospitality_chef_skills = array(
            'name'              => _x( 'Staff Position Type', 'taxonomy general name', 'textdomain' ),
            'singular_name'     => _x( 'Staff Position Type', 'taxonomy singular name', 'textdomain' ),
            'search_items'      => __( 'Search Staff Position Type', 'textdomain' ),
            'all_items'         => __( 'All Staff Position Type', 'textdomain' ),
            'parent_item'       => __( 'Parent Staff Position Type', 'textdomain' ),
            'parent_item_colon' => __( 'Parent Staff Position Type:', 'textdomain' ),
            'edit_item'         => __( 'Edit Staff Position Type', 'textdomain' ),
            'update_item'       => __( 'Update Staff Position Type', 'textdomain' ),
            'add_new_item'      => __( 'Add New Staff Position Type', 'textdomain' ),
            'new_item_name'     => __( 'New Staff Position Type Name', 'textdomain' ),
            'menu_name'         => __( 'Staff Position Type', 'textdomain' ),
        );

        $hospitality_position_type = array(
            'hierarchical'      => true,
            'labels'            => $hospitality_chef_skills,
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
            'rewrite'           => array( 'slug' => 'position_type' ),
        );

        register_taxonomy( 'position_type', array( 'hospitality_staff' ), $hospitality_position_type );


        /* ================ Area you reside in ================ */

        $staff_residency_in = array(
            'name'              => _x( 'Staff Residency', 'taxonomy general name', 'textdomain' ),
            'singular_name'     => _x( 'Staff Residency', 'taxonomy singular name', 'textdomain' ),
            'search_items'      => __( 'Search Staff Residency', 'textdomain' ),
            'all_items'         => __( 'All Staff Residency', 'textdomain' ),
            'parent_item'       => __( 'Parent Staff Residency', 'textdomain' ),
            'parent_item_colon' => __( 'Parent Staff Residency:', 'textdomain' ),
            'edit_item'         => __( 'Edit Staff Residency', 'textdomain' ),
            'update_item'       => __( 'Update Staff Residency', 'textdomain' ),
            'add_new_item'      => __( 'Add New Staff Residency', 'textdomain' ),
            'new_item_name'     => __( 'New Staff Residency Name', 'textdomain' ),
            'menu_name'         => __( 'Staff Residency', 'textdomain' ),
        );

        $staff_residency = array(
            'hierarchical'      => true,
            'labels'            => $staff_residency_in,
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
            'rewrite'           => array( 'slug' => 'staff_residency' ),
        );

        // register_taxonomy( 'staff_residency', array( 'hospitality_staff' ), $staff_residency );


        // +++++++++++++++++++++++++++ Custom Taxonomy for Recruiter Post type ++++++++++++++++++++++++++++++++++
        /* ================ Citizenship / Visa ================ */
        $labels = array(
            'name'              => _x( 'Locations', 'taxonomy general name', 'textdomain' ),
            'singular_name'     => _x( 'Location', 'taxonomy singular name', 'textdomain' ),
            'search_items'      => __( 'Search Locations', 'textdomain' ),
            'all_items'         => __( 'All Locations', 'textdomain' ),
            'parent_item'       => __( 'Parent Location', 'textdomain' ),
            'parent_item_colon' => __( 'Parent Location:', 'textdomain' ),
            'edit_item'         => __( 'Edit Location', 'textdomain' ),
            'update_item'       => __( 'Update Location', 'textdomain' ),
            'add_new_item'      => __( 'Add New Location', 'textdomain' ),
            'new_item_name'     => __( 'New Location Name', 'textdomain' ),
            'menu_name'         => __( 'Location', 'textdomain' ),
        );

        $args = array(
            'hierarchical'      => true,
            'labels'            => $labels,
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
            'rewrite'           => array( 'slug' => 'recruiter_location' ),
        );

        register_taxonomy( 'recruiter_location', array( 'recruiter' ), $args );
    }
/* ================================================================================================ */
// +++++++++++++++++++++++++++ Custom Taxonomy for job Post type ++++++++++++++++++++++++++++++++++ //
/* ================================================================================================ */

        /* ================ Job location ================ */
        $joblocation = array(
            'name'              => _x( 'Job Locations', 'taxonomy general name', 'textdomain' ),
            'singular_name'     => _x( 'Job Location', 'taxonomy singular name', 'textdomain' ),
            'search_items'      => __( 'Search Job Locations', 'textdomain' ),
            'all_items'         => __( 'All Job Locations', 'textdomain' ),
            'parent_item'       => __( 'Parent Job Location', 'textdomain' ),
            'parent_item_colon' => __( 'Parent Job Location:', 'textdomain' ),
            'edit_item'         => __( 'Edit Job Location', 'textdomain' ),
            'update_item'       => __( 'Update Job Location', 'textdomain' ),
            'add_new_item'      => __( 'Add New Job Location', 'textdomain' ),
            'new_item_name'     => __( 'New Job Location Name', 'textdomain' ),
            'menu_name'         => __( 'Job Location', 'textdomain' ),
        );

        $job_location = array(
            'hierarchical'      => true,
            'labels'            => $joblocation,
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
            'rewrite'           => array( 'slug' => 'job_location' ),
        );

        register_taxonomy( 'job_location', array( 'jobs' ), $job_location );

        /* ================ Job Type ================ */
        $jobtype = array(
            'name'              => _x( 'Job Openings', 'taxonomy general name', 'textdomain' ),
            'singular_name'     => _x( 'Job Opening', 'taxonomy singular name', 'textdomain' ),
            'search_items'      => __( 'Search Job Openings', 'textdomain' ),
            'all_items'         => __( 'All Job Openings', 'textdomain' ),
            'parent_item'       => __( 'Parent Job Opening', 'textdomain' ),
            'parent_item_colon' => __( 'Parent Job Opening:', 'textdomain' ),
            'edit_item'         => __( 'Edit Job Opening', 'textdomain' ),
            'update_item'       => __( 'Update Job Opening', 'textdomain' ),
            'add_new_item'      => __( 'Add New Job Opening', 'textdomain' ),
            'new_item_name'     => __( 'New Job Opening Name', 'textdomain' ),
            'menu_name'         => __( 'Job Opening', 'textdomain' ),
        );

        $job_type = array(
            'hierarchical'      => true,
            'labels'            => $jobtype,
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
            'rewrite'           => array( 'slug' => 'job_opening' ),
        );

        register_taxonomy( 'job_opening', array( 'jobs' ), $job_type );

        /* ================ Area you reside in ================ */

        $staff_residency_in = array(
            'name'              => _x( 'Residency', 'taxonomy general name', 'textdomain' ),
            'singular_name'     => _x( 'Residency', 'taxonomy singular name', 'textdomain' ),
            'search_items'      => __( 'Search Residency', 'textdomain' ),
            'all_items'         => __( 'All Residency', 'textdomain' ),
            'parent_item'       => __( 'Parent Residency', 'textdomain' ),
            'parent_item_colon' => __( 'Parent Residency:', 'textdomain' ),
            'edit_item'         => __( 'Edit Residency', 'textdomain' ),
            'update_item'       => __( 'Update Residency', 'textdomain' ),
            'add_new_item'      => __( 'Add New Residency', 'textdomain' ),
            'new_item_name'     => __( 'New Residency Name', 'textdomain' ),
            'menu_name'         => __( 'Residency', 'textdomain' ),
        );

        $staff_residency = array(
            'hierarchical'      => true,
            'labels'            => $staff_residency_in,
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
            'rewrite'           => array( 'slug' => 'location_residency' ),
        );

        register_taxonomy( 'location_residency', array( 'locations', 'jobs', 'hospitality_staff', 'recruiter', 'chef', 'rosters', 'timesheet', 'employee_timesheet' ), $staff_residency );

        /* ================ Client location ================ */
        $clienttype = array(
            'name'              => _x( 'client location', 'taxonomy general name', 'textdomain' ),
            'singular_name'     => _x( 'client location', 'taxonomy singular name', 'textdomain' ),
            'search_items'      => __( 'Search client location', 'textdomain' ),
            'all_items'         => __( 'All client location', 'textdomain' ),
            'parent_item'       => __( 'Parent client location', 'textdomain' ),
            'parent_item_colon' => __( 'Parent client location:', 'textdomain' ),
            'edit_item'         => __( 'Edit client location', 'textdomain' ),
            'update_item'       => __( 'Update client location', 'textdomain' ),
            'add_new_item'      => __( 'Add New client location', 'textdomain' ),
            'new_item_name'     => __( 'New client location Name', 'textdomain' ),
            'menu_name'         => __( 'client location', 'textdomain' ),
        );

        $client_type = array(
            'hierarchical'      => true,
            'labels'            => $clienttype,
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
            'rewrite'           => array( 'slug' => 'client_location' ),
        );

        register_taxonomy( 'client_location', array( 'employee_timesheet' ), $client_type );