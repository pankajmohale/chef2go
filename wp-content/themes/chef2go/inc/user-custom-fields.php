<?php 
/* 
* Add Extra fields in User Profile
*/

add_action( 'show_user_profile', 'my_show_extra_profile_fields' );
add_action( 'edit_user_profile', 'my_show_extra_profile_fields' );

function my_show_extra_profile_fields( $user ) { ?>
	
	<h3>Extra profile information</h3>

	<table class="form-table">

		<tr>
			<th><label for="custom_address_full">Custom Full Address</label></th>
			<td>
				<input type="text" name="custom_address_full" id="custom_address_full" value="<?php echo esc_attr( get_the_author_meta( 'custom_address_full', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description">Please enter your full address.</span>
			</td>
		</tr>
		<tr>
			<th><label for="user_phone_number">Phone</label></th>
			<td>
				<input type="text" name="user_phone_number" id="user_phone_number" value="<?php echo esc_attr( get_the_author_meta( 'user_phone_number', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description">Please enter your Phone.</span>
			</td>
		</tr>	
		<!-- <tr>
			<th><label for="street_address">Address (Street Address)</label></th>
			<td>
				<input type="text" name="street_address" id="street_address" value="<?php echo esc_attr( get_the_author_meta( 'street_address', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description">Please enter your Street Address.</span>
			</td>
		</tr>	
		<tr>
			<th><label for="address_line_2">Address (Address Line 2)</label></th>
			<td>
				<input type="text" name="address_line_2" id="address_line_2" value="<?php echo esc_attr( get_the_author_meta( 'address_line_2', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description">Please enter your address Line 2.</span>
			</td>
		</tr>	
		<tr>
			<th><label for="address_city">Address (City)</label></th>
			<td>
				<input type="text" name="address_city" id="address_city" value="<?php echo esc_attr( get_the_author_meta( 'address_city', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description">Please enter your Address (City).</span>
			</td>
		</tr>	
		<tr>
			<th><label for="state_address">Address (State / Province)</label></th>
			<td>
				<input type="text" name="state_address" id="state_address" value="<?php echo esc_attr( get_the_author_meta( 'state_address', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description">Please enter your State / Province.</span>
			</td>
		</tr>	
		<tr>
			<th><label for="postal_code_address">Address (ZIP / Postal Code)</label></th>
			<td>
				<input type="text" name="postal_code_address" id="postal_code_address" value="<?php echo esc_attr( get_the_author_meta( 'postal_code_address', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description">Please enter your ZIP / Postal Code.</span>
			</td>
		</tr>	
		<tr>
			<th><label for="country_address">Address (Country)</label></th>
			<td>
				<input type="text" name="country_address" id="country_address" value="<?php echo esc_attr( get_the_author_meta( 'country_address', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description">Please enter your Address (Country).</span>
			</td>
		</tr> -->	
		<!-- <tr>
			<th><label for="freelance_hourly_rate">Freelance Hourly rate</label></th>
			<td>
				<input type="text" name="freelance_hourly_rate" id="freelance_hourly_rate" value="<?php echo esc_attr( get_the_author_meta( 'freelance_hourly_rate', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description">Please enter Freelance Hourly rate.</span>
			</td>
		</tr>	
		<tr>
			<th><label for="you_reside_in">Area you reside in</label></th>
			<td>
				<input type="text" name="you_reside_in" id="you_reside_in" value="<?php echo esc_attr( get_the_author_meta( 'you_reside_in', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description">Please enter your residency.</span>
			</td>
		</tr>	
		<tr>
			<th><label for="citizenship_visa">Citizenship / Visa</label></th>
			<td>
				<input type="text" name="citizenship_visa" id="citizenship_visa" value="<?php echo esc_attr( get_the_author_meta( 'citizenship_visa', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description">Please enter your Citizenship / Visa.</span>
			</td>
		</tr>	
		<tr>
			<th><label for="recruitment_type">Type of Recruitment</label></th>
			<td>
				<input type="text" name="recruitment_type" id="recruitment_type" value="<?php echo esc_attr( get_the_author_meta( 'recruitment_type', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description">Please enter your Type of Recruitment.</span>
			</td>
		</tr>	
		<tr>
			<th><label for="chef_skills">Chef Skills</label></th>
			<td>
				<input type="text" name="chef_skills" id="chef_skills" value="<?php echo esc_attr( get_the_author_meta( 'chef_skills', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description">Please enter your Chef Skills.</span>
			</td>
		</tr>	
		<tr>
			<th><label for="field_of_interest">Field of Interest</label></th>
			<td>
				<input type="text" name="field_of_interest" id="field_of_interest" value="<?php echo esc_attr( get_the_author_meta( 'field_of_interest', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description">Please enter your Field of Interest.</span>
			</td>
		</tr>	
		<tr>
			<th><label for="work_in_remote_locations">I can work in Remote locations</label></th>
			<td>
				<input type="text" name="work_in_remote_locations" id="work_in_remote_locations" value="<?php echo esc_attr( get_the_author_meta( 'work_in_remote_locations', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description">Please enter your Remote locations.</span>
			</td>
		</tr>	
		<tr>
			<th><label for="specialising_in">Specialising in</label></th>
			<td>
				<input type="text" name="specialising_in" id="specialising_in" value="<?php echo esc_attr( get_the_author_meta( 'specialising_in', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description">Please enter your Specialising in.</span>
			</td>
		</tr>	
		<tr>
			<th><label for="work_history">Work History</label></th>
			<td>
				<input type="text" name="work_history" id="work_history" value="<?php echo esc_attr( get_the_author_meta( 'work_history', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description">Please enter your Work History.</span>
			</td>
		</tr>	
		<tr>
			<th><label for="new_resume">Upload a new Resume</label></th>
			<td>
				<input type="text" name="new_resume" id="new_resume" value="<?php echo esc_attr( get_the_author_meta( 'new_resume', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description">Please enter your Upload a new Resume.</span>
			</td>
		</tr>	
		<tr>
			<th><label for="profile_photo">Add a profile Photo</label></th>
			<td>
				<input type="text" name="profile_photo" id="profile_photo" value="<?php echo esc_attr( get_the_author_meta( 'profile_photo', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description">Please enter your Profile Photo.</span>
			</td>
		</tr> -->
	</table>
	
	<!-- <h3>Add Document</h3>
	<table class="form-table">
		<tr>
			<th><label for="document_upload">Upload New Document:</label></th>
			<td>
				<input type="text" name="document_upload" id="document_upload" value="<?php echo esc_attr( get_the_author_meta( 'document_upload', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description">Please enter your Profile Photo.</span>
			</td>
		</tr>
	</table> -->
<?php }
add_action( 'personal_options_update', 'my_save_extra_profile_fields' );
add_action( 'edit_user_profile_update', 'my_save_extra_profile_fields' );
function my_save_extra_profile_fields( $user_id ) {

	if ( !current_user_can( 'edit_user', $user_id ) )
		return false;

	/* Copy and paste this line for additional fields. Make sure to change 'twitter' to the field ID. */
	update_usermeta( $user_id, 'custom_address_full', $_POST['custom_address_full'] );
	update_usermeta( $user_id, 'user_phone_number', $_POST['user_phone_number'] );
	//update_usermeta( $user_id, 'document_upload', $_POST['document_upload'] );
	/*update_usermeta( $user_id, 'street_address', $_POST['street_address'] );
	update_usermeta( $user_id, 'address_line_2', $_POST['address_line_2'] );
	update_usermeta( $user_id, 'address_city', $_POST['address_city'] );
	update_usermeta( $user_id, 'state_address', $_POST['state_address'] );
	update_usermeta( $user_id, 'postal_code_address', $_POST['postal_code_address'] );
	update_usermeta( $user_id, 'country_address', $_POST['country_address'] );
	update_usermeta( $user_id, 'freelance_hourly_rate', $_POST['freelance_hourly_rate'] );
	update_usermeta( $user_id, 'you_reside_in', $_POST['you_reside_in'] );
	update_usermeta( $user_id, 'citizenship_visa', $_POST['citizenship_visa'] );
	update_usermeta( $user_id, 'recruitment_type', $_POST['recruitment_type'] );
	update_usermeta( $user_id, 'chef_skills', $_POST['chef_skills'] );
	update_usermeta( $user_id, 'field_of_interest', $_POST['field_of_interest'] );
	update_usermeta( $user_id, 'work_in_remote_locations', $_POST['work_in_remote_locations'] );
	update_usermeta( $user_id, 'specialising_in', $_POST['specialising_in'] );
	update_usermeta( $user_id, 'work_history', $_POST['work_history'] );
	update_usermeta( $user_id, 'new_resume', $_POST['new_resume'] );
	update_usermeta( $user_id, 'profile_photo', $_POST['profile_photo'] );*/
}