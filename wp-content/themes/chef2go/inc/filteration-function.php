<?php 
/* filteration-function.php */

/* ---------------------------- Show Result of recruiter My area module -------------------------------- */
add_action( 'wp_ajax_nopriv_recruiter_full_time_chef_listing', 'recruiter_full_time_chef_listing' );
add_action( 'wp_ajax_recruiter_full_time_chef_listing', 'recruiter_full_time_chef_listing' );
function recruiter_full_time_chef_listing(){
		$full_time_chef_level = $_POST['full_time_chef_level'];
		
		if ($full_time_chef_level == 'all') { 
			$full_time_chef_args = array(
				'post_type' => 'chef',
				'posts_per_page' => -1,			
				'tax_query' => array(
					array(
						'taxonomy' => 'type_of_recruitment',
						'field'    => 'slug',
						'terms'    => 'full-time-positions',
					)
				),
				'post_status' => array( 'publish' ),
			);

		} elseif ($full_time_chef_level != 'all') {

			$full_time_chef_args = array(
				'post_type' => 'chef',
				'posts_per_page' => -1,
				'tax_query' => array(
					'relation' => 'AND',
					array(
						'taxonomy' => 'type_of_recruitment',
						'field'    => 'slug',
						'terms'    => 'full-time-positions',
					),
					array(
						'taxonomy' => 'field_of_interest',
						'field'    => 'slug',
						'terms'    => $full_time_chef_level,
					),
				),
				'post_status' => array( 'publish' ),
			);
		}			
			// the query
			$the_query = new WP_Query( $full_time_chef_args ); ?>

			<?php if ( $the_query->have_posts() ) : ?>

				<!-- pagination here -->

				<!-- the loop -->
				<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

					<?php
						get_template_part( 'template-parts/recruiterchefs', 'seekinglist' );
					?>
				<?php endwhile; ?>
				<!-- end of the loop -->

				<!-- pagination here -->

				<?php wp_reset_postdata(); ?>

			<?php else : ?>
				<p><?php _e( 'Sorry, Chefs Seeking for Full Time Work are not found.' ); ?></p>
			<?php endif;

		echo '';
		die();
}


/* ---------------------------- Show Result of recruiter My area module -------------------------------- */
add_action( 'wp_ajax_nopriv_recruiter_part_time_chef_listing', 'recruiter_part_time_chef_listing' );
add_action( 'wp_ajax_recruiter_part_time_chef_listing', 'recruiter_part_time_chef_listing' );
function recruiter_part_time_chef_listing(){
		$part_time_chef_level = $_POST['part_time_chef_level_'];

		if ($part_time_chef_level == 'all') { 
			$args = array(
				'post_type' => 'chef',
				'posts_per_page' => -1,
				'tax_query' => array(
					array(
						'taxonomy' => 'type_of_recruitment',
						'field'    => 'slug',
						'terms'    => 'part-time-casual',
					)
				),
				'post_status' => array( 'publish' ),
			);
			
		} else {
			$args = array(
				'post_type' => 'chef',
				'posts_per_page' => -1,
				'post_status' => array( 'publish' ),
				'tax_query' => array(
					'relation' => 'AND',
					array(
						'taxonomy' => 'type_of_recruitment',
						'field'    => 'slug',
						'terms'    => 'part-time-casual',
					),
					array(
						'taxonomy' => 'field_of_interest',
						'field'    => 'slug',
						'terms'    => $part_time_chef_level,
					),
				),
			);
			// the query
			$the_query = new WP_Query( $args ); ?>

			<?php if ( $the_query->have_posts() ) : ?>

				<!-- pagination here -->

				<!-- the loop -->
				<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
					<?php 
						get_template_part( 'template-parts/recruiterchefs', 'seekinglist' );
					?>
				<?php endwhile; ?>
				<!-- end of the loop -->

				<!-- pagination here -->

				<?php wp_reset_postdata(); ?>

			<?php else : ?>
				<p><?php _e( 'Sorry, Chefs Seeking for Part Time Work are not found.' ); ?></p>
			<?php endif;			
		}

		echo '';
		die();
}



/* ============================================== function for recruiter Full time Staff listing ========================================== */

/* ---------------------------- Show Result of recruiter My area module -------------------------------- */
add_action( 'wp_ajax_nopriv_recruiter_full_time_staff_listing', 'recruiter_full_time_staff_listing' );
add_action( 'wp_ajax_recruiter_full_time_staff_listing', 'recruiter_full_time_staff_listing' );
function recruiter_full_time_staff_listing(){
		$full_time_staff_level = $_POST['full_time_staff_level'];
		
		if ($full_time_staff_level == 'all') { 
			$full_time_staff = array(
				'post_type' => 'hospitality_staff',
				'posts_per_page' => -1,
				'tax_query' => array(
					array(
						'taxonomy' => 'staff_type_of_recruitment',
						'field'    => 'slug',
						'terms'    => 'full-time-positions',
					),
				),
				'post_status' => array( 'publish' ),
			);
						
		} else {
			$full_time_staff = array(
				'post_type' => 'hospitality_staff',
				'posts_per_page' => -1,
				'tax_query' => array(
					'relation' => 'AND',
					array(
						'taxonomy' => 'staff_type_of_recruitment',
						'field'    => 'slug',
						'terms'    => 'full-time-positions',
					),
					array(
						'taxonomy' => 'position_type',
						'field'    => 'slug',
						'terms'    => $full_time_staff_level,
					),
				),
				'post_status' => array( 'publish' ),
			);
			// the query
			$the_query = new WP_Query( $full_time_staff ); ?>

			<?php if ( $the_query->have_posts() ) : ?>

				<!-- pagination here -->

				<!-- the loop -->
				<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
					<?php 
						get_template_part( 'template-parts/recruiterstaff', 'seekinglist' );
					?>
				<?php endwhile; ?>
				<!-- end of the loop -->

				<!-- pagination here -->

				<?php wp_reset_postdata(); ?>

			<?php else : ?>
				<p><?php _e( 'Sorry, no staff matched your criteria.' ); ?></p>
			<?php endif;			
		}
		
		echo '';
		die();
}


/* ==================== function for recruiter Part time Staff listing ========================= */

function staff_part_time_work_search_filter( $atts ) {
	//return "";
	// echo $atts['post_type'];
	?>
	<div class="recruiter-filter-option">
		
		<div class="recruiter-filter-option-inner">
			<div class="form-group">
		      <label for="recruiter-level">Filter by Level:</label>
				<?php 
					$terms = get_terms( 'position_type', array(
					    'orderby'    => 'count',
					    'hide_empty' => 0,
					) );
					if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
					    echo '<select class="form-control part_time_staff_recruitor level_select" id="recruiter-level">';
					        echo '<option value="all">All</option>';
					    foreach ( $terms as $term ) {
					        echo '<option value='.$term->slug.'>' . $term->name . '</option>';
					    }
					    echo '</select>';
					}
				?>
			</div>				
		</div>

		<div class="recruiter_part_time_staff_listing">
			<?php 
				$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
				$args = array(
					'post_type' => 'hospitality_staff',
					'posts_per_page' => 5,
            		'paged' => $paged,
					'tax_query' => array(
						array(
							'taxonomy' => 'staff_type_of_recruitment',
							'field'    => 'slug',
							'terms'    => 'part-time-casual',
						),
					),
				);
				// the query
				$the_query = new WP_Query( $args ); ?>

				<?php if ( $the_query->have_posts() ) : ?>

					<!-- pagination here -->

					<!-- the loop -->
					<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
						<?php 
							get_template_part( 'template-parts/recruiterstaff', 'seekinglist' );
						?>
					<?php endwhile; ?>
					<!-- end of the loop -->

					<?php if (function_exists("pagination")) {
			            pagination($the_query->max_num_pages);
			        } ?>
					<!-- pagination here -->

					<?php wp_reset_postdata(); ?>

				<?php else : ?>
					<p><?php _e( 'Sorry, no staff matched your criteria.' ); ?></p>
				<?php endif; ?>
		</div>

	</div>

<?php
}
add_shortcode( 'part_time_staff', 'staff_part_time_work_search_filter' );


/* ---------------------------- Show Result of recruiter My area module -------------------------------- */
add_action( 'wp_ajax_nopriv_recruiter_part_time_staff_listing', 'recruiter_part_time_staff_listing' );
add_action( 'wp_ajax_recruiter_part_time_staff_listing', 'recruiter_part_time_staff_listing' );
function recruiter_part_time_staff_listing(){
		$part_time_staff_level = $_POST['part_time_staff_level'];
		
		if ($part_time_staff_level == 'all') { 
			$args = array(
				'post_type' => 'hospitality_staff',
				'posts_per_page' => -1,
				'tax_query' => array(
					array(
						'taxonomy' => 'staff_type_of_recruitment',
						'field'    => 'slug',
						'terms'    => 'part-time-casual',
					),
				),
			);
			// the query
			$the_query = new WP_Query( $args ); ?>

			<?php if ( $the_query->have_posts() ) : ?>

				<!-- pagination here -->

				<!-- the loop -->
				<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

					<?php
						get_template_part( 'template-parts/recruiterstaff', 'seekinglist' );
					?>
				<?php endwhile; ?>
				<!-- end of the loop -->

				<!-- pagination here -->

				<?php wp_reset_postdata(); ?>

			<?php else : ?>
				<p><?php _e( 'Sorry, no staff matched your criteria.' ); ?></p>
			<?php endif;
			
		} else {

			$args = array(
				'post_type' => 'hospitality_staff',
				'posts_per_page' => -1,
				'tax_query' => array(
					'relation' => 'AND',
					array(
						'taxonomy' => 'staff_type_of_recruitment',
						'field'    => 'slug',
						'terms'    => 'part-time-casual',
					),
					array(
						'taxonomy' => 'position_type',
						'field'    => 'slug',
						'terms'    => $part_time_staff_level,
					),
				),
			);
			// the query
			$the_query = new WP_Query( $args ); ?>

			<?php if ( $the_query->have_posts() ) : ?>

				<!-- pagination here -->

				<!-- the loop -->
				<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
					<?php 
						get_template_part( 'template-parts/recruiterstaff', 'seekinglist' );
					?>
				<?php endwhile; ?>
				<!-- end of the loop -->

				<!-- pagination here -->

				<?php wp_reset_postdata(); ?>

			<?php else : ?>
				<p><?php _e( 'Sorry, no staff matched your criteria.' ); ?></p>
			<?php endif;			
		}
		
		echo '';
		die();
}