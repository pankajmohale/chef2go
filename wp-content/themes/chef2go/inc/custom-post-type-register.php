<?php
/* ************************ Post Type ********************** */
add_action( 'init', 'custom_post_register_init' );
/**
 * Register a Chef post type.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function custom_post_register_init() {
    $labels = array(
        'name'               => _x( 'Chefs', 'post type general name', 'your-plugin-textdomain' ),
        'singular_name'      => _x( 'Chef', 'post type singular name', 'your-plugin-textdomain' ),
        'menu_name'          => _x( 'Chefs', 'admin menu', 'your-plugin-textdomain' ),
        'name_admin_bar'     => _x( 'Chef', 'add new on admin bar', 'your-plugin-textdomain' ),
        'add_new'            => _x( 'Add New', 'Chef', 'your-plugin-textdomain' ),
        'add_new_item'       => __( 'Add New Chef', 'your-plugin-textdomain' ),
        'new_item'           => __( 'New Chef', 'your-plugin-textdomain' ),
        'edit_item'          => __( 'Edit Chef', 'your-plugin-textdomain' ),
        'view_item'          => __( 'View Chef', 'your-plugin-textdomain' ),
        'all_items'          => __( 'All Chefs', 'your-plugin-textdomain' ),
        'search_items'       => __( 'Search Chefs', 'your-plugin-textdomain' ),
        'parent_item_colon'  => __( 'Parent Chefs:', 'your-plugin-textdomain' ),
        'not_found'          => __( 'No Chefs found.', 'your-plugin-textdomain' ),
        'not_found_in_trash' => __( 'No Chefs found in Trash.', 'your-plugin-textdomain' )
    );

    $args = array(
        'labels'             => $labels,
        'description'        => __( 'Description.', 'your-plugin-textdomain' ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'chef' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments','revisions','page-attributes','post-formats' )
    );

    register_post_type( 'chef', $args );

    // For Recruiters Post Type
    
    $recruiters = array(
        'name'               => _x( 'Recruiters', 'post type general name', 'your-plugin-textdomain' ),
        'singular_name'      => _x( 'Recruiter', 'post type singular name', 'your-plugin-textdomain' ),
        'menu_name'          => _x( 'Recruiters', 'admin menu', 'your-plugin-textdomain' ),
        'name_admin_bar'     => _x( 'Recruiter', 'add new on admin bar', 'your-plugin-textdomain' ),
        'add_new'            => _x( 'Add New', 'Recruiter', 'your-plugin-textdomain' ),
        'add_new_item'       => __( 'Add New Recruiter', 'your-plugin-textdomain' ),
        'new_item'           => __( 'New Recruiter', 'your-plugin-textdomain' ),
        'edit_item'          => __( 'Edit Recruiter', 'your-plugin-textdomain' ),
        'view_item'          => __( 'View Recruiter', 'your-plugin-textdomain' ),
        'all_items'          => __( 'All Recruiters', 'your-plugin-textdomain' ),
        'search_items'       => __( 'Search Recruiters', 'your-plugin-textdomain' ),
        'parent_item_colon'  => __( 'Parent Recruiters:', 'your-plugin-textdomain' ),
        'not_found'          => __( 'No Recruiters found.', 'your-plugin-textdomain' ),
        'not_found_in_trash' => __( 'No Recruiters found in Trash.', 'your-plugin-textdomain' )
    );

    $recruiter = array(
        'labels'             => $recruiters,
        'description'        => __( 'Description.', 'your-plugin-textdomain' ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'recruiter' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments','revisions','page-attributes','post-formats' )
    );

    register_post_type( 'recruiter', $recruiter );

    // For hospital Post Type

    $hospitality = array(
        'name'               => _x( 'Hospitality Staff', 'post type general name', 'your-plugin-textdomain' ),
        'singular_name'      => _x( 'Hospitality Staff', 'post type singular name', 'your-plugin-textdomain' ),
        'menu_name'          => _x( 'Hospitality Staff', 'admin menu', 'your-plugin-textdomain' ),
        'name_admin_bar'     => _x( 'Hospitality Staff', 'add new on admin bar', 'your-plugin-textdomain' ),
        'add_new'            => _x( 'Add New', 'Hospitality Staff', 'your-plugin-textdomain' ),
        'add_new_item'       => __( 'Add New Hospitality Staff', 'your-plugin-textdomain' ),
        'new_item'           => __( 'New Hospitality Staff', 'your-plugin-textdomain' ),
        'edit_item'          => __( 'Edit Hospitality Staff', 'your-plugin-textdomain' ),
        'view_item'          => __( 'View Hospitality Staff', 'your-plugin-textdomain' ),
        'all_items'          => __( 'All Hospitality Staff', 'your-plugin-textdomain' ),
        'search_items'       => __( 'Search Hospitality Staff', 'your-plugin-textdomain' ),
        'parent_item_colon'  => __( 'Parent Hospitality Staff:', 'your-plugin-textdomain' ),
        'not_found'          => __( 'No Hospitality Staff found.', 'your-plugin-textdomain' ),
        'not_found_in_trash' => __( 'No Hospitality Staff found in Trash.', 'your-plugin-textdomain' )
    );

    $hospitality_staff = array(
        'labels'             => $hospitality,
        'description'        => __( 'Hospitality Staff Post type.', 'your-plugin-textdomain' ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'hospitality_staff' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments','revisions','page-attributes','post-formats' )
    );

    register_post_type( 'hospitality_staff', $hospitality_staff ); 

    $labels = array(
        'name'               => _x( 'Jobs', 'post type general name', 'your-plugin-textdomain' ),
        'singular_name'      => _x( 'Job', 'post type singular name', 'your-plugin-textdomain' ),
        'menu_name'          => _x( 'Jobs', 'admin menu', 'your-plugin-textdomain' ),
        'name_admin_bar'     => _x( 'Job', 'add new on admin bar', 'your-plugin-textdomain' ),
        'add_new'            => _x( 'Add New', 'job', 'your-plugin-textdomain' ),
        'add_new_item'       => __( 'Add New Job', 'your-plugin-textdomain' ),
        'new_item'           => __( 'New Job', 'your-plugin-textdomain' ),
        'edit_item'          => __( 'Edit Job', 'your-plugin-textdomain' ),
        'view_item'          => __( 'View Job', 'your-plugin-textdomain' ),
        'all_items'          => __( 'All Jobs', 'your-plugin-textdomain' ),
        'search_items'       => __( 'Search Jobs', 'your-plugin-textdomain' ),
        'parent_item_colon'  => __( 'Parent Jobs:', 'your-plugin-textdomain' ),
        'not_found'          => __( 'No jobs found.', 'your-plugin-textdomain' ),
        'not_found_in_trash' => __( 'No jobs found in Trash.', 'your-plugin-textdomain' )
    );

    $args = array(
        'labels'             => $labels,
        'description'        => __( 'Description.', 'your-plugin-textdomain' ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'jobs' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
    );

    register_post_type( 'jobs', $args );

    $labels = array(
        'name'               => _x( 'Locations', 'post type general name', 'your-plugin-textdomain' ),
        'singular_name'      => _x( 'Location', 'post type singular name', 'your-plugin-textdomain' ),
        'menu_name'          => _x( 'Locations', 'admin menu', 'your-plugin-textdomain' ),
        'name_admin_bar'     => _x( 'Location', 'add new on admin bar', 'your-plugin-textdomain' ),
        'add_new'            => _x( 'Add New', 'Location', 'your-plugin-textdomain' ),
        'add_new_item'       => __( 'Add New Location', 'your-plugin-textdomain' ),
        'new_item'           => __( 'New Location', 'your-plugin-textdomain' ),
        'edit_item'          => __( 'Edit Location', 'your-plugin-textdomain' ),
        'view_item'          => __( 'View Location', 'your-plugin-textdomain' ),
        'all_items'          => __( 'All Locations', 'your-plugin-textdomain' ),
        'search_items'       => __( 'Search Locations', 'your-plugin-textdomain' ),
        'parent_item_colon'  => __( 'Parent Locations:', 'your-plugin-textdomain' ),
        'not_found'          => __( 'No locations found.', 'your-plugin-textdomain' ),
        'not_found_in_trash' => __( 'No locations found in Trash.', 'your-plugin-textdomain' )
    );

    $args = array(
        'labels'             => $labels,
        'description'        => __( 'Description.', 'your-plugin-textdomain' ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'locations' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title' )
    );

    register_post_type( 'locations', $args );
    
    // Roster post type register
    $rosterlabels = array(
        'name'               => _x( 'Rosters', 'post type general name', 'your-plugin-textdomain' ),
        'singular_name'      => _x( 'Roster', 'post type singular name', 'your-plugin-textdomain' ),
        'menu_name'          => _x( 'Rosters', 'admin menu', 'your-plugin-textdomain' ),
        'name_admin_bar'     => _x( 'Roster', 'add new on admin bar', 'your-plugin-textdomain' ),
        'add_new'            => _x( 'Add New', 'Roster', 'your-plugin-textdomain' ),
        'add_new_item'       => __( 'Add New Roster', 'your-plugin-textdomain' ),
        'new_item'           => __( 'New Roster', 'your-plugin-textdomain' ),
        'edit_item'          => __( 'Edit Roster', 'your-plugin-textdomain' ),
        'view_item'          => __( 'View Roster', 'your-plugin-textdomain' ),
        'all_items'          => __( 'All Rosters', 'your-plugin-textdomain' ),
        'search_items'       => __( 'Search Rosters', 'your-plugin-textdomain' ),
        'parent_item_colon'  => __( 'Parent Rosters:', 'your-plugin-textdomain' ),
        'not_found'          => __( 'No rosters found.', 'your-plugin-textdomain' ),
        'not_found_in_trash' => __( 'No rosters found in Trash.', 'your-plugin-textdomain' )
    );

    $rosterargs = array(
        'labels'             => $rosterlabels,
        'description'        => __( 'Description.', 'your-plugin-textdomain' ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'rosters' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
    );

    register_post_type( 'rosters', $rosterargs );

    // Timesheet post type register
    $rosterlabels = array(
        'name'               => _x( 'Timesheets', 'post type general name', 'your-plugin-textdomain' ),
        'singular_name'      => _x( 'Timesheet', 'post type singular name', 'your-plugin-textdomain' ),
        'menu_name'          => _x( 'Timesheets', 'admin menu', 'your-plugin-textdomain' ),
        'name_admin_bar'     => _x( 'Timesheet', 'add new on admin bar', 'your-plugin-textdomain' ),
        'add_new'            => _x( 'Add New', 'Timesheet', 'your-plugin-textdomain' ),
        'add_new_item'       => __( 'Add New Timesheet', 'your-plugin-textdomain' ),
        'new_item'           => __( 'New Timesheet', 'your-plugin-textdomain' ),
        'edit_item'          => __( 'Edit Timesheet', 'your-plugin-textdomain' ),
        'view_item'          => __( 'View Timesheet', 'your-plugin-textdomain' ),
        'all_items'          => __( 'All Timesheets', 'your-plugin-textdomain' ),
        'search_items'       => __( 'Search Timesheets', 'your-plugin-textdomain' ),
        'parent_item_colon'  => __( 'Parent Timesheets:', 'your-plugin-textdomain' ),
        'not_found'          => __( 'No timesheets found.', 'your-plugin-textdomain' ),
        'not_found_in_trash' => __( 'No timesheets found in Trash.', 'your-plugin-textdomain' )
    );

    $rosterargs = array(
        'labels'             => $rosterlabels,
        'description'        => __( 'Description.', 'your-plugin-textdomain' ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'rosters' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'author' )
    );

    register_post_type( 'timesheet', $rosterargs );


    // Timesheet post type register
    $rosterlabels = array(
        'name'               => _x( 'Employee Timesheets', 'post type general name', 'your-plugin-textdomain' ),
        'singular_name'      => _x( 'Employee Timesheet', 'post type singular name', 'your-plugin-textdomain' ),
        'menu_name'          => _x( 'Employee Timesheets', 'admin menu', 'your-plugin-textdomain' ),
        'name_admin_bar'     => _x( 'Employee Timesheet', 'add new on admin bar', 'your-plugin-textdomain' ),
        'add_new'            => _x( 'Add New', 'Employee Timesheet', 'your-plugin-textdomain' ),
        'add_new_item'       => __( 'Add New Employee Timesheet', 'your-plugin-textdomain' ),
        'new_item'           => __( 'New Employee Timesheet', 'your-plugin-textdomain' ),
        'edit_item'          => __( 'Edit Employee Timesheet', 'your-plugin-textdomain' ),
        'view_item'          => __( 'View Employee Timesheet', 'your-plugin-textdomain' ),
        'all_items'          => __( 'All Employee Timesheets', 'your-plugin-textdomain' ),
        'search_items'       => __( 'Search Employee Timesheets', 'your-plugin-textdomain' ),
        'parent_item_colon'  => __( 'Parent Employee Timesheets:', 'your-plugin-textdomain' ),
        'not_found'          => __( 'No employee timesheets found.', 'your-plugin-textdomain' ),
        'not_found_in_trash' => __( 'No employee timesheets found in Trash.', 'your-plugin-textdomain' )
    );

    $rosterargs = array(
        'labels'             => $rosterlabels,
        'description'        => __( 'Description.', 'your-plugin-textdomain' ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'employee_timesheet' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'author' )
    );

    register_post_type( 'employee_timesheet', $rosterargs );

}