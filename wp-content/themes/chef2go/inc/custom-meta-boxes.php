<?php
/*
* Add new custom meta box for chef cpt
*/

/* Meta box */
function adding_chef_custom_meta_boxes() {
    add_meta_box( 'chef_meta_fields','Chef Information','chefs_metabox','chef','normal','default');
    add_meta_box( 'staff_meta_fields','Hospitality Staff Information','hospitality_metabox','hospitality_staff','normal','default');
    add_meta_box( 'recruiter_meta_fields','Recruiter Information','recruiter_metabox','recruiter','normal','default');
    add_meta_box( 'timesheet_metabox_fields','Timesheet Information','timesheet_metabox','timesheet','normal','default');
    add_meta_box( 'roster_meta_fields','Roster Information','roster_metabox','rosters','normal','default');
}
add_action( 'add_meta_boxes', 'adding_chef_custom_meta_boxes', 10, 2 );

function chefs_metabox() {
    global $post; 
    $_custom_address_full = get_post_meta($post->ID,'custom_address_full',true); 
    $mail_id = get_post_meta($post->ID,'mail_id',true); 
    $user_phone_number = get_post_meta($post->ID,'user_phone_number',true); 
    $freelance_hourly_rate = get_post_meta($post->ID,'freelance_hourly_rate',true); 
    $you_reside_in = get_post_meta($post->ID,'you_reside_in',true); 
    $citizenship_visa = get_post_meta($post->ID,'citizenship_visa',false); 
    $recruitment_type = get_post_meta($post->ID,'recruitment_type',false); 
    $chef_skills = get_post_meta($post->ID,'chef_skills',false); 
    $field_of_interest = get_post_meta($post->ID,'field_of_interest',true); 
    $work_in_remote_locations = get_post_meta($post->ID,'work_in_remote_locations',true); 
    $specialising_in = get_post_meta($post->ID,'specialising_in',false); 
    $work_history = get_post_meta($post->ID,'work_history',true); 
    $new_resume = get_post_meta($post->ID,'new_resume',true); 
    $profile_photo = get_post_meta($post->ID,'profile_photo',true);
    $freelance_position_details_document = get_post_meta($post->ID,'freelance_position_details_document',true);
    $freelance_position_engagement_letter = get_post_meta($post->ID,'freelance_position_engagement_letter',true);
    $freelance_position_flexibility_agreement_12_hour = get_post_meta($post->ID,'freelance_position_flexibility_agreement_12_hour',true);
    $freelance_position_flexibility_agreement_day_worker = get_post_meta($post->ID,'freelance_position_flexibility_agreement_day_worker',true);
    $freelance_position_independent_contractor_agreement = get_post_meta($post->ID,'freelance_position_independent_contractor_agreement',true);
    $employee_user_id = get_post_meta($post->ID,'employee_user_id',true);
    //print_r($citizenship_visa);
    ?>
    <table class="form-table">
		<tbody>
		<tr>
			<th><label for="custom_address_full">Custom Full Address</label></th>
			<td>
				<input type="text" name="custom_address_full" id="custom_address_full" value="<?php echo $_custom_address_full; ?>" class="regular-text" /><br />
				<span class="description">Please enter your full address.</span>
			</td>
		</tr>	
		<tr>
			<th><label for="mail_id">Email</label></th>
			<td>
				<input type="text" name="mail_id" id="mail_id" value="<?php echo $mail_id; ?>" class="regular-text" /><br />
				<span class="description">Please enter your Street Address.</span>
			</td>
		</tr>	
		<tr>
			<th><label for="user_phone_number">Phone</label></th>
			<td>
				<input type="text" name="user_phone_number" id="user_phone_number" value="<?php echo $user_phone_number; ?>" class="regular-text" /><br />
				<span class="description">Please enter your Phone.</span>
			</td>
		</tr>	
		<tr>
			<th><label for="freelance_hourly_rate">Freelance Hourly rate</label></th>
			<td>
				<input type="text" name="freelance_hourly_rate" id="freelance_hourly_rate" value="<?php echo $freelance_hourly_rate; ?>" class="regular-text" /><br />
				<span class="description">Please enter Freelance Hourly rate.</span>
			</td>
		</tr>	
		<!-- <tr>
			<th><label for="you_reside_in">Area you reside in</label></th>
			<td>
				<input type="text" name="you_reside_in" id="you_reside_in" value="<?php echo $you_reside_in; ?>" class="regular-text" /><br />
				<span class="description">Please enter your residency.</span>
			</td>
		</tr> -->	
		<tr>
			<th><label for="work_in_remote_locations">I can work in Remote locations</label></th>
			<td>
				<input type="text" name="work_in_remote_locations" id="work_in_remote_locations" value="<?php echo $work_in_remote_locations; ?>" class="regular-text" /><br />
				<span class="description">Please enter your Remote locations.</span>
			</td>
		</tr>	
		<!-- <tr>
			<th><label for="specialising_in">Specialising in</label></th>
			<td>
				<input type="text" name="specialising_in" id="specialising_in" value=" --><?php
				/*$specialising_count = count($specialising_in);
				$i = 0;
				foreach ($specialising_in as $specialising_in) { 
					if(++$i === $specialising_count) {
						echo $specialising_in; 						
					}else{
						echo $specialising_in .', '; 						
					}
				}*/ ?><!-- " class="regular-text" /><br />
				<span class="description">Please enter your Specialising in.</span>
			</td>
		</tr> -->	
		<!-- <tr>
			<th><label for="work_history">Work History</label></th>
			<td>
				<input type="text" name="work_history" id="work_history" value="<?php echo $work_history; ?>" class="regular-text" /><br />
				<span class="description">Please enter your Work History.</span>
			</td>
		</tr> -->	
		<tr>
			<th><label for="new_resume">Upload a new Resume</label></th>
			<td>
				<input type="text" name="new_resume" id="new_resume" value="<?php echo $new_resume; ?>" class="regular-text" /><br />
				<span class="description">Please enter your Upload a new Resume.</span>
			</td>
		</tr>
		<tr>
			<th><label for="freelance_position_details_document">Upload back the chefs details document</label></th>
			<td>
				<input type="text" name="freelance_position_details_document" id="freelance_position_details_document" value="<?php echo $freelance_position_details_document; ?>" class="regular-text" /><br />
			</td>
		</tr>	
		<tr>
			<th><label for="freelance_position_engagement_letter">Upload back the engagement letter document</label></th>
			<td>
				<input type="text" name="freelance_position_engagement_letter" id="freelance_position_engagement_letter" value="<?php echo $freelance_position_engagement_letter; ?>" class="regular-text" /><br />
			</td>
		</tr>
		<tr>
			<th><label for="freelance_position_flexibility_agreement_12_hour">Upload back the Flexibility agreement 12 hour shifts document</label></th>
			<td>
				<input type="text" name="freelance_position_flexibility_agreement_12_hour" id="freelance_position_flexibility_agreement_12_hour" value="<?php echo $freelance_position_flexibility_agreement_12_hour; ?>" class="regular-text" /><br />
			</td>
		</tr>	
		<tr>
			<th><label for="freelance_position_flexibility_agreement_day_worker">Upload back the flexibility agreement not for profit day worker documentt</label></th>
			<td>
				<input type="text" name="freelance_position_flexibility_agreement_day_worker" id="freelance_position_flexibility_agreement_day_worker" value="<?php echo $freelance_position_flexibility_agreement_day_worker; ?>" class="regular-text" /><br />
			</td>
		</tr>
		<tr>
			<th><label for="freelance_position_independent_contractor_agreement">Upload back the independent contractor agreement document</label></th>
			<td>
				<input type="text" name="freelance_position_independent_contractor_agreement" id="freelance_position_independent_contractor_agreement" value="<?php echo $freelance_position_independent_contractor_agreement; ?>" class="regular-text" /><br />
			</td>
		</tr>
		<tr>
			<th><label for="employee_user_id">User ID:</label></th>
			<td>
				<input type="text" name="employee_user_id" id="employee_user_id" value="<?php echo $employee_user_id; ?>" class="regular-text" readonly/><br />
			</td>
		</tr>
		</tbody>
	</table>
<?php }

//Hospitality Staff Meta box fields
function hospitality_metabox() {
    global $post; 
    $staff_address_full = get_post_meta($post->ID,'staff_address_full',true); 
    $staff_mail_id = get_post_meta($post->ID,'staff_mail_id',true); 
    $staff_phone_number = get_post_meta($post->ID,'staff_phone_number',true); 
    $staff_hourly_rate = get_post_meta($post->ID,'staff_hourly_rate',true); 
    $staff_work_in_remote_locations = get_post_meta($post->ID,'staff_work_in_remote_locations',true); 
    $staff_new_resume = get_post_meta($post->ID,'staff_new_resume',true); 
    $staff_are_you_rsa_certified = get_post_meta($post->ID,'staff_are_you_rsa_certified',true); 
    $staff_upload_rsa_certificate = get_post_meta($post->ID,'staff_upload_rsa_certificate',true); 
    $staff_freelance_position_independent_contractor_agreement = get_post_meta($post->ID,'staff_freelance_position_independent_contractor_agreement',true); 
    $staff_freelance_position_flexibility_agreement_day_worker = get_post_meta($post->ID,'staff_freelance_position_flexibility_agreement_day_worker',true); 
    $staff_freelance_position_flexibility_agreement_12_hour = get_post_meta($post->ID,'staff_freelance_position_flexibility_agreement_12_hour',true); 
    $staff_freelance_position_engagement_letter = get_post_meta($post->ID,'staff_freelance_position_engagement_letter',true); 
    $staff_freelance_position_details_document = get_post_meta($post->ID,'staff_freelance_position_details_document',true); 
    $employee_user_id = get_post_meta($post->ID,'employee_user_id',true); 
     ?>
    <table class="form-table">
		<tbody>
		<tr>
			<th><label for="staff_address_full">Custom Full Address</label></th>
			<td>
				<input type="text" name="staff_address_full" id="staff_address_full" value="<?php echo $staff_address_full; ?>" class="regular-text" /><br />
				<span class="description">Please enter your full address.</span>
			</td>
		</tr>	
		<tr>
			<th><label for="staff_mail_id">Email</label></th>
			<td>
				<input type="text" name="staff_mail_id" id="staff_mail_id" value="<?php echo $staff_mail_id; ?>" class="regular-text" /><br />
				<span class="description">Please enter your Street Address.</span>
			</td>
		</tr>	
		<tr>
			<th><label for="staff_phone_number">Phone</label></th>
			<td>
				<input type="text" name="staff_phone_number" id="staff_phone_number" value="<?php echo $staff_phone_number; ?>" class="regular-text" /><br />
				<span class="description">Please enter your Phone.</span>
			</td>
		</tr>	
		<tr>
			<th><label for="staff_hourly_rate">Freelance Hourly rate</label></th>
			<td>
				<input type="text" name="staff_hourly_rate" id="staff_hourly_rate" value="<?php echo $staff_hourly_rate; ?>" class="regular-text" /><br />
				<span class="description">Please enter Freelance Hourly rate.</span>
			</td>
		</tr>	
		<tr>
			<th><label for="staff_work_in_remote_locations">I can work in Remote locations</label></th>
			<td>
				<input type="text" name="staff_work_in_remote_locations" id="staff_work_in_remote_locations" value="<?php echo $staff_work_in_remote_locations; ?>" class="regular-text" /><br />
				<span class="description">Please enter your Remote locations.</span>
			</td>
		</tr>	
		<tr>
			<th><label for="staff_new_resume">Upload a new Resume</label></th>
			<td>
				<input type="text" name="staff_new_resume" id="staff_new_resume" value="<?php echo $staff_new_resume; ?>" class="regular-text" /><br />
				<span class="description">Please enter your Upload a new Resume.</span>
			</td>
		</tr>
		<tr>
			<th><label for="staff_are_you_rsa_certified">Are you RSA Certified?</label></th>
			<td>
				<input type="text" name="staff_are_you_rsa_certified" id="staff_are_you_rsa_certified" value="<?php echo $staff_are_you_rsa_certified; ?>" class="regular-text" /><br />
			</td>
		</tr>	
		<tr>
			<th><label for="staff_upload_rsa_certificate">Please Upload RSA Certificate</label></th>
			<td>
				<input type="text" name="staff_upload_rsa_certificate" id="staff_upload_rsa_certificate" value="<?php echo $staff_upload_rsa_certificate; ?>" class="regular-text" /><br />
			</td>
		</tr>	
		<tr>
			<th><label for="staff_freelance_position_details_document">Upload back the chefs details document</label></th>
			<td>
				<input type="text" name="staff_freelance_position_details_document" id="staff_freelance_position_details_document" value="<?php echo $staff_freelance_position_details_document; ?>" class="regular-text" /><br />
			</td>
		</tr>	
		<tr>
			<th><label for="staff_freelance_position_engagement_letter">Upload back the engagement letter document</label></th>
			<td>
				<input type="text" name="staff_freelance_position_engagement_letter" id="staff_freelance_position_engagement_letter" value="<?php echo $staff_freelance_position_engagement_letter; ?>" class="regular-text" /><br />
			</td>
		</tr>
		<tr>
			<th><label for="staff_freelance_position_flexibility_agreement_12_hour">Upload back the Flexibility agreement 12 hour shifts document</label></th>
			<td>
				<input type="text" name="staff_freelance_position_flexibility_agreement_12_hour" id="staff_freelance_position_flexibility_agreement_12_hour" value="<?php echo $staff_freelance_position_flexibility_agreement_12_hour; ?>" class="regular-text" /><br />
			</td>
		</tr>	
		<tr>
			<th><label for="staff_freelance_position_flexibility_agreement_day_worker">Upload back the flexibility agreement not for profit day worker documentt</label></th>
			<td>
				<input type="text" name="staff_freelance_position_flexibility_agreement_day_worker" id="staff_freelance_position_flexibility_agreement_day_worker" value="<?php echo $staff_freelance_position_flexibility_agreement_day_worker; ?>" class="regular-text" /><br />
			</td>
		</tr>
		<tr>
			<th><label for="staff_freelance_position_independent_contractor_agreement">Upload back the independent contractor agreement document</label></th>
			<td>
				<input type="text" name="staff_freelance_position_independent_contractor_agreement" id="staff_freelance_position_independent_contractor_agreement" value="<?php echo $staff_freelance_position_independent_contractor_agreement; ?>" class="regular-text" /><br />
			</td>
		</tr>
		<tr>
			<th><label for="employee_user_id">User ID:</label></th>
			<td>
				<input type="text" name="employee_user_id" id="employee_user_id" value="<?php echo $employee_user_id; ?>" class="regular-text" readonly/><br />
			</td>
		</tr>
		</tbody>
	</table>
<?php }

//Recruiter Meta box fields
function recruiter_metabox() {
    global $post; 
    $recruiter_selected_staff = get_post_meta($post->ID,'recruiter_selected_staff',true); 
    $recruiter_user_id = get_post_meta($post->ID,'recruiter_user_id',true); 
    $recruiter_phone_number = get_post_meta($post->ID,'recruiter_phone_number',true); 
    $recruiter_venue_name = get_post_meta($post->ID,'recruiter_venue_name',true); 
    $recruiter_email_address = get_post_meta($post->ID,'recruiter_email_address',true); 
     ?>
    <table class="form-table">
		<tbody>
		<tr>
			<th><label for="recruiter_phone_number">Phone Number:</label></th>
			<td>
				<input type="text" name="recruiter_phone_number" id="recruiter_phone_number" value="<?php echo $recruiter_phone_number; ?>" class="regular-text" /><br />
				<span class="description">Please enter your Phone Number.</span>
			</td>
		</tr>	
		<tr>
			<th><label for="recruiter_venue_name">Venue name:</label></th>
			<td>
				<input type="text" name="recruiter_venue_name" id="recruiter_venue_name" value="<?php echo $recruiter_venue_name; ?>" class="regular-text" /><br />
				<span class="description">Please enter Venue name.</span>
			</td>
		</tr>	
		<tr>
			<th><label for="recruiter_email_address">Email:</label></th>
			<td>
				<input type="text" name="recruiter_email_address" id="recruiter_email_address" value="<?php echo $recruiter_email_address; ?>" class="regular-text" /><br />
				<span class="description">Please enter Email.</span>
			</td>
		</tr>	
		<tr>
			<th><label for="recruiter_selected_staff">User Staff:</label></th>
			<td>
				<input type="text" name="recruiter_selected_staff" id="recruiter_selected_staff" value="<?php echo $recruiter_selected_staff; ?>" class="regular-text" readonly/><br />
			</td>
		</tr>	
		<tr>
			<th><label for="recruiter_user_id">User ID:</label></th>
			<td>
				<input type="text" name="recruiter_user_id" id="recruiter_user_id" value="<?php echo $recruiter_user_id; ?>" class="regular-text" readonly/><br />
			</td>
		</tr>	
		</tbody>
	</table>
<?php }

//Timesheet Meta box fields
function timesheet_metabox() {
    global $post; 
    $timesheet_uploaded_sheet = get_post_meta($post->ID,'timesheet_uploaded_sheet',true); 
    $timesheet_uploaded_userid = get_post_meta($post->ID,'timesheet_uploaded_userid',true); 
    $timesheet_uploaded_client = get_post_meta($post->ID,'timesheet_uploaded_client',true); 
    $timesheet_uploaded_date = get_post_meta($post->ID,'timesheet_uploaded_date',true); 
    $timesheet_uploaded_location = get_post_meta($post->ID,'timesheet_uploaded_location',true); 
     ?>
    <table class="form-table">
		<tbody>
			<tr>
				<th><label for="timesheet_uploaded_sheet">Timesheet Url:</label></th>
				<td>
					<input type="text" name="timesheet_uploaded_sheet" id="timesheet_uploaded_sheet" value="<?php echo $timesheet_uploaded_sheet; ?>" class="regular-text" readonly style="width: 100%;"/><br />
				</td>
			</tr>	
			<tr>
				<th><label for="timesheet_uploaded_userid">User id:</label></th>
				<td>
					<input type="text" name="timesheet_uploaded_userid" id="timesheet_uploaded_userid" value="<?php echo $timesheet_uploaded_userid; ?>" class="regular-text" readonly/><br />
				</td>
			</tr>
			<tr>
				<th><label for="timesheet_uploaded_client">Client id:</label></th>
				<td>
					<input type="text" name="timesheet_uploaded_client" id="timesheet_uploaded_client" value="<?php echo $timesheet_uploaded_client; ?>" class="regular-text" readonly/><br />
				</td>
			</tr>
			<tr>
				<th><label for="timesheet_uploaded_location">Timesheet Date:</label></th>
				<td>
					<input type="text" name="timesheet_uploaded_location" id="timesheet_uploaded_location" value="<?php echo $timesheet_uploaded_location; ?>" class="regular-text" readonly/><br />
				</td>
			</tr>
			<tr>
				<th><label for="timesheet_uploaded_date">Timesheet Date:</label></th>
				<td>
					<input type="text" name="timesheet_uploaded_date" id="timesheet_uploaded_date" value="<?php echo $timesheet_uploaded_date; ?>" class="regular-text" readonly/><br />
				</td>
			</tr>	
		</tbody>
	</table>
<?php }

//rosters Meta box fields
function roster_metabox() {
	    global $post; 
	    $roster_clients_1 = get_post_meta($post->ID,'roster_clients_1',true); 
	    $roster_clients_2 = get_post_meta($post->ID,'roster_clients_2',true); 
	    $roster_clients_3 = get_post_meta($post->ID,'roster_clients_3',true); 
	    $roster_clients_4 = get_post_meta($post->ID,'roster_clients_4',true); 
	    $roster_clients_5 = get_post_meta($post->ID,'roster_clients_5',true); 
	    $roster_clients_6 = get_post_meta($post->ID,'roster_clients_6',true); 
	    $roster_clients_7 = get_post_meta($post->ID,'roster_clients_7',true); 
	    $roster_clients_8 = get_post_meta($post->ID,'roster_clients_8',true); 
	    $roster_clients_9 = get_post_meta($post->ID,'roster_clients_9',true); 
	    $roster_clients_10 = get_post_meta($post->ID,'roster_clients_10',true); 
	    
	    $roster_member_list = get_post_meta($post->ID,'roster_member_list',true);
	    // Monday
	    $roster_week_day_1 = get_post_meta($post->ID,'roster_week_day_1',true); 
	    $roster_staff_member_1 = get_post_meta($post->ID,'roster_staff_member_1',true); 
	    $roster_time_start_1 = get_post_meta($post->ID,'roster_time_start_1',true); 
	    $roster_time_finish_1 = get_post_meta($post->ID,'roster_time_finish_1',true);
	    $roster_client_1 = get_post_meta($post->ID,'roster_client_1',true);
	    $roster_location_1 = get_post_meta($post->ID,'roster_location_1',true);

	    // Tuesday
	    $roster_week_day_2 = get_post_meta($post->ID,'roster_week_day_2',true); 
	    $roster_staff_member_2 = get_post_meta($post->ID,'roster_staff_member_2',true); 
	    $roster_time_start_2 = get_post_meta($post->ID,'roster_time_start_2',true); 
	    $roster_time_finish_2 = get_post_meta($post->ID,'roster_time_finish_2',true); 
	    $roster_client_2 = get_post_meta($post->ID,'roster_client_2',true); 
	    $roster_location_2 = get_post_meta($post->ID,'roster_location_2',true); 
	    // Wednesday
	    $roster_week_day_3 = get_post_meta($post->ID,'roster_week_day_3',true); 
	    $roster_staff_member_3 = get_post_meta($post->ID,'roster_staff_member_3',true); 
	    $roster_time_start_3 = get_post_meta($post->ID,'roster_time_start_3',true); 
	    $roster_time_finish_3 = get_post_meta($post->ID,'roster_time_finish_3',true);
	    $roster_client_3 = get_post_meta($post->ID,'roster_client_3',true);
	    $roster_location_3 = get_post_meta($post->ID,'roster_location_3',true);
	    // Thursday
	    $roster_week_day_4 = get_post_meta($post->ID,'roster_week_day_4',true); 
	    $roster_staff_member_4 = get_post_meta($post->ID,'roster_staff_member_4',true); 
	    $roster_time_start_4 = get_post_meta($post->ID,'roster_time_start_4',true); 
	    $roster_time_finish_4 = get_post_meta($post->ID,'roster_time_finish_4',true);
	    $roster_client_4 = get_post_meta($post->ID,'roster_client_4',true);
	    $roster_location_4 = get_post_meta($post->ID,'roster_location_4',true);
	    // Friday
	    $roster_week_day_5 = get_post_meta($post->ID,'roster_week_day_5',true); 
	    $roster_staff_member_5 = get_post_meta($post->ID,'roster_staff_member_5',true); 
	    $roster_time_start_5 = get_post_meta($post->ID,'roster_time_start_5',true); 
	    $roster_time_finish_5 = get_post_meta($post->ID,'roster_time_finish_5',true);
	    $roster_client_5 = get_post_meta($post->ID,'roster_client_5',true);
	    $roster_location_5 = get_post_meta($post->ID,'roster_location_5',true);
	    // Saturday
	    $roster_week_day_6 = get_post_meta($post->ID,'roster_week_day_6',true); 
	    $roster_staff_member_6 = get_post_meta($post->ID,'roster_staff_member_6',true); 
	    $roster_time_start_6 = get_post_meta($post->ID,'roster_time_start_6',true); 
	    $roster_time_finish_6 = get_post_meta($post->ID,'roster_time_finish_6',true);
	    $roster_client_6 = get_post_meta($post->ID,'roster_client_6',true);
	    $roster_location_6 = get_post_meta($post->ID,'roster_location_6',true);
	    // Sunday
	    $roster_week_day_7 = get_post_meta($post->ID,'roster_week_day_7',true); 
	    $roster_staff_member_7 = get_post_meta($post->ID,'roster_staff_member_7',true); 
	    $roster_time_start_7 = get_post_meta($post->ID,'roster_time_start_7',true); 
	    $roster_time_finish_7 = get_post_meta($post->ID,'roster_time_finish_7',true);
	    $roster_client_7 = get_post_meta($post->ID,'roster_client_7',true);
	    $roster_location_7 = get_post_meta($post->ID,'roster_location_7',true);
	    // Roster field 8
	    $roster_week_day_8 = get_post_meta($post->ID,'roster_week_day_8',true); 
	    $roster_staff_member_8 = get_post_meta($post->ID,'roster_staff_member_8',true); 
	    $roster_time_start_8 = get_post_meta($post->ID,'roster_time_start_8',true); 
	    $roster_time_finish_8 = get_post_meta($post->ID,'roster_time_finish_8',true);
	    $roster_client_8 = get_post_meta($post->ID,'roster_client_8',true);
	    $roster_location_8 = get_post_meta($post->ID,'roster_location_8',true);
	    // Roster field 9
	    $roster_week_day_9 = get_post_meta($post->ID,'roster_week_day_9',true); 
	    $roster_staff_member_9 = get_post_meta($post->ID,'roster_staff_member_9',true); 
	    $roster_time_start_9 = get_post_meta($post->ID,'roster_time_start_9',true); 
	    $roster_time_finish_9 = get_post_meta($post->ID,'roster_time_finish_9',true);
	    $roster_location_9 = get_post_meta($post->ID,'roster_location_9',true);
	    // Roster field 8
	    $roster_week_day_10 = get_post_meta($post->ID,'roster_week_day_10',true); 
	    $roster_staff_member_10 = get_post_meta($post->ID,'roster_staff_member_10',true); 
	    $roster_time_start_10 = get_post_meta($post->ID,'roster_time_start_10',true); 
	    $roster_time_finish_10 = get_post_meta($post->ID,'roster_time_finish_10',true);
	    $roster_client_10 = get_post_meta($post->ID,'roster_client_10',true);
	    $roster_location_10 = get_post_meta($post->ID,'roster_location_10',true);
    ?>
    <table class="form-table">
		<tbody>
			<!-- Add roster 1 -->
			<tr align="center">
				<td class="text-center"><h3 for="Week_Heading">Roster Info</h3></td>
			</tr>
			<tr>
				<th><label for="roster_week_day_1">Week Day:</label></th>
				<td>
					<input type="text" name="roster_week_day_1" id="roster_week_day_1" value="<?php echo $roster_week_day_1; ?>" class="regular-text" /><br />
				</td>
			</tr>
			<tr>
				<th><label for="roster_staff_member_1">Staff Member:</label></th>
				<td>
					<input type="text" name="roster_staff_member_1" id="roster_staff_member_1" value="<?php echo $roster_staff_member_1; ?>" class="regular-text" /><br />
				</td>
			</tr>
			<tr>
				<th><label for="roster_time_start_1">Time Start:</label></th>
				<td>
					<input type="text" name="roster_time_start_1" id="roster_time_start_1" value="<?php echo $roster_time_start_1; ?>" class="regular-text" /><br />
				</td>
			</tr>
			<tr>
				<th><label for="roster_time_finish_1">Time Finish:</label></th>
				<td>
					<input type="text" name="roster_time_finish_1" id="roster_time_finish_1" value="<?php echo $roster_time_finish_1; ?>" class="regular-text" /><br />
				</td>
			</tr>
			<tr>
				<th><label for="roster_client_1">Client:</label></th>
				<td>
					<input type="text" name="roster_client_1" id="roster_client_1" value="<?php echo $roster_client_1; ?>" class="regular-text" /><br />
				</td>
			</tr>
			<tr>
				<th><label for="roster_location_1">Location:</label></th>
				<td>
					<input type="text" name="roster_location_1" id="roster_location_1" value="<?php echo $roster_location_1; ?>" class="regular-text" /><br />
				</td>
			</tr>
			<tr>
				<th><label for="roster_location">Roster Client:</label></th>
				<td>
					<select name="roster_clients_1">
						<?php
							$locationargs = array(
								'post_type' => 'locations',
								'posts_per_page' => -1,
							); 
							// the query
							$location_the_query = new WP_Query( $locationargs ); 
							?>

							<?php if ( $location_the_query->have_posts() ) : ?>

								<!-- pagination here -->

								<!-- the loop -->
								<?php while ( $location_the_query->have_posts() ) : $location_the_query->the_post(); ?>
									<option value="<?php echo get_the_ID(); ?>" <?php if ($roster_clients_1 == get_the_ID()) echo ' selected="selected"'; ?>><?php echo get_the_title(); ?></option>
								<?php endwhile; ?>
								<!-- end of the loop -->

								<!-- pagination here -->

								<?php $location_the_query->wp_reset_postdata(); ?>
								<?php $location_the_query->reset_postdata(); ?>

						<?php else : ?>
							<p><?php _e( 'Sorry, no client your criteria.' ); ?></p>
						<?php endif; ?>
					</select>
				</td>
			</tr>
			<!-- Add roster 2 -->
			<tr align="center">
				<td class="text-center"><h3 for="Week_Heading">Roster Info</h3></td>
			</tr>		
			<tr>
				<th><label for="roster_week_day_2">Week Day:</label></th>
				<td>
					<input type="text" name="roster_week_day_2" id="roster_week_day_2" value="<?php echo $roster_week_day_2; ?>" class="regular-text" /><br />
				</td>
			</tr>
			<tr>
				<th><label for="roster_staff_member_2">Staff Member:</label></th>
				<td>
					<input type="text" name="roster_staff_member_2" id="roster_staff_member_2" value="<?php echo $roster_staff_member_2; ?>" class="regular-text" /><br />
				</td>
			</tr>
			<tr>
				<th><label for="roster_time_start_2">Time Start:</label></th>
				<td>
					<input type="text" name="roster_time_start_2" id="roster_time_start_2" value="<?php echo $roster_time_start_2; ?>" class="regular-text" /><br />
				</td>
			</tr>
			<tr>
				<th><label for="roster_time_finish_2">Time Finish:</label></th>
				<td>
					<input type="text" name="roster_time_finish_2" id="roster_time_finish_2" value="<?php echo $roster_time_finish_2; ?>" class="regular-text" /><br />
				</td>
			</tr>
			<tr>
				<th><label for="roster_client_2">Client:</label></th>
				<td>
					<input type="text" name="roster_client_2" id="roster_client_2" value="<?php echo $roster_client_2; ?>" class="regular-text" /><br />
				</td>
			</tr>
			<tr>
				<th><label for="roster_location_2">Location:</label></th>
				<td>
					<input type="text" name="roster_location_2" id="roster_location_2" value="<?php echo $roster_location_2; ?>" class="regular-text" /><br />
				</td>
			</tr>
			<tr>
				<th><label for="roster_location">Roster Client:</label></th>
				<td>
					<select name="roster_clients_2">
						<?php
							$locationargs = array(
								'post_type' => 'locations',
								'posts_per_page' => -1,
							); 
							// the query
							$location_the_query = new WP_Query( $locationargs ); 
							?>

							<?php if ( $location_the_query->have_posts() ) : ?>

								<!-- pagination here -->

								<!-- the loop -->
								<?php while ( $location_the_query->have_posts() ) : $location_the_query->the_post(); ?>
									<option value="<?php echo get_the_ID(); ?>" <?php if ($roster_clients_2 == get_the_ID()) echo ' selected="selected"'; ?>><?php echo get_the_title(); ?></option>
								<?php endwhile; ?>
								<!-- end of the loop -->

								<!-- pagination here -->

								<?php $location_the_query->wp_reset_postdata(); ?>
								<?php $location_the_query->reset_postdata(); ?>

						<?php else : ?>
							<p><?php _e( 'Sorry, no client your criteria.' ); ?></p>
						<?php endif; ?>
					</select>
				</td>
			</tr>
			<!-- Add roster 3 -->
			<tr align="center">
				<td class="text-center"><h3 for="Week_Heading">Roster Info</h3></td>
			</tr>		
			<tr>
				<th><label for="roster_week_day_3">Week Day:</label></th>
				<td>
					<input type="text" name="roster_week_day_3" id="roster_week_day_3" value="<?php echo $roster_week_day_3; ?>" class="regular-text" /><br />
				</td>
			</tr>
			<tr>
				<th><label for="roster_staff_member_3">Staff Member:</label></th>
				<td>
					<input type="text" name="roster_staff_member_3" id="roster_staff_member_3" value="<?php echo $roster_staff_member_3; ?>" class="regular-text" /><br />
				</td>
			</tr>
			<tr>
				<th><label for="roster_time_start_3">Time Start:</label></th>
				<td>
					<input type="text" name="roster_time_start_3" id="roster_time_start_3" value="<?php echo $roster_time_start_3; ?>" class="regular-text" /><br />
				</td>
			</tr>
			<tr>
				<th><label for="roster_time_finish_3">Time Finish:</label></th>
				<td>
					<input type="text" name="roster_time_finish_3" id="roster_time_finish_3" value="<?php echo $roster_time_finish_3; ?>" class="regular-text" /><br />
				</td>
			</tr>
			<tr>
				<th><label for="roster_client_3">Client:</label></th>
				<td>
					<input type="text" name="roster_client_3" id="roster_client_3" value="<?php echo $roster_client_3; ?>" class="regular-text" /><br />
				</td>
			</tr>
			<tr>
				<th><label for="roster_location_3">Location:</label></th>
				<td>
					<input type="text" name="roster_location_3" id="roster_location_3" value="<?php echo $roster_location_3; ?>" class="regular-text" /><br />
				</td>
			</tr>
			<tr>
				<th><label for="roster_location">Roster Client:</label></th>
				<td>
					<select name="roster_clients_3">
						<?php
							$locationargs = array(
								'post_type' => 'locations',
								'posts_per_page' => -1,
							); 
							// the query
							$location_the_query = new WP_Query( $locationargs ); 
							?>

							<?php if ( $location_the_query->have_posts() ) : ?>

								<!-- pagination here -->

								<!-- the loop -->
								<?php while ( $location_the_query->have_posts() ) : $location_the_query->the_post(); ?>
									<option value="<?php echo get_the_ID(); ?>" <?php if ($roster_clients_3 == get_the_ID()) echo ' selected="selected"'; ?>><?php echo get_the_title(); ?></option>
								<?php endwhile; ?>
								<!-- end of the loop -->

								<!-- pagination here -->

								<?php $location_the_query->wp_reset_postdata(); ?>
								<?php $location_the_query->reset_postdata(); ?>

						<?php else : ?>
							<p><?php _e( 'Sorry, no client your criteria.' ); ?></p>
						<?php endif; ?>
					</select>
				</td>
			</tr>
			<!-- Add roster 4 -->
			<tr align="center">
				<td class="text-center"><h3 for="Week_Heading">Roster Info</h3></td>
			</tr>		
			<tr>
				<th><label for="roster_week_day_4">Week Day:</label></th>
				<td>
					<input type="text" name="roster_week_day_4" id="roster_week_day_4" value="<?php echo $roster_week_day_4; ?>" class="regular-text" /><br />
				</td>
			</tr>
			<tr>
				<th><label for="roster_staff_member_4">Staff Member:</label></th>
				<td>
					<input type="text" name="roster_staff_member_4" id="roster_staff_member_4" value="<?php echo $roster_staff_member_4; ?>" class="regular-text" /><br />
				</td>
			</tr>
			<tr>
				<th><label for="roster_time_start_4">Time Start:</label></th>
				<td>
					<input type="text" name="roster_time_start_4" id="roster_time_start_4" value="<?php echo $roster_time_start_4; ?>" class="regular-text" /><br />
				</td>
			</tr>
			<tr>
				<th><label for="roster_time_finish_4">Time Finish:</label></th>
				<td>
					<input type="text" name="roster_time_finish_4" id="roster_time_finish_4" value="<?php echo $roster_time_finish_4; ?>" class="regular-text" /><br />
				</td>
			</tr>
			<tr>
				<th><label for="roster_client_4">Client:</label></th>
				<td>
					<input type="text" name="roster_client_4" id="roster_client_4" value="<?php echo $roster_client_4; ?>" class="regular-text" /><br />
				</td>
			</tr>
			<tr>
				<th><label for="roster_location_4">Client:</label></th>
				<td>
					<input type="text" name="roster_location_4" id="roster_location_4" value="<?php echo $roster_location_4; ?>" class="regular-text" /><br />
				</td>
			</tr>
			<tr>
				<th><label for="roster_location">Roster Client:</label></th>
				<td>
					<select name="roster_clients_4">
						<?php
							$locationargs = array(
								'post_type' => 'locations',
								'posts_per_page' => -1,
							); 
							// the query
							$location_the_query = new WP_Query( $locationargs ); 
							?>

							<?php if ( $location_the_query->have_posts() ) : ?>

								<!-- pagination here -->

								<!-- the loop -->
								<?php while ( $location_the_query->have_posts() ) : $location_the_query->the_post(); ?>
									<option value="<?php echo get_the_ID(); ?>" <?php if ($roster_clients_4 == get_the_ID()) echo ' selected="selected"'; ?>><?php echo get_the_title(); ?></option>
								<?php endwhile; ?>
								<!-- end of the loop -->

								<!-- pagination here -->

								<?php $location_the_query->wp_reset_postdata(); ?>
								<?php $location_the_query->reset_postdata(); ?>

						<?php else : ?>
							<p><?php _e( 'Sorry, no client your criteria.' ); ?></p>
						<?php endif; ?>
					</select>
				</td>
			</tr>
			<!-- Add roster 5 -->
			<tr align="center">
				<td class="text-center"><h3 for="Week_Heading">Roster Info</h3></td>
			</tr>		
			<tr>
				<th><label for="roster_week_day_5">Week Day:</label></th>
				<td>
					<input type="text" name="roster_week_day_5" id="roster_week_day_5" value="<?php echo $roster_week_day_5; ?>" class="regular-text" /><br />
				</td>
			</tr>
			<tr>
				<th><label for="roster_staff_member_5">Staff Member:</label></th>
				<td>
					<input type="text" name="roster_staff_member_5" id="roster_staff_member_5" value="<?php echo $roster_staff_member_5; ?>" class="regular-text" /><br />
				</td>
			</tr>
			<tr>
				<th><label for="roster_time_start_5">Time Start:</label></th>
				<td>
					<input type="text" name="roster_time_start_5" id="roster_time_start_5" value="<?php echo $roster_time_start_5; ?>" class="regular-text" /><br />
				</td>
			</tr>
			<tr>
				<th><label for="roster_time_finish_5">Time Finish:</label></th>
				<td>
					<input type="text" name="roster_time_finish_5" id="roster_time_finish_5" value="<?php echo $roster_time_finish_5; ?>" class="regular-text" /><br />
				</td>
			</tr>
			<tr>
				<th><label for="roster_client_5">Client:</label></th>
				<td>
					<input type="text" name="roster_client_5" id="roster_client_5" value="<?php echo $roster_client_5; ?>" class="regular-text" /><br />
				</td>
			</tr>
			<tr>
				<th><label for="roster_location_5">Location:</label></th>
				<td>
					<input type="text" name="roster_location_5" id="roster_location_5" value="<?php echo $roster_location_5; ?>" class="regular-text" /><br />
				</td>
			</tr>
			<tr>
				<th><label for="roster_location">Roster Client:</label></th>
				<td>
					<select name="roster_clients_5">
						<?php
							$locationargs = array(
								'post_type' => 'locations',
								'posts_per_page' => -1,
							); 
							// the query
							$location_the_query = new WP_Query( $locationargs ); 
							?>

							<?php if ( $location_the_query->have_posts() ) : ?>

								<!-- pagination here -->

								<!-- the loop -->
								<?php while ( $location_the_query->have_posts() ) : $location_the_query->the_post(); ?>
									<option value="<?php echo get_the_ID(); ?>" <?php if ($roster_clients_5 == get_the_ID()) echo ' selected="selected"'; ?>><?php echo get_the_title(); ?></option>
								<?php endwhile; ?>
								<!-- end of the loop -->

								<!-- pagination here -->

								<?php $location_the_query->wp_reset_postdata(); ?>
								<?php $location_the_query->reset_postdata(); ?>

						<?php else : ?>
							<p><?php _e( 'Sorry, no client your criteria.' ); ?></p>
						<?php endif; ?>
					</select>
				</td>
			</tr>
			<!-- Add roster 6 -->
			<tr align="center">
				<td class="text-center"><h3 for="Week_Heading">Roster Info</h3></td>
			</tr>		
			<tr>
				<th><label for="roster_week_day_6">Week Day:</label></th>
				<td>
					<input type="text" name="roster_week_day_6" id="roster_week_day_6" value="<?php echo $roster_week_day_6; ?>" class="regular-text" /><br />
				</td>
			</tr>
			<tr>
				<th><label for="roster_staff_member_6">Staff Member:</label></th>
				<td>
					<input type="text" name="roster_staff_member_6" id="roster_staff_member_6" value="<?php echo $roster_staff_member_6; ?>" class="regular-text" /><br />
				</td>
			</tr>
			<tr>
				<th><label for="roster_time_start_6">Time Start:</label></th>
				<td>
					<input type="text" name="roster_time_start_6" id="roster_time_start_6" value="<?php echo $roster_time_start_6; ?>" class="regular-text" /><br />
				</td>
			</tr>
			<tr>
				<th><label for="roster_time_finish_6">Time Finish:</label></th>
				<td>
					<input type="text" name="roster_time_finish_6" id="roster_time_finish_6" value="<?php echo $roster_time_finish_6; ?>" class="regular-text" /><br />
				</td>
			</tr>
			<tr>
				<th><label for="roster_client_6">Client:</label></th>
				<td>
					<input type="text" name="roster_client_6" id="roster_client_6" value="<?php echo $roster_client_6; ?>" class="regular-text" /><br />
				</td>
			</tr>
			<tr>
				<th><label for="roster_location_6">Location:</label></th>
				<td>
					<input type="text" name="roster_location_6" id="roster_location_6" value="<?php echo $roster_location_6; ?>" class="regular-text" /><br />
				</td>
			</tr>
			<tr>
				<th><label for="roster_location">Roster Client:</label></th>
				<td>
					<select name="roster_clients_6">
						<?php
							$locationargs = array(
								'post_type' => 'locations',
								'posts_per_page' => -1,
							); 
							// the query
							$location_the_query = new WP_Query( $locationargs ); 
							?>

							<?php if ( $location_the_query->have_posts() ) : ?>

								<!-- pagination here -->

								<!-- the loop -->
								<?php while ( $location_the_query->have_posts() ) : $location_the_query->the_post(); ?>
									<option value="<?php echo get_the_ID(); ?>" <?php if ($roster_clients_6 == get_the_ID()) echo ' selected="selected"'; ?>><?php echo get_the_title(); ?></option>
								<?php endwhile; ?>
								<!-- end of the loop -->

								<!-- pagination here -->

								<?php $location_the_query->wp_reset_postdata(); ?>
								<?php $location_the_query->reset_postdata(); ?>

						<?php else : ?>
							<p><?php _e( 'Sorry, no client your criteria.' ); ?></p>
						<?php endif; ?>
					</select>
				</td>
			</tr>
			<!-- Add roster 7 -->
			<tr align="center">
				<td class="text-center"><h3 for="Week_Heading">Roster Info</h3></td>
			</tr>		
			<tr>
				<th><label for="roster_week_day_7">Week Day:</label></th>
				<td>
					<input type="text" name="roster_week_day_7" id="roster_week_day_7" value="<?php echo $roster_week_day_7; ?>" class="regular-text" /><br />
				</td>
			</tr>
			<tr>
				<th><label for="roster_staff_member_7">Staff Member:</label></th>
				<td>
					<input type="text" name="roster_staff_member_7" id="roster_staff_member_7" value="<?php echo $roster_staff_member_7; ?>" class="regular-text" /><br />
				</td>
			</tr>
			<tr>
				<th><label for="roster_time_start_7">Time Start:</label></th>
				<td>
					<input type="text" name="roster_time_start_7" id="roster_time_start_7" value="<?php echo $roster_time_start_7; ?>" class="regular-text" /><br />
				</td>
			</tr>
			<tr>
				<th><label for="roster_time_finish_7">Time Finish:</label></th>
				<td>
					<input type="text" name="roster_time_finish_7" id="roster_time_finish_7" value="<?php echo $roster_time_finish_7; ?>" class="regular-text" /><br />
				</td>
			</tr>
			<tr>
				<th><label for="roster_client_7">Client:</label></th>
				<td>
					<input type="text" name="roster_client_7" id="roster_client_7" value="<?php echo $roster_client_7; ?>" class="regular-text" /><br />
				</td>
			</tr>
			<tr>
				<th><label for="roster_location_7">Location:</label></th>
				<td>
					<input type="text" name="roster_location_7" id="roster_location_7" value="<?php echo $roster_location_7; ?>" class="regular-text" /><br />
				</td>
			</tr>
			<tr>
				<th><label for="roster_location">Roster Client:</label></th>
				<td>
					<select name="roster_clients_7">
						<?php
							$locationargs = array(
								'post_type' => 'locations',
								'posts_per_page' => -1,
							); 
							// the query
							$location_the_query = new WP_Query( $locationargs ); 
							?>

							<?php if ( $location_the_query->have_posts() ) : ?>

								<!-- pagination here -->

								<!-- the loop -->
								<?php while ( $location_the_query->have_posts() ) : $location_the_query->the_post(); ?>
									<option value="<?php echo get_the_ID(); ?>" <?php if ($roster_clients_7 == get_the_ID()) echo ' selected="selected"'; ?>><?php echo get_the_title(); ?></option>
								<?php endwhile; ?>
								<!-- end of the loop -->

								<!-- pagination here -->

								<?php $location_the_query->wp_reset_postdata(); ?>
								<?php $location_the_query->reset_postdata(); ?>

						<?php else : ?>
							<p><?php _e( 'Sorry, no client your criteria.' ); ?></p>
						<?php endif; ?>
					</select>
				</td>
			</tr>
			<!-- Add roster 8 -->
			<tr align="center">
				<td class="text-center"><h3 for="Week_Heading">Roster Info</h3></td>
			</tr>		
			<tr>
				<th><label for="roster_week_day_8">Week Day:</label></th>
				<td>
					<input type="text" name="roster_week_day_8" id="roster_week_day_8" value="<?php echo $roster_week_day_8; ?>" class="regular-text" /><br />
				</td>
			</tr>
			<tr>
				<th><label for="roster_staff_member_8">Staff Member:</label></th>
				<td>
					<input type="text" name="roster_staff_member_8" id="roster_staff_member_8" value="<?php echo $roster_staff_member_8; ?>" class="regular-text" /><br />
				</td>
			</tr>
			<tr>
				<th><label for="roster_time_start_8">Time Start:</label></th>
				<td>
					<input type="text" name="roster_time_start_8" id="roster_time_start_8" value="<?php echo $roster_time_start_8; ?>" class="regular-text" /><br />
				</td>
			</tr>
			<tr>
				<th><label for="roster_time_finish_8">Time Finish:</label></th>
				<td>
					<input type="text" name="roster_time_finish_8" id="roster_time_finish_8" value="<?php echo $roster_time_finish_8; ?>" class="regular-text" /><br />
				</td>
			</tr>
			<tr>
				<th><label for="roster_client_8">Client:</label></th>
				<td>
					<input type="text" name="roster_client_8" id="roster_client_8" value="<?php echo $roster_client_8; ?>" class="regular-text" /><br />
				</td>
			</tr>
			<tr>
				<th><label for="roster_location_8">Location:</label></th>
				<td>
					<input type="text" name="roster_location_8" id="roster_location_8" value="<?php echo $roster_location_8; ?>" class="regular-text" /><br />
				</td>
			</tr>
			<tr>
				<th><label for="roster_location">Roster Client:</label></th>
				<td>
					<select name="roster_clients_8">
						<?php
							$locationargs = array(
								'post_type' => 'locations',
								'posts_per_page' => -1,
							); 
							// the query
							$location_the_query = new WP_Query( $locationargs ); 
							?>

							<?php if ( $location_the_query->have_posts() ) : ?>

								<!-- pagination here -->

								<!-- the loop -->
								<?php while ( $location_the_query->have_posts() ) : $location_the_query->the_post(); ?>
									<option value="<?php echo get_the_ID(); ?>" <?php if ($roster_clients_8 == get_the_ID()) echo ' selected="selected"'; ?>><?php echo get_the_title(); ?></option>
								<?php endwhile; ?>
								<!-- end of the loop -->

								<!-- pagination here -->

								<?php $location_the_query->wp_reset_postdata(); ?>
								<?php $location_the_query->reset_postdata(); ?>

						<?php else : ?>
							<p><?php _e( 'Sorry, no client your criteria.' ); ?></p>
						<?php endif; ?>
					</select>
				</td>
			</tr>
			<!-- Add roster 9 -->
			<tr align="center">
				<td class="text-center"><h3 for="Week_Heading">Roster Info</h3></td>
			</tr>		
			<tr>
				<th><label for="roster_week_day_9">Week Day:</label></th>
				<td>
					<input type="text" name="roster_week_day_9" id="roster_week_day_9" value="<?php echo $roster_week_day_9; ?>" class="regular-text" /><br />
				</td>
			</tr>
			<tr>
				<th><label for="roster_staff_member_9">Staff Member:</label></th>
				<td>
					<input type="text" name="roster_staff_member_9" id="roster_staff_member_9" value="<?php echo $roster_staff_member_9; ?>" class="regular-text" /><br />
				</td>
			</tr>
			<tr>
				<th><label for="roster_time_start_9">Time Start:</label></th>
				<td>
					<input type="text" name="roster_time_start_9" id="roster_time_start_9" value="<?php echo $roster_time_start_9; ?>" class="regular-text" /><br />
				</td>
			</tr>
			<tr>
				<th><label for="roster_time_finish_9">Time Finish:</label></th>
				<td>
					<input type="text" name="roster_time_finish_9" id="roster_time_finish_9" value="<?php echo $roster_time_finish_9; ?>" class="regular-text" /><br />
				</td>
			</tr>
			<tr>
				<th><label for="roster_client_9">Client:</label></th>
				<td>
					<input type="text" name="roster_client_9" id="roster_client_9" value="<?php echo $roster_client_9; ?>" class="regular-text" /><br />
				</td>
			</tr>
			<tr>
				<th><label for="roster_location_9">Location:</label></th>
				<td>
					<input type="text" name="roster_location_9" id="roster_location_9" value="<?php echo $roster_location_9; ?>" class="regular-text" /><br />
				</td>
			</tr>
			<tr>
				<th><label for="roster_location">Roster Client:</label></th>
				<td>
					<select name="roster_clients_9">
						<?php
							$locationargs = array(
								'post_type' => 'locations',
								'posts_per_page' => -1,
							); 
							// the query
							$location_the_query = new WP_Query( $locationargs ); 
							?>

							<?php if ( $location_the_query->have_posts() ) : ?>

								<!-- pagination here -->

								<!-- the loop -->
								<?php while ( $location_the_query->have_posts() ) : $location_the_query->the_post(); ?>
									<option value="<?php echo get_the_ID(); ?>" <?php if ($roster_clients_9 == get_the_ID()) echo ' selected="selected"'; ?>><?php echo get_the_title(); ?></option>
								<?php endwhile; ?>
								<!-- end of the loop -->

								<!-- pagination here -->

								<?php $location_the_query->wp_reset_postdata(); ?>
								<?php $location_the_query->reset_postdata(); ?>

						<?php else : ?>
							<p><?php _e( 'Sorry, no client your criteria.' ); ?></p>
						<?php endif; ?>
					</select>
				</td>
			</tr>
			<!-- Add roster 10 -->
			<tr align="center">
				<td class="text-center"><h3 for="Week_Heading">Roster Info</h3></td>
			</tr>		
			<tr>
				<th><label for="roster_week_day_10">Week Day:</label></th>
				<td>
					<input type="text" name="roster_week_day_10" id="roster_week_day_10" value="<?php echo $roster_week_day_10; ?>" class="regular-text" /><br />
				</td>
			</tr>
			<tr>
				<th><label for="roster_staff_member_10">Staff Member:</label></th>
				<td>
					<input type="text" name="roster_staff_member_10" id="roster_staff_member_10" value="<?php echo $roster_staff_member_10; ?>" class="regular-text" /><br />
				</td>
			</tr>
			<tr>
				<th><label for="roster_time_start_10">Time Start:</label></th>
				<td>
					<input type="text" name="roster_time_start_10" id="roster_time_start_10" value="<?php echo $roster_time_start_10; ?>" class="regular-text" /><br />
				</td>
			</tr>
			<tr>
				<th><label for="roster_time_finish_10">Time Finish:</label></th>
				<td>
					<input type="text" name="roster_time_finish_10" id="roster_time_finish_10" value="<?php echo $roster_time_finish_10; ?>" class="regular-text" /><br />
				</td>
			</tr>
			<tr>
				<th><label for="roster_client_10">Client:</label></th>
				<td>
					<input type="text" name="roster_client_10" id="roster_client_10" value="<?php echo $roster_client_10; ?>" class="regular-text" /><br />
				</td>
			</tr>
			<tr>
				<th><label for="roster_location_10">Location:</label></th>
				<td>
					<input type="text" name="roster_location_10" id="roster_location_10" value="<?php echo $roster_location_10; ?>" class="regular-text" /><br />
				</td>
			</tr>
			<tr>
				<th><label for="roster_location">Roster Client:</label></th>
				<td>
					<select name="roster_clients_10">
						<?php
							$locationargs = array(
								'post_type' => 'locations',
								'posts_per_page' => -1,
							); 
							// the query
							$location_the_query = new WP_Query( $locationargs ); 
							?>

							<?php if ( $location_the_query->have_posts() ) : ?>

								<!-- pagination here -->

								<!-- the loop -->
								<?php while ( $location_the_query->have_posts() ) : $location_the_query->the_post(); ?>
									<option value="<?php echo get_the_ID(); ?>" <?php if ($roster_clients_10 == get_the_ID()) echo ' selected="selected"'; ?>><?php echo get_the_title(); ?></option>
								<?php endwhile; ?>
								<!-- end of the loop -->

								<!-- pagination here -->

								<?php $location_the_query->wp_reset_postdata(); ?>
								<?php $location_the_query->reset_postdata(); ?>

						<?php else : ?>
							<p><?php _e( 'Sorry, no client your criteria.' ); ?></p>
						<?php endif; ?>
					</select>
				</td>
			</tr>
			<tr>
				<th><label for="roster_member_list">Roster Member:</label></th>
				<td>
					<select name="roster_member_list">
						<?php

						// get the chefs
						$chef_query = new WP_User_Query(
							array(
								'role'	 =>	'chef_employee',
							)
						);
						$chefs = $chef_query->get_results();

						// get the staff
						$staff_employee_query = new WP_User_Query(
							array(
								'role'	 =>	'staff_employee',
							)
						);
						$staffs = $staff_employee_query->get_results();

						// store them all as users
						$users = array_merge( $staffs, $chefs );

						// User Loop
						if ( ! empty( $users ) ) {
							foreach ( $users as $user ) { ?>
							<option value="<?php  echo $user->id; ?>" <?php if ($roster_member_list == $user->id) echo ' selected="selected"'; ?>><?php echo $user->user_login; ?></option>
						<?php 
							}
						} else {
							echo 'No users found.';
						}

						?>

					</select>
				</td>
			</tr>
		</tbody>
	</table>
<?php }

function save_chefs_meta($post_id) {
    global $post;
    if( 'chef' == $post->post_type ){
        update_post_meta($post_id, 'custom_address_full', $_POST['custom_address_full']);
        update_post_meta($post_id, 'mail_id', $_POST['mail_id']);
        update_post_meta($post_id, 'user_phone_number', $_POST['user_phone_number']);
        update_post_meta($post_id, 'freelance_hourly_rate', $_POST['freelance_hourly_rate']);
        update_post_meta($post_id, 'you_reside_in', $_POST['you_reside_in']);
        update_post_meta($post_id, 'citizenship_visa', $_POST['citizenship_visa']);
        update_post_meta($post_id, 'recruitment_type', $_POST['recruitment_type']);
        update_post_meta($post_id, 'chef_skills', $_POST['chef_skills']);
        update_post_meta($post_id, 'field_of_interest', $_POST['field_of_interest']);
        update_post_meta($post_id, 'work_in_remote_locations', $_POST['work_in_remote_locations']);
        update_post_meta($post_id, 'specialising_in', $_POST['specialising_in']);
        update_post_meta($post_id, 'new_resume', $_POST['new_resume']);
        update_post_meta($post_id, 'freelance_position_details_document', $_POST['freelance_position_details_document']);
        update_post_meta($post_id, 'freelance_position_engagement_letter', $_POST['freelance_position_engagement_letter']);
        update_post_meta($post_id, 'freelance_position_flexibility_agreement_12_hour', $_POST['freelance_position_flexibility_agreement_12_hour']);
        update_post_meta($post_id, 'freelance_position_flexibility_agreement_day_worker', $_POST['freelance_position_flexibility_agreement_day_worker']);
        update_post_meta($post_id, 'freelance_position_independent_contractor_agreement', $_POST['freelance_position_independent_contractor_agreement']);
        update_post_meta($post_id, 'employee_user_id', $_POST['employee_user_id']);
    }
    
    if( 'hospitality_staff' == $post->post_type ){
        update_post_meta($post_id, 'employee_user_id', $_POST['employee_user_id']);
        update_post_meta($post_id, 'staff_address_full', $_POST['staff_address_full']);
        update_post_meta($post_id, 'staff_mail_id', $_POST['staff_mail_id']);
        update_post_meta($post_id, 'staff_phone_number', $_POST['staff_phone_number']);
        update_post_meta($post_id, 'staff_hourly_rate', $_POST['staff_hourly_rate']);
        update_post_meta($post_id, 'staff_work_in_remote_locations', $_POST['staff_work_in_remote_locations']);
        update_post_meta($post_id, 'staff_new_resume', $_POST['staff_new_resume']);
        update_post_meta($post_id, 'staff_are_you_rsa_certified', $_POST['staff_are_you_rsa_certified']);
        update_post_meta($post_id, 'staff_upload_rsa_certificate', $_POST['staff_upload_rsa_certificate']);
        update_post_meta($post_id, 'staff_freelance_position_details_document', $_POST['staff_freelance_position_details_document']);
        update_post_meta($post_id, 'staff_freelance_position_engagement_letter', $_POST['staff_freelance_position_engagement_letter']);
        update_post_meta($post_id, 'staff_freelance_position_flexibility_agreement_12_hour', $_POST['staff_freelance_position_flexibility_agreement_12_hour']);
        update_post_meta($post_id, 'staff_freelance_position_flexibility_agreement_day_worker', $_POST['staff_freelance_position_flexibility_agreement_day_worker']);
        update_post_meta($post_id, 'staff_freelance_position_independent_contractor_agreement', $_POST['staff_freelance_position_independent_contractor_agreement']);
    }
	if( 'recruiter' == $post->post_type ){
	    update_post_meta($post_id, 'recruiter_user_id', $_POST['recruiter_user_id']);
	    update_post_meta($post_id, 'recruiter_selected_staff', $_POST['recruiter_selected_staff']);
	    update_post_meta($post_id, 'recruiter_phone_number', $_POST['recruiter_phone_number']);
	    update_post_meta($post_id, 'recruiter_email_address', $_POST['recruiter_email_address']);
	    update_post_meta($post_id, 'recruiter_venue_name', $_POST['recruiter_venue_name']);
	}
	if( 'timesheet' == $post->post_type ){
	    update_post_meta($post_id, 'timesheet_uploaded_sheet', $_POST['timesheet_uploaded_sheet']);
	    update_post_meta($post_id, 'timesheet_uploaded_userid', $_POST['timesheet_uploaded_userid']);
	    update_post_meta($post_id, 'timesheet_uploaded_client', $_POST['timesheet_uploaded_client']);
	    update_post_meta($post_id, 'timesheet_uploaded_date', $_POST['timesheet_uploaded_date']);
	    update_post_meta($post_id, 'timesheet_uploaded_location', $_POST['timesheet_uploaded_location']);
	}
	if( 'rosters' == $post->post_type ){
		update_post_meta($post_id, 'roster_clients_1', $_POST['roster_clients_1']);
		update_post_meta($post_id, 'roster_clients_2', $_POST['roster_clients_2']);
		update_post_meta($post_id, 'roster_clients_3', $_POST['roster_clients_3']);
		update_post_meta($post_id, 'roster_clients_4', $_POST['roster_clients_4']);
		update_post_meta($post_id, 'roster_clients_5', $_POST['roster_clients_5']);
		update_post_meta($post_id, 'roster_clients_6', $_POST['roster_clients_6']);
		update_post_meta($post_id, 'roster_clients_7', $_POST['roster_clients_7']);
		update_post_meta($post_id, 'roster_clients_8', $_POST['roster_clients_8']);
		update_post_meta($post_id, 'roster_clients_9', $_POST['roster_clients_9']);
		update_post_meta($post_id, 'roster_clients_10', $_POST['roster_clients_10']);

		update_post_meta($post_id, 'roster_member_list', $_POST['roster_member_list']);
		// Monday
		update_post_meta($post_id, 'roster_week_day_1', $_POST['roster_week_day_1']);
		update_post_meta($post_id, 'roster_staff_member_1', $_POST['roster_staff_member_1']);
		update_post_meta($post_id, 'roster_time_start_1', $_POST['roster_time_start_1']);
		update_post_meta($post_id, 'roster_time_finish_1', $_POST['roster_time_finish_1']);
		update_post_meta($post_id, 'roster_client_1', $_POST['roster_client_1']);
		update_post_meta($post_id, 'roster_location_1', $_POST['roster_location_1']);
		// Tuesday
		update_post_meta($post_id, 'roster_week_day_2', $_POST['roster_week_day_2']);
		update_post_meta($post_id, 'roster_staff_member_2', $_POST['roster_staff_member_2']);
		update_post_meta($post_id, 'roster_time_start_2', $_POST['roster_time_start_2']);
		update_post_meta($post_id, 'roster_time_finish_2', $_POST['roster_time_finish_2']);
		update_post_meta($post_id, 'roster_client_2', $_POST['roster_client_2']);
		update_post_meta($post_id, 'roster_location_2', $_POST['roster_location_2']);
		// Wednesday
		update_post_meta($post_id, 'roster_week_day_3', $_POST['roster_week_day_3']);
		update_post_meta($post_id, 'roster_staff_member_3', $_POST['roster_staff_member_3']);
		update_post_meta($post_id, 'roster_time_start_3', $_POST['roster_time_start_3']);
		update_post_meta($post_id, 'roster_time_finish_3', $_POST['roster_time_finish_3']);
		update_post_meta($post_id, 'roster_client_3', $_POST['roster_client_3']);
		update_post_meta($post_id, 'roster_location_3', $_POST['roster_location_3']);
		// Thursday
		update_post_meta($post_id, 'roster_week_day_4', $_POST['roster_week_day_4']);
		update_post_meta($post_id, 'roster_staff_member_4', $_POST['roster_staff_member_4']);
		update_post_meta($post_id, 'roster_time_start_4', $_POST['roster_time_start_4']);
		update_post_meta($post_id, 'roster_time_finish_4', $_POST['roster_time_finish_4']);
		update_post_meta($post_id, 'roster_client_4', $_POST['roster_client_4']);
		update_post_meta($post_id, 'roster_location_4', $_POST['roster_location_4']);
		// Friday
		update_post_meta($post_id, 'roster_week_day_5', $_POST['roster_week_day_5']);
		update_post_meta($post_id, 'roster_staff_member_5', $_POST['roster_staff_member_5']);
		update_post_meta($post_id, 'roster_time_start_5', $_POST['roster_time_start_5']);
		update_post_meta($post_id, 'roster_time_finish_5', $_POST['roster_time_finish_5']);
		update_post_meta($post_id, 'roster_client_5', $_POST['roster_client_5']);
		update_post_meta($post_id, 'roster_location_5', $_POST['roster_location_5']);
		// Saturday
		update_post_meta($post_id, 'roster_week_day_6', $_POST['roster_week_day_6']);
		update_post_meta($post_id, 'roster_staff_member_6', $_POST['roster_staff_member_6']);
		update_post_meta($post_id, 'roster_time_start_6', $_POST['roster_time_start_6']);
		update_post_meta($post_id, 'roster_time_finish_6', $_POST['roster_time_finish_6']);
		update_post_meta($post_id, 'roster_client_6', $_POST['roster_client_6']);
		update_post_meta($post_id, 'roster_location_6', $_POST['roster_location_6']);
		// Sunday Field 7
		update_post_meta($post_id, 'roster_week_day_7', $_POST['roster_week_day_7']);
		update_post_meta($post_id, 'roster_staff_member_7', $_POST['roster_staff_member_7']);
		update_post_meta($post_id, 'roster_time_start_7', $_POST['roster_time_start_7']);
		update_post_meta($post_id, 'roster_time_finish_7', $_POST['roster_time_finish_7']);
		update_post_meta($post_id, 'roster_client_7', $_POST['roster_client_7']);
		update_post_meta($post_id, 'roster_location_7', $_POST['roster_location_7']);
		// Sunday Field 7
		update_post_meta($post_id, 'roster_week_day_8', $_POST['roster_week_day_8']);
		update_post_meta($post_id, 'roster_staff_member_8', $_POST['roster_staff_member_8']);
		update_post_meta($post_id, 'roster_time_start_8', $_POST['roster_time_start_8']);
		update_post_meta($post_id, 'roster_time_finish_8', $_POST['roster_time_finish_8']);
		update_post_meta($post_id, 'roster_client_8', $_POST['roster_client_8']);
		update_post_meta($post_id, 'roster_location_8', $_POST['roster_location_8']);
		// Sunday Field 9
		update_post_meta($post_id, 'roster_week_day_9', $_POST['roster_week_day_9']);
		update_post_meta($post_id, 'roster_staff_member_9', $_POST['roster_staff_member_9']);
		update_post_meta($post_id, 'roster_time_start_9', $_POST['roster_time_start_9']);
		update_post_meta($post_id, 'roster_time_finish_9', $_POST['roster_time_finish_9']);
		update_post_meta($post_id, 'roster_client_9', $_POST['roster_client_9']);
		update_post_meta($post_id, 'roster_location_9', $_POST['roster_location_9']);
		// Sunday Field 9
		update_post_meta($post_id, 'roster_week_day_10', $_POST['roster_week_day_10']);
		update_post_meta($post_id, 'roster_staff_member_10', $_POST['roster_staff_member_10']);
		update_post_meta($post_id, 'roster_time_start_10', $_POST['roster_time_start_10']);
		update_post_meta($post_id, 'roster_time_finish_10', $_POST['roster_time_finish_10']);
		update_post_meta($post_id, 'roster_client_10', $_POST['roster_client_10']);
		update_post_meta($post_id, 'roster_location_10', $_POST['roster_location_10']);
	}
}
add_action( 'save_post', 'save_chefs_meta' );