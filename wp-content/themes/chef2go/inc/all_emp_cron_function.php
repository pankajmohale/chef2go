<?php
/* get all emplyee timesheet from */

/* function weekly_mail_timesheet_function(){
	

	//$tovalue = date('d-m-Y');
						
	//$fromvalue =date('d-m-Y',time()+( 1 - date('w'))*24*3600);

	 $fromvalue = '1-7-2017';
	 $tovalue = '19-8-2017';
	
	$newfromvalue = date("Ymd", strtotime($fromvalue));
	$newtovalue = date("Ymd", strtotime($tovalue));

	$timesheet_args = array(
		'post_type' => 'employee_timesheet',
		'posts_per_page' => -1,
	);

	// the query
	$timesheet_query = new WP_Query( $timesheet_args ); 
		if ( $timesheet_query->have_posts() ) :
		$emp_value = '<div id="allemployeelisting">';
			$emp_value .= '<div class="timesheet-content">';
				$emp_value .= '<table border="1" cellpadding="10" style="width: 100%;">';
					$emp_value .= '<tbody align="center">';
						$emp_value .= '<tr>';
							$emp_value .= '<th style="text-align: center;">Location</th>';
							$emp_value .= '<th style="text-align: center;">date</th>';
							$emp_value .= '<th style="text-align: center;">Start</th>';
							$emp_value .= '<th style="text-align: center;">Finish</th>';
							$emp_value .= '<th style="text-align: center;">Daily Total HRS Worked</th>';
							$emp_value .= '</tr>';
				while ( $timesheet_query->have_posts() ) : $timesheet_query->the_post();
					for ($timesheet_meta = 1; $timesheet_meta < 11; $timesheet_meta++) { 
						    $datekey = 'timesheet_date_'.$timesheet_meta;
					        $timesheet_date = get_post_meta( get_the_ID(), $datekey, true );
					        if ($timesheet_date >= $newfromvalue && $timesheet_date <= $newtovalue ) { 
			        			$author_id = get_the_author_meta('ID');
			        			$author_id_array[] = $author_id;
			        			$author_unique_id_array = array_unique($author_id_array);
				        }
				    }
				endwhile;
				if ($author_unique_id_array) {
					foreach ($author_unique_id_array as $key => $author_id) {
	        				$user_info = get_userdata($author_id);
	        				$emp_value .= '<tr>';
							$emp_value .= '<th colspan="5" style="text-align: center;font-size: 18px;padding: 10px 0;">';
							$emp_value .= $user_info->nickname.' ( '.$user_info->user_login.' )';
							$emp_value .= '</th>';
							$emp_value .= '</tr>';
					        while ( $timesheet_query->have_posts() ) : $timesheet_query->the_post();
			        			$authorid = get_the_author_meta('ID');
					        	$post_id = get_the_ID();
				        		$clinedet = get_post_meta( get_the_ID(), false );

						        for ($timesheet_meta = 1; $timesheet_meta < 11; $timesheet_meta++) { 
								    $datekey = 'timesheet_date_'.$timesheet_meta;
							        $timesheet_date = get_post_meta( get_the_ID(), $datekey, true );
							        if ($timesheet_date >= $newfromvalue && $timesheet_date <= $newtovalue ) { 
							        	if ($authorid == $author_id) {
								        	$clientkey = 'clients_'.$timesheet_meta;
											$clientval = get_post_meta( get_the_ID(), $clientkey, true );						        
								            $startkey = 'start_time_'.$timesheet_meta;
									        $endkey = 'end_time_'.$timesheet_meta;
									        $value = weekly_mail_timesheet($clientkey, $datekey, $startkey, $endkey );
									        $emp_value .= $value;
									        $totaltime += difference_calculator($startkey, $endkey );        	
							        	}
							        }
						        	
						        }
							endwhile;
	        		}						
				}	

					$emp_value .= '</tbody>';
				$emp_value .= '</table>';
			$emp_value .= '</div>';
				$totalhourswork = $totaltime/60;
				$totaltime = explode(".",$totalhourswork);
				$minutes = $totaltime[1]*0.6;
				$numlength = strlen((string)$minutes);
				if ($numlength <= 1) {
					$min = $minutes.'0';
				} else {
					$min = $minutes; 
				}
			$emp_value .= '<div class="timesheet-footer">';
				$emp_value .= '<div style="text-align: right; font-size: 18px; margin-bottom: 20px;">'; 
				$emp_value .= 'Total:'; 
					$emp_value .= $totaltime[0] . ' Hours ' . $min . ' Minutes' ; 
				$emp_value .= '</div>';
			$emp_value .= '</div>';
		$emp_value .= '</div>';

	    wp_reset_postdata(); 

		else :
	    $emp_value .= '<h2>';
		$emp_value .= _e( 'Sorry, no timesheet matched your criteria.' ); 
		$emp_value .= '</h2>';
	endif;

	return $emp_value;
}

function weekly_mail_timesheet($clientkey, $timesheet_date_key, $start_time_key, $end_time_key){

		$client = get_post_meta( get_the_ID(), $clientkey, true );
		$date = get_post_meta( get_the_ID(), $timesheet_date_key, true );

		$start_time = get_post_meta( get_the_ID(), $start_time_key, true );
		$end_time = get_post_meta( get_the_ID(), $end_time_key, true );

		$starttime = get_post_meta( get_the_ID(), $start_time_key, true );
		$endtime = get_post_meta( get_the_ID(), $end_time_key, true );

		$start = array_filter(explode(' ',str_replace(':',' ',$starttime)));
		$end = array_filter(explode(' ',str_replace(':',' ',$endtime)));
		$newDate = date("m/d/Y", strtotime($date));
		
		// convert date and time arrays into datetime formats
		$startdate = date_create_from_format('m/d/Y@h:i a', $newDate . "@". $start[0].":".$start[1]." ".$start[2]);
		$enddate =   date_create_from_format('m/d/Y@h:i a', $newDate . "@". $end[0].":".$end[1]." ".$end[2]);

		//convert datetimes into seconds to compare
		$starttime = strtotime($startdate->format('Y-m-d H:i:s'));
		$endtime = strtotime($enddate->format('Y-m-d H:i:s'));

		// check to see if the times span overnight
		if($starttime > $endtime)
		$endtime = strtotime($enddate->format('Y-m-d H:i:s') . " +1 day");

		// perform calculation
		$diff = floor(($endtime - $starttime)/60);

		$totaldiff = $diff/60;
		$totaldiff = explode(".",$totaldiff);

		$totaltime += $diff;

		if ($diff >= 60 ) {
			$time = $diff/60 . ' Hours';
		} else {
			$time = $diff . ' Minutes';									
		}					
	$weekly_mail_timesheet = '<tr>';
		$weekly_mail_timesheet .= '<td>';
		$term = get_term_by('term_id', $client, 'client_location');
		$weekly_mail_timesheet .= $term->name;
		$weekly_mail_timesheet .= '</td>';
		$weekly_mail_timesheet .= '<td>';
		$weekly_mail_timesheet .= date("d-m-Y", strtotime($date));
		$weekly_mail_timesheet .= '</td>';
		$weekly_mail_timesheet .= '<td>';
		$weekly_mail_timesheet .= $start_time;
		$weekly_mail_timesheet .= '</td>';
		$weekly_mail_timesheet .='<td>'. $end_time .'</td>';
		$weekly_mail_timesheet .='<td>'. $totaldiff[0] . ' Hours ' . $diff%60 . ' Minutes</td>';
	$weekly_mail_timesheet .='</tr>';

	return $weekly_mail_timesheet;
}*/