<?php 
/* ====================================================================================================
									Add Roster Functionality
======================================================================================================= */

// Gravity Form Staff menber list 1 
/* ********************************** Show User value ********************************** */
add_filter( 'gform_pre_render_13', 'staff_member' );
add_filter( 'gform_pre_validation_13', 'staff_member' );
add_filter( 'gform_pre_submission_filter_13', 'staff_member' );
add_filter( 'gform_admin_pre_render_13', 'staff_member' );
function staff_member( $form ) {
	$current_user = wp_get_current_user();


	foreach ( $form['fields'] as &$field ) {

        if ( $field->type != 'select' || strpos( $field->cssClass, 'staff_member_1' ) === false ) {
            continue;
        }

        // you can add additional parameters here to alter the posts that are retrieved
        // more info: [http://codex.wordpress.org/Template_Tags/get_posts](http://codex.wordpress.org/Template_Tags/get_posts)
        $choices = array();

        // get the chefs
		$chef_query = new WP_User_Query(
			array(
				'role'			    =>	'chef_employee',
			)
		);
		$chefs = $chef_query->get_results();

		// get the staff
		$staff_employee_query = new WP_User_Query(
			array(
				'role'			    =>	'staff_employee',
			)
		);
		$staffs = $staff_employee_query->get_results();

		// store them all as users
		$users = array_merge( $staffs, $chefs );

		// User Loop
		if ( ! empty( $users ) ) {
			foreach ( $users as $user ) {
	            $choices[] = array( 'text' => $user->user_login, 'value' => $user->id );
			}
		} else {
			echo 'No users found.';
		}

        /*if ($already_val) {
        	foreach ( $already_val as $post ) {
	        	$user_info = get_userdata($post['u_id']);
	            $choices[] = array( 'text' => $user_info->first_name, 'value' => $post['u_id'] );
	        }
        }*/

        // update 'Select a staff member' to whatever you'd like the instructive option to be
        $field->placeholder = 'Select a staff member';
        $field->choices = $choices;
    }

    return $form;
}
/* ****************************************************************************************************************** */
// Gravity Form Staff menber list 2 
add_filter( 'gform_pre_render_13', 'staff_member_2' );
add_filter( 'gform_pre_validation_13', 'staff_member_2' );
add_filter( 'gform_pre_submission_filter_13', 'staff_member_2' );
add_filter( 'gform_admin_pre_render_13', 'staff_member_2' );
function staff_member_2( $form ) {
	$current_user = wp_get_current_user();

    foreach ( $form['fields'] as &$field ) {

        if ( $field->type != 'select' || strpos( $field->cssClass, 'staff_member_2' ) === false ) {
            continue;
        }

        // you can add additional parameters here to alter the posts that are retrieved
        // more info: [http://codex.wordpress.org/Template_Tags/get_posts](http://codex.wordpress.org/Template_Tags/get_posts)
        $choices = array();

        // get the chefs
		$chef_query = new WP_User_Query(
			array(
				'role'	 =>	'chef_employee',
			)
		);
		$chefs = $chef_query->get_results();

		// get the staff
		$staff_employee_query = new WP_User_Query(
			array(
				'role'	 =>	'staff_employee',
			)
		);
		$staffs = $staff_employee_query->get_results();

		// store them all as users
		$users = array_merge( $staffs, $chefs );

		// User Loop
		if ( ! empty( $users ) ) {
			foreach ( $users as $user ) {
	            $choices[] = array( 'text' => $user->user_login, 'value' => $user->id );
			}
		} else {
			echo 'No users found.';
		}

        /*if ($already_val) {
        	foreach ( $already_val as $post ) {
	        	$user_info = get_userdata($post['u_id']);
	            $choices[] = array( 'text' => $user_info->first_name, 'value' => $post['u_id'] );
	        }
        }*/

        // update 'Select a staff member' to whatever you'd like the instructive option to be
        $field->placeholder = 'Select a staff member';
        $field->choices = $choices;
    }

    return $form;
}
/* ****************************************************************************************************************** */
// Gravity Form Staff menber list 3 
add_filter( 'gform_pre_render_13', 'staff_member_3' );
add_filter( 'gform_pre_validation_13', 'staff_member_3' );
add_filter( 'gform_pre_submission_filter_13', 'staff_member_3' );
add_filter( 'gform_admin_pre_render_13', 'staff_member_3' );
function staff_member_3( $form ) {
	$current_user = wp_get_current_user();

	
    foreach ( $form['fields'] as &$field ) {

        if ( $field->type != 'select' || strpos( $field->cssClass, 'staff_member_3' ) === false ) {
            continue;
        }

        // you can add additional parameters here to alter the posts that are retrieved
        // more info: [http://codex.wordpress.org/Template_Tags/get_posts](http://codex.wordpress.org/Template_Tags/get_posts)
        $choices = array();

        // get the chefs
		$chef_query = new WP_User_Query(
			array(
				'role'	 =>	'chef_employee',
			)
		);
		$chefs = $chef_query->get_results();

		// get the staff
		$staff_employee_query = new WP_User_Query(
			array(
				'role'	 =>	'staff_employee',
			)
		);
		$staffs = $staff_employee_query->get_results();

		// store them all as users
		$users = array_merge( $staffs, $chefs );

		// User Loop
		if ( ! empty( $users ) ) {
			foreach ( $users as $user ) {
	            $choices[] = array( 'text' => $user->user_login, 'value' => $user->id );
			}
		} else {
			echo 'No users found.';
		}

        /*if ($already_val) {
        	foreach ( $already_val as $post ) {
	        	$user_info = get_userdata($post['u_id']);
	            $choices[] = array( 'text' => $user_info->first_name, 'value' => $post['u_id'] );
	        }
        }*/

        // update 'Select a staff member' to whatever you'd like the instructive option to be
        $field->placeholder = 'Select a staff member';
        $field->choices = $choices;
    }

    return $form;
}


/* ****************************************************************************************************************** */
// Gravity Form Staff menber list 4 
add_filter( 'gform_pre_render_13', 'staff_member_4' );
add_filter( 'gform_pre_validation_13', 'staff_member_4' );
add_filter( 'gform_pre_submission_filter_13', 'staff_member_4' );
add_filter( 'gform_admin_pre_render_13', 'staff_member_4' );
function staff_member_4( $form ) {
	$current_user = wp_get_current_user();

	foreach ( $form['fields'] as &$field ) {

        if ( $field->type != 'select' || strpos( $field->cssClass, 'staff_member_4' ) === false ) {
            continue;
        }

        // you can add additional parameters here to alter the posts that are retrieved
        // more info: [http://codex.wordpress.org/Template_Tags/get_posts](http://codex.wordpress.org/Template_Tags/get_posts)
        $choices = array();

        // get the chefs
		$chef_query = new WP_User_Query(
			array(
				'role'	 =>	'chef_employee',
			)
		);
		$chefs = $chef_query->get_results();

		// get the staff
		$staff_employee_query = new WP_User_Query(
			array(
				'role'	 =>	'staff_employee',
			)
		);
		$staffs = $staff_employee_query->get_results();

		// store them all as users
		$users = array_merge( $staffs, $chefs );

		// User Loop
		if ( ! empty( $users ) ) {
			foreach ( $users as $user ) {
	            $choices[] = array( 'text' => $user->user_login, 'value' => $user->id );
			}
		} else {
			echo 'No users found.';
		}


        /*if ($already_val) {
        	foreach ( $already_val as $post ) {
	        	$user_info = get_userdata($post['u_id']);
	            $choices[] = array( 'text' => $user_info->first_name, 'value' => $post['u_id'] );
	        }
        }*/

        // update 'Select a staff member' to whatever you'd like the instructive option to be
        $field->placeholder = 'Select a staff member';
        $field->choices = $choices;
    }

    return $form;
}


/* ****************************************************************************************************************** */
// Gravity Form Staff menber list 5 
// This code is used in add roster section  
add_filter( 'gform_pre_render_13', 'staff_member_5' );
add_filter( 'gform_pre_validation_13', 'staff_member_5' );
add_filter( 'gform_pre_submission_filter_13', 'staff_member_5' );
add_filter( 'gform_admin_pre_render_13', 'staff_member_5' );
function staff_member_5( $form ) {
	$current_user = wp_get_current_user();

	foreach ( $form['fields'] as &$field ) {

        if ( $field->type != 'select' || strpos( $field->cssClass, 'staff_member_5' ) === false ) {
            continue;
        }

        // you can add additional parameters here to alter the posts that are retrieved
        // more info: [http://codex.wordpress.org/Template_Tags/get_posts](http://codex.wordpress.org/Template_Tags/get_posts)
        $choices = array();

        // get the chefs
		$chef_query = new WP_User_Query(
			array(
				'role'	 =>	'chef_employee',
			)
		);
		$chefs = $chef_query->get_results();

		// get the staff
		$staff_employee_query = new WP_User_Query(
			array(
				'role'	 =>	'staff_employee',
			)
		);
		$staffs = $staff_employee_query->get_results();

		// store them all as users
		$users = array_merge( $staffs, $chefs );

		// User Loop
		if ( ! empty( $users ) ) {
			foreach ( $users as $user ) {
	            $choices[] = array( 'text' => $user->user_login, 'value' => $user->id );
			}
		} else {
			echo 'No users found.';
		}

        /*if ($already_val) {
        	foreach ( $already_val as $post ) {
	        	$user_info = get_userdata($post['u_id']);
	            $choices[] = array( 'text' => $user_info->first_name, 'value' => $post['u_id'] );
	        }
        }*/

        // update 'Select a staff member' to whatever you'd like the instructive option to be
        $field->placeholder = 'Select a staff member';
        $field->choices = $choices;
    }

    return $form;
}

/* ****************************************************************************************************************** */
// Gravity Form Staff menber list 6 
// This code is used in add roster section  
add_filter( 'gform_pre_render_13', 'staff_member_6' );
add_filter( 'gform_pre_validation_13', 'staff_member_6' );
add_filter( 'gform_pre_submission_filter_13', 'staff_member_6' );
add_filter( 'gform_admin_pre_render_13', 'staff_member_6' );
function staff_member_6( $form ) {
	$current_user = wp_get_current_user();

	foreach ( $form['fields'] as &$field ) {

        if ( $field->type != 'select' || strpos( $field->cssClass, 'staff_member_6' ) === false ) {
            continue;
        }

        // you can add additional parameters here to alter the posts that are retrieved
        // more info: [http://codex.wordpress.org/Template_Tags/get_posts](http://codex.wordpress.org/Template_Tags/get_posts)
        $choices = array();

        // get the chefs
		$chef_query = new WP_User_Query(
			array(
				'role'	 =>	'chef_employee',
			)
		);
		$chefs = $chef_query->get_results();

		// get the staff
		$staff_employee_query = new WP_User_Query(
			array(
				'role'	 =>	'staff_employee',
			)
		);
		$staffs = $staff_employee_query->get_results();

		// store them all as users
		$users = array_merge( $staffs, $chefs );

		// User Loop
		if ( ! empty( $users ) ) {
			foreach ( $users as $user ) {
	            $choices[] = array( 'text' => $user->user_login, 'value' => $user->id );
			}
		} else {
			echo 'No users found.';
		}

        /*if ($already_val) {
        	foreach ( $already_val as $post ) {
	        	$user_info = get_userdata($post['u_id']);
	            $choices[] = array( 'text' => $user_info->first_name, 'value' => $post['u_id'] );
	        }
        }*/

        // update 'Select a staff member' to whatever you'd like the instructive option to be
        $field->placeholder = 'Select a staff member';
        $field->choices = $choices;
    }

    return $form;
}

/* ****************************************************************************************************************** */
// Gravity Form display Staff menber list 7 field 
// This code is used in add roster section  
add_filter( 'gform_pre_render_13', 'staff_member_7' );
add_filter( 'gform_pre_validation_13', 'staff_member_7' );
add_filter( 'gform_pre_submission_filter_13', 'staff_member_7' );
add_filter( 'gform_admin_pre_render_13', 'staff_member_7' );
function staff_member_7( $form ) {
	$current_user = wp_get_current_user();

	foreach ( $form['fields'] as &$field ) {

        if ( $field->type != 'select' || strpos( $field->cssClass, 'staff_member_7' ) === false ) {
            continue;
        }

        // you can add additional parameters here to alter the posts that are retrieved
        // more info: [http://codex.wordpress.org/Template_Tags/get_posts](http://codex.wordpress.org/Template_Tags/get_posts)
        $choices = array();

        // get the chefs
		$chef_query = new WP_User_Query(
			array(
				'role'	 =>	'chef_employee',
			)
		);
		$chefs = $chef_query->get_results();

		// get the staff
		$staff_employee_query = new WP_User_Query(
			array(
				'role'	 =>	'staff_employee',
			)
		);
		$staffs = $staff_employee_query->get_results();

		// store them all as users
		$users = array_merge( $staffs, $chefs );

		// User Loop
		if ( ! empty( $users ) ) {
			foreach ( $users as $user ) {
	            $choices[] = array( 'text' => $user->user_login, 'value' => $user->id );
			}
		} else {
			echo 'No users found.';
		}

        /*if ($already_val) {
        	foreach ( $already_val as $post ) {
	        	$user_info = get_userdata($post['u_id']);
	            $choices[] = array( 'text' => $user_info->first_name, 'value' => $post['u_id'] );
	        }
        }*/

        // update 'Select a staff member' to whatever you'd like the instructive option to be
        $field->placeholder = 'Select a staff member';
        $field->choices = $choices;
    }

    return $form;
}

/* ****************************************************************************************************************** */
// Gravity Form display Staff menber list 8 field
// This code is used in add roster section  
add_filter( 'gform_pre_render_13', 'staff_member_8' );
add_filter( 'gform_pre_validation_13', 'staff_member_8' );
add_filter( 'gform_pre_submission_filter_13', 'staff_member_8' );
add_filter( 'gform_admin_pre_render_13', 'staff_member_8' );
function staff_member_8( $form ) {
	$current_user = wp_get_current_user();

	foreach ( $form['fields'] as &$field ) {

        if ( $field->type != 'select' || strpos( $field->cssClass, 'staff_member_8' ) === false ) {
            continue;
        }

        // you can add additional parameters here to alter the posts that are retrieved
        // more info: [http://codex.wordpress.org/Template_Tags/get_posts](http://codex.wordpress.org/Template_Tags/get_posts)
        $choices = array();

        // get the chefs
		$chef_query = new WP_User_Query(
			array(
				'role'	 =>	'chef_employee',
			)
		);
		$chefs = $chef_query->get_results();

		// get the staff
		$staff_employee_query = new WP_User_Query(
			array(
				'role'	 =>	'staff_employee',
			)
		);
		$staffs = $staff_employee_query->get_results();

		// store them all as users
		$users = array_merge( $staffs, $chefs );

		// User Loop
		if ( ! empty( $users ) ) {
			foreach ( $users as $user ) {
	            $choices[] = array( 'text' => $user->user_login, 'value' => $user->id );
			}
		} else {
			echo 'No users found.';
		}

        /*if ($already_val) {
        	foreach ( $already_val as $post ) {
	        	$user_info = get_userdata($post['u_id']);
	            $choices[] = array( 'text' => $user_info->first_name, 'value' => $post['u_id'] );
	        }
        }*/

        // update 'Select a staff member' to whatever you'd like the instructive option to be
        $field->placeholder = 'Select a staff member';
        $field->choices = $choices;
    }

    return $form;
}


/* ****************************************************************************************************************** */
// Gravity Form display Staff menber list 9 field
// This code is used in add roster section  
add_filter( 'gform_pre_render_13', 'staff_member_9' );
add_filter( 'gform_pre_validation_13', 'staff_member_9' );
add_filter( 'gform_pre_submission_filter_13', 'staff_member_9' );
add_filter( 'gform_admin_pre_render_13', 'staff_member_9' );
function staff_member_9( $form ) {
	$current_user = wp_get_current_user();

	
    foreach ( $form['fields'] as &$field ) {

        if ( $field->type != 'select' || strpos( $field->cssClass, 'staff_member_9' ) === false ) {
            continue;
        }

        // you can add additional parameters here to alter the posts that are retrieved
        // more info: [http://codex.wordpress.org/Template_Tags/get_posts](http://codex.wordpress.org/Template_Tags/get_posts)
        $choices = array();

        // get the chefs
		$chef_query = new WP_User_Query(
			array(
				'role'	 =>	'chef_employee',
			)
		);
		$chefs = $chef_query->get_results();

		// get the staff
		$staff_employee_query = new WP_User_Query(
			array(
				'role'	 =>	'staff_employee',
			)
		);
		$staffs = $staff_employee_query->get_results();

		// store them all as users
		$users = array_merge( $staffs, $chefs );

		// User Loop
		if ( ! empty( $users ) ) {
			foreach ( $users as $user ) {
	            $choices[] = array( 'text' => $user->user_login, 'value' => $user->id );
			}
		} else {
			echo 'No users found.';
		}

        /*if ($already_val) {
        	foreach ( $already_val as $post ) {
	        	$user_info = get_userdata($post['u_id']);
	            $choices[] = array( 'text' => $user_info->first_name, 'value' => $post['u_id'] );
	        }
        }*/

        // update 'Select a staff member' to whatever you'd like the instructive option to be
        $field->placeholder = 'Select a staff member';
        $field->choices = $choices;
    }

    return $form;
}

/* ****************************************************************************************************************** */
// Gravity Form display Staff menber list 9 field
// This code is used in add roster section  
add_filter( 'gform_pre_render_13', 'staff_member_10' );
add_filter( 'gform_pre_validation_13', 'staff_member_10' );
add_filter( 'gform_pre_submission_filter_13', 'staff_member_10' );
add_filter( 'gform_admin_pre_render_13', 'staff_member_10' );
function staff_member_10( $form ) {
	$current_user = wp_get_current_user();

	
    foreach ( $form['fields'] as &$field ) {

        if ( $field->type != 'select' || strpos( $field->cssClass, 'staff_member_10' ) === false ) {
            continue;
        }

        // you can add additional parameters here to alter the posts that are retrieved
        // more info: [http://codex.wordpress.org/Template_Tags/get_posts](http://codex.wordpress.org/Template_Tags/get_posts)
        $choices = array();

        // get the chefs
		$chef_query = new WP_User_Query(
			array(
				'role'	 =>	'chef_employee',
			)
		);
		$chefs = $chef_query->get_results();

		// get the staff
		$staff_employee_query = new WP_User_Query(
			array(
				'role'	 =>	'staff_employee',
			)
		);
		$staffs = $staff_employee_query->get_results();

		// store them all as users
		$users = array_merge( $staffs, $chefs );

		// User Loop
		if ( ! empty( $users ) ) {
			foreach ( $users as $user ) {
	            $choices[] = array( 'text' => $user->user_login, 'value' => $user->id );
			}
		} else {
			echo 'No users found.';
		}

        /*if ($already_val) {
        	foreach ( $already_val as $post ) {
	        	$user_info = get_userdata($post['u_id']);
	            $choices[] = array( 'text' => $user_info->first_name, 'value' => $post['u_id'] );
	        }
        }*/

        // update 'Select a staff member' to whatever you'd like the instructive option to be
        $field->placeholder = 'Select a staff member';
        $field->choices = $choices;
    }

    return $form;
}

/* ***************************************** Hide **************************************** */
// Gravity Form display Staff menber list 9 field
// This code is used in add roster section  
add_filter( 'gform_pre_render_13', 'employee_member' );
add_filter( 'gform_pre_validation_13', 'employee_member' );
add_filter( 'gform_pre_submission_filter_13', 'employee_member' );
add_filter( 'gform_admin_pre_render_13', 'employee_member' );
function employee_member( $form ) {
	$current_user = wp_get_current_user();

	
    foreach ( $form['fields'] as &$field ) {

        if ( $field->type != 'select' || strpos( $field->cssClass, 'employee_member' ) === false ) {
            continue;
        }

        // you can add additional parameters here to alter the posts that are retrieved
        // more info: [http://codex.wordpress.org/Template_Tags/get_posts](http://codex.wordpress.org/Template_Tags/get_posts)
        $choices = array();

        // get the chefs
		$chef_query = new WP_User_Query(
			array(
				'role'	 =>	'chef_employee',
			)
		);
		$chefs = $chef_query->get_results();

		// get the staff
		$staff_employee_query = new WP_User_Query(
			array(
				'role'	 =>	'staff_employee',
			)
		);
		$staffs = $staff_employee_query->get_results();

		// store them all as users
		$users = array_merge( $staffs, $chefs );

		// User Loop
		if ( ! empty( $users ) ) {
			foreach ( $users as $user ) {
	            $choices[] = array( 'text' => $user->user_login, 'value' => $user->id );
			}
		} else {
			echo 'No users found.';
		}

        /*if ($already_val) {
        	foreach ( $already_val as $post ) {
	        	$user_info = get_userdata($post['u_id']);
	            $choices[] = array( 'text' => $user_info->first_name, 'value' => $post['u_id'] );
	        }
        }*/

        // update 'Select a staff member' to whatever you'd like the instructive option to be
        $field->placeholder = 'Select a staff member';
        $field->choices = $choices;
    }

    return $form;
}



// GForm staff member Update Post Meta value
// add_action( 'gform_post_submission', 'set_post_content', 10, 2 );
function set_post_content( $entry, $form ) {

    //getting post
    $post = get_post( $entry['post_id'] );

    //changing post content
	$member_1 = get_userdata($entry[43]);
	$member_2 = get_userdata($entry[44]);
	$member_3 = get_userdata($entry[45]);
	$member_4 = get_userdata($entry[46]);
	$member_5 = get_userdata($entry[47]);
	$member_6 = get_userdata($entry[48]);
	$member_7 = get_userdata($entry[49]);
	$member_8 = get_userdata($entry[53]);
	$member_9 = get_userdata($entry[65]);
	$member_10 = get_userdata($entry[59]);

	update_post_meta($entry['post_id'], 'roster_staff_member_1', $member_1->first_name);
	update_post_meta($entry['post_id'], 'roster_staff_member_2', $member_2->first_name);
	update_post_meta($entry['post_id'], 'roster_staff_member_3', $member_3->first_name);
	update_post_meta($entry['post_id'], 'roster_staff_member_4', $member_4->first_name);
	update_post_meta($entry['post_id'], 'roster_staff_member_5', $member_5->first_name);
	update_post_meta($entry['post_id'], 'roster_staff_member_6', $member_6->first_name);
	update_post_meta($entry['post_id'], 'roster_staff_member_7', $member_7->first_name);
	update_post_meta($entry['post_id'], 'roster_staff_member_8', $member_8->first_name);
	update_post_meta($entry['post_id'], 'roster_staff_member_9', $member_9->first_name);
	update_post_meta($entry['post_id'], 'roster_staff_member_10', $member_10->first_name);
	
	// Update client information
	// $client_1 = get_the_title($entry[68]);
	// $client_2 = get_the_title($entry[70]);
	// $client_3 = get_the_title($entry[72]);
	// $client_4 = get_the_title($entry[74]);
	// $client_5 = get_the_title($entry[76]);
	// $client_6 = get_the_title($entry[78]);
	// $client_7 = get_the_title($entry[80]);
	// $client_8 = get_the_title($entry[82]);
	// $client_9 = get_the_title($entry[84]);
	// $client_10 = get_the_title($entry[86]);

	update_post_meta($entry['post_id'], 'roster_client_1', $entry[68]);
	update_post_meta($entry['post_id'], 'roster_client_2', $entry[70]);
	update_post_meta($entry['post_id'], 'roster_client_3', $entry[72]);
	update_post_meta($entry['post_id'], 'roster_client_4', $entry[74]);
	update_post_meta($entry['post_id'], 'roster_client_5', $entry[76]);
	update_post_meta($entry['post_id'], 'roster_client_6', $entry[78]);
	update_post_meta($entry['post_id'], 'roster_client_7', $entry[80]);
	update_post_meta($entry['post_id'], 'roster_client_8', $entry[82]);
	update_post_meta($entry['post_id'], 'roster_client_9', $entry[84]);
	update_post_meta($entry['post_id'], 'roster_client_10', $entry[86]);

	// Update client information
	// $term1 = get_term( $entry[69], 'location_residency' );
	// $location_1 = $term1->name;
	// $term2 = get_term( $entry[71], 'location_residency' );
	// $location_2 = $term2->name;
	// $term3 = get_term( $entry[73], 'location_residency' );
	// $location_3 = $term3->name;
	// $term4 = get_term( $entry[75], 'location_residency' );
	// $location_4 = $term3->name;
	// $term5 = get_term( $entry[77], 'location_residency' );
	// $location_5 = $term5->name;
	// $term6 = get_term( $entry[79], 'location_residency' );
	// $location_6 = $term6->name;
	// $term7 = get_term( $entry[81], 'location_residency' );
	// $location_7 = $term7->name;
	// $term8 = get_term( $entry[83], 'location_residency' );
	// $location_8 = $term8->name;
	// $term9 = get_term( $entry[85], 'location_residency' );
	// $location_9 = $term9->name;
	// $term10 = get_term( $entry[87], 'location_residency' );
	// $location_10 = $term10->name;


	update_post_meta($entry['post_id'], 'roster_location_1', $entry[69]);
	update_post_meta($entry['post_id'], 'roster_location_2', $entry[71]);
	update_post_meta($entry['post_id'], 'roster_location_3', $entry[73]);
	update_post_meta($entry['post_id'], 'roster_location_4', $entry[75]);
	update_post_meta($entry['post_id'], 'roster_location_5', $entry[77]);
	update_post_meta($entry['post_id'], 'roster_location_6', $entry[79]);
	update_post_meta($entry['post_id'], 'roster_location_7', $entry[81]);
	update_post_meta($entry['post_id'], 'roster_location_8', $entry[83]);
	update_post_meta($entry['post_id'], 'roster_location_9', $entry[85]);
	update_post_meta($entry['post_id'], 'roster_location_10', $entry[87]);
	
    //updating post
    wp_update_post( $post );
}

add_action( 'gform_post_submission', 'set_roster_content', 10, 2 );
function set_roster_content( $entry, $form ) {

	$post = get_post( $entry['post_id'] );

    //changing post content
	// $member_1 = get_userdata($entry[89]);
	update_post_meta($entry['post_id'], 'roster_clients_1', $entry[89]);
	update_post_meta($entry['post_id'], 'roster_clients_2', $entry[91]);
	update_post_meta($entry['post_id'], 'roster_clients_3', $entry[92]);
	update_post_meta($entry['post_id'], 'roster_clients_4', $entry[93]);
	update_post_meta($entry['post_id'], 'roster_clients_5', $entry[94]);
	update_post_meta($entry['post_id'], 'roster_clients_6', $entry[95]);
	update_post_meta($entry['post_id'], 'roster_clients_7', $entry[96]);
	update_post_meta($entry['post_id'], 'roster_clients_8', $entry[97]);
	update_post_meta($entry['post_id'], 'roster_clients_9', $entry[98]);
	update_post_meta($entry['post_id'], 'roster_clients_10', $entry[99]);

	update_post_meta($entry['post_id'], 'roster_member_list', $entry[90]);
	

    //updating post
    wp_update_post( $post );

}



add_filter("gform_pre_render_13", "monitor_location_dropdown");
add_filter( 'gform_pre_validation_13', 'monitor_location_dropdown' );
add_filter( 'gform_pre_submission_filter_13', 'monitor_location_dropdown' );
add_filter( 'gform_admin_pre_render_13', 'monitor_location_dropdown' );
function monitor_location_dropdown($form){

?>
    <script type="text/javascript">
    jQuery(document).ready(function($){

        jQuery('#input_13_88').bind('change', function() {
            //get selected value from drop down;
            var selectedValue = jQuery("#input_13_88").val();

			$.ajax({
			    type: 'POST',
			    url: ajaxurl,
			    data: {
		            action:'fatch_client_by_location',
		            selectedValue : selectedValue,
			    },
			    success:function(response){
			    	if (response != null ) {
				        var responsearray = jQuery.parseJSON(response);
				        $('#input_13_89 option:gt(0)').remove();
				        $('#input_13_91 option:gt(0)').remove();
				        $('#input_13_92 option:gt(0)').remove();
				        $('#input_13_93 option:gt(0)').remove();
				        $('#input_13_94 option:gt(0)').remove();
				        $('#input_13_95 option:gt(0)').remove();
				        $('#input_13_96 option:gt(0)').remove();
				        $('#input_13_97 option:gt(0)').remove();
				        $('#input_13_98 option:gt(0)').remove();
				        $('#input_13_99 option:gt(0)').remove();
				        for (var i = responsearray.length - 1; i >= 0; i--) {
				        	$('#input_13_89').append('<option value=' + responsearray[i].value + '>' + responsearray[i].text + '</option>');
				        	$('#input_13_91').append('<option value=' + responsearray[i].value + '>' + responsearray[i].text + '</option>');
				        	$('#input_13_92').append('<option value=' + responsearray[i].value + '>' + responsearray[i].text + '</option>');
				        	$('#input_13_93').append('<option value=' + responsearray[i].value + '>' + responsearray[i].text + '</option>');
				        	$('#input_13_94').append('<option value=' + responsearray[i].value + '>' + responsearray[i].text + '</option>');
				        	$('#input_13_95').append('<option value=' + responsearray[i].value + '>' + responsearray[i].text + '</option>');
				        	$('#input_13_96').append('<option value=' + responsearray[i].value + '>' + responsearray[i].text + '</option>');
				        	$('#input_13_97').append('<option value=' + responsearray[i].value + '>' + responsearray[i].text + '</option>');
				        	$('#input_13_98').append('<option value=' + responsearray[i].value + '>' + responsearray[i].text + '</option>');
				        	$('#input_13_99').append('<option value=' + responsearray[i].value + '>' + responsearray[i].text + '</option>');
				        }			    		
			    	}
			    }
			});


            //populate a text field with the selected drop down value
            // jQuery("#input_167_1").val(selectedValue);
        });
    });
    </script>
<?php

return $form;
}


/* =====================================================================================================
									Employee Time sheet Section
===================================================================================================== */

add_action( 'gform_post_submission_7', 'set_timesheet_client_content', 10, 2 );
function set_timesheet_client_content( $entry, $form ) {

	$post = get_post( $entry['post_id'] );

    //changing post content
	// $member_1 = get_userdata($entry[89]);
	update_post_meta($entry['post_id'], 'timesheet_uploaded_client', $entry[9]);
	

    //updating post
    wp_update_post( $post );

}

// add_filter("gform_pre_render_7", "monitor_client_dropdown");
// add_filter( 'gform_pre_validation_7', 'monitor_client_dropdown' );
// add_filter( 'gform_pre_submission_filter_7', 'monitor_client_dropdown' );
// add_filter( 'gform_admin_pre_render_7', 'monitor_client_dropdown' );
function monitor_client_dropdown($form){

?>
    <script type="text/javascript">
    jQuery(document).ready(function($){

        jQuery('#input_7_6').bind('change', function() {
            //get selected value from drop down;
            var selectedValue = jQuery("#input_7_6").val();

			$.ajax({
			    type: 'POST',
			    url: ajaxurl,
			    data: {
		            action:'fatch_client_by_location_in_timesheet',
		            selectedValue : selectedValue,
			    },
			    success:function(response){
			    	if (response != null ) {
				        var responsearray = jQuery.parseJSON(response);
				        $('#input_7_9 option:gt(0)').remove();
				        for (var i = responsearray.length - 1; i >= 0; i--) {
				        	$('#input_7_9').append('<option value=' + responsearray[i].value + '>' + responsearray[i].text + '</option>');
				        }			    		
			    	}
			    }
			});


            //populate a text field with the selected drop down value
            // jQuery("#input_167_1").val(selectedValue);
        });
    });
    </script>
<?php

return $form;
}


/* ==================================================================================================================================================================================
																		Add timesheet
================================================================================================================================================================================== */

add_action( 'gform_post_submission_24', 'set_timesheet_employee_content', 10, 2 );
add_action( 'gform_post_submission_4', 'set_timesheet_employee_content', 10, 2 );
function set_timesheet_employee_content( $entry, $form ) {
	$post = get_post( $entry['post_id'] );

    //changing post content
	// $member_1 = get_userdata($entry[89]);
	update_post_meta($entry['post_id'], 'clients_1', $entry[2]);
	update_post_meta($entry['post_id'], 'clients_2', $entry[17]);
	update_post_meta($entry['post_id'], 'clients_3', $entry[24]);
	update_post_meta($entry['post_id'], 'clients_4', $entry[31]);
	update_post_meta($entry['post_id'], 'clients_5', $entry[37]);
	update_post_meta($entry['post_id'], 'clients_6', $entry[45]);
	update_post_meta($entry['post_id'], 'clients_7', $entry[49]);
	update_post_meta($entry['post_id'], 'clients_8', $entry[55]);
	update_post_meta($entry['post_id'], 'clients_9', $entry[63]);
	update_post_meta($entry['post_id'], 'clients_10', $entry[67]);
	
	$date = $entry[12];
	$newdate = date("Ymd", strtotime($date));
	update_post_meta($entry['post_id'], 'timesheet_date_1', $newdate);

	$date_2 = $entry[19];
	$newdate_2 = date("Ymd", strtotime($date_2));
	update_post_meta($entry['post_id'], 'timesheet_date_2', $newdate_2);

	$date_3 = $entry[25];
	$newdate_3 = date("Ymd", strtotime($date_3));
	update_post_meta($entry['post_id'], 'timesheet_date_3', $newdate_3);

	$date_4 = $entry[32];
	$newdate_4 = date("Ymd", strtotime($date_4));
	update_post_meta($entry['post_id'], 'timesheet_date_4', $newdate_4);

	$date_5 = $entry[38];
	$newdate_5 = date("Ymd", strtotime($date_5));
	update_post_meta($entry['post_id'], 'timesheet_date_5', $newdate_5);

	$date_6 = $entry[46];
	$newdate_6 = date("Ymd", strtotime($date_6));
	update_post_meta($entry['post_id'], 'timesheet_date_6', $newdate_6);

	$date_7 = $entry[50];
	$newdate_7 = date("Ymd", strtotime($date_7));
	update_post_meta($entry['post_id'], 'timesheet_date_7', $newdate_7);

	$date_8 = $entry[56];
	$newdate_8 = date("Ymd", strtotime($date_8));
	update_post_meta($entry['post_id'], 'timesheet_date_8', $newdate_8);

	$date_9 = $entry[64];
	$newdate_9 = date("Ymd", strtotime($date_9));
	update_post_meta($entry['post_id'], 'timesheet_date_9', $newdate_9);

	$date_10 = $entry[68];
	$newdate_10 = date("Ymd", strtotime($date_10));
	update_post_meta($entry['post_id'], 'timesheet_date_10', $newdate_10);

    //updating post
    wp_update_post( $post );

}