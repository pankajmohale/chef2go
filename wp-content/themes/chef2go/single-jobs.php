<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package _s
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<div class="row">
			<div id="main" class="col-xs-12 col-sm-9 site-main" role="main">

			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'job' );

				//the_post_navigation();
	
			// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>

			</div><!-- #main -->
			<div id="sidebar" class="sidebar col-xs-12 col-sm-3">
				<?php get_sidebar(); ?>
			</div>			
		</div>
	</div><!-- #primary -->

<?php
get_footer();