<?php
/* Staff Myarea Template */
?>
<?php 
global $redux_demo;
$current_user = wp_get_current_user(); ?>
		<div class="row">
			<div class="col-xs-12">
				<div class="section-title text-center">
					<h2>Welcome to Chefs2Go Team.</h2>
					<h5>At the beginning of the week please print off a blank PDF timesheet. Making Sure you take the blank timesheet, to each establishment you work at in a week. Please don’t put multiple establishments on one timesheets.</h5>
					<?php $upload_dir = wp_upload_dir(); ?>
					<a class="btn custom-btn blank-pdf-btn btn-blank-timesheet" href="<?php echo $upload_dir['baseurl'].'/timesheets.pdf'; ?>" download>Generate Blank PDF</a>
				</div>
			</div>
		</div>
		<?php echo "<div class='fill_timesheet'>";
			echo do_shortcode('[gravityform id="4" title="false" description="false" ajax="false"]');
		echo "</div>";
		?>
		<?php
		$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
		$timesheet_args = array(
		    'post_type' => 'employee_timesheet',
		    'author' => $current_user->ID,
		    'posts_per_page' => 8,
            'paged' => $paged,
		);
		?>
		<div class="user-timesheet-wrapper">
			<table class="user-timesheet" border="1" cellpadding="10">
				<?php 

				// the query
				$timesheet_query = new WP_Query( $timesheet_args ); ?>

				<?php if ( $timesheet_query->have_posts() ) : ?>
				<tbody>
						<tr>
							<th>Locations</th>
							<th>Date</th>
							<th>Start Time</th>
							<th>End Time</th>
							<th>Total Hours</th>										
						</tr>

					    <!-- pagination here -->

					    <!-- the loop -->
					    <?php while ( $timesheet_query->have_posts() ) : $timesheet_query->the_post(); ?>
					        <?php 

					        $clinedet = get_post_meta( get_the_ID(), false );
					        
					        for ($timesheet_meta = 1; $timesheet_meta < 11; $timesheet_meta++) { 
						        
						        $clientkey = 'clients_'.$timesheet_meta;
								$clientval = get_post_meta( get_the_ID(), $clientkey, true );						        
						        if ($clientval) {
							        $datekey = 'timesheet_date_'.$timesheet_meta;
							        $startkey = 'start_time_'.$timesheet_meta;
							        $endkey = 'end_time_'.$timesheet_meta;
							        show_timesheet($clientkey, $datekey, $startkey, $endkey );
							        $totaltime += difference_calculator($startkey, $endkey );
						        }
					        	
					        }

							// $client = get_post_meta( get_the_ID(), 'clients', true );
							// $date = get_post_meta( get_the_ID(), 'timesheet_date', true );

							// $start_time = get_post_meta( get_the_ID(), 'start_time', true );
							// $end_time = get_post_meta( get_the_ID(), 'end_time', true );

							// $starttime = get_post_meta( get_the_ID(), 'start_time', true );
							// $endtime = get_post_meta( get_the_ID(), 'end_time', true );

							// $start = array_filter(explode(' ',str_replace(':',' ',$starttime)));
							// $end = array_filter(explode(' ',str_replace(':',' ',$endtime)));
							// $newDate = date("m/d/Y", strtotime($date));
							
							// // convert date and time arrays into datetime formats
							// $startdate = date_create_from_format('m/d/Y@h:i a', $newDate . "@". $start[0].":".$start[1]." ".$start[2]);
							// $enddate =   date_create_from_format('m/d/Y@h:i a', $newDate . "@". $end[0].":".$end[1]." ".$end[2]);

							// //convert datetimes into seconds to compare
							// $starttime = strtotime($startdate->format('Y-m-d H:i:s'));
							// $endtime = strtotime($enddate->format('Y-m-d H:i:s'));

							// // check to see if the times span overnight
							// if($starttime > $endtime)
							// $endtime = strtotime($enddate->format('Y-m-d H:i:s') . " +1 day");

							// // perform calculation
							// $diff = floor(($endtime - $starttime)/60);

							// $totaldiff = $diff/60;
							// $totaldiff = explode(".",$totaldiff);

							// $totaltime += $diff;

							// if ($diff >= 60 ) {
							// 	$time = $diff/60 . ' Hours';
							// } else {
							// 	$time = $diff . ' Minutes';									
							// }					
						?>
							<!-- <tr>
								<td><?php 
									// location/ client name
									$term = get_term_by('term_id', $client, 'client_location');
									echo $term->name;
								?></td>
								<td><?php echo date("d-m-Y", strtotime($date)); ?></td>
								<td><?php echo $start_time; ?></td>
								<td><?php echo $end_time; ?></td>
								<td><?php echo $totaldiff[0] . ' Hours ' . $diff%60 . ' Minutes' ; ?> <span class="delete-timedheet pull-right" timesheet-data="<?php echo get_the_ID(); ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></span><a href="<?php echo site_url().'/edit-timesheet/?timesheet_id='.get_the_ID();?>" class="edit-timesheet-btn pull-right">Edit</a></td>
							</tr> -->


					    <?php endwhile; ?>
					    <!-- end of the loop -->


				</tbody>
				<tbody align="center">
					<?php 
						$totalhourswork = $totaltime/60;
						$totaltime = explode(".",$totalhourswork);

						function count_digit($number) {
							return strlen((string) $number);
						}
						$num = $totaltime[1]*0.6;
						$number_of_digits = count_digit($num); //this is call :)
						if ($number_of_digits == 1) {
							$minutes_val = $num.'0'; 
						} else {
							$minutes_val = $num;
						}
					?>
					<tr>
						<td colspan="3">Total Work:</td>
						<td colspan="2"><?php echo $totaltime[0] . ' Hours ' . $minutes_val . ' Minutes' ; ?></td>
						<?php 
						?>
					</tr>
				</tbody>
					    <!-- pagination here -->

					    <?php if (function_exists("pagination")) {
						    pagination($timesheet_query->max_num_pages);
						} ?>

					    <?php wp_reset_postdata(); ?>

					<?php else : ?>
					    <h2><?php _e( 'Sorry, no timesheets added in your criteria.' ); ?></h2>
					<?php endif; ?>
			</table>
		</div>
		<!-- Search entries form -->
		<div class="search-entries-wrapper clearfix">
			<div class="row">
				<div class="col-xs-12">
					<div class="section-title text-center">
						<h2>Report Generation</h2>
						<h5>Please specify the location and time to download the timesheet.</h5>
					</div>
				</div>
			</div>
			<div id="search-entries" class="row" method="post">
				<div class="form-group hide-location">
					<div class="col-xs-12 col-sm-3 col-md-4">
						<label for="searchlocation" class="col-form-label">Location</label>
						<select class="form-control searchlocation" name="searchlocation" id="searchlocation" aria-describedby="searchlocationHelp">
							<option value="">Select Location</option>
							<?php 
							// foreach ($entries as $value) {
							// 	echo '<option value="'.$value[2].'">'.$value[2].'</option>';
							// }
							?>	
						<?php 
						// the query
						$args = array(
							'post_type' => 'locations',
							'posts_per_page' => -1,
							'orderby' => 'title',
							'order'   => 'ASC',
						);

						$the_query = new WP_Query( $args ); ?>

						<?php if ( $the_query->have_posts() ) : ?>

							<!-- pagination here -->

							<!-- the loop -->
							<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
								<option value="<?php the_id(); ?>"><?php the_title(); ?></option>
							<?php endwhile; ?>
							<!-- end of the loop -->

							<!-- pagination here -->

							<?php wp_reset_postdata(); ?>

						<?php else : ?>
							<p><?php _e( 'Sorry, no location matched your criteria.' ); ?></p>
						<?php endif; ?>
						</select>						
					</div>
				</div>
				<div class="form-group">
					<div class="col-xs-12 col-sm-2 col-md-2">
						<label for="searchfrominput" class="col-form-label">From</label>
						<input class="form-control datepicker searchfrominput" type="text" name="searchfrominput" id="searchfrominput" value="" aria-describedby="starttimeHelp">
					</div>
				</div>
				<div class="form-group">
					<div class="col-xs-12 col-sm-2 col-md-2">
						<label for="searchtoinput" class="col-form-label">To</label>
						<input class="form-control datepicker searchtoinput" type="text" name="searchtoinput" id="searchtoinput" value="" aria-describedby="endtimeHelp">
					</div>
				</div>
				<div class="form-group button-wrapper">
					<div class="col-xs-12 col-sm-5 col-md-5">
						<label for="searchtoinput" class="col-form-label custom-label">&nbsp;</label>
						<button class="btn custom-btn-pdf search_entries">Generate</button>
						<?php $upload_dir = wp_upload_dir(); ?>
						<a class="btn custom-btn blank-pdf-btn" href="<?php echo $upload_dir['baseurl'].'/timesheets.pdf'; ?>" download>Generate Blank PDF</a>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-12">
					<h5 class="sample-text">After downloading/printing, please get it signed from your host employer and upload the signed form below.</h5>
				</div>
			</div>
		</div>
		<div id="editor" class="search_entries_result"></div>
		<!-- <div class="downloal-print-btn clearfix">
			<div class="print-btn hide-btn printcmd download-pdf col-sm-2"><div class="pdflogo"></div>Download PDF</div>
		</div> -->

		<div class="myarea-download-btn">
			<div id="myeditor"></div>
			<button id="mycmd" class="btn custom-btn">Generate Prefilled Timesheet</button>
			<div class="mycmd-print-btn col-sm-2"><?php echo do_shortcode('[print-me target="div.search_entries_result" title="Print"]'); ?></div>
			<!-- <?php $upload_dir = wp_upload_dir(); ?>
			<a class="btn custom-btn blank-pdf-btn" href="<?php echo $upload_dir['baseurl'].'/timesheets.pdf'; ?>" download>Generate Blank PDF</a> -->
		</div>


		<div class="chefliastingeditor-download-btn">
			<div id="chefliastingeditor"></div>
			<button id="chefliastingcmd" class="btn custom-btn">Generate Prefilled Timesheet</button>
			<div class="chefliastingcmd-print-btn col-sm-2"><?php echo do_shortcode('[print-me target="div.search_entries_result" title="Print"]'); ?></div>								
			<!-- <?php $upload_dir = wp_upload_dir(); ?>
			<a class="btn custom-btn blank-pdf-btn" href="<?php echo $upload_dir['baseurl'].'/timesheets.pdf'; ?>" download>Generate Blank Timesheet</a> -->
		</div>

		<div id="" class="clearfix">
			<div class="row">
				<div class="col-xs-12">
					<div class="section-title text-center">
						<h2>Upload Timesheet</h2>
					</div>
				</div>
			</div>
			<div class="upload-document"><?php echo do_shortcode('[gravityform id="7" title="false" description="false" ajax="true"]'); ?></div>		
		</div>

		<div class="added-roster-display">
			<div class="row">
				<div class="col-xs-12">
					<div class="section-title text-center">
						<h2>Allocated Roster List</h2>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="row">
						<?php 
						// the query
						$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
						$args = array(
							'post_type' => 'rosters',
							'posts_per_page' => 4,
							'meta_query' => array(
								array(
									'key'     => 'roster_member_list',
									'value'   => $current_user->ID,
									'compare' => 'LIKE',
								),
							),
							'paged' => $paged,
						);
						$the_query = new WP_Query( $args ); ?>

						<?php if ( $the_query->have_posts() ) : ?>

							<!-- pagination here -->

							<!-- the loop -->
							<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
								<div class="col-md-6">
									<div class="employee-roster-wrapper equalheight">
									<span class="corner-top-left"></span>
									<span class="corner-top-right"></span>
									<span class="corner-top-right delete-roster" data-postid="<?php echo get_the_ID(); ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></span>
									<span class="corner-bottom-left"></span>
									<span class="corner-bottom-right"></span>
										<div class="roster-title">
											<div class="row">
												<div class="col-sm-12">
													<div class="text-center">
														<div class="rosterheading"><h3><?php the_title(); ?></h3></div>
														<!-- <div class="roster-client-name"><h6><?php // echo get_the_title( $roster_clients ); ?></h6></div> -->
													</div>
												</div>										
											</div>
										</div>
										<div class="roster-details">
											<?php 
												for ($rostermeta = 1; $rostermeta <= 10; $rostermeta++) { 
													$daymetakey = 'roster_week_day_'.$rostermeta;
													$startmetakey = 'roster_time_start_'.$rostermeta;
													$endmetakey = 'roster_time_finish_'.$rostermeta;
													$clientsmetakey = 'roster_clients_'.$rostermeta;

													$daymetakey = get_post_meta( get_the_ID(), $daymetakey, true );
													$startmetakey = get_post_meta( get_the_ID(), $startmetakey, true );
													$endmetakey = get_post_meta( get_the_ID(), $endmetakey, true );
													$roster_clients = get_post_meta( get_the_ID(), $clientsmetakey, true ); 
												if ($startmetakey && $startmetakey && $endmetakey) {
												?>
												<div class="row">
													<div class="roster-timeing">
														<div class="col-sm-3 text-left"><?php echo $daymetakey; ?></div>
														<div class="col-sm-4 text-left"><?php echo get_the_title( $roster_clients ); ?></div>
														<div class="col-sm-5 text-right"><?php echo $startmetakey; ?> to <?php echo $endmetakey; ?></div>											
													</div>
												</div>
											<?php	} 
												} /* loop end */
											?>										
										</div>
									</div>
								</div>
							<?php endwhile; ?>
							<!-- end of the loop -->

							<!-- pagination here -->
							<?php if (function_exists("pagination")) {
							    pagination($the_query->max_num_pages);
							} ?>

							<?php wp_reset_postdata(); ?>

						<?php else : ?>
							<div class="col-sm-12">
								<h4><?php _e( 'Sorry, no rosters added your criteria.' ); ?></h4>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>