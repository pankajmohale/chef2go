<?php
/* Staff Myarea Template */
?>
<?php 
	global $redux_demo;
	$current_user = wp_get_current_user();
?>

	<?php if( have_rows('document_upload', 'user_'. $current_user->ID) ): ?>
		<div class="documents-section">
			<div class="row">
				<div class="col-xs-12">
					<div class="section-title text-center">
						<h2>Employee Documents</h2>
						<h5>Please download all documents below:  </h5>
						<h5> Please do the following, fill in the Staff Details and Tax Declaration, and Sign the Health Declaration and the last page of the Handbook and attach below: </h5>
<h5>The Handbook is for your reference of our company policies and procedures. While the Fair Work Statement is for your reference only.</h5>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<div class="download_docuemt_link">
							<div class="list-group">
							    <?php while( have_rows('document_upload', 'user_'. $current_user->ID) ): the_row(); ?>

							      	<a href="<?php the_sub_field('upload_document'); ?>" class="list-group-item" download><span class="badge list-bg"><i class="fa fa-download" aria-hidden="true"></i></span><?php the_sub_field('upload_document_title'); ?></a>

							    <?php endwhile; ?>
							</div>

					</div>				
				</div>
				<div class="col-sm-6"></div>
			</div>
			<div class="clearfix">
				<div class="upload-document"><?php echo do_shortcode('[gravityform id="9" title="false" description="false" ajax="true"]'); ?></div>		
			</div>
		</div>
	<?php endif; ?>
	<div class="uploaded-timesheet">
		<div class="row">
			<div class="col-xs-12">
				<div class="section-title text-center">
					<h2>Uploaded Timesheet</h2>
				</div>
			</div>
		</div>
		<div class="row">
			<?php 
				$timesheet_args = array(
			        'post_type' => 'timesheet',
			        'posts_per_page' => -1,
			        'author' => $current_user->ID,
			    );
				// the query
				$timesheet_query = new WP_Query( $timesheet_args );
			?>
			<?php if ( $timesheet_query->have_posts() ) : ?>
				<div class="user-timesheet-list-wrapper">
					<!-- pagination here -->
					<ul class="user-timesheet-list list-group">
						<!-- the loop -->
						<?php while ( $timesheet_query->have_posts() ) : $timesheet_query->the_post(); ?>
								<div class="col-sm-6 col-xs-12">
									<li class="list-group-item clearfix">
										<div class="download_link">
											<?php $timesheet_uploaded_sheet_value = get_post_meta( get_the_ID(), 'timesheet_uploaded_sheet', true ); ?>
											<a href='<?php echo $timesheet_uploaded_sheet_value; ?>' download>Download</a>
										</div>
										<div class="download_date">
											<?php $timesheet_uploaded_date = get_post_meta( get_the_ID(), 'timesheet_uploaded_date', true ); ?>
											<?php 
											if ($timesheet_uploaded_date) {
												$entrival = explode("/", $timesheet_uploaded_date);

												$fromvalue = $entrival[0];
												$tovalue = $entrival[1];
												echo $fromvalue.' to '. $tovalue; 												
											} ?>
										</div>
									</li>
								</div>							
						<?php endwhile; ?>
						<!-- end of the loop -->
					</ul>

					<!-- pagination here -->
				</div>

				<?php wp_reset_postdata(); ?>

			<?php else : ?>
				<div class="col-xs-12"><p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p></div>
			<?php endif; ?>
		</div>		
	</div>								
