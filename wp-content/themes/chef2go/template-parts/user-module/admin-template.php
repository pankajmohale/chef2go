<?php
/* Staff Myarea Template */
?>
<?php 
	global $redux_demo;
	$current_user = wp_get_current_user();
?>
	<div class="admin-area-module">
		<div class="col-xs-6">
			<div class="myarea-page-links text-center">
				<a href="<?php echo site_url(); ?>" class="btn custom-btn">Home</a>
			</div>
		</div>
		<div class="col-xs-6">
			<div class="myarea-page-links text-center">
				<a href="<?php echo site_url().'/wp-admin'; ?>" class="btn custom-btn">Admin Backend</a>
			</div>
		</div>
	</div>
