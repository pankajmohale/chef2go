<?php 
/* Add Staff Page Template */
// This page Template is use for Adding staff into roster.
?>
<div class="staff-form clearfix">
	<?php 
	// This Query is for get already present data in recruiter_selected_staff meta box and made array.
	$current_user = wp_get_current_user();
	$recruiterargs = array(
		'post_type' => 'recruiter',
		'post_status' => array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash'),
	); 
	// the query
	$the_query = new WP_Query( $recruiterargs ); 
	?>

	<?php if ( $the_query->have_posts() ) : ?>

		<!-- pagination here -->

		<!-- the loop -->
		<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
			<?php
				$recruiter_user_id = get_post_meta(get_the_ID(),'recruiter_user_id',true);
				$recruiter_selected_staff_val = get_post_meta(get_the_ID(),'recruiter_selected_staff',true);
				$already_val = unserialize($recruiter_selected_staff_val);
				if (!empty($already_val)) {
					if ($current_user->ID == $recruiter_user_id) {
						foreach ($already_val as $key => $value) {
							$user_id[] = $value['u_id'];
						}
					}					
				}
			?>							
		<?php endwhile; ?>
		<!-- end of the loop -->

		<!-- pagination here -->

		<?php wp_reset_postdata(); ?>

	<?php else : ?>
		<p><?php _e( 'Sorry, no recruiter matched your criteria.' ); ?></p>
	<?php endif; ?>

	<div class="col-xs-12 col-sm-4 col-md-5">
		<div class="form-group">
			<label for="addstaffuser" class="col-form-label">Staff Members Name</label>
			<select class="form-control addstaffuser" name="addstaffuser" id="addstaffuser">
				<option value="">Select User</option>

				<?php
					$employee_user_query = new WP_User_Query(
						array(
							'role__in'	=>	array( 'staff_employee', 'chef_employee' ),
						)
					);
					$user_data = $employee_user_query->get_results();
					
					// Check for results
					if (!empty($user_data))
					{
					    // loop trough each author
					    foreach ($user_data as $author)
					    {
					    	// Check that if user meta is already updated 
					    	if (in_array($author->ID, $user_id)) { 
					    		
					    	} else {
						        // Show all not updated user's data
						        $author_info = get_userdata($author->ID);
						        echo '<option value="'. $author->ID .'">'. $author_info->first_name.'</option>';
					    	}
					    }
					} else {
					    echo 'No authors found';
					}
				?>				
			</select>
		</div>
	</div>
	<div class="col-xs-12 col-sm-4 col-md-4">
		<div class="form-group">
			<label for="addstaffuserrole" class="col-form-label">Staff Members Role</label>
			<input class="form-control datepicker addstaffuserrole" type="text" name="addstaffuserrole" id="addstaffuserrole" value="">
		</div>
	</div>
	<div class="col-xs-12 col-sm-3 col-sm-3">
		<div class="form-group add-staff-button-wrapper">
			<label for="addstaffmember" class="col-form-label custom-label">&nbsp;</label>
			<button class="btn custom-btn-pdf addstaffmember">Add Staff Member</button>
		</div>
	</div>
</div>