<?php
/* recruiter myarea Template */
?>
<?php 
global $redux_demo;
$current_user = wp_get_current_user();
?>
	<div class="row">
		<div class="col-sm-4 col-md-4">
			<div class="find-staff">
				<div class="chef-section-title">
					<h3>Find Staff</h3>
				</div>
				<div class="find-staff-links">
					<div class="staff-links-listing">
						<div class="list-group">
							<a href="<?php echo get_page_link($redux_demo['recruiter_chefs_full_time_work']); ?>" class="list-group-item">
							<span class="badge"><i class="fa fa-angle-right" aria-hidden="true"></i></span>
							Chefs Seeking Full Time Work</a>

							<a href="<?php echo get_page_link($redux_demo['recruiter_chefs_part_time_work']); ?>" class="list-group-item">
							<span class="badge"><i class="fa fa-angle-right" aria-hidden="true"></i></span>
							Chefs Seeking Part Time Work</a>
							
							<a href="<?php echo get_page_link($redux_demo['recruiter_freelancing_chefs']); ?>" class="list-group-item">
							<span class="badge"><i class="fa fa-angle-right" aria-hidden="true"></i></span>
							Find a Freelance Chef</a>
							
							<a href="<?php echo get_page_link($redux_demo['recruiter_staff_full_time_work']); ?>" class="list-group-item">
							<span class="badge"><i class="fa fa-angle-right" aria-hidden="true"></i></span>
							Hospitality Staff Seeking Full Time Work</a>
							
							<a href="<?php echo get_page_link($redux_demo['recruiter_staff_part_time_work']); ?>" class="list-group-item">
							<span class="badge"><i class="fa fa-angle-right" aria-hidden="true"></i></span>
							Hospitality Staff Seeking Part Time Work</a>
							
							<a href="<?php echo get_page_link($redux_demo['recruiter_freelanceing_staff']); ?>" class="list-group-item">
							<span class="badge"><i class="fa fa-angle-right" aria-hidden="true"></i></span>
							Find Freelance Hospitality Staff</a>
							
							<a href="<?php echo get_page_link($redux_demo['job_posting']); ?>" class="list-group-item">
							<span class="badge"><i class="fa fa-angle-right" aria-hidden="true"></i></span>
							Job Posting</a>
							
							<a href="<?php echo get_page_link($redux_demo['massage_page_url']); ?>" class="list-group-item">
							<span class="badge"><i class="fa fa-angle-right" aria-hidden="true"></i></span>
							Messages</a>													
						</div>
					</div>												
				</div>
			</div>
		</div>
		<div class="col-sm-8 col-md-8">
			<?php echo do_shortcode('[front-end-pm]'); ?>
		</div>								
	</div>