<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package _s
 */

?>

<?php 
	$current_user_id = get_current_user_id();
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php
		if ( is_single() ) :
			the_title( '<h1 class="entry-title">', '</h1>' );
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;

		if ( 'post' === get_post_type() ) : ?>
		<div class="entry-meta">
			<?php _s_posted_on(); ?>
		</div><!-- .entry-meta -->
		<?php
		endif; ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
			the_content( sprintf(
				/* translators: %s: Name of current post. */
				wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', '_s' ), array( 'span' => array( 'class' => array() ) ) ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			) );

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', '_s' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<?php
	// Apply now button for user login and logout user
	if ( is_user_logged_in() ) { 				
				
	?>
			
	<?php
		$current_user_id = get_current_user_id();

		$user_meta = get_userdata($current_user_id);
		foreach ($user_meta->roles as $key => $user_role) {
			if ($user_role == 'chef_employee' || $user_role == 'staff_employee') { ?>
			<?php 
				$applied_job_val = get_user_meta( $current_user_id,  'applied_job', true);
				$applied_jobArray = explode(',', $applied_job_val);
				if (in_array(get_the_ID(), $applied_jobArray)) { ?>
					<button type="button" class="btn custom-btn pull-right" data-toggle="modal" disabled>Already applied</button>
				<?php } else { ?>
					<button type="button" class="btn custom-btn pull-right job_apply" data-toggle="modal" data-target=".bs-example-modal-sm-<?php echo get_the_ID(); ?>" applied_post_id="<?php echo get_the_ID(); ?>" applied_user="<?php echo $current_user_id; ?>">Apply Now</button>
			<?php	}
			?>

		<?php	
			}
		}
	 ?>

	<?php } else { ?>

		<div class="apply-btn clearfix">
		    <!-- Small modal -->
			<a class="btn custom-btn pull-right" href="<?php echo esc_url( get_permalink( get_page_by_title( 'Login' ) ) ); ?>"><?php esc_html_e( 'Apply Now', 'textdomain' ); ?></a>			
		</div>

	<?php } ?>

	<footer class="entry-footer">
		<?php _s_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
