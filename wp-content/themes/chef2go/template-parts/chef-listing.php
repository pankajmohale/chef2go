<?php
/**
 * Template part for displaying chef listing content in template-chef-listing.php.
 *
 */
?>
	<div class="panel-group chef-listing" id="accordion" role="tablist" aria-multiselectable="true">
		<div class="panel panel-default">
			<div class="panel-heading" role="tab" id="collapse<?php echo get_the_id(); ?>">
				<h4 class="panel-title clearfix">
					<div class="col-xs-10 col-sm-10 col-md-10">
						<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_<?php echo get_the_id(); ?>" aria-expanded="true" aria-controls="collapse_<?php echo get_the_id(); ?>">
							<?php the_title(); ?>
						</a>						
					</div>
					<div class="col-xs-2 col-sm-2 col-md-2">
						<?php if (has_post_thumbnail()) { ?>
							<img src="<?php echo the_post_thumbnail_url( array(60, 60) ); ?>" class="img-responsive"/>
						<?php } else { ?>
							<img src="<?php echo get_template_directory_uri() . '/img/chef-placeholder.jpg'; ?>" class="img-responsive"/>
						<?php } ?>
					</div>
				</h4>
	    	</div>
			<div id="collapse_<?php echo get_the_id(); ?>" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="collapse<?php echo get_the_id(); ?>">
				<div class="panel-body">
					<div class="recruitment-type-outer chef-listing-outer">
						<div class="chef-listing-inner">
							<div class="chef-listing-title">
								<strong>Seeking work as : </strong>										
							</div>
							<div class="chef-listing-content">
								<?php 
									$term_list = wp_get_post_terms(get_the_id(), 'type_of_recruitment', array("fields" => "all"));
									$term_list_count = count($term_list);
									$i = 0;
									foreach($term_list as $term_single) {
										if(++$i === $term_list_count) {
											echo $term_single->name;
										}else{
											echo $term_single->name.', ';
										}
									}
								?>										
							</div>									
						</div>
					</div>
					<div class="chef-level-outer chef-listing-outer">
						<div class="chef-listing-inner">
							<div class="chef-listing-title">
								<strong>Chef Level : </strong>										
							</div>
							<div class="chef-listing-content">
								<?php 
									$field_of_interest_list = wp_get_post_terms(get_the_id(), 'field_of_interest', array("fields" => "all"));
									$interest_single_list_count = count($field_of_interest_list);
									$i = 0;
									foreach($field_of_interest_list as $interest_single) {
										if(++$i === $interest_single_list_count) {
											echo $interest_single->name;
										}else{
											echo $interest_single->name.', ';
										}
									}
								?>														
							</div>
						</div>
					</div>
					<div class="chef-skill-outer chef-listing-outer">
						<div class="chef-listing-inner">
							<div class="chef-listing-title">
								<strong>Skill Level : </strong>
							</div>
							<div class="chef-listing-content">
								<?php 
									$chef_skill_list = wp_get_post_terms(get_the_id(), 'chef_skill', array("fields" => "all"));
									$chef_skill_list_count = count($chef_skill_list);
									$i = 0;
									foreach($chef_skill_list as $chef_skill_single) {
										if(++$i === $chef_skill_list_count) {
											echo $chef_skill_single->name;
										}else{
											echo $chef_skill_single->name.', ';
										}
									}
								?>														
							</div>
						</div>
					</div>

					<div class="specialising-in-outer chef-listing-outer">
						<div class="chef-listing-inner">
							<div class="chef-listing-title">
								<strong>Specialises In : </strong>
							</div>
							<div class="chef-listing-content">
								<?php 
									$specialising_in_list = wp_get_post_terms(get_the_id(), 'specialising_in', array("fields" => "all"));
									$specialising_in_list_count = count($specialising_in_list);
									$i = 0;
									foreach($specialising_in_list as $specialising_in_single) {
										if(++$i === $specialising_in_list_count) {
											echo $specialising_in_single->name;
										}else{
											echo $specialising_in_single->name.', ';
										}
									}
								?>														
							</div>
						</div>
					</div>

					<div class="work-history-outer chef-listing-outer">
						<div class="chef-listing-inner">
							<div class="chef-listing-title">
								<strong>Work History : </strong>
							</div>
							<div class="chef-listing-content">
								<p><?php the_content(); ?></p>
							</div>
						</div>
					</div>

					<div class="hourly-rate-outer chef-listing-outer">
						<div class="chef-listing-inner">
							<div class="chef-listing-title">
								<strong>Freelance Hourly Rate : </strong>
							</div>
							<div class="chef-listing-content">
								<?php 
									$_hourly_rate = get_post_meta(get_the_id(),'freelance_hourly_rate',true);
								?>
								<?php echo $_hourly_rate; ?>
							</div>
						</div>
					</div>

					<div class="remote-locations-outer chef-listing-outer">
						<div class="chef-listing-inner">
							<div class="chef-listing-title">
								<strong>Can work in remote locations : </strong>
							</div>
							<div class="chef-listing-content">
								<?php 
									$_remote_locations = get_post_meta(get_the_id(),'work_in_remote_locations',true);
								?>
								<?php echo $_remote_locations; ?>
							</div>
						</div>
					</div>

					<div class="citizenship-visa-outer chef-listing-outer">
						<div class="chef-listing-inner">
							<div class="chef-listing-title">
								<strong>Citizen Status : </strong>
							</div>
							<div class="chef-listing-content">
								<?php 
									$citizenship_visa_list = wp_get_post_terms(get_the_id(), 'citizenship_visa', array("fields" => "all"));
									$citizenship_visa_list_count = count($specialising_in_list);
									$i = 0;
									foreach($citizenship_visa_list as $citizenship_visa_single) {
										if(++$i === $citizenship_visa_list_count) {
											echo $citizenship_visa_single->name;
										}else{
											echo $citizenship_visa_single->name.', ';
										}
									}
								?>				
							</div>
						</div>
					</div>

					<?php $current_user = wp_get_current_user(); ?>
					<?php 
					foreach ($current_user->roles as $current_user_value) {
						$current_user_role = $current_user_value;
					}
					if ($current_user_role == 'recruiter') { ?> 
					<div class="citizenship-visa-outer chef-listing-outer">
						<div class="chef-listing-inner">
							<div class="chef-listing-title">
								<strong>Email ID : </strong>
							</div>
							<div class="chef-listing-content">
								<i class="fa fa-envelope-o" aria-hidden="true"></i> <a href="mailto:<?php echo get_post_meta($post->ID,'mail_id',true); ?>">
								<?php echo get_post_meta($post->ID,'mail_id',true); ?>
							</a>			
							</div>
						</div>
					</div>
					<div class="citizenship-visa-outer chef-listing-outer">
						<div class="chef-listing-inner">
							<div class="chef-listing-title">
								<strong>Phone Number: </strong>
							</div>
							<div class="chef-listing-content">
								<i class="fa fa-phone" aria-hidden="true"></i> <a href="tel:<?php echo get_post_meta($post->ID,'user_phone_number',true);?>"><?php echo get_post_meta($post->ID,'user_phone_number',true);?></a>
							</a>			
							</div>
						</div>
					</div>
					<div class="citizenship-visa-outer chef-listing-outer">
						<div class="chef-listing-inner">
							<div class="chef-listing-title">
								<strong>Resume: </strong>
							</div>
							<div class="chef-listing-content">
								<i class="fa fa-file-text-o" aria-hidden="true"></i> <a href="<?php echo get_post_meta($post->ID,'new_resume',true);?>" download>Download a resume</a>			
							</div>
						</div>
					</div>
					<?php	} ?>

				</div>
			</div>
	  	</div>
	</div>