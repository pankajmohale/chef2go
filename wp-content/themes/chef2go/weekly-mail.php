<?php

/**
 * Tell WordPress we are doing the CRON task.
 *
 * @var bool
 */
 
define('DOING_CRON', true);

if ( !defined('ABSPATH') ) {
	/** Set up WordPress environment */
	require_once( '/home/rgbastud/public_html/chefs2go/wp-load.php' );
}

require('/home/rgbastud/public_html/chefs2go/wp-content/themes/chef2go/mpdf60/mpdf.php');
$timesheet_value = weekly_mail_timesheet_function(); 

function weekly_mail_timesheet_function(){
	
	// $monday = date('Y-m-d',strtotime('-1 Monday'));
	// $sunday = date('Y-m-d',strtotime('-0 Sunday'));
	// $tovalue = date('d-m-Y');
	
	$fromvalue = date('Y-m-d',strtotime('-2 Monday'));
	$tovalue = date('Y-m-d',strtotime('last sunday'));
	// $fromvalue = '28-8-2017';
	// $tovalue = '1-9-2017';
	
	$newfromvalue = date("Ymd", strtotime($fromvalue));
	$newtovalue = date("Ymd", strtotime($tovalue));

	$timesheet_args = array(
		'post_type' => 'employee_timesheet',
		'posts_per_page' => -1,
	);

	// the query
	$timesheet_query = new WP_Query( $timesheet_args ); 
		if ( $timesheet_query->have_posts() ) :
		$emp_value = '<div id="allemployeelisting">';
		$emp_value = '<div style="text-align: center;margin-bottom:20px;"><img src="/home/rgbastud/public_html/chefs2go/logo.png" width="250" style="vertical-align: middle; text-align: center;" /></div>';
			$emp_value .= '<div class="timesheet-content">';
				while ( $timesheet_query->have_posts() ) : $timesheet_query->the_post();
					for ($timesheet_meta = 1; $timesheet_meta < 11; $timesheet_meta++) { 
						    $datekey = 'timesheet_date_'.$timesheet_meta;
					        $timesheet_date = get_post_meta( get_the_ID(), $datekey, true );
					        if ($timesheet_date >= $newfromvalue && $timesheet_date <= $newtovalue ) { 
			        			$author_id = get_the_author_meta('ID');
			        			$author_id_array[] = $author_id;
			        			$author_unique_id_array = array_unique($author_id_array);
				        }
				    }
				endwhile;
				$emp_value .= '<table border="1" cellpadding="10" style="width: 100%;">';
					$emp_value .= '<tbody align="center">';
				if ($author_unique_id_array) {
					foreach ($author_unique_id_array as $key => $author_id) {
							$totaltime = '';
	        				$user_info = get_userdata($author_id);
	        				$emp_value .= '<tr>';
							$emp_value .= '<th colspan="5" style="text-align: center;font-size: 18px;padding: 10px 0;">';
							$emp_value .= $user_info->nickname.' ( '.$user_info->user_login.' )';
							$emp_value .= '</th>';
							$emp_value .= '</tr>';
							$emp_value .= '<tr>';
							$emp_value .= '<th style="text-align: center;">Location</th>';
							$emp_value .= '<th style="text-align: center;">date</th>';
							$emp_value .= '<th style="text-align: center;">Start</th>';
							$emp_value .= '<th style="text-align: center;">Finish</th>';
							$emp_value .= '<th style="text-align: center;">Daily Total HRS Worked</th>';
							$emp_value .= '</tr>';
					        while ( $timesheet_query->have_posts() ) : $timesheet_query->the_post();
			        			$authorid = get_the_author_meta('ID');
					        	$post_id = get_the_ID();
				        		$clinedet = get_post_meta( get_the_ID(), false );

						        for ($timesheet_meta = 1; $timesheet_meta < 11; $timesheet_meta++) { 
								    $datekey = 'timesheet_date_'.$timesheet_meta;
							        $timesheet_date = get_post_meta( get_the_ID(), $datekey, true );
							        if ($timesheet_date >= $newfromvalue && $timesheet_date <= $newtovalue ) { 
							        	if ($authorid == $author_id) {
								        	$clientkey = 'clients_'.$timesheet_meta;
											$clientval = get_post_meta( get_the_ID(), $clientkey, true );						        
								            $startkey = 'start_time_'.$timesheet_meta;
									        $endkey = 'end_time_'.$timesheet_meta;
									        $value = weekly_mail_timesheet($clientkey, $datekey, $startkey, $endkey );
									        $emp_value .= $value;
									        $totaltime += difference_calculator_function($startkey, $endkey );
									        $difference_total = difference_total_function($totaltime);
							        	}
							        }
						        	
						        }
							endwhile;
						        $emp_value .= $difference_total;
	        		}						
				}	

					$emp_value .= '</tbody>';
				$emp_value .= '</table>';
			$emp_value .= '</div>';
				/*$totalhourswork = $totaltime/60;
				$totaltime = explode(".",$totalhourswork);
				$minutes = $totaltime[1]*0.6;
				$numlength = strlen((string)$minutes);
				if ($numlength <= 1) {
					$min = $minutes.'0';
				} else {
					$min = $minutes; 
				}
			$emp_value .= '<div class="timesheet-footer">';
				$emp_value .= '<div style="text-align: right; font-size: 18px; margin-bottom: 20px;">'; 
				$emp_value .= 'Total:'; 
					$emp_value .= $totaltime[0] . ' Hours ' . $min . ' Minutes' ; 
				$emp_value .= '</div>';
			$emp_value .= '</div>';*/
		$emp_value .= '</div>';

	    wp_reset_postdata(); 

		else :
	    $emp_value .= '<h2>';
		$emp_value .= _e( 'Sorry, no timesheet matched your criteria.' ); 
		$emp_value .= '</h2>';
	endif;

	return $emp_value;
}

function weekly_mail_timesheet($clientkey, $timesheet_date_key, $start_time_key, $end_time_key){

		$client = get_post_meta( get_the_ID(), $clientkey, true );
		$date = get_post_meta( get_the_ID(), $timesheet_date_key, true );

		$start_time = get_post_meta( get_the_ID(), $start_time_key, true );
		$end_time = get_post_meta( get_the_ID(), $end_time_key, true );

		$starttime = get_post_meta( get_the_ID(), $start_time_key, true );
		$endtime = get_post_meta( get_the_ID(), $end_time_key, true );

		$start = array_filter(explode(' ',str_replace(':',' ',$starttime)));
		$end = array_filter(explode(' ',str_replace(':',' ',$endtime)));
		$newDate = date("m/d/Y", strtotime($date));
		
		// convert date and time arrays into datetime formats
		$startdate = date_create_from_format('m/d/Y@h:i a', $newDate . "@". $start[0].":".$start[1]." ".$start[2]);
		$enddate =   date_create_from_format('m/d/Y@h:i a', $newDate . "@". $end[0].":".$end[1]." ".$end[2]);

		//convert datetimes into seconds to compare
		$starttime = strtotime($startdate->format('Y-m-d H:i:s'));
		$endtime = strtotime($enddate->format('Y-m-d H:i:s'));

		// check to see if the times span overnight
		if($starttime > $endtime)
		$endtime = strtotime($enddate->format('Y-m-d H:i:s') . " +1 day");

		// perform calculation
		$diff = floor(($endtime - $starttime)/60);

		$totaldiff = $diff/60;
		$totaldiff = explode(".",$totaldiff);

		$totaltime += $diff;

		if ($diff >= 60 ) {
			$time = $diff/60 . ' Hours';
		} else {
			$time = $diff . ' Minutes';									
		}					
	$weekly_mail_timesheet = '<tr>';
		$weekly_mail_timesheet .= '<td>';
		$term = get_term_by('term_id', $client, 'client_location');
		$weekly_mail_timesheet .= $term->name;
		$weekly_mail_timesheet .= '</td>';
		$weekly_mail_timesheet .= '<td>';
		$weekly_mail_timesheet .= date("d-m-Y", strtotime($date));
		$weekly_mail_timesheet .= '</td>';
		$weekly_mail_timesheet .= '<td>';
		$weekly_mail_timesheet .= $start_time;
		$weekly_mail_timesheet .= '</td>';
		$weekly_mail_timesheet .='<td>'. $end_time .'</td>';
		$weekly_mail_timesheet .='<td>'. $totaldiff[0] . ' Hours ' . $diff%60 . ' Minutes</td>';
	$weekly_mail_timesheet .='</tr>';

	return $weekly_mail_timesheet;
}

function difference_calculator_function($start_time_key, $end_time_key){
	
	$starttime = get_post_meta( get_the_ID(), $start_time_key, true );
	$endtime = get_post_meta( get_the_ID(), $end_time_key, true );

	$start = array_filter(explode(' ',str_replace(':',' ',$starttime)));
	$end = array_filter(explode(' ',str_replace(':',' ',$endtime)));
	$newDate = date("m/d/Y", strtotime($date));

	// convert date and time arrays into datetime formats
	$startdate = date_create_from_format('m/d/Y@h:i a', $newDate . "@". $start[0].":".$start[1]." ".$start[2]);
	$enddate =   date_create_from_format('m/d/Y@h:i a', $newDate . "@". $end[0].":".$end[1]." ".$end[2]);

	//convert datetimes into seconds to compare
	$starttime = strtotime($startdate->format('Y-m-d H:i:s'));
	$endtime = strtotime($enddate->format('Y-m-d H:i:s'));

	// check to see if the times span overnight
	if($starttime > $endtime)
	$endtime = strtotime($enddate->format('Y-m-d H:i:s') . " +1 day");

	// perform calculation
	$diff = floor(($endtime - $starttime)/60);

	return $diff;
}

function difference_total_function($difference_total){

	$totalhourswork = $difference_total/60;
	$totaltime = explode(".",$totalhourswork);
	$minutes = $totaltime[1]*0.6;
	$numlength = strlen((string)$minutes);
	if ($numlength <= 1) {
		$min = $minutes.'0';
	} else {
		$min = $minutes; 
	}
	$difference_calculator_timesheet .= '<tr class="timesheet-footer">';
		$difference_calculator_timesheet .= '<td style="text-align: right; font-size: 18px; margin-bottom: 20px;" colspan="5">'; 
		$difference_calculator_timesheet .= 'Total:'; 
			$difference_calculator_timesheet .= $totaltime[0] . ' Hours ' . $min . ' Minutes' ; 
		$difference_calculator_timesheet .= '</td>';
	$difference_calculator_timesheet .= '</tr>';

	return $difference_calculator_timesheet;
}


ob_start();				
$mpdf = new mPDF('F'); 
$mpdf->WriteHTML($timesheet_value);
$content = $mpdf->Output('/home/rgbastud/public_html/chefs2go/pdf/lmn.pdf','F');

$content = $mpdf->Output('', 'S');
$content = chunk_split(base64_encode($content));

$to = 'test.bizcatalyst@gmail.com, ' . $Email;
$subject = 'Employee Timesheet Details';
$repEmail = 'info@chefs2go.com';

$fileName = 'weekly-timesheet.pdf';
$eol = PHP_EOL;
$separator = md5(time());

$headers = 'From: chefs2go.com <'.$repEmail.'>'.$eol;
$headers .= 'MIME-Version: 1.0' .$eol;
$headers .= "Content-Type: multipart/mixed; boundary=\"".$separator."\"";

$message = "--".$separator.$eol;
$message .= "Content-Transfer-Encoding: 7bit".$eol.$eol;
$message .= "Please check attached pdf for all employee details.".$eol;

$message .= "--".$separator.$eol;
$message .= "Content-Type: text/html; charset=\"iso-8859-1\"".$eol;
$message .= "Content-Transfer-Encoding: 8bit".$eol.$eol;

$message .= "--".$separator.$eol;
$message .= "Content-Type: application/pdf; name=\"".$fileName."\"".$eol; 
$message .= "Content-Transfer-Encoding: base64".$eol;
$message .= "Content-Disposition: attachment".$eol.$eol;
$message .= $content.$eol;
$message .= "--".$separator."--";

mail($to, $subject, $message, $headers);

/*if (mail($to, $subject, $message, $headers)){
	echo "Email sent to pankaj.mohale@bizcatalyst.co.in";
} else {
	echo "Email failed";
}*/

ob_end_flush();
exit;
?>