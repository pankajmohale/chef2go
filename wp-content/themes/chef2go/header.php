<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700|Open+Sans:400,600,700,800" rel="stylesheet">
<!--[if lt IE 9]>
  <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
  <![endif]-->
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="box-layout" class="site box-layout">
<span class="corner-top-left"></span>
<span class="corner-top-right"></span>
	<?php 
		global $redux_demo;  // This is your opt_name. 
	?>
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', '_s' ); ?></a>

	<header id="header" class="header" role="banner">
		<div class="container">
			<div class="row">
				<div class="logo-bar">
					<div class="col-xs-12 col-sm-3 col-md-3">
						<div class="phone-number">
							<div class="header-phone-number"><?php echo $redux_demo['header-opt-phone-number']; ?></div>
							<div class="header-phone-below-text"><?php echo $redux_demo['header-opt-phone-text']; ?></div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6">
						<div class="main-logo">
							<?php if($redux_demo['header-opt-media']['url']) { ?>
								<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php echo $redux_demo['header-opt-media']['url']; ?>" class="img-responsive logo-image"></a>
							<?php } else { ?>
								<div class="site-branding">
										<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
									<?php
										$description = get_bloginfo( 'description', 'display' );
										if ( $description || is_customize_preview() ) { ?>
											<p class="site-description"><?php echo $description; /* WPCS: xss ok. */ ?></p>
										<?php }	?>
								</div><!-- .site-branding -->
							<?php } ?>
						</div>
					</div>
					<div class="col-xs-12 col-sm-3 col-md-3">
						<div class="social-links">
							<?php if($redux_demo['header-opt-facebook-link']){ ?>
								<div class="fb-links so-links"><a href="<?php echo $redux_demo['header-opt-facebook-link']; ?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></div>
							<?php }
							if ($redux_demo['header-opt-rss-link']) { ?>
								<div class="rss-links so-links"><a href="<?php echo $redux_demo['header-opt-rss-link'];?>" target="_blank"><i class="fa fa-rss" aria-hidden="true"></i></a></div>
							<?php }
							if ($redux_demo['header-opt-twitter-link']) { ?>
								<div class="twitter-links so-links"><a href="<?php echo $redux_demo['header-opt-twitter-link'];?>" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></div>
							<?php }
							if ($redux_demo['header-opt-youtube-link']) { ?>
								<div class="youtube-links so-links"><a href="<?php echo $redux_demo['header-opt-youtube-link']; ?>" target="_blank"><i class="fa fa-youtube" aria-hidden="true"></i></a></div>
							<?php } ?>						
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="header-divider"></div>
		<div class="container">
			<div class="row">
				<div class="menu-container hidden-xs col-sm-10 col-md-10">
					<nav class="navbar navbar-default" role="navigation">
					    <!-- Brand and toggle get grouped for better mobile display -->
					    <div class="navbar-header">
					      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					        <span class="sr-only">Toggle navigation</span>
					        <span class="icon-bar"></span>
					        <span class="icon-bar"></span>
					        <span class="icon-bar"></span>
					      </button>
					    </div>

					        <?php
					            wp_nav_menu( array(
					                'menu'              => 'primary',
					                'theme_location'    => 'primary',
					                'depth'             => 2,
					                'container'         => 'div',
					                'container_class'   => 'collapse navbar-collapse',
					        'container_id'      => 'bs-example-navbar-collapse-1',
					                'menu_class'        => 'nav navbar-nav',
					                'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
					                'walker'            => new wp_bootstrap_navwalker())
					            );
					        ?>
					</nav><!-- #site-navigation -->
				</div>
				<?php if(is_user_logged_in()){ ?>
				<div class="menu-login-link col-sm-2 col-md-2"><a href="<?php $redirect = $redux_demo['header-login']; echo wp_logout_url($redirect);	?>" class="btn custom-btn">Logout</a></div>
				<?php } else { ?>
				<div class="menu-login-link col-sm-2 col-md-2"><a href="<?php echo $redux_demo['header-login']; ?>" class="btn custom-btn">Login</a></div>
				<?php } ?>
				
			</div>
		</div>
	</header><!-- #masthead -->
	
	<?php if(is_front_page()){ ?>
		<div id="home-page" class="home-page-wrapper">
	<?php } elseif (is_page_template('template-contact-us.php')) { ?>
		<div id="content" class="container-fluid site-content">
	<?php } elseif (is_page_template('template-other-page.php')) { ?>
		<div id="other-page" class="content">
	<?php } elseif (is_page_template('template-chef-listing.php')) { ?>
		<div id="chef-listing" class="content">
	<?php } elseif (is_page_template('template-staff-listing.php')) { ?>
		<div id="chef-listing" class="content">
	<?php } elseif (is_page_template()) { ?>
		<div id="custom-page-template" class="content">
	<?php } else { ?>	
		<div id="content" class="container site-content">
		<div class="row">
			<div class="breadcrumbs clearfix">
				<div class="col-xs-12">
					<?php dimox_breadcrumbs(); ?>
				</div>
			</div>
		</div>
	<?php } ?>

