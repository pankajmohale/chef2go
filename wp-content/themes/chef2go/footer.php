<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>
<?php global $redux_demo;  // This is your opt_name.  ?>
</div><!-- #content -->
	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="footer-widget-section clearfix">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-2 col-md-2">
						<?php if ( is_active_sidebar( 'footer-logo' ) ) : ?>
						        <?php dynamic_sidebar( 'footer-logo' ); ?>
						<?php endif; ?>
					</div>
					<div class="col-xs-12 col-sm-2 col-md-2">
						<?php if ( is_active_sidebar( 'footer-first-address' ) ) : ?>
						        <?php dynamic_sidebar( 'footer-first-address' ); ?>
						<?php endif; ?>
					</div>
					<div class="col-xs-12 col-sm-2 col-md-2">
						<?php if ( is_active_sidebar( 'footer-second-address' ) ) : ?>
						        <?php dynamic_sidebar( 'footer-second-address' ); ?>
						<?php endif; ?>
					</div>
					<div class="col-xs-12 col-sm-2 col-md-2">
						<?php if ( is_active_sidebar( 'footer-third-address' ) ) : ?>
						        <?php dynamic_sidebar( 'footer-third-address' ); ?>
						<?php endif; ?>
					</div>
					<div class="col-xs-12 col-sm-2 col-md-2">
						<?php if ( is_active_sidebar( 'footer-fourth-address' ) ) : ?>
						        <?php dynamic_sidebar( 'footer-fourth-address' ); ?>
						<?php endif; ?>
					</div>
					<div class="col-xs-12 col-sm-2 col-md-2">
						<?php if ( is_active_sidebar( 'footer-fifth-address' ) ) : ?>
						        <?php dynamic_sidebar( 'footer-fifth-address' ); ?>
						<?php endif; ?>
					</div>				
				</div>				
			</div>
		</div>
		<div class="footer-copyright-bar">
			<div class="container">
				<div class="site-info text-center">
					<div class="copyright"><?php echo $redux_demo['footer-copyright-text']; ?></div>
				</div><!-- .site-info -->			
			</div>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
