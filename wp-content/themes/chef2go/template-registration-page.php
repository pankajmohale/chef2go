<?php
/**
* Template Name: Registration Pages
*/

get_header(); ?>

	<div id="primary" class="content-area">
		<div class="other-entry-header text-center">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
							<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
						</div><!-- .entry-header -->
				</div>
			</div>
		</div>
		<div class="breadcrumbs-container">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<?php dimox_breadcrumbs(); ?>
					</div>
				</div>
			</div>			
		</div>
		<main id="main" class="container site-main" role="main">
			<div class="row">
			<?php if(is_user_logged_in()){ ?>
				<div class="col-xs-12">
					<div class="error-login-wrapper text-center">
						<h4>Please logout first to visit this page.</h4>
					</div>
				</div>				
			<?php } else { ?>
				<div class="col-xs-12 col-sm-9">
					<?php
					while ( have_posts() ) : the_post();

						get_template_part( 'template-parts/other', 'page' );

						// If comments are open or we have at least one comment, load up the comment template.
						if ( comments_open() || get_comments_number() ) :
							comments_template();
						endif;

					endwhile; // End of the loop.
					?>					
				</div>
				<div class="col-xs-12 col-sm-3">
					<div id="sidebar" class="sidebar">
						<?php get_sidebar(); ?>
					</div>
				</div>
			<?php }	?>

			</div>

		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();
