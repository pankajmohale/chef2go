<?php
    /**
     * ReduxFramework Sample Config File
     * For full documentation, please visit: http://docs.reduxframework.com/
     */

    if ( ! class_exists( 'Redux' ) ) {
        return;
    }


    // This is your option name where all the Redux data is stored.
    $opt_name = "redux_demo";

    // This line is only for altering the demo. Can be easily removed.
    $opt_name = apply_filters( 'redux_demo/opt_name', $opt_name );

    /*
     *
     * --> Used within different fields. Simply examples. Search for ACTUAL DECLARATION for field examples
     *
     */

    $sampleHTML = '';
    if ( file_exists( dirname( __FILE__ ) . '/info-html.html' ) ) {
        Redux_Functions::initWpFilesystem();

        global $wp_filesystem;

        $sampleHTML = $wp_filesystem->get_contents( dirname( __FILE__ ) . '/info-html.html' );
    }

    // Background Patterns Reader
    $sample_patterns_path = ReduxFramework::$_dir . '../sample/patterns/';
    $sample_patterns_url  = ReduxFramework::$_url . '../sample/patterns/';
    $sample_patterns      = array();
    
    if ( is_dir( $sample_patterns_path ) ) {

        if ( $sample_patterns_dir = opendir( $sample_patterns_path ) ) {
            $sample_patterns = array();

            while ( ( $sample_patterns_file = readdir( $sample_patterns_dir ) ) !== false ) {

                if ( stristr( $sample_patterns_file, '.png' ) !== false || stristr( $sample_patterns_file, '.jpg' ) !== false ) {
                    $name              = explode( '.', $sample_patterns_file );
                    $name              = str_replace( '.' . end( $name ), '', $sample_patterns_file );
                    $sample_patterns[] = array(
                        'alt' => $name,
                        'img' => $sample_patterns_url . $sample_patterns_file
                    );
                }
            }
        }
    }

    /**
     * ---> SET ARGUMENTS
     * All the possible arguments for Redux.
     * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
     * */

    $theme = wp_get_theme(); // For use with some settings. Not necessary.

    $args = array(
        // TYPICAL -> Change these values as you need/desire
        'opt_name'             => $opt_name,
        // This is where your data is stored in the database and also becomes your global variable name.
        'display_name'         => $theme->get( 'Name' ),
        // Name that appears at the top of your panel
        'display_version'      => $theme->get( 'Version' ),
        // Version that appears at the top of your panel
        'menu_type'            => 'menu',
        //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
        'allow_sub_menu'       => true,
        // Show the sections below the admin menu item or not
        'menu_title'           => __( 'chefs2go Options', 'redux-framework-demo' ),
        'page_title'           => __( 'chefs2go Options', 'redux-framework-demo' ),
        // You will need to generate a Google API key to use this feature.
        // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
        'google_api_key'       => '',
        // Set it you want google fonts to update weekly. A google_api_key value is required.
        'google_update_weekly' => false,
        // Must be defined to add google fonts to the typography module
        'async_typography'     => true,
        // Use a asynchronous font on the front end or font string
        //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
        'admin_bar'            => true,
        // Show the panel pages on the admin bar
        'admin_bar_icon'       => 'dashicons-portfolio',
        // Choose an icon for the admin bar menu
        'admin_bar_priority'   => 50,
        // Choose an priority for the admin bar menu
        'global_variable'      => '',
        // Set a different name for your global variable other than the opt_name
        'dev_mode'             => true,
        // Show the time the page took to load, etc
        'update_notice'        => true,
        // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
        'customizer'           => true,
        // Enable basic customizer support
        //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
        //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

        // OPTIONAL -> Give you extra features
        'page_priority'        => null,
        // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
        'page_parent'          => 'themes.php',
        // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
        'page_permissions'     => 'manage_options',
        // Permissions needed to access the options panel.
        'menu_icon'            => '',
        // Specify a custom URL to an icon
        'last_tab'             => '',
        // Force your panel to always open to a specific tab (by id)
        'page_icon'            => 'icon-themes',
        // Icon displayed in the admin panel next to your menu_title
        'page_slug'            => '',
        // Page slug used to denote the panel, will be based off page title then menu title then opt_name if not provided
        'save_defaults'        => true,
        // On load save the defaults to DB before user clicks save or not
        'default_show'         => false,
        // If true, shows the default value next to each field that is not the default value.
        'default_mark'         => '',
        // What to print by the field's title if the value shown is default. Suggested: *
        'show_import_export'   => true,
        // Shows the Import/Export panel when not used as a field.

        // CAREFUL -> These options are for advanced use only
        'transient_time'       => 60 * MINUTE_IN_SECONDS,
        'output'               => true,
        // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
        'output_tag'           => true,
        // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
        // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

        // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
        'database'             => '',
        // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
        'use_cdn'              => true,
        // If you prefer not to use the CDN for Select2, Ace Editor, and others, you may download the Redux Vendor Support plugin yourself and run locally or embed it in your code.

        // HINTS
        'hints'                => array(
            'icon'          => 'el el-question-sign',
            'icon_position' => 'right',
            'icon_color'    => 'lightgray',
            'icon_size'     => 'normal',
            'tip_style'     => array(
                'color'   => 'red',
                'shadow'  => true,
                'rounded' => false,
                'style'   => '',
            ),
            'tip_position'  => array(
                'my' => 'top left',
                'at' => 'bottom right',
            ),
            'tip_effect'    => array(
                'show' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'mouseover',
                ),
                'hide' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'click mouseleave',
                ),
            ),
        )
    );

    // ADMIN BAR LINKS -> Setup custom links in the admin bar menu as external items.
    $args['admin_bar_links'][] = array(
        'id'    => 'redux-docs',
        'href'  => '#',
        'title' => __( 'Documentation', 'redux-framework-demo' ),
    );

    $args['admin_bar_links'][] = array(
        //'id'    => 'redux-support',
        'href'  => '#',
        'title' => __( 'Support', 'redux-framework-demo' ),
    );

    $args['admin_bar_links'][] = array(
        'id'    => 'redux-extensions',
        'href'  => '#',
        'title' => __( 'Extensions', 'redux-framework-demo' ),
    );

    // SOCIAL ICONS -> Setup custom links in the footer for quick links in your panel footer icons.
    $args['share_icons'][] = array(
        'url'   => '#',
        'title' => 'Visit us on GitHub',
        'icon'  => 'el el-github'
        //'img'   => '', // You can use icon OR img. IMG needs to be a full URL.
    );
    $args['share_icons'][] = array(
        'url'   => '#',
        'title' => 'Like us on Facebook',
        'icon'  => 'el el-facebook'
    );
    $args['share_icons'][] = array(
        'url'   => '#',
        'title' => 'Follow us on Twitter',
        'icon'  => 'el el-twitter'
    );
    $args['share_icons'][] = array(
        'url'   => '#',
        'title' => 'Find us on LinkedIn',
        'icon'  => 'el el-linkedin'
    );

    // Panel Intro text -> before the form
    if ( ! isset( $args['global_variable'] ) || $args['global_variable'] !== false ) {
        if ( ! empty( $args['global_variable'] ) ) {
            $v = $args['global_variable'];
        } else {
            $v = str_replace( '-', '_', $args['opt_name'] );
        }
        $args['intro_text'] = sprintf( __( '<p>Did you know that Redux sets a global variable for you? To access any of your saved options from within your code you can use your global variable: <strong>$%1$s</strong></p>', 'redux-framework-demo' ), $v );
    } else {
        $args['intro_text'] = __( '<p>This text is displayed above the options panel. It isn\'t required, but more info is always better! The intro_text field accepts all HTML.</p>', 'redux-framework-demo' );
    }

    // Add content after the form.
    $args['footer_text'] = __( '<p>This text is displayed below the options panel. It isn\'t required, but more info is always better! The footer_text field accepts all HTML.</p>', 'redux-framework-demo' );

    Redux::setArgs( $opt_name, $args );

    /*
     * ---> END ARGUMENTS
     */


    /*
     * ---> START HELP TABS
     */

    $tabs = array(
        array(
            'id'      => 'redux-help-tab-1',
            'title'   => __( 'Theme Information 1', 'redux-framework-demo' ),
            'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'redux-framework-demo' )
        ),
        array(
            'id'      => 'redux-help-tab-2',
            'title'   => __( 'Theme Information 2', 'redux-framework-demo' ),
            'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'redux-framework-demo' )
        )
    );
    Redux::setHelpTab( $opt_name, $tabs );

    // Set the help sidebar
    $content = __( '<p>This is the sidebar content, HTML is allowed.</p>', 'redux-framework-demo' );
    Redux::setHelpSidebar( $opt_name, $content );


    /*
     * <--- END HELP TABS
     */


    /*
     *
     * ---> START SECTIONS
     *
     */

    /*

        As of Redux 3.5+, there is an extensive API. This API can be used in a mix/match mode allowing for


     */
    // -> Theme Chef2Go Fields
    Redux::setSection( $opt_name, array(
        'title'            => __( 'Chef2go Fields', 'redux-framework-demo' ),
        'id'               => 'cheftogo',
        'desc'             => __( 'These are theme option fields for theme!', 'redux-framework-demo' ),
        'customizer_width' => '400px',
        'icon'             => 'el el-home'
    ) );

    Redux::setSection( $opt_name, array(
        'title'      => __( 'Header', 'redux-framework-demo' ),
        'id'         => 'header-opt-media',
        'desc'       => __( 'For full documentation on this field, visit: ', 'redux-framework-demo' ) . '<a href="//docs.reduxframework.com/core/fields/media/" target="_blank">docs.reduxframework.com/core/fields/media/</a>',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'header-opt-media',
                'type'     => 'media',
                'url'      => true,
                'title'    => __( 'Media w/ URL', 'redux-framework-demo' ),
                'compiler' => 'true',
                //'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'     => __( 'Basic media uploader with disabled URL input field.', 'redux-framework-demo' ),
                'subtitle' => __( 'Upload any media using the WordPress native uploader', 'redux-framework-demo' ),
                'default'  => array( 'url' => 'http://s.wordpress.org/style/images/codeispoetry.png' ),
                //'hint'      => array(
                //    'title'     => 'Hint Title',
                //    'content'   => 'This is a <b>hint</b> for the media field with a Title.',
                //)
            ),
            array(
                'id'       => 'header-opt-phone-number',
                'type'     => 'text',
                'title'    => __( 'Phone Number', 'redux-framework-demo' ),
                'subtitle' => __( 'Subtitle', 'redux-framework-demo' ),
                'desc'     => __( 'Field Description', 'redux-framework-demo' ),
                'default'  => '',
            ),
            array(
                'id'       => 'header-opt-phone-text',
                'type'     => 'text',
                'title'    => __( 'Text Below Text', 'redux-framework-demo' ),
                'subtitle' => __( 'Subtitle', 'redux-framework-demo' ),
                'desc'     => __( 'Field Description', 'redux-framework-demo' ),
                'default'  => '',
            ),
            array(
                'id'       => 'header-opt-facebook-link',
                'type'     => 'text',
                'title'    => __( 'Facebook link', 'redux-framework-demo' ),
                'subtitle' => __( 'Subtitle', 'redux-framework-demo' ),
                'desc'     => __( 'Field Description', 'redux-framework-demo' ),
                'default'  => '',
            ),
            array(
                'id'       => 'header-opt-rss-link',
                'type'     => 'text',
                'title'    => __( 'RSS link', 'redux-framework-demo' ),
                'subtitle' => __( 'Subtitle', 'redux-framework-demo' ),
                'desc'     => __( 'Field Description', 'redux-framework-demo' ),
                'default'  => '',
            ),
            array(
                'id'       => 'header-opt-twitter-link',
                'type'     => 'text',
                'title'    => __( 'Twitter Link', 'redux-framework-demo' ),
                'subtitle' => __( 'Subtitle', 'redux-framework-demo' ),
                'desc'     => __( 'Field Description', 'redux-framework-demo' ),
                'default'  => '',
            ),
            array(
                'id'       => 'header-opt-youtube-link',
                'type'     => 'text',
                'title'    => __( 'YouTube Link', 'redux-framework-demo' ),
                'subtitle' => __( 'Subtitle', 'redux-framework-demo' ),
                'desc'     => __( 'Field Description', 'redux-framework-demo' ),
                'default'  => '',
            ),

        )
    ) );

    Redux::setSection( $opt_name, array(
        'title'      => __( 'Footer', 'redux-framework-demo' ),
        'id'         => 'cheftogo-opt-media',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'footer-copyright-text',
                'type'     => 'text',
                'title'    => __( 'Copyright Text', 'redux-framework-demo' ),
                'default'  => '&copy; Copyright 2016',
            ),

        )
    ) );

    Redux::setSection( $opt_name, array(
        'title'            => __( 'Home Page', 'redux-framework-demo' ),
        'id'               => 'home',
        'customizer_width' => '500px',
        'icon'             => 'el el-edit',
    ) );

    // Home page content
    Redux::setSection( $opt_name, array(
        'title'      => __( 'Home Featured Section', 'redux-framework-demo' ),
        'id'         => 'home-section',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'recruitment-section',
                'type'     => 'section',
                'title'    => __( 'First Section', 'redux-framework-demo' ),
                'indent'   => true, // Indent all options below until the next 'section' option is set.
            ),
            array(
                'id'       => 'recruitment-section-title',
                'type'     => 'text',
                'title'    => __( 'Content Title', 'redux-framework-demo' ),
            ),
            array(
                'id'       => 'recruitment-section-editor',
                'type'     => 'editor',
                'title'    => __( 'Content Description', 'redux-framework-demo' ),
                'default'  => 'Powered by Redux Framework.',
            ),
            array(
                'id'       => 'recruitment-section-media',
                'type'     => 'media',
                'title'    => __( 'Section Image', 'redux-framework-demo' ),
            ),
            array(
                'id'       => 'recruitment-section-link',
                'type'     => 'text',
                'title'    => __( 'Section Link', 'redux-framework-demo' ),
            ),
            array(
                'id'     => 'recruitment-section-end',
                'type'   => 'section',
                'indent' => false, // Indent all options below until the next 'section' option is set.
            ),
            // Second Section
            array(
                'id'       => 'staff-on-demand-section',
                'type'     => 'section',
                'title'    => __( 'Second Section', 'redux-framework-demo' ),
                'indent'   => true, // Indent all options below until the next 'section' option is set.
            ),
            array(
                'id'       => 'staff-on-demand-section-title',
                'type'     => 'text',
                'title'    => __( 'Content Title', 'redux-framework-demo' ),
            ),
            array(
                'id'       => 'staff-on-demand-section-editor',
                'type'     => 'editor',
                'title'    => __( 'Content Description', 'redux-framework-demo' ),
                'subtitle' => __( 'Use any of the features of WordPress editor inside your panel!', 'redux-framework-demo' ),
                'default'  => 'Powered by Redux Framework.',
            ),
            array(
                'id'       => 'staff-on-demand-section-media',
                'type'     => 'media',
                'title'    => __( 'Section Image', 'redux-framework-demo' ),
                'subtitle' => __( 'Field Subtitle', 'redux-framework-demo' ),
            ),
            array(
                'id'       => 'staff-on-demand-section-link',
                'type'     => 'text',
                'title'    => __( 'Section Link', 'redux-framework-demo' ),
                'subtitle' => __( 'Field Subtitle', 'redux-framework-demo' ),
            ),
            array(
                'id'     => 'staff-on-demand-section-end',
                'type'   => 'section',
                'indent' => false, // Indent all options below until the next 'section' option is set.
            ),
            // Third Section
            array(
                'id'       => 'jobs-on-candidates-section',
                'type'     => 'section',
                'title'    => __( 'Third Section', 'redux-framework-demo' ),
                'indent'   => true, // Indent all options below until the next 'section' option is set.
            ),
            array(
                'id'       => 'jobs-on-candidates-section-title',
                'type'     => 'text',
                'title'    => __( 'Content Title', 'redux-framework-demo' ),
                'subtitle' => __( 'Field Subtitle', 'redux-framework-demo' ),
            ),
            array(
                'id'       => 'jobs-on-candidates-section-editor',
                'type'     => 'editor',
                'title'    => __( 'Content Description', 'redux-framework-demo' ),
                'subtitle' => __( 'Use any of the features of WordPress editor inside your panel!', 'redux-framework-demo' ),
                'default'  => 'Powered by Redux Framework.',
            ),
            array(
                'id'       => 'jobs-on-candidates-section-media',
                'type'     => 'media',
                'title'    => __( 'Section Image', 'redux-framework-demo' ),
                'subtitle' => __( 'Field Subtitle', 'redux-framework-demo' ),
            ),
            array(
                'id'       => 'jobs-on-candidates-section-link',
                'type'     => 'text',
                'title'    => __( 'Section Link', 'redux-framework-demo' ),
                'subtitle' => __( 'Field Subtitle', 'redux-framework-demo' ),
            ),
            array(
                'id'     => 'jobs-on-candidates-section-end',
                'type'   => 'section',
                'indent' => false, // Indent all options below until the next 'section' option is set.
            ),
        ),
    ) );

    Redux::setSection( $opt_name, array(
        'title'      => __( 'Home About Section', 'redux-framework-demo' ),
        'id'         => 'home-about-section',
        'desc'       => __( 'For full documentation on this field, visit: ', 'redux-framework-demo' ) . '<a href="//docs.reduxframework.com/core/fields/media/" target="_blank">docs.reduxframework.com/core/fields/media/</a>',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'about-section-title',
                'type'     => 'text',
                'title'    => __( 'Title', 'redux-framework-demo' ),
                'default'  => '',
            ),
            array(
                'id'       => 'about-section-short-desc',
                'type'     => 'text',
                'title'    => __( 'Short Description', 'redux-framework-demo' ),
                'default'  => '',
            ),
            array(
                'id'       => 'about-section-editor',
                'type'     => 'editor',
                'title'    => __( 'Content Description', 'redux-framework-demo' ),
                'default'  => 'Powered by Redux Framework.',
            ),
            array(
                'id'       => 'about-section-link',
                'type'     => 'text',
                'title'    => __( 'About Link', 'redux-framework-demo' ),
                'default'  => '',
            ),
        )
    ) );

    Redux::setSection( $opt_name, array(
        'title'      => __( 'Home Testimonial Section', 'redux-framework-demo' ),
        'id'         => 'home-testimonials-section',
        'desc'       => 'Testimonials Slider settings',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'testimonials-section-title',
                'type'     => 'text',
                'title'    => __( 'Title', 'redux-framework-demo' ),
                'default'  => '',
            ),
            array(
                'id'       => 'testimonials-select-transition',
                'type'     => 'select',
                'title'    => __( 'Slider Transition', 'redux-framework-demo' ),
                'desc'     => __( 'Sets the type of transition for the slider.', 'redux-framework-demo' ),
                //Must provide key => value pairs for select options
                'options'  => array(
                    'fade' => 'fade',
                    'slide' => 'slide',
                ),
                'default'  => 'fade'
            ),
            array(
                'id'       => 'testimonials-transition-duration',
                'type'     => 'text',
                'title'    => __( 'Slider Transition Duration', 'redux-framework-demo' ),
                'default'  => '3000',
            ),
            array(
                'id'       => 'testimonials-transition-speed',
                'type'     => 'text',
                'title'    => __( 'Slider Transition Speed', 'redux-framework-demo' ),
                'default'  => '300',
            ),
        )
    ) );

    Redux::setSection( $opt_name, array(
        'title'            => __( 'Custom Links', 'redux-framework-demo' ),
        'id'               => 'sitelinks',
        'customizer_width' => '500px',
        'icon'             => 'el el-edit',
    ) );

    // Home page content
    Redux::setSection( $opt_name, array(
        'title'      => __( 'All Site Links Section', 'redux-framework-demo' ),
        'id'         => 'sitelinks-section',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'header-login',
                'type'     => 'text',
                'title'    => __( 'Header Login Link', 'redux-framework-demo' ),
            ),
            /*array(
                'id'       => 'chef-registration',
                'type'     => 'text',
                'title'    => __( 'Chef Registration', 'redux-framework-demo' ),
            ),
            array(
                'id'       => 'staff-registration',
                'type'     => 'text',
                'title'    => __( 'Staff Registration', 'redux-framework-demo' ),
            ),
            array(
                'id'       => 'recruiter-registration-link',
                'type'     => 'text',
                'title'    => __( 'recruiter Registration', 'redux-framework-demo' ),
            ),
            array(
                'id'       => 'jobs-on-candidates-section-title',
                'type'     => 'text',
                'title'    => __( 'Content Title', 'redux-framework-demo' ),
                'subtitle' => __( 'Field Subtitle', 'redux-framework-demo' ),
            ),
            array(
                'id'       => 'jobs-on-candidates-section-link',
                'type'     => 'text',
                'title'    => __( 'Section Link', 'redux-framework-demo' ),
            ),*/
        ),
    ) );


    Redux::setSection( $opt_name, array(
        'title'            => __( 'Recruiter Module', 'redux-framework-demo' ),
        'id'               => 'recruiter',
        'customizer_width' => '500px',
        'icon'             => 'el el-edit',
    ) );

    // Home page content
    Redux::setSection( $opt_name, array(
        'title'      => __( 'Recruiter Links', 'redux-framework-demo' ),
        'id'         => 'recruiter-section',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'recruiter_chefs_full_time_work',
                'type'     => 'select',
                'multi'    => false,
                'data'     => 'pages',
                'title'    => __( 'Select Page for Chefs Seeking Full Time Work', TD ),
                //'desc'     => __( 'Page will be marked as front for this post type', TD ),
            ),
            array(
                'id'       => 'recruiter_chefs_part_time_work',
                'type'     => 'select',
                'multi'    => false,
                'data'     => 'pages',
                'title'    => __( 'Select Page for Chefs Seeking Part Time Work', TD ),
                //'desc'     => __( 'Page will be marked as front for this post type', TD ),
            ),
            array(
                'id'       => 'recruiter_freelancing_chefs',
                'type'     => 'select',
                'multi'    => false,
                'data'     => 'pages',
                'title'    => __( 'Select Page for Chefs Seeking Part Time Work', TD ),
                //'desc'     => __( 'Page will be marked as front for this post type', TD ),
            ),
            array(
                'id'       => 'recruiter_staff_full_time_work',
                'type'     => 'select',
                'multi'    => false,
                'data'     => 'pages',
                'title'    => __( 'Select Page for Staff Seeking Full Time Work', TD ),
                //'desc'     => __( 'Page will be marked as front for this post type', TD ),
            ),
            array(
                'id'       => 'recruiter_staff_part_time_work',
                'type'     => 'select',
                'multi'    => false,
                'data'     => 'pages',
                'title'    => __( 'Select Page for Staff Seeking Part Time Work', TD ),
                //'desc'     => __( 'Page will be marked as front for this post type', TD ),
            ),
            array(
                'id'       => 'recruiter_freelanceing_staff',
                'type'     => 'select',
                'multi'    => false,
                'data'     => 'pages',
                'title'    => __( 'Select Page for Staff Seeking Part Time Work', TD ),
                //'desc'     => __( 'Page will be marked as front for this post type', TD ),
            ),
            array(
                'id'       => 'job_posting',
                'type'     => 'select',
                'multi'    => false,
                'data'     => 'pages',
                'title'    => __( 'Job Posing', TD ),
                //'desc'     => __( 'Page will be marked as front for this post type', TD ),
            ),
            array(
                'id'       => 'massage_page_url',
                'type'     => 'select',
                'multi'    => false,
                'data'     => 'pages',
                'title'    => __( 'Massage', TD ),
                //'desc'     => __( 'Page will be marked as front for this post type', TD ),
            ),
            array(
                'id'       => 'add_staff',
                'type'     => 'select',
                'multi'    => false,
                'data'     => 'pages',
                'title'    => __( 'Add Staff', TD ),
                //'desc'     => __( 'Page will be marked as front for this post type', TD ),
            ),
            array(
                'id'       => '_add_roster',
                'type'     => 'select',
                'multi'    => false,
                'data'     => 'pages',
                'title'    => __( 'Add Roster', TD ),
                //'desc'     => __( 'Page will be marked as front for this post type', TD ),
            ),
            array(
                'id'       => 'edit_roster',
                'type'     => 'select',
                'multi'    => false,
                'data'     => 'pages',
                'title'    => __( 'Edit Roster', TD ),
                //'desc'     => __( 'Page will be marked as front for this post type', TD ),
            ),
            array(
                'id'       => 'edit_staff',
                'type'     => 'select',
                'multi'    => false,
                'data'     => 'pages',
                'title'    => __( 'Edit Staff', TD ),
                //'desc'     => __( 'Page will be marked as front for this post type', TD ),
            ),
            array(
                'id'       => 'delete_staff',
                'type'     => 'select',
                'multi'    => false,
                'data'     => 'pages',
                'title'    => __( 'Delete Staff', TD ),
                //'desc'     => __( 'Page will be marked as front for this post type', TD ),
            ),
        ),
    ) );

    Redux::setSection( $opt_name, array(
        'title'            => __( 'Other Module', 'redux-framework-demo' ),
        'id'               => 'other',
        'customizer_width' => '500px',
        'icon'             => 'el el-edit',
    ) );

    // Home page content
    Redux::setSection( $opt_name, array(
        'title'      => __( 'Shortcodes', 'redux-framework-demo' ),
        'id'         => 'other-section',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'recruter_job_posting',
                'type'     => 'text',
                'title'    => __( 'Recruter Job Posting', 'redux-framework-demo' ),
            ),
/*            array(
                'id'       => 'chef_job_listing',
                'type'     => 'text',
                'title'    => __( 'Chef Job listing', 'redux-framework-demo' ),
            ),*/
        ),
    ) );

    // Home page content
    /*Redux::setSection( $opt_name, array(
        'title'      => __( 'E-mailer Content Area', 'redux-framework-demo' ),
        'id'         => 'emailer-section',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'weekly-section',
                'type'     => 'section',
                'title'    => __( 'Weekly Reminder Section', 'redux-framework-demo' ),
                'indent'   => true, // Indent all options below until the next 'section' option is set.
            ),
            array(
                'id'       => 'weekly-subject-title',
                'type'     => 'text',
                'title'    => __( 'Mail Subject Title', 'redux-framework-demo' ),
            ),
            array(
                'id'       => 'weekly-email-editor',
                'type'     => 'editor',
                'title'    => __( 'Content Description', 'redux-framework-demo' ),
                'default'  => 'Powered by Redux Framework.',
                'wpautop'  => true,
            ),
            array(
                'id'     => 'weekly-section-end',
                'type'   => 'section',
                'indent' => false, // Indent all options below until the next 'section' option is set.
            ),
            // Second Section
            array(
                'id'       => 'monthly-reminder-section',
                'type'     => 'section',
                'title'    => __( 'Monthly reminder Section', 'redux-framework-demo' ),
                'indent'   => true, // Indent all options below until the next 'section' option is set.
            ),
            array(
                'id'       => 'monthly-mail-subject-title',
                'type'     => 'text',
                'title'    => __( 'Mail Subject', 'redux-framework-demo' ),
            ),
            array(
                'id'       => 'monthly-email-editor',
                'type'     => 'editor',
                'title'    => __( 'Mail Description', 'redux-framework-demo' ),
                'subtitle' => __( 'Use any of the features of WordPress editor inside your panel!', 'redux-framework-demo' ),
                'default'  => 'Powered by Redux Framework.',
            ),
            array(
                'id'     => 'monthly-on-demand-section-end',
                'type'   => 'section',
                'indent' => false, // Indent all options below until the next 'section' option is set.
            ),
            // Third Section
            array(
                'id'       => 'jobs-on-candidates-section',
                'type'     => 'section',
                'title'    => __( 'Third Section', 'redux-framework-demo' ),
                'indent'   => true, // Indent all options below until the next 'section' option is set.
            ),
            array(
                'id'       => 'jobs-on-candidates-section-title',
                'type'     => 'text',
                'title'    => __( 'Content Title', 'redux-framework-demo' ),
                'subtitle' => __( 'Field Subtitle', 'redux-framework-demo' ),
            ),
            array(
                'id'       => 'jobs-on-candidates-section-editor',
                'type'     => 'editor',
                'title'    => __( 'Content Description', 'redux-framework-demo' ),
                'subtitle' => __( 'Use any of the features of WordPress editor inside your panel!', 'redux-framework-demo' ),
                'default'  => 'Powered by Redux Framework.',
            ),
            array(
                'id'       => 'jobs-on-candidates-section-media',
                'type'     => 'media',
                'title'    => __( 'Section Image', 'redux-framework-demo' ),
                'subtitle' => __( 'Field Subtitle', 'redux-framework-demo' ),
            ),
            array(
                'id'       => 'jobs-on-candidates-section-link',
                'type'     => 'text',
                'title'    => __( 'Section Link', 'redux-framework-demo' ),
                'subtitle' => __( 'Field Subtitle', 'redux-framework-demo' ),
            ),
            array(
                'id'     => 'jobs-on-candidates-section-end',
                'type'   => 'section',
                'indent' => false, // Indent all options below until the next 'section' option is set.
            ),
        ),
    ) );*/


    /* ============================================================================================================================================ */
    // -> START Basic Fields
   
    
    
    /*
     * <--- END SECTIONS
     */


    /*
     *
     * YOU MUST PREFIX THE FUNCTIONS BELOW AND ACTION FUNCTION CALLS OR ANY OTHER CONFIG MAY OVERRIDE YOUR CODE.
     *
     */

    /*
    *
    * --> Action hook examples
    *
    */

    // If Redux is running as a plugin, this will remove the demo notice and links
    //add_action( 'redux/loaded', 'remove_demo' );

    // Function to test the compiler hook and demo CSS output.
    // Above 10 is a priority, but 2 in necessary to include the dynamically generated CSS to be sent to the function.
    //add_filter('redux/options/' . $opt_name . '/compiler', 'compiler_action', 10, 3);

    // Change the arguments after they've been declared, but before the panel is created
    //add_filter('redux/options/' . $opt_name . '/args', 'change_arguments' );

    // Change the default value of a field after it's been set, but before it's been useds
    //add_filter('redux/options/' . $opt_name . '/defaults', 'change_defaults' );

    // Dynamically add a section. Can be also used to modify sections/fields
    //add_filter('redux/options/' . $opt_name . '/sections', 'dynamic_section');

    /**
     * This is a test function that will let you see when the compiler hook occurs.
     * It only runs if a field    set with compiler=>true is changed.
     * */
    if ( ! function_exists( 'compiler_action' ) ) {
        function compiler_action( $options, $css, $changed_values ) {
            echo '<h1>The compiler hook has run!</h1>';
            echo "<pre>";
            print_r( $changed_values ); // Values that have changed since the last save
            echo "</pre>";
            //print_r($options); //Option values
            //print_r($css); // Compiler selector CSS values  compiler => array( CSS SELECTORS )
        }
    }

    /**
     * Custom function for the callback validation referenced above
     * */
    if ( ! function_exists( 'redux_validate_callback_function' ) ) {
        function redux_validate_callback_function( $field, $value, $existing_value ) {
            $error   = false;
            $warning = false;

            //do your validation
            if ( $value == 1 ) {
                $error = true;
                $value = $existing_value;
            } elseif ( $value == 2 ) {
                $warning = true;
                $value   = $existing_value;
            }

            $return['value'] = $value;

            if ( $error == true ) {
                $field['msg']    = 'your custom error message';
                $return['error'] = $field;
            }

            if ( $warning == true ) {
                $field['msg']      = 'your custom warning message';
                $return['warning'] = $field;
            }

            return $return;
        }
    }

    /**
     * Custom function for the callback referenced above
     */
    if ( ! function_exists( 'redux_my_custom_field' ) ) {
        function redux_my_custom_field( $field, $value ) {
            print_r( $field );
            echo '<br/>';
            print_r( $value );
        }
    }

    /**
     * Custom function for filtering the sections array. Good for child themes to override or add to the sections.
     * Simply include this function in the child themes functions.php file.
     * NOTE: the defined constants for URLs, and directories will NOT be available at this point in a child theme,
     * so you must use get_template_directory_uri() if you want to use any of the built in icons
     * */
    if ( ! function_exists( 'dynamic_section' ) ) {
        function dynamic_section( $sections ) {
            //$sections = array();
            $sections[] = array(
                'title'  => __( 'Section via hook', 'redux-framework-demo' ),
                'desc'   => __( '<p class="description">This is a section created by adding a filter to the sections array. Can be used by child themes to add/remove sections from the options.</p>', 'redux-framework-demo' ),
                'icon'   => 'el el-paper-clip',
                // Leave this as a blank section, no options just some intro text set above.
                'fields' => array()
            );

            return $sections;
        }
    }

    /**
     * Filter hook for filtering the args. Good for child themes to override or add to the args array. Can also be used in other functions.
     * */
    if ( ! function_exists( 'change_arguments' ) ) {
        function change_arguments( $args ) {
            //$args['dev_mode'] = true;

            return $args;
        }
    }

    /**
     * Filter hook for filtering the default value of any given field. Very useful in development mode.
     * */
    if ( ! function_exists( 'change_defaults' ) ) {
        function change_defaults( $defaults ) {
            $defaults['str_replace'] = 'Testing filter hook!';

            return $defaults;
        }
    }

    /**
     * Removes the demo link and the notice of integrated demo from the redux-framework plugin
     */
    if ( ! function_exists( 'remove_demo' ) ) {
        function remove_demo() {
            // Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
            if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
                remove_filter( 'plugin_row_meta', array(
                    ReduxFrameworkPlugin::instance(),
                    'plugin_metalinks'
                ), null, 2 );

                // Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
                remove_action( 'admin_notices', array( ReduxFrameworkPlugin::instance(), 'admin_notices' ) );
            }
        }
    }

