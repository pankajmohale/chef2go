<?php
/**
* Template Name: Manager client management
*/

get_header(); 
global $redux_demo;
?>

	<div id="primary" class="content-area">
		<div class="other-entry-header text-center">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
							<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
						</div><!-- .entry-header -->
				</div>
			</div>
		</div>
		<div class="breadcrumbs-container">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<?php dimox_breadcrumbs(); ?>
					</div>
				</div>
			</div>			
		</div>
		<main id="main" class="container site-main" role="main">
			<div class="row">
			<?php if(is_user_logged_in()){ ?>
				<div class="col-xs-12 col-sm-4 col-md-3">
					<div id="sidebar" class="sidebar">
						<?php if ( is_active_sidebar( 'manager-page-widget' ) ) : ?>
							<div id="manager-sidebar">
								<?php dynamic_sidebar( 'manager-page-widget' ); ?>
							</div>
						<?php endif; ?>
					</div>
				</div>
				<div class="col-xs-12 col-sm-8 col-md-9">
			<?php $current_user = wp_get_current_user();
				foreach ($current_user->roles as $roleskey => $rolesvalue) {
					if ($rolesvalue == 'manager') { ?>
					<div class="row">
						<div class="col-xs-12">
							<div class="myarea-page-links pull-right">
								<a href="<?php echo site_url(); ?>/manager-myarea/" class="btn custom-btn">Back to Manager Area</a>
							</div>
						</div>
						<div class="col-sm-12 col-md-6">
							<?php echo do_shortcode('[gravityform id="16" title="false" description="false" ajax="true"]'); ?>							
						</div>
						<div class="col-sm-12 col-md-6">
						<div class="client-filter">
							<div class="manager-client-filter-option-inner">
								<div class="form-group">
							      <label for="manager-client">Location:</label>
										<select class="form-control manager_client_residency" id="manager-client">
									<?php 
										$clientterms = get_terms( 'location_residency', array(
										    'orderby'    => 'count',
										    'hide_empty' => 0,
										) );
										if ( ! empty( $clientterms ) && ! is_wp_error( $clientterms ) ){

										        echo '<option value="all">All</option>';
										    foreach ( $clientterms as $clientterm ) {
										        echo '<option value='.$clientterm->slug.'>' . $clientterm->name . '</option>';
										    }
										}
									?>
									</select>
								</div>				
							</div>
						</div>
						<?php 
						$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
						$client_args = array(
							'post_type' => 'locations',
							'posts_per_page' => 15,
							'paged' => $paged,
							'orderby' => 'title',
							'order'   => 'ASC',
						);

						// the query
						$client_query = new WP_Query( $client_args ); ?>

						<?php if ( $client_query->have_posts() ) : ?>

							<!-- pagination here -->
							<div class="list-group client-listing">
								<!-- the loop -->
								<?php while ( $client_query->have_posts() ) : $client_query->the_post(); ?>
									<div class="list-group-item">
										<div class="row">
											<div class="col-xs-12 col-sm-6"><?php echo get_the_title(); ?></div>
											<div class="col-xs-6 col-sm-3"><?php 
											$categories = get_the_terms( $post->ID, 'location_residency' );
											if ($categories) {
												foreach( $categories as $category ) {
												   echo $category->name;
												}												
											}
											?></div>
											<div class="col-xs-3 col-sm-2"><a class="client-edit">Edit</a></div>
											<div class="client-delete" postid="<?php echo get_the_ID(); ?>" postslug="<?php echo $post->post_name; ?>">
												<span class="badge"><i class="fa fa-trash-o" aria-hidden="true"></i></span>
											</div>
										</div>
										<div class="row">
											<?php 
										$term_list = wp_get_post_terms(get_the_ID(), 'location_residency', array("fields" => "names"));
										$postterm = $term_list;
										$terms = get_terms(array(
											'taxonomy' => 'location_residency',
											'hide_empty' => false,
										));
											?>
											<div class="edit-client clearfix">
												<div class="form-inline">
													<div class="col-xs-12 col-md-5 form-group padding-right-0">
														<input type="text" class="form-control" id="<?php echo $post->post_name; ?><?php echo get_the_ID(); ?>" value="<?php echo get_the_title(); ?>">
													</div>
													<div class="col-xs-12 col-md-4 form-group padding-right-0">
														<select class="form-control" id="<?php echo $post->post_name; ?><?php echo get_the_ID(); ?>">
															<?php 
															if ($terms) {
															foreach ($terms as $termkey => $termvalue) { ?>
																<option value="<?php echo $termvalue->term_id; ?>" <?php if( $postterm[0] == $termvalue->name ){ echo 'selected="selected"'; }?>>
																	<?php echo $termvalue->name; ?>
																</option>
															<?php } 
																
															} ?>
														</select>
													</div>
													<div class="col-xs-12 col-md-3 form-group padding-right-0">
														<button type="submit" class="btn btn-brown update-client" postid="<?php echo get_the_ID(); ?>" postslug="<?php echo $post->post_name; ?>">Update</button>
													</div>
												</div>
											</div>											
										</div>
									</div>
								<?php endwhile; ?>
								<!-- end of the loop -->
							

							<?php if (function_exists("pagination")) {
							    pagination($client_query->max_num_pages);
							} ?>
							<!-- pagination here -->
							</div>

							<?php wp_reset_postdata(); ?>

						<?php else : ?>
							<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
						<?php endif; ?>
						</div>
					</div>		

					<?php } ?> 
				<?php } ?> 
				</div>
			<?php } else { ?>
					<div class="error-login-wrapper text-center">
						<h4>Please login to visit this page.</h4>
						<a href="<?php echo $redux_demo['header-login']; ?>" class="btn custom-btn">Login</a>
					</div>
				<?php 
					}
				?>					
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();
