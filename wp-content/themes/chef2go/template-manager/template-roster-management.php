<?php
/**
* Template Name: Manager Roster management
*/

get_header(); 
global $redux_demo;
?>

	<div id="primary" class="content-area">
		<div class="other-entry-header text-center">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
							<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
						</div><!-- .entry-header -->
				</div>
			</div>
		</div>
		<div class="breadcrumbs-container">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<?php dimox_breadcrumbs(); ?>
					</div>
				</div>
			</div>			
		</div>
		<main id="main" class="container site-main" role="main">
			<div class="row">
			<?php if(is_user_logged_in()){ ?>
				<div class="col-xs-12 col-sm-3">
					<div id="sidebar" class="sidebar">
						<?php if ( is_active_sidebar( 'manager-page-widget' ) ) : ?>
							<div id="manager-sidebar">
								<?php dynamic_sidebar( 'manager-page-widget' ); ?>
							</div>
						<?php endif; ?>
					</div>
				</div>
				<div class="col-xs-12 col-sm-9">
				<?php $current_user = wp_get_current_user();
				foreach ($current_user->roles as $roleskey => $rolesvalue) {
					if ($rolesvalue == 'manager') { ?>
						<div class="row">
							<div class="col-xs-12">
								<div class="manager-add-roster">
									<a href="<?php echo site_url(); ?>/add-roster/" class="btn custom-btn">Add Roster</a>
								</div>
							</div>
						</div>
						<div class="row"><!-- Start Roster Listing -->
							<div class="manager-roster-filter clearfix">
								<div class="col-sm-6">
									<div class="manager-roster-filter-option-inner">
										<div class="form-group">
									      <label for="manager-roster">Location:</label>
												<select class="form-control manager_roster_residency" id="manager-roster">
											<?php 
												$rosterterms = get_terms( 'location_residency', array(
												    'orderby'    => 'count',
												    'hide_empty' => 0,
												) );
												if ( ! empty( $rosterterms ) && ! is_wp_error( $rosterterms ) ){

												        echo '<option value="all">All</option>';
												    foreach ( $rosterterms as $rosterterm ) {
												        echo '<option value='.$rosterterm->slug.'>' . $rosterterm->name . '</option>';
												    }
												}
											?>
											</select>
										</div>				
									</div>
								</div>
								<div class="col-sm-6">
									<div class="roster-user-filter-option-inner">
										<div class="form-group">
									      <label for="roster-user-filter">Filter by User:</label>
												<select class="form-control" id="roster-user-filter">
											<?php 
												// get the chefs
												$chef_query = new WP_User_Query(
													array(
														'role'	 =>	'chef_employee',
													)
												);
												$chefs = $chef_query->get_results();

												// get the staff
												$staff_employee_query = new WP_User_Query(
													array(
														'role'	 =>	'staff_employee',
													)
												);
												$staffs = $staff_employee_query->get_results();

												// store them all as users
												$users = array_merge( $staffs, $chefs );

												// User Loop
												if ( ! empty( $users ) ) {
													/*foreach ( $users as $user ) {
											            $choices[] = array( 'text' => $user->user_login, 'value' => $user->id );
													}*/
											    	echo '<option value="all">All</option>';
											    foreach ( $users as $user ) {
											        echo '<option value='.$user->id.'>' . $user->user_login . '</option>';
											    }

												} else {
											    echo '<option value="all">No users found</option>';
												}
											?>
											</select>
										</div>				
									</div>
								</div>
							</div>
							<div class="col-xs-12">
								<div class="row">
									<div class="manager-roster-listing">
									<?php 
										$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
										$rosterargs = array(
											'post_type' => 'rosters',
											'posts_per_page' => 15,
											'paged' => $paged,
										); 
										// the query
										$roster_the_query = new WP_Query( $rosterargs ); 
										?>

										<?php if ( $roster_the_query->have_posts() ) : ?>

										<!-- pagination here -->

										<!-- the loop -->
										<?php while ( $roster_the_query->have_posts() ) : $roster_the_query->the_post(); ?>
											<div class="col-sm-6">
												<div id="manager-roster-list" class="list-group-item">
													<span class="badge delete-roster-data" post-id="<?php echo get_the_ID(); ?>" title="Delete Roster"><i class="fa fa-trash-o" aria-hidden="true"></i></span>
													<span class="badge edit-roster" title="Edit Roster"><a href="<?php echo get_page_link($redux_demo['edit_roster']).'?post_id='.get_the_ID(); ?>" data-value="<?php echo get_the_ID(); ?>"><i class="fa fa-pencil" aria-hidden="true"></i></a></span>
													<div class="rosters-list-function">
													<?php
														$get_title = get_the_title(); 
														$date=date_create($get_title);
														echo date_format($date,"D, j F, Y"); 
													?>											
													</div>
													<div class="roster-employee-name roster-meta">
														<span>Name: </span>
														<?php
															$roster_member_list = get_post_meta( get_the_ID(), 'roster_member_list', true );
															$author_name = get_user_by('id', $roster_member_list);
															echo $author_name->first_name .' '. $author_name->last_name;
														?>
													</div>
													<div class="roster_location roster-meta">
														<span>Location: </span>
														<?php 
															$term_list = wp_get_post_terms($post->ID, 'location_residency', array("fields" => "names"));
															foreach ($term_list as $term_listkey => $term_listvalue) {
																	echo $term_listvalue;
																}	
														?>														
													</div>
													<div class="roster-show-more">
														<a class="show-less-btn">Show More</a>
													</div>
													<div class="extradata">
														<table>
														<?php 
															for ($metaloop = 1; $metaloop <= 10; $metaloop++) { 
																$rosterweekday = 'roster_week_day_'.$metaloop;
																$rosterweekdayval = get_post_meta( get_the_ID(), $rosterweekday, true );	
																
																$roster_time_start = 'roster_time_start_'.$metaloop;
																$roster_time_startval = get_post_meta( get_the_ID(), $roster_time_start, true );

																$roster_time_finish = 'roster_time_finish_'.$metaloop;
																$roster_time_finishval = get_post_meta( get_the_ID(), $roster_time_finish, true );

																$roster_clients = 'roster_clients_'.$metaloop;
																$roster_clientsval = get_post_meta( get_the_ID(), $roster_clients, true );

															if ($rosterweekdayval && $roster_time_startval && $roster_time_finishval && $roster_clientsval) {
															?>
															<tr class="roster-meta-details">
																<td><?php echo $rosterweekdayval; ?></td>
																<td><?php echo $roster_time_startval; ?></td>
																<td><?php echo $roster_time_finishval; ?></td>
																<td><?php echo get_the_title($roster_clientsval); ?></td>														
															</tr>
														<?php }
														}
														?>
														</table>
													</div>											
												</div>
											</div>
										<?php endwhile; ?>
										<!-- end of the loop -->
										<!-- pagination here -->
										<?php if (function_exists("pagination")) {
										    pagination($roster_the_query->max_num_pages);
										} ?>
										<div class="delete-sucess-message"><h4 class="text-center"></h4></div>

										<?php wp_reset_postdata(); ?>

									<?php else : ?>
										<div class="list-group">
											<a href="#" class="list-group-item" title="No Roster Added">
												<span class="badge"></span>
												<div class="no-rosters-added"><?php _e( 'Sorry, no roster found.' ); ?></div>
											</a>
										</div>
									<?php endif; ?>
									</div>
								</div>
							</div>
						</div><!-- End roster listing -->
					<?php } ?> 
				<?php } ?> 
				</div>
			<?php } else { ?>
					<div class="error-login-wrapper text-center">
						<h4>Please login to visit this page.</h4>
						<a href="<?php echo $redux_demo['header-login']; ?>" class="btn custom-btn">Login</a>
					</div>
				<?php 
					}
				?>					
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();
