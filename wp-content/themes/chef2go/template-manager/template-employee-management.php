<?php
/**
* Template Name: Manager Employee Management
*/

get_header(); 
global $redux_demo;
?>

	<div id="primary" class="content-area">
		<div class="other-entry-header text-center">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
							<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
						</div><!-- .entry-header -->
				</div>
			</div>
		</div>
		<div class="breadcrumbs-container">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<?php dimox_breadcrumbs(); ?>
					</div>
				</div>
			</div>			
		</div>
		<main id="main" class="container site-main" role="main">
			<div class="row">
			<?php if(is_user_logged_in()){ ?>
				<div class="col-xs-12 col-sm-3">
					<div id="sidebar" class="sidebar">
						<?php if ( is_active_sidebar( 'manager-page-widget' ) ) : ?>
							<div id="manager-sidebar">
								<?php dynamic_sidebar( 'manager-page-widget' ); ?>
							</div>
						<?php endif; ?>
					</div>
				</div>
				<div class="col-xs-12 col-sm-9">
			<?php $current_user = wp_get_current_user();
				foreach ($current_user->roles as $roleskey => $rolesvalue) {
					if ($rolesvalue == 'manager') { ?>
					<div class="row">
						<div class="manager-employee-filter clearfix">
							<div class="col-sm-6">
								<div class="employee-filter-option-inner">
									<div class="form-group">
								      <label for="manager-employee">Location:</label>
											<select class="form-control manager_employee_residency" id="manager-employee-filter">
										<?php 
											$rosterterms = get_terms( 'location_residency', array(
											    'orderby'    => 'count',
											    'hide_empty' => 0,
											) );
											if ( ! empty( $rosterterms ) && ! is_wp_error( $rosterterms ) ){

											        echo '<option value="all">All</option>';
											    foreach ( $rosterterms as $rosterterm ) {
											        echo '<option value='.$rosterterm->slug.'>' . $rosterterm->name . '</option>';
											    }
											}
										?>
										</select>
									</div>				
								</div>
							</div>
							<div class="col-sm-6">
								<div class="employee-user-filter-option-inner">
									<div class="form-group">
								      <label for="employee-user-filter">Filter by User:</label>
											<select class="form-control" id="employee-user-filter">
										<?php 
											// get the chefs
											$chef_query = new WP_User_Query(
												array(
													'role'	 =>	'chef_employee',
												)
											);
											$chefs = $chef_query->get_results();

											// get the staff
											$staff_employee_query = new WP_User_Query(
												array(
													'role'	 =>	'staff_employee',
												)
											);
											$staffs = $staff_employee_query->get_results();

											// store them all as users
											$users = array_merge( $staffs, $chefs );

											// User Loop
											if ( ! empty( $users ) ) {
												/*foreach ( $users as $user ) {
										            $choices[] = array( 'text' => $user->user_login, 'value' => $user->id );
												}*/
										    	echo '<option value="all">All</option>';
										    foreach ( $users as $user ) {
										        echo '<option value='.$user->id.'>' . $user->user_login . '</option>';
										    }

											} else {
										    echo '<option value="all">No users found</option>';
											}
										?>
										</select>
									</div>				
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="employee-listing clearfix">
						<?php 
							$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
							$user_args = array(
						        'post_type' => array('chef', 'hospitality_staff'),
						        'posts_per_page' => 12,
		            			'paged' => $paged,
						    );
							// the query
							$user_query = new WP_Query( $user_args ); ?>

						<?php if ( $user_query->have_posts() ) : ?>

							<!-- pagination here -->

							<!-- the loop -->
							<?php while ( $user_query->have_posts() ) : $user_query->the_post(); ?>
								<div class="manager-user-listing-wrapper">
									<div class="col-xs-12 col-sm-4">
										<div class="user-listing equalheight">
											<div class="user-listing-title">
												<h2><?php the_title(); ?></h2>
											</div>
											<div class="user-listing-description">
												<?php my_excerpt(20); ?>
											</div>
											<div class="user-listing-readmore">
												<a href='<?php the_permalink(); ?>'>Read More</a>
											</div>											
										</div>
									</div>
								</div>
							<?php endwhile; ?>
							<!-- end of the loop -->
							
							<?php if (function_exists("pagination")) {
							   pagination($user_query->max_num_pages);
				  			} ?>
							<!-- pagination here -->

							<?php wp_reset_postdata(); ?>

						<?php else : ?>
							<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
						<?php endif; ?>							
						</div>
					</div><!-- Row end -->

					<?php } ?> 
				<?php } ?> 
				</div>
			<?php } else { ?>
					<div class="error-login-wrapper text-center">
						<h4>Please login to visit this page.</h4>
						<a href="<?php echo $redux_demo['header-login']; ?>" class="btn custom-btn">Login</a>
					</div>
				<?php 
					}
				?>					
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();
