<?php
/**
* Template Name: Manager Invoice
*/

get_header(); 
global $redux_demo;
?>

	<div id="primary" class="content-area">
		<div class="other-entry-header text-center">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
							<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
						</div><!-- .entry-header -->
				</div>
			</div>
		</div>
		<div class="breadcrumbs-container">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<?php dimox_breadcrumbs(); ?>
					</div>
				</div>
			</div>			
		</div>
		<main id="main" class="container site-main" role="main">
			<div class="row">
			<?php if(is_user_logged_in()){ ?>
				<div class="col-xs-12 col-sm-3">
					<div id="sidebar" class="sidebar">
						<?php if ( is_active_sidebar( 'manager-page-widget' ) ) : ?>
							<div id="manager-sidebar">
								<?php dynamic_sidebar( 'manager-page-widget' ); ?>
							</div>
						<?php endif; ?>
					</div>
				</div>
				<div class="col-xs-12 col-sm-9">
					<?php
					while ( have_posts() ) : the_post();

						the_content();

					endwhile; // End of the loop.
					?> 
				</div>
			<?php } else { ?>
					<div class="error-login-wrapper text-center">
						<h4>Please login to visit this page.</h4>
						<a href="<?php echo $redux_demo['header-login']; ?>" class="btn custom-btn">Login</a>
					</div>
				<?php 
					}
				?>					
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();
