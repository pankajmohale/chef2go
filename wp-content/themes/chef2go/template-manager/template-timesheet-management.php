<?php
/**
* Template Name: Manager Timesheet Management
*/

get_header(); 
global $redux_demo;
?>

	<div id="primary" class="content-area">
		<div class="other-entry-header text-center">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
							<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
						</div><!-- .entry-header -->
				</div>
			</div>
		</div>
		<div class="breadcrumbs-container">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<?php dimox_breadcrumbs(); ?>
					</div>
				</div>
			</div>			
		</div>
		<main id="main" class="container site-main" role="main">
			<div class="row">
			<?php if(is_user_logged_in()){ ?>
				<div class="col-xs-12 col-sm-3">
					<div id="sidebar" class="sidebar">
						<?php if ( is_active_sidebar( 'manager-page-widget' ) ) : ?>
							<div id="manager-sidebar">
								<?php dynamic_sidebar( 'manager-page-widget' ); ?>
							</div>
						<?php endif; ?>
					</div>
				</div>
				<div class="col-xs-12 col-sm-9">
			<?php $current_user = wp_get_current_user();
				foreach ($current_user->roles as $roleskey => $rolesvalue) {
					if ($rolesvalue == 'manager') { ?>
					<div class="row">
						<div class="manager-timesheet-filter clearfix">
							<div class="col-xs-6">
								<div class="timesheet-filter-option-inner">
									<div class="form-group">
								      <label for="manager-timesheet">Location:</label>
											<select class="form-control manager_timesheet_residency" id="manager-timesheet-filter">
										<?php 
											$rosterterms = get_terms( 'location_residency', array(
											    'orderby'    => 'count',
											    'hide_empty' => 0,
											) );
											if ( ! empty( $rosterterms ) && ! is_wp_error( $rosterterms ) ){

											        echo '<option value="all">All</option>';
											    foreach ( $rosterterms as $rosterterm ) {
											        echo '<option value='.$rosterterm->slug.'>' . $rosterterm->name . '</option>';
											    }
											}
										?>
										</select>
									</div>				
								</div>
							</div>
							<div class="col-xs-6">
								<div class="timesheet-user-filter-option-inner">
									<div class="form-group">
								      <label for="timesheet-user-filter">Filter by User:</label>
											<select class="form-control" id="timesheet-user-filter">
										<?php 
											// get the chefs
											$chef_query = new WP_User_Query(
												array(
													'role'	 =>	'chef_employee',
												)
											);
											$chefs = $chef_query->get_results();

											// get the staff
											$staff_employee_query = new WP_User_Query(
												array(
													'role'	 =>	'staff_employee',
												)
											);
											$staffs = $staff_employee_query->get_results();

											// store them all as users
											$users = array_merge( $staffs, $chefs );

											// User Loop
											if ( ! empty( $users ) ) {
												/*foreach ( $users as $user ) {
										            $choices[] = array( 'text' => $user->user_login, 'value' => $user->id );
												}*/
										    	echo '<option value="all">All</option>';
										    foreach ( $users as $user ) {
										        echo '<option value='.$user->id.'>' . $user->user_login . '</option>';
										    }

											} else {
										    echo '<option value="all">No users found</option>';
											}
										?>
										</select>
									</div>				
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="timesheet-listing col-xs-12">
							<?php 
							$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
							$timesheet_args = array(
						        'post_type' => array('timesheet'),
						        'posts_per_page' => 10,
		            			'paged' => $paged,
						    );
							// the query
							$timesheet_query = new WP_Query( $timesheet_args ); ?>

							<?php if ( $timesheet_query->have_posts() ) : ?>

								<!-- pagination here -->
								<div class="manager-timesheet-listing-wrapper">
									<ul class="timesheet-list list-group">
									<!-- the loop -->
									<?php while ( $timesheet_query->have_posts() ) : $timesheet_query->the_post(); ?>
										<li class="list-group-item clearfix">
							  				<div class="col-sm-3">
							  					<?php 
							  						$timesheet_user_id = get_post_meta( get_the_ID(), 'timesheet_uploaded_userid', true );
							  						$user = get_user_by('id', $timesheet_user_id);
							  					?>
												<?php echo $user->first_name . ' ' . $user->last_name; ?>						  					
							  				</div>
							  				<div class="col-sm-3">
							  					<?php $timesheet_date = get_the_date( 'l, F j, Y', get_the_ID() ); ?>
							  					<?php echo $timesheet_date; ?>
							  				</div>
							  				<div class="col-sm-4">
							  					<?php 
							  						$timesheet_uploaded_client = get_post_meta( get_the_ID(), 'timesheet_uploaded_client', true );
							  						echo get_the_title($timesheet_uploaded_client); 
							  					?>
							  				</div>
							  				<div class="col-sm-2">
							  					<?php 
							  						$timesheet_uploaded_sheet_value = get_post_meta( get_the_ID(), 'timesheet_uploaded_sheet', true );
							  					?>
												<a href='<?php echo $timesheet_uploaded_sheet_value; ?>' download>Download</a>
							  				</div>
							  			</li>
									<?php endwhile; ?>
									<!-- end of the loop -->
									</ul>
								</div>
								
								<?php if (function_exists("pagination")) {
								   pagination($user_query->max_num_pages);
					  			} ?>
								<!-- pagination here -->

								<?php wp_reset_postdata(); ?>

							<?php else : ?>
								<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
							<?php endif; ?>						
						</div>
					</div><!-- Row end -->

					<?php } ?> 
				<?php } ?> 
				</div>
			<?php } else { ?>
					<div class="error-login-wrapper text-center">
						<h4>Please login to visit this page.</h4>
						<a href="<?php echo $redux_demo['header-login']; ?>" class="btn custom-btn">Login</a>
					</div>
				<?php 
					}
				?>					
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();
