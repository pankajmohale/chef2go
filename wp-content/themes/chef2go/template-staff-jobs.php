<?php
/**
* Template Name: Staff jobs
*/

get_header(); 
global $redux_demo;
?>

	<div id="primary" class="content-area">
		<div class="other-entry-header text-center">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
							<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
						</div><!-- .entry-header -->
				</div>
			</div>
		</div>
		<div class="breadcrumbs-container">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<?php dimox_breadcrumbs(); ?>
					</div>
				</div>
			</div>			
		</div>
		<main id="main" class="container site-main" role="main">
			<div class="row">
				<div class="col-xs-12 col-sm-9">
					<?php
					while ( have_posts() ) : the_post();

						get_template_part( 'template-parts/other', 'page' );

					endwhile; // End of the loop.
					?>

					<?php 
					$args = array(
						'post_type' => 'jobs',
						'tax_query' => array(
							array(
								'taxonomy' => 'job_opening',
								'field'    => 'slug',
								'terms'    => 'staff',
							),
						),
					);
					// the query
					$the_query = new WP_Query( $args ); ?>

					<?php if ( $the_query->have_posts() ) : ?>

						<!-- pagination here -->

						<!-- the loop -->
						<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
							<h4><?php the_title(); ?></h4>
							<?php the_content(); ?>

									<?php
									// Apply now button for user login and logout user
									if ( is_user_logged_in() ) { 
										$current_user_id = get_current_user_id();
										?>
										<div class="apply-btn clearfix">
										    <!-- Small modal -->
											<!-- <button type="button" class="btn custom-btn pull-right job_apply" data-toggle="modal" data-target=".bs-example-modal-sm-<?php echo get_the_ID(); ?>" applied_post_id="<?php echo get_the_ID(); ?>" applied_user="<?php echo $current_user_id; ?>">Apply Now</button> -->

											<?php 
												$applied_job_val = get_user_meta( $current_user_id,  'applied_job', true);
												$applied_jobArray = explode(',', $applied_job_val);
												if (in_array(get_the_ID(), $applied_jobArray)) { ?>
													<button type="button" class="btn custom-btn pull-right" data-toggle="modal" disabled>Already applied</button>
												<?php } else { ?>
													<button type="button" class="btn custom-btn pull-right job_apply" data-toggle="modal" data-target=".bs-example-modal-sm-<?php echo get_the_ID(); ?>" applied_post_id="<?php echo get_the_ID(); ?>" applied_user="<?php echo $current_user_id; ?>">Apply Now</button>
											<?php	}
											?>

											<div class="modal fade bs-example-modal-sm-<?php echo get_the_ID(); ?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
											  <div class="modal-dialog modal-sm" role="document">
											    <div class="modal-content">
											    	<div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button> <h4 class="modal-title" id="mySmallModalLabel"><?php the_title(); ?></h4> </div>
											    	<div class="modal-body">
											    		Thank you for appling this job.
											    	</div>
											    </div>
											  </div>
											</div>
										</div>
									<?php } else { ?>
										<div class="apply-btn clearfix">
										    <!-- Small modal -->
											<a class="btn custom-btn pull-right" href="<?php echo esc_url( get_permalink( get_page_by_title( 'Login' ) ) ); ?>"><?php esc_html_e( 'Apply Now', 'textdomain' ); ?></a>
											<!-- <button type="button" class="btn custom-btn pull-right" data-toggle="modal" data-target=".bs-example-modal-sm-<?php echo get_the_ID(); ?>">Apply Now</button> -->

											<!-- <div class="modal fade bs-example-modal-sm-<?php echo get_the_ID(); ?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
											  <div class="modal-dialog modal-sm" role="document">
											    <div class="modal-content">
											    	<div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button> <h4 class="modal-title" id="mySmallModalLabel"><?php the_title(); ?></h4> </div>
											    	<div class="modal-body">
											      		You need to register first to apply this job.
											    	</div>
											    </div>
											  </div>
											</div> -->
											
										</div>
									<?php 
										}
									?>

							<hr />
						<?php endwhile; ?>
						<!-- end of the loop -->

						<!-- pagination here -->

						<?php wp_reset_postdata(); ?>

					<?php else : ?>
						<p><?php _e( 'Sorry, no jobs matched your criteria.' ); ?></p>
					<?php endif; ?>


				</div>
				<div class="col-xs-12 col-sm-3">
					<div id="sidebar" class="sidebar">
						<?php get_sidebar(); ?>
					</div>
				</div>
			</div>

		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();