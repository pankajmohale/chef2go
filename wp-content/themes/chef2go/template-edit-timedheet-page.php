<?php
/**
* Template Name: Edit Timesheet Pages
*/

get_header(); ?>

	<div id="primary" class="content-area">
		<div class="other-entry-header text-center">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
							<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
						</div><!-- .entry-header -->
				</div>
			</div>
		</div>
		<div class="breadcrumbs-container">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<?php dimox_breadcrumbs(); ?>
					</div>
				</div>
			</div>			
		</div>
		<main id="main" class="container site-main" role="main">
			<div class="row">
				<div class="col-xs-12 col-sm-9">
					<div class="myarea-page-links">
						<a href="<?php echo site_url(); ?>/my-area/" class="btn custom-btn">Back to my Area</a>
					</div>
					<div class="gravityform-update-post">
						<?php
							$timesheet_id = $_GET['timesheet_id'];
							echo do_shortcode('[gravityform id="24" title="false" description="false" update="'. $timesheet_id .'"]');
						?>
					</div>
				</div>
				<div class="col-xs-12 col-sm-3">
					<div id="sidebar" class="sidebar">
						<?php get_sidebar(); ?>
					</div>
				</div>
			</div>

		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();
