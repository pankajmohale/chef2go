<?php
/**
* Template Name: Edit Staff Template
*/
get_header(); 
$current_user = wp_get_current_user();
?>

	<div id="primary" class="content-area">
		<div class="other-entry-header text-center">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
							<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
						</div><!-- .entry-header -->
				</div>
			</div>
		</div>
		<div class="breadcrumbs-container">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<?php dimox_breadcrumbs(); ?>
					</div>
				</div>
			</div>			
		</div>
		<main id="main" class="container site-main" role="main">
			<div class="row">
				<?php 
					foreach ($current_user->roles as $current_user_value) {
						$current_user_role = $current_user_value;
					}
					if ($current_user_role == 'recruiter') { 
				?>
				<div class="col-xs-12">
					<div class="myarea-page-links pull-right">
						<a href="<?php echo site_url(); ?>/my-area/" class="btn custom-btn">My Area</a>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12">
					<?php
					while ( have_posts() ) : the_post();

						get_template_part( 'template-parts/other', 'page' );

						// If comments are open or we have at least one comment, load up the comment template.
						if ( comments_open() || get_comments_number() ) :
							comments_template();
						endif;

					endwhile; // End of the loop.
					?>					
				</div>
				<div class="col-xs-12 col-sm-12">
					<div class="row">
						<div id="add_staff_section" class="add-staff-form">
							<div class="staff-form clearfix">
								<div class="col-xs-12 col-sm-4 col-md-5">
									<div class="form-group">
										
										<label for="editstaffuser" class="col-form-label">Edit Staff Members</label>
										<?php 
											$recruiterargs = array(
												'post_type' => 'recruiter',
												'post_status' => array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash'),
											); 
											// the query
											$the_query = new WP_Query( $recruiterargs );	
										?>

										<?php if ( $the_query->have_posts() ) : ?>

											<!-- pagination here -->

											<!-- the loop -->
											<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
												<?php

													$recruiter_user_id = get_post_meta(get_the_ID(),'recruiter_user_id',true);
													


													$recruiter_selected_staff_val = get_post_meta(get_the_ID(),'recruiter_selected_staff',true);

													if ($current_user->ID == $recruiter_user_id) {
														if ($recruiter_selected_staff_val != '') {

															$already_val = unserialize($recruiter_selected_staff_val);
															foreach ($already_val as $metavalue) {
																$user_info = get_userdata($metavalue['u_id']);
																if ($metavalue['u_id'] == $_GET['aut_id']){ 
															?>
																<input class="form-control datepicker editstaffuser" type="text" name="editstaffuser" id="editstaffuser" value="<?php echo $user_info->first_name; ?>" user_id="<?php echo $_GET['aut_id']; ?>" readonly>

															<?php
																}
															}

														}
													}
												?>							
											<?php endwhile; ?>
											<!-- end of the loop -->

											<!-- pagination here -->

											<?php wp_reset_postdata(); ?>

										<?php else : ?>
											<p><?php _e( 'Sorry, no recruiter matched your criteria.' ); ?></p>
										<?php endif; ?>

									</div>
								</div>
								<div class="col-xs-12 col-sm-4 col-md-4">
									<div class="form-group">
										<label for="editstaffuserrole" class="col-form-label">Edit Staff Members Role</label>
										<input class="form-control datepicker editstaffuserrole" type="text" name="editstaffuserrole" id="editstaffuserrole" value="">
									</div>
								</div>
								<div class="col-xs-12 col-sm-3 col-md-3">
									<div class="form-group edit-staff-button-wrapper">
										<label for="editstaffmember" class="col-form-label custom-label">&nbsp;</label>
										<button class="btn custom-btn-pdf editstaffmember">Edit Add Staff Member</button>
									</div>
								</div>
							</div>
						</div>	
						<div class="col-xs-12">
							<div class="sucess-message"></div>
						</div>
					</div>
				</div>
				<?php	
					} else { ?>
						<div class="error-login-wrapper text-center">
							<h4>Please login to visit this page.</h4>
							<a href="<?php echo $redux_demo['header-login']; ?>" class="btn custom-btn">Login</a>							
						</div>
				<?php	
					}
				?>
			</div>

		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();
