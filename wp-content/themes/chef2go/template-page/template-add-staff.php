<?php
/**
* Template Name: Add Staff Template
*/
get_header(); 
$current_user = wp_get_current_user();
?>

	<div id="primary" class="content-area">
		<div class="other-entry-header text-center">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
							<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
						</div><!-- .entry-header -->
				</div>
			</div>
		</div>
		<div class="breadcrumbs-container">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<?php dimox_breadcrumbs(); ?>
					</div>
				</div>
			</div>			
		</div>
		<main id="main" class="container site-main" role="main">
			<div class="row">
				<?php 
					foreach ($current_user->roles as $current_user_value) {
						$current_user_role = $current_user_value;
					}
					if ($current_user_role == 'recruiter') { 
				?>
				<div class="col-xs-12">
					<div class="myarea-page-links pull-right">
						<a href="<?php echo site_url(); ?>/my-area/" class="btn custom-btn">My Area</a>
					</div>
				</div>					
				<div class="col-xs-12 col-sm-12">
					<?php
					while ( have_posts() ) : the_post();

						get_template_part( 'template-parts/other', 'page' );

						// If comments are open or we have at least one comment, load up the comment template.
						if ( comments_open() || get_comments_number() ) :
							comments_template();
						endif;

					endwhile; // End of the loop.
					?>					
				</div>
				<div class="col-xs-12 col-sm-12">
					<div class="row">
						<div id="add_staff_section" class="add-staff-form">
							<?php get_template_part( 'template-parts/user-module/recruiter-add-staff', 'template' ); ?>
						</div>	
						<div class="col-xs-12">
							<div class="sucess-message"></div>
						</div>
					</div>
				</div>				
				<?php	
					} else { ?>
						<div class="error-login-wrapper text-center">
							<h4>Please login to visit this page.</h4>
							<a href="<?php echo $redux_demo['header-login']; ?>" class="btn custom-btn">Login</a>							
						</div>
				<?php	
					}
				?>
			</div>

		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();
