<?php
/**
 * The template for displaying Home page.
 */

get_header();
?>

            <section class="slider-section">
                <?php 
                    if ( function_exists( 'soliloquy' ) ) { soliloquy( 'home-slider', 'slug' ); }
                ?>
            </section>
            <section class="home-featured-section home-sections">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="home-featured">
                                <div class="home-featured-thumb"><img src="<?php echo $redux_demo['recruitment-section-media']['url']; ?>" class="img-responsive"></div>
                                <div class="home-featured-title"><h2><?php echo $redux_demo['recruitment-section-title']; ?></h2></div>
                                <div class="home-featured-description"><p><?php echo $redux_demo['recruitment-section-editor']; ?></p></div>
                                <div class="home-featured-link"><a href="<?php echo $redux_demo['recruitment-section-link']; ?>" >Read More &nbsp; <i class="fa fa-angle-right" aria-hidden="true"></i></a></div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="home-featured">
                                <div class="home-featured-thumb"><img src="<?php echo $redux_demo['staff-on-demand-section-media']['url']; ?>" class="img-responsive"></div>
                                <div class="home-featured-title"><h2><?php echo $redux_demo['staff-on-demand-section-title']; ?></h2></div>
                                <div class="home-featured-description"><p><?php echo $redux_demo['staff-on-demand-section-editor']; ?></p></div>
                                <div class="home-featured-link"><a href="<?php echo $redux_demo['staff-on-demand-section-link']; ?>" >Read More &nbsp; <i class="fa fa-angle-right" aria-hidden="true"></i></a></div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="home-featured">
                                <div class="home-featured-thumb"><img src="<?php echo$redux_demo['jobs-on-candidates-section-media']['url']; ?>" class="img-responsive"></div>
                                <div class="home-featured-title"><h2><?php echo $redux_demo['jobs-on-candidates-section-title']; ?></h2></div>
                                <div class="home-featured-description"><p><?php echo $redux_demo['jobs-on-candidates-section-editor']; ?></p></div>
                                <div class="home-featured-link"><a href="<?php echo $redux_demo['jobs-on-candidates-section-link']; ?>" >Read More &nbsp; <i class="fa fa-angle-right" aria-hidden="true"></i></a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="divider home-sections">
                <div class="container">
                    <div class="row">
                        <div class="icon-divider-wrapper col-xs-12">
                            <div class="divide-line">
                                <div class="icon icons-divider"></div>
                            </div>
                        </div>
                    </div>                    
                </div>
            </section>
            <section class="home-about-section home-sections">
                <div class="container">
                    <div class="home-about-wrapper clearfix">
                        <div class="home-about-title-section clearfix">
                            <div class="row">
                                <div class="col-sm-10">
                                    <div class="home-about-title"><h2><?php echo $redux_demo['about-section-title']; ?></h2></div>
                                    <div class="home-about-short-desc"><h6><?php echo $redux_demo['about-section-short-desc']; ?></h6></div>
                                </div>
                            </div>
                        </div>
                        <div class="home-about-full-description">
                            <div class="row">
                                <div class="col-sm-9 col-md-10">
                                    <p><?php echo $redux_demo['about-section-editor']; ?></p>
                                </div>                                
                                <div class="col-sm-3 col-md-2">
                                    <div class="home-about-desc"><a href="<?php echo $redux_demo['about-section-link']; ?>" class="btn custom-btn">Read More</a></div>                                
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="home-upcoming-section home-sections">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12"><div class="home-top-jos-title"><h2>Top Jobs</h2></div></div>
                    </div>
                    <div class="row upcoming-section">
                        <!-- <?php if ( is_active_sidebar( 'home-upcoming-section' ) ) : ?>
                                <?php dynamic_sidebar( 'home-upcoming-section' ); ?>
                        <?php endif; ?> -->
                        <div class="home-latest-jobs-post clearfix">
                            
                            <?php 
                            $args = array(
                                'post_type'         => 'jobs',
                                'orderby'           => 'date',
                                'order'             => 'DESC',
                                'posts_per_page'    => 4,                               
                            );
                            // the query
                            $the_query = new WP_Query( $args ); ?>

                            <?php if ( $the_query->have_posts() ) : ?>

                                <!-- pagination here -->

                                <!-- the loop -->
                                <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="home-jobs-wrapper equalheight">
                                            <div class="home-jobs-post-title"><h3><?php the_title(); ?></h3></div>
                                            <div class="home-jobs-post-desc"><?php the_excerpt(); ?></div>
                                            <div class="home-jobs-post-link"><a href="<?php the_permalink(); ?>">Read More <i class="fa fa-angle-right" aria-hidden="true"></i></a></div>
                                        </div>                                        
                                    </div>

                                <?php endwhile; ?>
                                <!-- end of the loop -->

                                <!-- pagination here -->

                                <?php wp_reset_postdata(); ?>

                            <?php else : ?>
                                <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
                            <?php endif; ?>
                        </div>            
                    </div>
                </div>
            </section>
            <section class="home-testimonial-section home-sections">
                
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12"><div class="home-testimonial-title"><h2><?php echo $redux_demo['testimonials-section-title']; ?></h2></div></div>
                    </div>
                    <div class="row">
                        <?php 
                            $testi = array(
                                'post_type' => 'testimonial',                                
                            );
                            $testimonial = new WP_Query( $testi ); ?>

                        <?php if ( $testimonial->have_posts() ) : ?>
                            <div class="testimonials">
                            <!-- pagination here -->
                              <div class="swiper-container testimonial-slider">
                                    <!-- Add Pagination -->
                                    <div class="swiper-pagination testimonial-pagination"></div>
                                <div class="swiper-wrapper">
                                <!-- the loop -->
                                <?php while ( $testimonial->have_posts() ) : $testimonial->the_post(); ?>
                                    <?php 
                                        $testimonials_author = get_post_meta(get_the_id(),'_testimonials_author',true); 
                                        $testimonials_title = get_post_meta(get_the_id(),'_testimonials_title',true); 
                                        $testimonials_location = get_post_meta(get_the_id(),'_testimonials_location',true); 
                                        $testimonials_email = get_post_meta(get_the_id(),'_testimonials_email',true); 
                                        $testimonials_company = get_post_meta(get_the_id(),'_testimonials_company',true); 
                                        $testimonials_url = get_post_meta(get_the_id(),'_testimonials_url',true);  
                                    ?>
                                    <div class="swiper-slide testimonial-slide">
                                        <blockquote class="quote">
                                            <div class="testimonial-quote"><?php the_content(); ?></div>
                                            <div class="author-details">
                                                <?php if($testimonials_author){ ?>
                                                    <div class="testimonial-author-name"><?php echo $testimonials_author; ?></div>
                                                <?php } ?>
                                                <?php if($testimonials_title){ ?>
                                                    <div class="testimonial-author-job-title"><?php echo $testimonials_title; ?></div>
                                                <?php } ?>
                                                <?php if($testimonials_location){ ?>
                                                    <div class="testimonial-author-location"><?php echo $testimonials_location; ?></div>
                                                <?php } ?>
                                                <?php if($testimonials_company){ ?>
                                                    <div class="testimonial-author-company"><?php echo $testimonials_company; ?></div>
                                                <?php } ?>
                                                <?php if($testimonials_email){ ?>
                                                    <div class="testimonial-author-email"><?php echo $testimonials_email; ?></div>
                                                <?php } ?>
                                                <?php if($testimonials_url){ ?>
                                                    <div class="testimonial-author-url"><?php echo $testimonials_url; ?></div>
                                                <?php } ?>
                                            </div>
                                        </blockquote>
                                    </div>

                                    <?php endwhile; ?>
                                    <!-- end of the loop -->

                                    <!-- pagination here -->

                                    <?php wp_reset_postdata(); ?>

                                    </div>
                                    <!-- Add Arrows -->
                                </div>
                            </div>
                            <?php else : ?>
                                <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
                            <?php endif; ?>
                    </div>
                </div>
            </section>
            <section class="divider home-sections">
                <div class="container">
                    <div class="row">
                        <div class="icon-divider-wrapper col-xs-12">
                            <div class="divide-line">
                                <div class="icon icons-divider"></div>
                            </div>
                        </div>
                    </div>                    
                </div>
            </section>
            <section class="home-blog-section home-sections">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12"><div class="home-blog-title"><h2>Latest Blog Posts</h2></div></div>
                    </div>
                    <div class="row">
                        <div class="home-latest-blog-post clearfix">
                            
                                <?php 
                                $args = array(
                                    'post_type' => 'post',
                                    'orderby' => 'date',
                                    'order'   => 'DESC',                                
                                );
                                // the query
                                $the_query = new WP_Query( $args ); ?>

                                <?php if ( $the_query->have_posts() ) : ?>

                                    <!-- pagination here -->

                                    <!-- the loop -->
                                    <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                                        <div class="col-sm-3">
                                            <div class="home-blog-post-thumb"><img src="<?php the_post_thumbnail_url( 'medium' ); ?>" class="img-responsive"/></div>
                                            <div class="home-blog-post-title"><p><?php the_title(); ?></p></div>
                                            <div class="home-blog-post-date"><p><?php echo get_the_date('F j, Y'); ?></p></div>
                                        </div>

                                    <?php endwhile; ?>
                                    <!-- end of the loop -->

                                    <!-- pagination here -->

                                    <?php wp_reset_postdata(); ?>

                                <?php else : ?>
                                    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
                                <?php endif; ?>
                        </div>
                    </div>
                </div>
            </section>
            <section class="home-map-section">
                <div class="container-fluid-">
                    <?php echo do_shortcode('[google_map_easy id="1"]')?>
                </div>
            </section>

<?php
get_footer();
