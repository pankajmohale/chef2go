<?php
/**
* Template Name: Myarea Pages
*/

get_header(); 
global $redux_demo;
?>

	<div id="primary" class="content-area">
		<div class="other-entry-header text-center">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
							<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
						</div><!-- .entry-header -->
				</div>
			</div>
		</div>
		<div class="breadcrumbs-container">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<?php dimox_breadcrumbs(); ?>
					</div>
				</div>
			</div>			
		</div>
		<main id="main" class="container site-main" role="main">
			<div class="row">
				<div class="col-xs-12">
			<?php if(is_user_logged_in()){
				$current_user = wp_get_current_user();
				foreach ($current_user->roles as $roleskey => $rolesvalue) {
					if ($rolesvalue == 'chef_employee') { ?>
							
						<!-- Chef Myarea Template -->
						<?php get_template_part( 'template-parts/user-module/chef', 'template' ); ?>
						<?php get_template_part( 'template-parts/user-module/staff', 'template' ); ?>
						<!-- End Chef Myarea Template -->

					<?php } elseif ($rolesvalue == 'recruiter') { ?>

						<!-- Recruiter Myarea Template -->
						<?php get_template_part( 'template-parts/user-module/recruiter', 'template' ); ?>			
						<!-- End Recruiter Myarea Template -->

					<?php } elseif ($rolesvalue == 'staff_employee') { ?>
								
						<!-- Staff Myarea Template -->
						<?php get_template_part( 'template-parts/user-module/chef', 'template' ); ?>
						<?php get_template_part( 'template-parts/user-module/staff', 'template' ); ?>
						<!-- End Staff Myarea Template -->

					<?php } else { ?>

						<!-- Admin Myarea Template -->
						<?php get_template_part( 'template-parts/user-module/admin', 'template' ); ?>
						<!-- End Admin Myarea Template -->
					
					<?php } ?> 
				<?php } ?> 
			<?php } else { ?>
					<div class="error-login-wrapper text-center">
						<h4>Please login to visit this page.</h4>
						<a href="<?php echo $redux_demo['header-login']; ?>" class="btn custom-btn">Login</a>							
					</div>
				<?php 
					}
				?>					
				</div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();
