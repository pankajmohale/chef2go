<?php
/**
* Template Name: Staff listing
*/

get_header(); ?>

	<div id="primary" class="content-area">
		<div class="other-entry-header text-center">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
							<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
						</div><!-- .entry-header -->
				</div>
			</div>
		</div>
		<div class="breadcrumbs-container">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<?php dimox_breadcrumbs(); ?>
					</div>
				</div>
			</div>			
		</div>
		<main id="main" class="container site-main" role="main">
			<div class="row">
				<div class="col-xs-12 col-sm-9">
				<?php $current_user = wp_get_current_user();
					foreach ($current_user->roles as $key => $value) {
						if ($value == 'recruiter') { ?>
						<div class="myarea-page-links pull-right">
							<a href="<?php echo site_url(); ?>/my-area/" class="btn custom-btn">Back to my Area</a>
						</div>							
				<?php	}
					}
				?>
					<?php
					while ( have_posts() ) : the_post();

						the_content();
					
					endwhile; // End of the loop.
					/* =========== Chef lising =========== */
					?>
					<div class="listing-wrapper">					
						<div class="custom-filter-option">
							<div class="custom-filter-option-inner">
								<div class="form-group">
							      <label for="sel1">Filter by Location:</label>
									<?php 
										$terms = get_terms( 'location_residency', array(
										    'orderby'    => 'count',
										    'hide_empty' => 0,
										) );
										if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
										    echo '<select class="form-control staff_residency_select" id="sel1">';
										        echo '<option value="all">All</option>';
										    foreach ( $terms as $term ) {
										        echo '<option value='.$term->slug.'>' . $term->name . '</option>';
										    }
										    echo '</select>';
										}
									?>
								</div>				
							</div>
							<div class="custom-filter-option-inner">
								<div class="form-group">
							      <label for="sel2">Filter by Level:</label>
									<?php 
										$terms = get_terms( 'position_type', array(
										    'orderby'    => 'count',
										    'hide_empty' => 0,
										) );
										if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
										    echo '<select class="form-control staff_level_select" id="sel2">';
										        echo '<option value="all">All</option>';
										    foreach ( $terms as $term ) {
										        echo '<option value='.$term->slug.'>' . $term->name . '</option>';
										    }
										    echo '</select>';
										}
									?>
								</div>				
							</div>
						</div>
						<div class="staff-lising-content">
						<?php 		
							$cheflisting_args = array(
								'post_type' => 'hospitality_staff'
							);
							$cheflisting = new WP_Query( $cheflisting_args ); ?>

							<?php if ( $cheflisting->have_posts() ) : ?>

								<!-- pagination here -->
								
								<!-- the loop -->
								<?php while ( $cheflisting->have_posts() ) : $cheflisting->the_post(); ?>
									
									<?php 
										get_template_part( 'template-parts/staff', 'listing' );
									?>


								<?php endwhile; ?>
								<!-- end of the loop -->

								<!-- pagination here -->

								<?php wp_reset_postdata(); ?>

							<?php else : ?>
								<p><?php _e( 'Sorry, no staff matched your criteria.' ); ?></p>
							<?php endif; ?>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-3">
					<div id="sidebar" class="sidebar">
						<?php get_sidebar(); ?>
					</div>
				</div>
			</div>

		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();
