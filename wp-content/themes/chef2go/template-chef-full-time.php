<?php
/**
* Template Name: Chefs Seeking Full Time Work
*/

get_header(); ?>

	<div id="primary" class="content-area">
		<div class="other-entry-header text-center">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
							<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
						</div><!-- .entry-header -->
				</div>
			</div>
		</div>
		<div class="breadcrumbs-container">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<?php dimox_breadcrumbs(); ?>
					</div>
				</div>
			</div>			
		</div>
		<main id="main" class="container site-main" role="main">
			<?php if(is_user_logged_in()){ 
				$current_user = wp_get_current_user();
				foreach ($current_user->roles as $key => $recruiter) { ?>
				<?php if ($recruiter == 'recruiter') { ?>
					<div class="row">
						<div class="col-xs-12">
							<div class="myarea-page-links pull-right">
								<a href="<?php echo site_url(); ?>/my-area/" class="btn custom-btn">My Area</a>
							</div>
						</div>
						<div class="col-xs-12 col-sm-12">
							<?php
							while ( have_posts() ) : the_post();

								get_template_part( 'template-parts/other', 'page' );

							endwhile; // End of the loop.
							?>

							<div class="recruiter-filter-option">
		
								<div class="recruiter-filter-option-inner">
									<div class="form-group">
								      <label for="recruiter-level">Filter by Level:</label>
										<?php 
											$terms = get_terms( 'field_of_interest', array(
											    'orderby'    => 'count',
											    'hide_empty' => 0,
											) );
											if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
											    echo '<select class="form-control full-time-chef-filter" id="fulltime-chefs">';
											        echo '<option value="all">All</option>';
											    foreach ( $terms as $term ) {
											        echo '<option value='.$term->slug.'>' . $term->name . '</option>';
											    }
											    echo '</select>';
											}
										?>
									</div>				
								</div>

								<div class="recruiter_full_time_chef_listing">
									<?php 
										$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
										$args = array(
											'post_type' => 'chef',
											'tax_query' => array(
												array(
													'taxonomy' => 'type_of_recruitment',
													'field'    => 'slug',
													'terms'    => 'full-time-positions',
												),
											),
											'posts_per_page' => 5,
            								'paged' => $paged,
										);
										// the query
										$the_query = new WP_Query( $args ); ?>

										<?php if ( $the_query->have_posts() ) : ?>

											<!-- pagination here -->

											<!-- the loop -->
											<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
												<?php 
													get_template_part( 'template-parts/recruiterchefs', 'seekinglist' );
												?>
											<?php endwhile; ?>
											<!-- end of the loop -->

											<?php if (function_exists("pagination")) {
									            pagination($the_query->max_num_pages);
									        } ?>

											<!-- pagination here -->

											<?php wp_reset_postdata(); ?>

										<?php else : ?>
											<p><?php _e( 'Sorry, Chefs Seeking for Full Time Work are not found.' ); ?></p>
										<?php endif; ?>
								</div>

							</div>

						</div>
						<!-- <div class="col-xs-12 col-sm-3">
							<div id="sidebar" class="sidebar">
								<?php //get_sidebar(); ?>
							</div>
						</div> -->
					</div>
				<?php }  // recruiter user condition end ?>	
					
				<?php } ?>
			<?php } else { // user condition end	?>
				<div class="error-login-wrapper text-center">
					<h4>Please login to visit this page.</h4>
					<a href="<?php echo $redux_demo['header-login']; ?>" class="btn custom-btn">Login</a>
				</div>
			<?php } // Not Login (else condition) user condition end ?>									

		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();
