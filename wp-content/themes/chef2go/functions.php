<?php
/**
 * _s functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package _s
 */

if ( ! function_exists( '_s_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function _s_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on _s, use a find and replace
	 * to change '_s' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( '_s', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', '_s' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( '_s_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', '_s_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function _s_content_width() {
	$GLOBALS['content_width'] = apply_filters( '_s_content_width', 640 );
}
add_action( 'after_setup_theme', '_s_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function _s_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', '_s' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', '_s' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer Logo section', '_s' ),
		'id'            => 'footer-logo',
		'description'   => esc_html__( 'Add footer logo widgets here.', '_s' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer Address First Section', '_s' ),
		'id'            => 'footer-first-address',
		'description'   => esc_html__( 'Add footer logo widgets here.', '_s' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer Address Second Section', '_s' ),
		'id'            => 'footer-second-address',
		'description'   => esc_html__( 'Add footer logo widgets here.', '_s' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer Address Third Section', '_s' ),
		'id'            => 'footer-third-address',
		'description'   => esc_html__( 'Add footer logo widgets here.', '_s' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer Address Fourth Section', '_s' ),
		'id'            => 'footer-fourth-address',
		'description'   => esc_html__( 'Add footer logo widgets here.', '_s' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer Address Fifth Section', '_s' ),
		'id'            => 'footer-fifth-address',
		'description'   => esc_html__( 'Add footer logo widgets here.', '_s' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Home Upcoming Event Section', '_s' ),
		'id'            => 'home-upcoming-section',
		'before_widget' => '<div id="%1$s" class="home-upcoming widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Login', '_s' ),
		'id'            => 'login-page-widget',
		'before_widget' => '<div id="%1$s" class="login-page widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Manager Sidebar', '_s' ),
		'id'            => 'manager-page-widget',
		'before_widget' => '<div id="%1$s" class="manager-menu widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', '_s_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function _s_scripts() {
	wp_enqueue_style( '_s-style', get_stylesheet_uri() );

	wp_enqueue_script( '_s-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( '_s-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', '_s_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';
/**
 * Add breadchrumb/ Ajax functions function.
 */
require get_template_directory() . '/inc/custom-functions.php';
/**
 * Add Custom value in profile.
 */
require get_template_directory() . '/inc/user-custom-fields.php';
/**
 * Add Custom register cpt.
 */
require get_template_directory() . '/inc/custom-post-type-register.php';
/**
 * Add Custom meta boxes.
 */
require get_template_directory() . '/inc/custom-meta-boxes.php';
/**
 * Add Custom Taxonomies.
 */
require get_template_directory() . '/inc/custom-taxonomies.php';
/**
 * All Shortcodes.
 */
require get_template_directory() . '/inc/shortcode.php';
/**
 * Functions files for filteration of recruiter lising.
 */
require get_template_directory() . '/inc/filteration-function.php';
/**
 * Functions files for filteration of recruiter lising.
 */
require get_template_directory() . '/inc/roster-gform-function.php';
/**
 * Functions files for filteration of recruiter lising.
 */
require get_template_directory() . '/inc/custom-gform-function.php';
/**
 * Functions files for manager ajax function of manager.
 */
require get_template_directory() . '/inc/manager/manager-ajax-function.php';
/**
 * Functions files for manager function of manager.
 */
require get_template_directory() . '/inc/manager/manager-function.php';
/**
 * Functions files for Cron JOB.
 * Cron Function
 */
require get_template_directory() . '/inc/cron_function.php';

/**
 * Functions All employee timesheet and send it to manager .
 * Cron Function
 */
require get_template_directory() . '/inc/all_emp_cron_function.php';

add_action( 'wp_enqueue_scripts', 'crunchify_enqueue_script' );
function crunchify_enqueue_script() {
	global $redux_demo;
	$slider_type = $redux_demo['testimonials-select-transition'];
	$slider_duration = $redux_demo['testimonials-transition-duration'];
	$slider_speed = $redux_demo['testimonials-transition-speed'];
	$slider_data = array(
	    'slider_type'     => $slider_type,
	    'slider_duration' => $slider_duration,
	    'slider_speed' => $slider_speed
	);

	wp_register_style( 'bootstrap_min', get_template_directory_uri() .'/css/bootstrap.min.css', array(), '21');
	wp_enqueue_style( 'bootstrap_min' ); 

	wp_register_style( 'bootstrap_min_theme', get_template_directory_uri() .'/css/bootstrap-theme.min.css', array(), '21');
	wp_enqueue_style( 'bootstrap_min_theme' ); 

	wp_register_style( 'bootstrap_font_awesome', get_template_directory_uri() .'/css/font-awesome.min.css', array(), '21');
	wp_enqueue_style( 'bootstrap_font_awesome' ); 

    wp_register_style( 'second-style', get_template_directory_uri() .'/css/second-style.css', array(), '21');
    wp_enqueue_style( 'second-style' ); 

    wp_register_style( 'swiper_min', get_template_directory_uri() .'/css/swiper.min.css', array(), '21');
    wp_enqueue_style( 'swiper_min' ); 

    wp_register_style( 'classic', get_template_directory_uri() .'/css/classic.css', array(), '21');
    wp_enqueue_style( 'classic' ); 

    wp_register_style( 'classic_time', get_template_directory_uri() .'/css/classic.time.css', array(), '21');
    wp_enqueue_style( 'classic_time' ); 
	
	wp_register_style( 'classic_date', get_template_directory_uri() .'/css/classic.date.css', array(), '21');
	wp_enqueue_style( 'classic_date' ); 
	
	wp_register_style( 'gform_style', get_template_directory_uri() .'/css/gform.style.css', array(), '21');
	wp_enqueue_style( 'gform_style' ); 
	
	wp_register_style( 'manager_style', get_template_directory_uri() .'/css/manager-style.css', array(), '21');
	wp_enqueue_style( 'manager_style' ); 
	
	wp_deregister_script('jquery');
	wp_register_script('jquery', "https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js", false, null);
	wp_enqueue_script('jquery');

    wp_register_script( 'jquery-migrate', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-migrate/3.0.0/jquery-migrate.js', array(), '1.0.0', false);
    wp_enqueue_script( 'jquery-migrate' );

    wp_register_script( 'manager_js', get_stylesheet_directory_uri() . '/js/manager_js.js', array(), '1.0.0', true);
    wp_enqueue_script( 'manager_js' );
    
    wp_register_script( 'employee_js', get_stylesheet_directory_uri() . '/js/employee_js.js', array(), '1.0.0', true);
    wp_enqueue_script( 'employee_js' );

    wp_register_script( 'custom_js', get_stylesheet_directory_uri() . '/js/custom_js.js', array(), '1.0.0', true);
    wp_enqueue_script( 'custom_js' );
    wp_localize_script( 'custom_js', 'slider_data_value', $slider_data );

    wp_register_script( 'weekly_cron_js', get_stylesheet_directory_uri() . '/js/weekly_cron_js.js', array(), '1.0.0', true);
    wp_enqueue_script( 'weekly_cron_js' );

    wp_register_script( 'bootstrap_min', get_template_directory_uri() . '/js/bootstrap.min.js', array(), '1.0.0', true);
    wp_enqueue_script( 'bootstrap_min' );

    wp_register_script( 'swiper_jquery_min', get_template_directory_uri() . '/js/swiper.jquery.min.js', array(), '1.0.0', true);
    wp_enqueue_script( 'swiper_jquery_min' );

    wp_register_script( 'swiper_min', get_template_directory_uri() . '/js/swiper.min.js', array(), '1.0.0', true);
    wp_enqueue_script( 'swiper_min' );

    wp_register_script( 'matchHeight_min', get_template_directory_uri() . '/js/jquery.matchHeight-min.js', array(), '1.0.0', true);
    wp_enqueue_script( 'matchHeight_min' );

    wp_register_script( 'picker', get_template_directory_uri() . '/js/picker.js', array(), '1.0.0', true);
    wp_enqueue_script( 'picker' );

    wp_register_script( 'picker_time', get_template_directory_uri() . '/js/picker.time.js', array(), '1.0.0', true);
    wp_enqueue_script( 'picker_time' );

    wp_register_script( 'picker_date', get_template_directory_uri() . '/js/picker.date.js', array(), '1.0.0', true);
    wp_enqueue_script( 'picker_date' );

    wp_register_script( 'time_picker_custom_js', get_template_directory_uri() . '/js/time_picker_custom_js.js', array(), '1.0.0', true);
    wp_enqueue_script( 'time_picker_custom_js' );

    wp_register_script( 'jspdf_min', get_template_directory_uri() . '/js/jspdf.min.js', array(), '1.0.0', true);
    wp_enqueue_script( 'jspdf_min' );


}

require_once (dirname(__FILE__) . '/sample/sample-config.php');
require_once (dirname(__FILE__) . '/wp_bootstrap_navwalker.php');


/* ************************ Post Type ********************** */
add_action( 'init', 'register_post_type_init' );
/**
 * Register a book post type.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function register_post_type_init() {
    $testimonials = array(
        'name'               => _x( 'Testimonials', 'post type general name', 'your-plugin-textdomain' ),
        'singular_name'      => _x( 'Testimonial', 'post type singular name', 'your-plugin-textdomain' ),
        'menu_name'          => _x( 'Testimonials', 'admin menu', 'your-plugin-textdomain' ),
        'name_admin_bar'     => _x( 'Testimonial', 'add new on admin bar', 'your-plugin-textdomain' ),
        'add_new'            => _x( 'Add New', 'Testimonial', 'your-plugin-textdomain' ),
        'add_new_item'       => __( 'Add New Testimonial', 'your-plugin-textdomain' ),
        'new_item'           => __( 'New Testimonial', 'your-plugin-textdomain' ),
        'edit_item'          => __( 'Edit Testimonial', 'your-plugin-textdomain' ),
        'view_item'          => __( 'View Testimonial', 'your-plugin-textdomain' ),
        'all_items'          => __( 'All Testimonials', 'your-plugin-textdomain' ),
        'search_items'       => __( 'Search Testimonials', 'your-plugin-textdomain' ),
        'parent_item_colon'  => __( 'Parent Testimonials:', 'your-plugin-textdomain' ),
        'not_found'          => __( 'No testimonials found.', 'your-plugin-textdomain' ),
        'not_found_in_trash' => __( 'No testimonials found in Trash.', 'your-plugin-textdomain' )
    );

    $testimonial = array(
        'labels'             => $testimonials,
                'description'        => __( 'Description.', 'your-plugin-textdomain' ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'testimonial' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
    );

    register_post_type( 'testimonial', $testimonial );
}


/* Meta box */
function adding_custom_meta_boxes() {
    add_meta_box( 'testimonial_meta_fields','Testimonial Data','testimonial_metabox','testimonial','normal','default');
}
add_action( 'add_meta_boxes', 'adding_custom_meta_boxes', 10, 2 );

function testimonial_metabox() {
    global $post; 
    $_testimonials_author = get_post_meta($post->ID,'_testimonials_author',true); 
    $_testimonials_title = get_post_meta($post->ID,'_testimonials_title',true); 
    $_testimonials_location = get_post_meta($post->ID,'_testimonials_location',true); 
    $_testimonials_email = get_post_meta($post->ID,'_testimonials_email',true); 
    $_testimonials_company = get_post_meta($post->ID,'_testimonials_company',true); 
    $_testimonials_url = get_post_meta($post->ID,'_testimonials_url',true); 

    ?>
    <table class="form-table">
		<tbody>
		<tr>
			<th scope="row" style="width: 140px">
				<label for="testimonials-author">Author</label>
			</th>
			<td>
				<input id="testimonials-author" value="<?php echo $_testimonials_author; ?>" type="text" name="_testimonials_author" class="text large-text ">
				<span class="description">Use when the testimonial title is not the authors' name.</span>
			</td>
		</tr>
		<tr>
			<th scope="row" style="width: 140px">
				<label for="testimonials-title">Job Title</label>
			</th>
			<td>
				<input id="testimonials-title" value="<?php echo $_testimonials_title; ?>" type="text" name="_testimonials_title" class="text large-text ">
				<span class="description"></span>
			</td>
		</tr>
		<tr>
			<th scope="row" style="width: 140px">
				<label for="testimonials-location">Location</label>
			</th>
			<td>
				<input id="testimonials-widget-location" value="<?php echo $_testimonials_location; ?>" type="text" name="_testimonials_location" class="text large-text ">
				<span class="description"></span>
			</td>
		</tr>
		<tr>
			<th scope="row" style="width: 140px">
				<label for="testimonials-company">Company</label>
			</th>
			<td>
				<input id="testimonials-widget-company" value="<?php echo $_testimonials_company; ?>" type="text" name="_testimonials_company" class="text large-text ">
				<span class="description"></span>
			</td>
		</tr>
		<tr>
			<th scope="row" style="width: 140px">
				<label for="testimonials-email">Email</label>
			</th>
			<td>
				<input id="testimonials-email" value="<?php echo $_testimonials_email; ?>" type="text" name="_testimonials_email" class="text large-text ">
				<span class="description">If an email is provided, but not an image, a Gravatar icon will be attempted to be loaded.</span>
			</td>
		</tr>
		<tr>
			<th scope="row" style="width: 140px">
				<label for="testimonials-url">URL</label>
			</th>
			<td>
				<input id="testimonials-url" value="<?php echo $_testimonials_url; ?>" type="text" name="_testimonials_url" class="text large-text ">
				<span class="description"></span>
			</td>
		</tr>
		</tbody>
	</table>
<?php }
function save_utilities_meta($post_id) {
    global $post;
    if( 'testimonial' == $post->post_type ){
        update_post_meta($post_id, '_testimonials_author', $_POST['_testimonials_author']);
        update_post_meta($post_id, '_testimonials_title', $_POST['_testimonials_title']);
        update_post_meta($post_id, '_testimonials_location', $_POST['_testimonials_location']);
        update_post_meta($post_id, '_testimonials_company', $_POST['_testimonials_company']);
        update_post_meta($post_id, '_testimonials_email', $_POST['_testimonials_email']);
        update_post_meta($post_id, '_testimonials_url', $_POST['_testimonials_url']);
    }
}
add_action( 'save_post', 'save_utilities_meta' );


/* =============================== Add first and last menu class ============================== */
function add_first_and_last($items) {
    $items[1]->classes[] = 'first';
    $items[count($items)-1]->classes[] = 'last';
    return $items;
}
add_filter('wp_nav_menu_objects', 'add_first_and_last');


/* ====================================== Set Ajax Url ====================================== */
add_action('wp_head','chef2go_ajaxurl');
function chef2go_ajaxurl() {
?>
	<script type="text/javascript">
		var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
	</script>
<?php
}

/* ====================================== Create User Role ====================================== */
$result = add_role(
    'chef_employee',
    __( 'Chef Employee' ),
    array(
        'read'         => true,  // true allows this capability
        'edit_posts'   => true,
        'delete_posts' => false, // Use false to explicitly deny
    )
);

$result = add_role(
    'staff_employee',
    __( 'Staff Employee' ),
    array(
        'read'         => true,  // true allows this capability
        'edit_posts'   => true,
        'delete_posts' => false, // Use false to explicitly deny
    )
);

$result = add_role(
    'recruiter',
    __( 'Recruiter' ),
    array(
        'read'         => true,  // true allows this capability
        'edit_posts'   => true,
        'delete_posts' => false, // Use false to explicitly deny
    )
);

$result = add_role(
    'manager',
    __( 'Manager' ),
    array(
        'read'         => true,  // true allows this capability
        'edit_posts'   => true,
        'delete_posts' => false, // Use false to explicitly deny
    )
);

/*remove_role('s2member_level1');*/

/* ============================== Notification Mail to user when user update ============================*/

	add_action( 'profile_update', 'update_extra_profile_fields' );
	//add_action( 'edit_user_profile_update', 'update_extra_profile_fields' );
 
	function update_extra_profile_fields($user_id) {

		$uservalue = get_userdata($user_id);
		$usermail = $uservalue->user_email;
		$user_mail = $usermail;
		$username = $uservalue->first_name;
		$uservalue = get_field('notification_to_user', 'user_'. $user_id);
		if($uservalue[0] == 'yes'){
			$to = $user_mail;
			$subject = 'Notification for document upload';
			$body .= 'Hello '. $username .',<br/>';
			$body .= 'We have update new document in your dashboard.<br/>';
			$body .= 'Please check your myarea dashboard for uploaded document.<br/>';
			$headers = array('Content-Type: text/html; charset=UTF-8');
			wp_mail( $to, $subject, $body, $headers );			
		}
		
	}

/* ====================================== Hide admin bar ====================================== */
add_filter('show_admin_bar', '__return_false');


/* ==================================== Redirect login url ==========================================*/
add_filter( 'login_url', 'my_login_page', 10, 3 );
function my_login_page( $login_url, $redirect, $force_reauth ) {
    $login_page = home_url( '/login/' );
    //$login_url = add_query_arg( 'redirect_to', $redirect, $login_page );
    return $login_page;
}


/* ==================================== hide Logout Question  ======================================*/
add_action('check_admin_referer', 'logout_without_confirm', 10, 2);
function logout_without_confirm($action, $result) {
    /**
     * Allow logout without confirmation
     */
    if ($action == "log-out" && !isset($_GET['_wpnonce'])) {
        $redirect_to = isset($_REQUEST['redirect_to']) ? $_REQUEST['redirect_to'] : '';
        $location = str_replace('&amp;', '&', wp_logout_url($redirect_to));;
        header("Location: $location");
        die;
    }
}
/* ==================================== hide Menu Question  ======================================*/
function remove_menu_links() {
    if( !current_user_can( 'manage_options' ) ) {
        remove_menu_page( 'gf_edit_forms' ); // this is the pages url
    }
}
add_action( 'admin_menu', 'remove_menu_links', 9999 );


add_action('admin_menu', 'remove_admin_menu_links');
function remove_admin_menu_links(){
    $user = wp_get_current_user();
    foreach ($user->roles as $userkey => $uservalue) {
	    if( $user && isset($uservalue) && 'manager' == $uservalue ) {
		    remove_menu_page('tools.php');
		    remove_menu_page('themes.php');
		    remove_menu_page('options-general.php');
		    remove_menu_page('plugins.php');
			remove_menu_page('wpcf7');
			remove_menu_page('edit-comments.php');
			remove_menu_page('page.php');
			remove_menu_page('upload.php');
			remove_menu_page( 'edit.php?post_type=page' ); 
			remove_menu_page( 'edit.php?post_type=videos' );
			remove_menu_page( 'edit.php?post_type=fep_message' );
			remove_menu_page( 'edit.php?post_type=testimonial' );
			remove_menu_page( 'edit.php?post_type=rosters' );
			remove_menu_page( 'edit.php?post_type=locations' );
			remove_menu_page( 'edit.php?post_type=jobs' );
			remove_menu_page( 'edit.php?post_type=hospitality_staff' );
			remove_menu_page( 'edit.php?post_type=recruiter' );
			remove_menu_page( 'edit.php?post_type=chef' );
			remove_menu_page( 'edit.php?post_type=event' );
			remove_menu_page( 'edit.php?post_type=soliloquy' );
			remove_menu_page( 'edit.php?post_type=timesheet' );
			remove_menu_page( 'gf_edit_forms' );
			remove_menu_page( 'edit.php' );	
	    }
    }
}

function add_gf_cap()
{
    $role = get_role( 'manager' );
    $role->add_cap( 'gform_full_access' );
    $role->add_cap( 'edit_user_options' );
	$role->add_cap('edit_users');
	$role->add_cap('list_users');
	$role->add_cap('promote_users');
	$role->add_cap('create_users');
	$role->add_cap('add_users');
	$role->add_cap('delete_users');
}
 
add_action( 'admin_init', 'add_gf_cap' );

/* Setup cron function to send mail to employee on every monday */


function my_cronjob_action_user() {
    // Could probably do with some logic here to stop it running if just after running.
    // codes go here

global $wpdb;
    // code to execute on cron run
    $myrows = $wpdb->get_results( "SELECT * FROM wp_options WHERE option_name LIKE 'redux_demo'" );
	$varval = unserialize($myrows[0]->option_value);
	$weeklymailsubject = $varval['weekly-subject-title']; 
	$weeklymailbody = $varval['weekly-email-editor'];
	$monthlymailsubject = $varval['monthly-mail-subject-title']; 
	$monthlymailbody = $varval['monthly-email-editor'];

	//wp_mail( 'pankajprodigi@gmail.com', 'Automatic email', $body);

	if(date('D') === 'Mon'){
	
		// get the chefs
		$chef_query = new WP_User_Query(
			array(
				'role'	 =>	'chef_employee',
			)
		);
		$chefs = $chef_query->get_results();

		// get the staff
		$staff_employee_query = new WP_User_Query(
			array(
				'role'	 =>	'staff_employee',
			)
		);
		$staffs = $staff_employee_query->get_results();

		// store them all as users
		$users = array_merge( $staffs, $chefs );

		// User Loop
		if ( ! empty( $users ) ) {
			foreach ( $users as $user ) { 
				$user_email_array[] = $user->user_email;
			}
			foreach ($user_email_array as $email_array) {
				$to = $email_array;
				$subject = $weeklymailsubject;
				$body = $weeklymailbody;
				$headers = array('Content-Type: text/html; charset=UTF-8');
			
				wp_mail( $to, $subject, $body, $headers );
			}
		} else {
			echo 'No users found.';
		}
	}



// get the Recruiter
	$recruiter_query = new WP_User_Query(
		array(
			'role'	 =>	'recruiter',
		)
	);
	$recruiter = $recruiter_query->get_results();

	// User Loop
	// Loop for get user registered date and add 28 days in that date.
	// for loop for check current date is same as expiredate if yes then it will send mail to
	// that user.
	if ( ! empty( $recruiter ) ) {
		foreach ( $recruiter as $user ) {
			$registered = $user->user_registered;
			$recruiter_id = $user->id;
			$type_of_subscription = get_user_meta( $recruiter_id, 'type_of_subscription', true );
			$registered_expire_date = date( "d M Y", strtotime( $registered.'+12 day' ) );
			$registered_expire_date_year = date( "d M Y", strtotime( $registered.'+330 day' ) );
			if (date( "d M Y") === $registered_expire_date && $type_of_subscription === 'Per Month Subscription') {
				$to = $user->user_email;
				$subject = $monthlymailsubject;
				$body = $monthlymailbody;
				$headers = array('Content-Type: text/html; charset=UTF-8');
			
				wp_mail( $to, $subject, $body, $headers );
			} elseif (date( "d M Y") === $registered_expire_date_year && $type_of_subscription === 'Per Year Subscription') {
				$to = $user->user_email;
				$subject = $monthlymailsubject;
				$body = $monthlymailbody;
				$headers = array('Content-Type: text/html; charset=UTF-8');
			
				wp_mail( $to, $subject, $body, $headers );
			}
		}
	} else {
		echo 'No recruiter found.';
	}


}
//add_action('my_cronjob_action', 'my_cronjob_action_user');

if ( ! get_transient( 'every_5_minutes' ) ) {
    //set_transient( 'every_5_minutes', true, 5 * MINUTE_IN_SECONDS );
    //run_evry_five_minutes();
}