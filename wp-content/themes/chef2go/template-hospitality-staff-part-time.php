<?php
/**
* Template Name: Hospitality Staff Seeking Part Time Work
*/

get_header(); ?>

	<div id="primary" class="content-area">
		<div class="other-entry-header text-center">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
							<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
						</div><!-- .entry-header -->
				</div>
			</div>
		</div>
		<div class="breadcrumbs-container">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<?php dimox_breadcrumbs(); ?>
					</div>
				</div>
			</div>			
		</div>
		<main id="main" class="container site-main" role="main">
			<div class="row">
				<div class="col-xs-12">
					<div class="myarea-page-links pull-right">
						<a href="<?php echo site_url(); ?>/my-area/" class="btn custom-btn">back to my Area</a>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12">
					<?php
					while ( have_posts() ) : the_post();

						get_template_part( 'template-parts/other', 'page' );

					
					endwhile; // End of the loop.
					?>	
					<?php if(is_user_logged_in()){ 
						$current_user = wp_get_current_user();
						if ($current_user->roles[1] == 'recruiter' || $current_user->roles[0] == 'recruiter') { ?>

							<?php echo do_shortcode('[part_time_staff]'); ?>

							<?php }  // recruiter user condition end ?>	
						<?php } else { // user condition end	?>
					
							<div class="error-login-wrapper text-center">
								<h4>Please login to visit this page.</h4>
								<a href="<?php echo $redux_demo['header-login']; ?>" class="btn custom-btn">Login</a>							
							</div>
						<?php } // Not Login (else condition) user condition end ?>									
				</div>
				<!-- <div class="col-xs-12 col-sm-3">
					<div id="sidebar" class="sidebar">
						<?php //get_sidebar(); ?>
					</div>
				</div> -->
			</div>

		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();