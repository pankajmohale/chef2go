<?php
/**
* Template Name: Test Template
*/?><?php
require('/home/rgbastud/public_html/chefs2go/wp-content/themes/chef2go/mpdf60/mpdf.php');
$timesheet_value = weekly_mail_timesheet_function(); 
ob_start();				
//$mpdf = new mPDF('F'); 
//$mpdf->WriteHTML($timesheet_value);
//$mpdf->Output();

$mpdf = new mPDF();
$mpdf->WriteHTML($timesheet_value);
$content = $mpdf->Output('', 'S');
$content = chunk_split(base64_encode($content));
$mailto = 'pankaj.mohale@prodigitas.com';
$from_name = 'Your name';
$from_mail = 'sender@domain.com';
$replyto = 'sender@domain.com';
$uid = md5(uniqid(time()));
$subject = 'Your e-mail subject here';
$message = 'Your e-mail message here';
$filename = 'filename.pdf';
$header = "From: ".$from_name." <".$from_mail.">\r\n";
$header .= "Reply-To: ".$replyto."\r\n";
$header .= "MIME-Version: 1.0\r\n";
$header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"\r\n\r\n";
$header .= "This is a multi-part message in MIME format.\r\n";
$header .= "--".$uid."\r\n";
$header .= "Content-type:text/plain; charset=iso-8859-1\r\n";
$header .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
$header .= $message."\r\n\r\n";
$header .= "--".$uid."\r\n";
$header .= "Content-Type: application/pdf; name=\"".$filename."\"\r\n";
$header .= "Content-Transfer-Encoding: base64\r\n";
$header .= "Content-Disposition: attachment; filename=\"".$filename."\"\r\n\r\n";
$header .= $content."\r\n\r\n";
$header .= "--".$uid."--";
$is_sent = @mail($mailto, $subject, "", $header);
$mpdf->Output();
ob_end_flush();
exit;
?>