<?php
/**
* Template Name: Update User 
*/

get_header(); ?>

		
	<?php if(is_user_logged_in()){ ?> 
	<div class="container">
		<div id="primary" class="row content-area">
			<main id="main" class="col-xs-12 col-sm-9 site-main" role="main">
				<?php $current_user = wp_get_current_user();
				foreach ($current_user->roles as $userkey => $uservalue) { ?>
				<?php if ($uservalue == 'recruiter') { ?>
					<div class="myarea-page-links pull-right">
						<a href="<?php echo site_url(); ?>/my-area/" class="btn custom-btn">Back to my Area</a>
					</div>
					
				<?php } ?>

				<?php
				while ( have_posts() ) : the_post();

					// get_template_part( 'template-parts/content', 'page' );

					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;

				endwhile; // End of the loop.
				?>

				<?php if ($uservalue == 'chef_employee') { ?>
					<?php echo do_shortcode('[gravityform id="19" title="false" description="false" ajax="true"]'); ?>
				<?php } ?>
				<?php if ($uservalue == 'staff_employee') { ?>
					<?php echo do_shortcode('[gravityform id="20" title="false" description="false" ajax="true"]'); ?>
				<?php } ?>
				<?php if ($uservalue == 'recruiter') { ?>
					<?php echo do_shortcode('[gravityform id="21" title="false" description="false" ajax="true"]'); ?>
					<div class="package-update">
						<div class="package-update-title">
							<h2>Renew Subscription</h2>		
						</div>
					<?php echo do_shortcode('[gravityform id="22" title="false" description="false" ajax="true"]'); ?>
					</div>
				<?php } ?>


				<?php }  // recruiter user condition end ?>	
			</main><!-- #main -->
			<div id="sidebar" class="sidebar col-xs-12 col-sm-3">
				<?php get_sidebar(); ?>
			</div>
		</div><!-- #primary -->
	</div>
	<?php } else { ?>
		
	<?php	} ?>

<?php
get_footer();
