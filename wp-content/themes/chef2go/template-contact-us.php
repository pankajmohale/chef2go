<?php
/**
* Template Name: Contact Us
*/

get_header(); ?>

	<div id="primary" class="row content-area">
		<div class="contact-entry-header text-center">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
							<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
						</div><!-- .entry-header -->
				</div>
			</div>
		</div>
		<div class="breadcrumbs-container">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<?php dimox_breadcrumbs(); ?>
					</div>
				</div>
			</div>			
		</div>
		<?php 
			global $post; 
		    $_slider_shortcode = get_post_meta($post->ID,'_slider_shortcode',true);		    
		?>
		<div class="template-slider">
			<?php echo do_shortcode($_slider_shortcode); ?>
		</div>
		<main id="main" class="container site-main" role="main">

			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/contact', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();
