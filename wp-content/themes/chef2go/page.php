<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package _s
 */

get_header(); ?>

	<div id="primary" class="row content-area">
		<main id="main" class="col-xs-12 col-sm-9 site-main" role="main">
			<?php if(is_user_logged_in()){ 
				$current_user = wp_get_current_user();
				if ($current_user->roles[1] == 'recruiter' || $current_user->roles[0] == 'recruiter') { ?>
				<div class="myarea-page-links pull-right">
					<a href="<?php echo site_url(); ?>/my-area/" class="btn custom-btn">My Area</a>
				</div>

				<?php }  // recruiter user condition end ?>	
			<?php } ?>
			
			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
		<div id="sidebar" class="sidebar col-xs-12 col-sm-3">
			<?php get_sidebar(); ?>
		</div>
	</div><!-- #primary -->

<?php
get_footer();
