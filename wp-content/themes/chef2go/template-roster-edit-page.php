<?php
/**
* Template Name: Edit Roster Page
*/

get_header(); 

?>

	<div id="primary" class="content-area">
		<div class="other-entry-header text-center">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
							<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
						</div><!-- .entry-header -->
				</div>
			</div>
		</div>
		<div class="breadcrumbs-container">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<?php dimox_breadcrumbs(); ?>
					</div>
				</div>
			</div>			
		</div>
		<main id="main" class="container site-main" role="main">
			<?php if(is_user_logged_in()){ 
				$current_user = wp_get_current_user();
				foreach ($current_user->roles as $rolekey => $rolevalue) {
				if ($rolevalue == 'manager') { ?>
				<div class="col-xs-12 clearfix">
					<div class="myarea-page-links pull-right">
						<a href="<?php echo site_url(); ?>/manager-myarea/manage-rosters/" class="btn custom-btn">Back to manage roster</a>
					</div>
				</div>

					<?php 
					// This Query is for get list of current user roster post 
					$get_post_val = $_GET['post_id']; 
					$rosterargs = array(
						'p' => $get_post_val,
						'post_type' => 'rosters',
					); 
					// the query
					$the_query = new WP_Query( $rosterargs ); 
					?>

					<?php if ( $the_query->have_posts() ) : ?>

					<!-- pagination here -->

					<!-- the loop -->
					<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
						<div class="post-wrapper-content">
							<div class="row">
								<div class="col-xs-4">
									<div class="form-group">
									    <label for="posttitle">Week Beginning</label>
									    <input type="text" class="form-control" id="posttitle" name="posttitle" value="<?php echo date("d-m-Y", strtotime(get_the_title())); ?>" readonly>

									</div>
								</div>
								<?php $get_post_val = $_GET['post_id']; ?>
								<div class="col-xs-4">
								<?php $roster_member_list = get_post_meta($get_post_val,'roster_member_list',true); ?>
									<div class="form-group">
									    <label for="posttitle">Roster Member</label>
						<select id="roster_member_list" name="roster_member_list" class="form-control">
						<?php
						// get the chefs
						$chef_query = new WP_User_Query(
							array(
								'role'	 =>	'chef_employee',
							)
						);
						$chefs = $chef_query->get_results();

						// get the staff
						$staff_employee_query = new WP_User_Query(
							array(
								'role'	 =>	'staff_employee',
							)
						);
						$staffs = $staff_employee_query->get_results();

						// store them all as users
						$users = array_merge( $staffs, $chefs );

						// User Loop
						if ( ! empty( $users ) ) {
							foreach ( $users as $user ) { ?>
							<option value="<?php  echo $user->id; ?>" <?php if ($roster_member_list == $user->id) echo ' selected="selected"'; ?>><?php echo $user->user_login; ?></option>
						<?php 
							}
						} else {
							echo 'No users found.';
						}

						?>

					</select>

									</div>
								</div>
							</div>
							<?php 
								// Monday
								$roster_week_day_1 = get_post_meta($get_post_val,'roster_week_day_1',true);
								$roster_staff_member_1 = get_post_meta($get_post_val,'roster_staff_member_1',true); 
							    $roster_time_start_1 = get_post_meta($get_post_val,'roster_time_start_1',true); 
							    $roster_time_finish_1 = get_post_meta($get_post_val,'roster_time_finish_1',true);
							?>
							<div class="row">
								<div class="col-xs-3">
									<div class="form-group">
									    <label for="monday_week_day">Week Day</label>
									    <!-- <input type="text" class="form-control" id="posttitle" value="<?php echo $roster_week_day_1; ?>" > -->
									    <select name="monday_week_day" class="form-control" id="monday_week_day">
										    <option value="">Select Week Day</option>
										    <option value="Monday" <?php if($roster_week_day_1 == 'Monday') echo "selected"; ?>>Monday</option>
										    <option value="Tuesday" <?php if($roster_week_day_1 == 'Tuesday') echo "selected"; ?>>Tuesday</option>
										    <option value="Wednesday" <?php if($roster_week_day_1 == 'Wednesday') echo "selected"; ?>>Wednesday</option>
										    <option value="Thursday" <?php if($roster_week_day_1 == 'Thursday') echo "selected"; ?>>Thursday</option>
										    <option value="Friday" <?php if($roster_week_day_1 == 'Friday') echo "selected"; ?>>Friday</option>
										    <option value="Saturday" <?php if($roster_week_day_1 == 'Saturday') echo "selected"; ?>>Saturday</option>
										    <option value="Sunday" <?php if($roster_week_day_1 == 'Sunday') echo "selected"; ?>>Sunday</option>
									    </select>
									</div>
								</div>
								<div class="col-xs-3">
									<div class="form-group">
									    <label for="monday_time_start">Time Start</label>
									    <input type="text" name="monday_time_start" class="form-control start-time" id="monday_time_start" value="<?php echo $roster_time_start_1; ?>" >
									</div>
								</div>
								<div class="col-xs-3">
									<div class="form-group">
									    <label for="monday_time_finish">Time Finish</label>
									    <input type="text" name="monday_time_finish" class="form-control finish-time" id="monday_time_finish" value="<?php echo $roster_time_finish_1; ?>">
									</div>
								</div>
								<div class="col-xs-3">
								<?php $roster_clients_1 = get_post_meta(get_the_ID(),'roster_clients_1',true); ?>
									<div class="form-group">
									    <label for="posttitle">Client</label>
									    <!-- <input type="text" class="form-control" id="roster_client" value="<?php //the_title(); ?>" readonly> -->
									    <select id="roster_clients_1" name="roster_clients_1" class="form-control">
									    <option value="">Select Client</option>
									    <?php
											$args = array( 'post_type' => 'locations', 'posts_per_page' => -1, 'post_status' => 'any', 'post_parent' => null ); 
											$attachments = get_posts( $args );
											if ( $attachments ) {
												foreach ( $attachments as $post ) {
													setup_postdata( $post ); ?>
												<option value="<?php echo get_the_ID(); ?>" <?php if ($roster_clients_1 == get_the_ID()) echo ' selected="selected"'; ?>><?php echo get_the_title(); ?></option>	
											<?php 
											}
												wp_reset_postdata();
											}
										?>									    	
									    </select>

									</div>
								</div>
							</div>
							
							<?php 
								// Tuesday
							    $roster_week_day_2 = get_post_meta($get_post_val,'roster_week_day_2',true); 
							    $roster_staff_member_2 = get_post_meta($get_post_val,'roster_staff_member_2',true); 
							    $roster_time_start_2 = get_post_meta($get_post_val,'roster_time_start_2',true); 
							    $roster_time_finish_2 = get_post_meta($get_post_val,'roster_time_finish_2',true);
							?>
							<div class="row">
								<div class="col-xs-3">
									<div class="form-group">
									    <label for="tuesday_week_day">Week Day</label>
									    <!-- <input type="text" class="form-control" id="posttitle" value="<?php echo $roster_week_day_2; ?>" > -->
									    <select name="tuesday_week_day" class="form-control" id="tuesday_week_day">
										    <option value="">Select Week Day</option>
										    <option value="Monday" <?php if($roster_week_day_2 == 'Monday') echo "selected"; ?>>Monday</option>
										    <option value="Tuesday" <?php if($roster_week_day_2 == 'Tuesday') echo "selected"; ?>>Tuesday</option>
										    <option value="Wednesday" <?php if($roster_week_day_2 == 'Wednesday') echo "selected"; ?>>Wednesday</option>
										    <option value="Thursday" <?php if($roster_week_day_2 == 'Thursday') echo "selected"; ?>>Thursday</option>
										    <option value="Friday" <?php if($roster_week_day_2 == 'Friday') echo "selected"; ?>>Friday</option>
										    <option value="Saturday" <?php if($roster_week_day_2 == 'Saturday') echo "selected"; ?>>Saturday</option>
										    <option value="Sunday" <?php if($roster_week_day_2 == 'Sunday') echo "selected"; ?>>Sunday</option>
									    </select>
									</div>
								</div>
								<div class="col-xs-3">
									<div class="form-group">
									    <label for="tuesday_time_start">Time Start</label>
									    <input type="text" name="tuesday_time_start" class="form-control tuesday_time_start start-time" id="tuesday_time_start" value="<?php echo $roster_time_start_2; ?>" >
									</div>
								</div>
								<div class="col-xs-3">
									<div class="form-group">
									    <label for="tuesday_time_finish">Time Finish</label>
									    <input type="text" name="tuesday_time_finish" class="form-control finish-time" id="tuesday_time_finish" value="<?php echo $roster_time_finish_2; ?>">
									</div>
								</div>
								<div class="col-xs-3">
								<?php $roster_clients_2 = get_post_meta($get_post_val,'roster_clients_2',true); ?>
									<div class="form-group">
									    <label for="posttitle">Client</label>
									    <!-- <input type="text" class="form-control" id="roster_client" value="<?php //the_title(); ?>" readonly> -->
									    <select id="roster_clients_2" name="roster_clients_2" class="form-control">
									    <option value="">Select Client</option>
									    <?php
											$args = array( 'post_type' => 'locations', 'posts_per_page' => -1, 'post_status' => 'any', 'post_parent' => null ); 
											$attachments = get_posts( $args );
											if ( $attachments ) {
												foreach ( $attachments as $post ) {
													setup_postdata( $post ); ?>
												<option value="<?php echo get_the_ID(); ?>" <?php if ($roster_clients_2 == get_the_ID()) echo ' selected="selected"'; ?>><?php echo get_the_title(); ?></option>	
											<?php 
											}
												wp_reset_postdata();
											}
										?>									    	
									    </select>

									</div>
								</div>
							</div>
							

							<?php 
								// Wednesday
							    $roster_week_day_3 = get_post_meta($get_post_val,'roster_week_day_3',true); 
							    $roster_staff_member_3 = get_post_meta($get_post_val,'roster_staff_member_3',true); 
							    $roster_time_start_3 = get_post_meta($get_post_val,'roster_time_start_3',true); 
							    $roster_time_finish_3 = get_post_meta($get_post_val,'roster_time_finish_3',true);
							?>
							<div class="row">
								<div class="col-xs-3">
									<div class="form-group">
									    <label for="wednesday_week_day">Week Day</label>
									    <!-- <input type="text" class="form-control" id="posttitle" value="<?php echo $roster_week_day_3; ?>" > -->
									    <select name="wednesday_week_day" class="form-control" id="wednesday_week_day">
										    <option value="">Select Week Day</option>
										    <option value="Monday" <?php if($roster_week_day_3 == 'Monday') echo "selected"; ?>>Monday</option>
										    <option value="Tuesday" <?php if($roster_week_day_3 == 'Tuesday') echo "selected"; ?>>Tuesday</option>
										    <option value="Wednesday" <?php if($roster_week_day_3 == 'Wednesday') echo "selected"; ?>>Wednesday</option>
										    <option value="Thursday" <?php if($roster_week_day_3 == 'Thursday') echo "selected"; ?>>Thursday</option>
										    <option value="Friday" <?php if($roster_week_day_3 == 'Friday') echo "selected"; ?>>Friday</option>
										    <option value="Saturday" <?php if($roster_week_day_3 == 'Saturday') echo "selected"; ?>>Saturday</option>
										    <option value="Sunday" <?php if($roster_week_day_3 == 'Sunday') echo "selected"; ?>>Sunday</option>
									    </select>
									</div>
								</div>
								<div class="col-xs-3">
									<div class="form-group">
									    <label for="wednesday_time_start">Time Start</label>
									    <input type="text" name="wednesday_time_start" class="form-control start-time" id="wednesday_time_start" value="<?php echo $roster_time_start_3; ?>" >
									</div>
								</div>
								<div class="col-xs-3">
									<div class="form-group">
									    <label for="wednesday_time_finish">Time Finish</label>
									    <input type="text" name="wednesday_time_finish" class="form-control finish-time" id="wednesday_time_finish" value="<?php echo $roster_time_finish_3; ?>">
									</div>
								</div>
								<div class="col-xs-3">
									<?php $roster_clients_3 = get_post_meta($get_post_val,'roster_clients_3',true); ?>
									<div class="form-group">
									    <label for="posttitle">Client</label>
									    <!-- <input type="text" class="form-control" id="roster_client" value="<?php //the_title(); ?>" readonly> -->
									    <select id="roster_clients_3" name="roster_clients_3" class="form-control">
									    <option value="">Select Client</option>
									    <?php
											$args = array( 'post_type' => 'locations', 'posts_per_page' => -1, 'post_status' => 'any', 'post_parent' => null ); 
											$attachments = get_posts( $args );
											if ( $attachments ) {
												foreach ( $attachments as $post ) {
													setup_postdata( $post ); ?>
												<option value="<?php echo get_the_ID(); ?>" <?php if ($roster_clients_3 == get_the_ID()) echo ' selected="selected"'; ?>><?php echo get_the_title(); ?></option>	
											<?php 
											}
												wp_reset_postdata();
											}
										?>									    	
									    </select>

									</div>
								</div>
							</div>
							

							<?php 
								// Thursday
							    $roster_week_day_4 = get_post_meta($get_post_val,'roster_week_day_4',true); 
							    $roster_staff_member_4 = get_post_meta($get_post_val,'roster_staff_member_4',true); 
							    $roster_time_start_4 = get_post_meta($get_post_val,'roster_time_start_4',true); 
							    $roster_time_finish_4 = get_post_meta($get_post_val,'roster_time_finish_4',true);
							?>
							<div class="row">
								<div class="col-xs-3">
									<div class="form-group">
									    <label for="thursday_week_day">Week Day</label>
									    <!-- <input type="text" class="form-control" id="posttitle" value="<?php echo $roster_week_day_4; ?>" > -->
									    <select name="thursday_week_day" class="form-control" id="thursday_week_day">
										    <option value="">Select Week Day</option>
										    <option value="Monday" <?php if($roster_week_day_4 == 'Monday') echo "selected"; ?>>Monday</option>
										    <option value="Tuesday" <?php if($roster_week_day_4 == 'Tuesday') echo "selected"; ?>>Tuesday</option>
										    <option value="Wednesday" <?php if($roster_week_day_4 == 'Wednesday') echo "selected"; ?>>Wednesday</option>
										    <option value="Thursday" <?php if($roster_week_day_4 == 'Thursday') echo "selected"; ?>>Thursday</option>
										    <option value="Friday" <?php if($roster_week_day_4 == 'Friday') echo "selected"; ?>>Friday</option>
										    <option value="Saturday" <?php if($roster_week_day_4 == 'Saturday') echo "selected"; ?>>Saturday</option>
										    <option value="Sunday" <?php if($roster_week_day_4 == 'Sunday') echo "selected"; ?>>Sunday</option>
									    </select>
									</div>
								</div>
								<div class="col-xs-3">
									<div class="form-group">
									    <label for="thursday_time_start">Time Start</label>
									    <input type="text" name="thursday_time_start" class="form-control start-time" id="thursday_time_start" value="<?php echo $roster_time_start_4; ?>" >
									</div>
								</div>
								<div class="col-xs-3">
									<div class="form-group">
									    <label for="thursday_time_finish">Time Finish</label>
									    <input type="text" name="thursday_time_finish" class="form-control finish-time" id="thursday_time_finish" value="<?php echo $roster_time_finish_4; ?>">
									</div>
								</div>
								<div class="col-xs-3">
									<?php $roster_clients_4 = get_post_meta($get_post_val,'roster_clients_4',true); ?>
									<div class="form-group">
									    <label for="posttitle">Client</label>
									    <!-- <input type="text" class="form-control" id="roster_client" value="<?php //the_title(); ?>" readonly> -->
									    <select id="roster_clients_4" name="roster_clients_4" class="form-control">
									    <option value="">Select Client</option>
									    <?php
											$args = array( 'post_type' => 'locations', 'posts_per_page' => -1, 'post_status' => 'any', 'post_parent' => null ); 
											$attachments = get_posts( $args );
											if ( $attachments ) {
												foreach ( $attachments as $post ) {
													setup_postdata( $post ); ?>
												<option value="<?php echo get_the_ID(); ?>" <?php if ($roster_clients_4 == get_the_ID()) echo ' selected="selected"'; ?>><?php echo get_the_title(); ?></option>	
											<?php 
											}
												wp_reset_postdata();
											}
										?>									    	
									    </select>

									</div>
								</div>
							</div>
							

							<?php 
								// Friday
							    $roster_week_day_5 = get_post_meta($get_post_val,'roster_week_day_5',true); 
							    $roster_staff_member_5 = get_post_meta($get_post_val,'roster_staff_member_5',true); 
							    $roster_time_start_5 = get_post_meta($get_post_val,'roster_time_start_5',true); 
							    $roster_time_finish_5 = get_post_meta($get_post_val,'roster_time_finish_5',true);
							?>
							<div class="row">
								<div class="col-xs-3">
									<div class="form-group">
									    <label for="friday_week_day">Week Day</label>
									    <!-- <input type="text" class="form-control" id="posttitle" value="<?php echo $roster_week_day_5; ?>" > -->
									    <select name="friday_week_day" class="form-control" id="friday_week_day">
										    <option value="">Select Week Day</option>
										    <option value="Monday" <?php if($roster_week_day_5 == 'Monday') echo "selected"; ?>>Monday</option>
										    <option value="Tuesday" <?php if($roster_week_day_5 == 'Tuesday') echo "selected"; ?>>Tuesday</option>
										    <option value="Wednesday" <?php if($roster_week_day_5 == 'Wednesday') echo "selected"; ?>>Wednesday</option>
										    <option value="Thursday" <?php if($roster_week_day_5 == 'Thursday') echo "selected"; ?>>Thursday</option>
										    <option value="Friday" <?php if($roster_week_day_5 == 'Friday') echo "selected"; ?>>Friday</option>
										    <option value="Saturday" <?php if($roster_week_day_5 == 'Saturday') echo "selected"; ?>>Saturday</option>
										    <option value="Sunday" <?php if($roster_week_day_5 == 'Sunday') echo "selected"; ?>>Sunday</option>
									    </select>
									</div>
								</div>
								<div class="col-xs-3">
									<div class="form-group">
									    <label for="friday_time_start">Time Start</label>
									    <input type="text" name="friday_time_start" class="form-control start-time" id="friday_time_start" value="<?php echo $roster_time_start_5; ?>" >
									</div>
								</div>
								<div class="col-xs-3">
									<div class="form-group">
									    <label for="friday_time_finish">Time Finish</label>
									    <input type="text" name="friday_time_finish" class="form-control finish-time" id="friday_time_finish" value="<?php echo $roster_time_finish_5; ?>">
									</div>
								</div>
								<div class="col-xs-3">
									<?php $roster_clients_5 = get_post_meta($get_post_val,'roster_clients_5',true); ?>
									<div class="form-group">
									    <label for="posttitle">Client</label>
									    <!-- <input type="text" class="form-control" id="roster_client" value="<?php //the_title(); ?>" readonly> -->
									    <select id="roster_clients_5" name="roster_clients_5" class="form-control">
									    <option value="">Select Client</option>
									    <?php
											$args = array( 'post_type' => 'locations', 'posts_per_page' => -1, 'post_status' => 'any', 'post_parent' => null ); 
											$attachments = get_posts( $args );
											if ( $attachments ) {
												foreach ( $attachments as $post ) {
													setup_postdata( $post ); ?>
												<option value="<?php echo get_the_ID(); ?>" <?php if ($roster_clients_5 == get_the_ID()) echo ' selected="selected"'; ?>><?php echo get_the_title(); ?></option>	
											<?php 
											}
												wp_reset_postdata();
											}
										?>									    	
									    </select>

									</div>
								</div>
							</div>
							

							<?php 
								// Saturday
							    $roster_week_day_6 = get_post_meta($get_post_val,'roster_week_day_6',true); 
							    $roster_staff_member_6 = get_post_meta($get_post_val,'roster_staff_member_6',true); 
							    $roster_time_start_6 = get_post_meta($get_post_val,'roster_time_start_6',true); 
							    $roster_time_finish_6 = get_post_meta($get_post_val,'roster_time_finish_6',true);
							?>
							<div class="row">
								<div class="col-xs-3">
									<div class="form-group">
									    <label for="saturday_week_day">Week Day</label>
									    <!-- <input type="text" class="form-control" id="posttitle" value="<?php echo $roster_week_day_6; ?>" > -->
									    <select name="saturday_week_day" class="form-control" id="saturday_week_day">
										    <option value="">Select Week Day</option>
										    <option value="Monday" <?php if($roster_week_day_6 == 'Monday') echo "selected"; ?>>Monday</option>
										    <option value="Tuesday" <?php if($roster_week_day_6 == 'Tuesday') echo "selected"; ?>>Tuesday</option>
										    <option value="Wednesday" <?php if($roster_week_day_6 == 'Wednesday') echo "selected"; ?>>Wednesday</option>
										    <option value="Thursday" <?php if($roster_week_day_6 == 'Thursday') echo "selected"; ?>>Thursday</option>
										    <option value="Friday" <?php if($roster_week_day_6 == 'Friday') echo "selected"; ?>>Friday</option>
										    <option value="Saturday" <?php if($roster_week_day_6 == 'Saturday') echo "selected"; ?>>Saturday</option>
										    <option value="Sunday" <?php if($roster_week_day_6 == 'Sunday') echo "selected"; ?>>Sunday</option>
									    </select>
									</div>
								</div>
								<div class="col-xs-3">
									<div class="form-group">
									    <label for="saturday_time_start">Time Start</label>
									    <input type="text" name="saturday_time_start" class="form-control start-time" id="saturday_time_start" value="<?php echo $roster_time_start_6; ?>" >
									</div>
								</div>
								<div class="col-xs-3">
									<div class="form-group">
									    <label for="saturday_time_finish">Time Finish</label>
									    <input type="text" name="saturday_time_finish" class="form-control finish-time" id="saturday_time_finish" value="<?php echo $roster_time_finish_6; ?>">
									</div>
								</div>
								<div class="col-xs-3">
									<?php $roster_clients_6 = get_post_meta($get_post_val,'roster_clients_6',true); ?>
									<div class="form-group">
									    <label for="posttitle">Client</label>
									    <!-- <input type="text" class="form-control" id="roster_client" value="<?php //the_title(); ?>" readonly> -->
									    <select id="roster_clients_6" name="roster_clients_6" class="form-control">
									    	<option value="">Select Client</option>
									    <?php
											$args = array( 'post_type' => 'locations', 'posts_per_page' => -1, 'post_status' => 'any', 'post_parent' => null ); 
											$attachments = get_posts( $args );
											if ( $attachments ) {
												foreach ( $attachments as $post ) {
													setup_postdata( $post ); ?>
												<option value="<?php echo get_the_ID(); ?>" <?php if ($roster_clients_6 == get_the_ID()) echo ' selected="selected"'; ?>><?php echo get_the_title(); ?></option>	
											<?php 
											}
												wp_reset_postdata();
											}
										?>									    	
									    </select>

									</div>
								</div>
							</div>
							

							<?php 
								// Sunday
							    $roster_week_day_7 = get_post_meta($get_post_val,'roster_week_day_7',true); 
							    $roster_staff_member_7 = get_post_meta($get_post_val,'roster_staff_member_7',true); 
							    $roster_time_start_7 = get_post_meta($get_post_val,'roster_time_start_7',true); 
							    $roster_time_finish_7 = get_post_meta($get_post_val,'roster_time_finish_7',true);
							?>
							<div class="row">
								<div class="col-xs-3">
									<div class="form-group">
									    <label for="sunday_week_day">Week Day</label>
									    <!-- <input type="text" class="form-control" id="posttitle" value="<?php echo $roster_week_day_7; ?>"> -->
									    <select name="sunday_week_day" class="form-control" id="sunday_week_day">
										    <option value="">Select Week Day</option>
										    <option value="Monday" <?php if($roster_week_day_7 == 'Monday') echo "selected"; ?>>Monday</option>
										    <option value="Tuesday" <?php if($roster_week_day_7 == 'Tuesday') echo "selected"; ?>>Tuesday</option>
										    <option value="Wednesday" <?php if($roster_week_day_7 == 'Wednesday') echo "selected"; ?>>Wednesday</option>
										    <option value="Thursday" <?php if($roster_week_day_7 == 'Thursday') echo "selected"; ?>>Thursday</option>
										    <option value="Friday" <?php if($roster_week_day_7 == 'Friday') echo "selected"; ?>>Friday</option>
										    <option value="Saturday" <?php if($roster_week_day_7 == 'Saturday') echo "selected"; ?>>Saturday</option>
										    <option value="Sunday" <?php if($roster_week_day_7 == 'Sunday') echo "selected"; ?>>Sunday</option>
									    </select>
									</div>
								</div>
								<div class="col-xs-3">
									<div class="form-group">
									    <label for="sunday_time_start">Time Start</label>
									    <input type="text" name="sunday_time_start" class="form-control start-time" id="sunday_time_start" value="<?php echo $roster_time_start_7; ?>" >
									</div>
								</div>
								<div class="col-xs-3">
									<div class="form-group">
									    <label for="sunday_time_finish">Time Finish</label>
									    <input type="text" name="sunday_time_finish" class="form-control finish-time" id="sunday_time_finish" value="<?php echo $roster_time_finish_7; ?>">
									</div>
								</div>
								<div class="col-xs-3">
									<?php $roster_clients_7 = get_post_meta($get_post_val,'roster_clients_7',true); ?>
									<div class="form-group">
									    <label for="posttitle">Client</label>
									    <!-- <input type="text" class="form-control" id="roster_client" value="<?php //the_title(); ?>" readonly> -->
									    <select id="roster_clients_7" name="roster_clients_7" class="form-control">
									    	<option value="">Select Client</option>
									    <?php
											$args = array( 'post_type' => 'locations', 'posts_per_page' => -1, 'post_status' => 'any', 'post_parent' => null ); 
											$attachments = get_posts( $args );
											if ( $attachments ) {
												foreach ( $attachments as $post ) {
													setup_postdata( $post ); ?>
												<option value="<?php echo get_the_ID(); ?>" <?php if ($roster_clients_7 == get_the_ID()) echo ' selected="selected"'; ?>><?php echo get_the_title(); ?></option>	
											<?php 
											}
												wp_reset_postdata();
											}
										?>									    	
									    </select>

									</div>
								</div>
							</div>

							<?php 
								// Field 8
							    $roster_week_day_8 = get_post_meta($get_post_val,'roster_week_day_8',true); 
							    $roster_staff_member_8 = get_post_meta($get_post_val,'roster_staff_member_8',true); 
							    $roster_time_start_8 = get_post_meta($get_post_val,'roster_time_start_8',true); 
							    $roster_time_finish_8 = get_post_meta($get_post_val,'roster_time_finish_8',true);
							?>
							<div class="row">
								<div class="col-xs-3">
									<div class="form-group">
									    <label for="field8_week_day">Week Day</label>
									    <!-- <input type="text" class="form-control" id="posttitle" value="<?php echo $roster_week_day_8; ?>"> -->
									    <select name="field8_week_day" class="form-control" id="field8_week_day">
										    <option value="">Select Week Day</option>
										    <option value="Monday" <?php if($roster_week_day_8 == 'Monday') echo "selected"; ?>>Monday</option>
										    <option value="Tuesday" <?php if($roster_week_day_8 == 'Tuesday') echo "selected"; ?>>Tuesday</option>
										    <option value="Wednesday" <?php if($roster_week_day_8 == 'Wednesday') echo "selected"; ?>>Wednesday</option>
										    <option value="Thursday" <?php if($roster_week_day_8 == 'Thursday') echo "selected"; ?>>Thursday</option>
										    <option value="Friday" <?php if($roster_week_day_8 == 'Friday') echo "selected"; ?>>Friday</option>
										    <option value="Saturday" <?php if($roster_week_day_8 == 'Saturday') echo "selected"; ?>>Saturday</option>
										    <option value="Sunday" <?php if($roster_week_day_8 == 'Sunday') echo "selected"; ?>>Sunday</option>
									    </select>
									</div>
								</div>
								<div class="col-xs-3">
									<div class="form-group">
									    <label for="field8_time_start">Time Start</label>
									    <input type="text" name="field8_time_start" class="form-control start-time" id="field8_time_start" value="<?php echo $roster_time_start_8; ?>" >
									</div>
								</div>
								<div class="col-xs-3">
									<div class="form-group">
									    <label for="field8_time_finish">Time Finish</label>
									    <input type="text" name="field8_time_finish" class="form-control finish-time" id="field8_time_finish" value="<?php echo $roster_time_finish_8; ?>">
									</div>
								</div>
								<div class="col-xs-3">
									<?php $roster_clients_8 = get_post_meta($get_post_val,'roster_clients_8',true); ?>
									<div class="form-group">
									    <label for="posttitle">Client</label>
									    <!-- <input type="text" class="form-control" id="roster_client" value="<?php //the_title(); ?>" readonly> -->
									    <select id="roster_clients_8" name="roster_clients_8" class="form-control">
									    	<option value="">Select Client</option>
									    <?php
											$args = array( 'post_type' => 'locations', 'posts_per_page' => -1, 'post_status' => 'any', 'post_parent' => null ); 
											$attachments = get_posts( $args );
											if ( $attachments ) {
												foreach ( $attachments as $post ) {
													setup_postdata( $post ); ?>
												<option value="<?php echo get_the_ID(); ?>" <?php if ($roster_clients_8 == get_the_ID()) echo ' selected="selected"'; ?>><?php echo get_the_title(); ?></option>	
											<?php 
											}
												wp_reset_postdata();
											}
										?>									    	
									    </select>

									</div>
								</div>
							</div>

							<?php 
								// Field 9
							    $roster_week_day_9 = get_post_meta($get_post_val,'roster_week_day_9',true); 
							    $roster_staff_member_9 = get_post_meta($get_post_val,'roster_staff_member_9',true); 
							    $roster_time_start_9 = get_post_meta($get_post_val,'roster_time_start_9',true); 
							    $roster_time_finish_9 = get_post_meta($get_post_val,'roster_time_finish_9',true);
							?>
							<div class="row">
								<div class="col-xs-3">
									<div class="form-group">
									    <label for="field9_week_day">Week Day</label>
									    <!-- <input type="text" class="form-control" id="posttitle" value="<?php echo $roster_week_day_9; ?>"> -->
									    <select name="field9_week_day" class="form-control" id="field9_week_day">
										    <option value="">Select Week Day</option>
										    <option value="Monday" <?php if($roster_week_day_9 == 'Monday') echo "selected"; ?>>Monday</option>
										    <option value="Tuesday" <?php if($roster_week_day_9 == 'Tuesday') echo "selected"; ?>>Tuesday</option>
										    <option value="Wednesday" <?php if($roster_week_day_9 == 'Wednesday') echo "selected"; ?>>Wednesday</option>
										    <option value="Thursday" <?php if($roster_week_day_9 == 'Thursday') echo "selected"; ?>>Thursday</option>
										    <option value="Friday" <?php if($roster_week_day_9 == 'Friday') echo "selected"; ?>>Friday</option>
										    <option value="Saturday" <?php if($roster_week_day_9 == 'Saturday') echo "selected"; ?>>Saturday</option>
										    <option value="Sunday" <?php if($roster_week_day_9 == 'Sunday') echo "selected"; ?>>Sunday</option>
									    </select>
									</div>
								</div>
								<div class="col-xs-3">
									<div class="form-group">
									    <label for="field9_time_start">Time Start</label>
									    <input type="text" name="field9_time_start" class="form-control start-time" id="field9_time_start" value="<?php echo $roster_time_start_9; ?>" >
									</div>
								</div>
								<div class="col-xs-3">
									<div class="form-group">
									    <label for="field9_time_finish">Time Finish</label>
									    <input type="text" name="field9_time_finish" class="form-control finish-time" id="field9_time_finish" value="<?php echo $roster_time_finish_9; ?>">
									</div>
								</div>
								<div class="col-xs-3">
									<?php $roster_clients_9 = get_post_meta($get_post_val,'roster_clients_9',true); ?>
									<div class="form-group">
									    <label for="posttitle">Client</label>
									    <!-- <input type="text" class="form-control" id="roster_client" value="<?php //the_title(); ?>" readonly> -->
									    <select id="roster_clients_9" name="roster_clients_9" class="form-control">
									    	<option value="">Select Client</option>
									    <?php
											$args = array( 'post_type' => 'locations', 'posts_per_page' => -1, 'post_status' => 'any', 'post_parent' => null ); 
											$attachments = get_posts( $args );
											if ( $attachments ) {
												foreach ( $attachments as $post ) {
													setup_postdata( $post ); ?>
												<option value="<?php echo get_the_ID(); ?>" <?php if ($roster_clients_9 == get_the_ID()) echo ' selected="selected"'; ?>><?php echo get_the_title(); ?></option>	
											<?php 
											}
												wp_reset_postdata();
											}
										?>									    	
									    </select>

									</div>
								</div>
							</div>

							<?php 
								// Field 10
							    $roster_week_day_10 = get_post_meta($get_post_val,'roster_week_day_10',true); 
							    $roster_staff_member_10 = get_post_meta($get_post_val,'roster_staff_member_10',true); 
							    $roster_time_start_10 = get_post_meta($get_post_val,'roster_time_start_10',true); 
							    $roster_time_finish_10 = get_post_meta($get_post_val,'roster_time_finish_10',true);
							?>
							<div class="row">
								<div class="col-xs-3">
									<div class="form-group">
									    <label for="field10_week_day">Week Day</label>
									    <!-- <input type="text" class="form-control" id="posttitle" value="<?php echo $roster_week_day_10; ?>"> -->
									    <select name="field10_week_day" class="form-control" id="field10_week_day">
										    <option value="">Select Week Day</option>
										    <option value="Monday" <?php if($roster_week_day_10 == 'Monday') echo "selected"; ?>>Monday</option>
										    <option value="Tuesday" <?php if($roster_week_day_10 == 'Tuesday') echo "selected"; ?>>Tuesday</option>
										    <option value="Wednesday" <?php if($roster_week_day_10 == 'Wednesday') echo "selected"; ?>>Wednesday</option>
										    <option value="Thursday" <?php if($roster_week_day_10 == 'Thursday') echo "selected"; ?>>Thursday</option>
										    <option value="Friday" <?php if($roster_week_day_10 == 'Friday') echo "selected"; ?>>Friday</option>
										    <option value="Saturday" <?php if($roster_week_day_10 == 'Saturday') echo "selected"; ?>>Saturday</option>
										    <option value="Sunday" <?php if($roster_week_day_10 == 'Sunday') echo "selected"; ?>>Sunday</option>
									    </select>
									</div>
								</div>
								<div class="col-xs-3">
									<div class="form-group">
									    <label for="field10_time_start">Time Start</label>
									    <input type="text" name="field10_time_start" class="form-control start-time" id="field10_time_start" value="<?php echo $roster_time_start_10; ?>" >
									</div>
								</div>
								<div class="col-xs-3">
									<div class="form-group">
									    <label for="field10_time_finish">Time Finish</label>
									    <input type="text" name="field10_time_finish" class="form-control finish-time" id="field10_time_finish" value="<?php echo $roster_time_finish_10; ?>">
									</div>
								</div>
								<div class="col-xs-3">
									<?php $roster_clients_10 = get_post_meta($get_post_val,'roster_clients_10',true); ?>
									<div class="form-group">
									    <label for="posttitle">Client</label>
									    <!-- <input type="text" class="form-control" id="roster_client" value="<?php //the_title(); ?>" readonly> -->
									    <select id="roster_clients_10" name="roster_clients_10" class="form-control">
											<option value="">Select Client</option>	
									    <?php
											$args = array( 'post_type' => 'locations', 'posts_per_page' => -1, 'post_status' => 'any', 'post_parent' => null ); 
											$attachments = get_posts( $args );
											if ( $attachments ) {
												foreach ( $attachments as $post ) {
													setup_postdata( $post ); ?>
												<option value="<?php echo get_the_ID(); ?>" <?php if ($roster_clients_10 == get_the_ID()) echo ' selected="selected"'; ?>><?php echo get_the_title(); ?></option>	
											<?php 
											}
												wp_reset_postdata();
											}
										?>									    	
									    </select>

									</div>
								</div>
							</div>

							<div class="row">
								<div class="roster-edit-wrapper col-xs-12">
									<div class="roster-title" style="text-align: center;"><strong>Week Beginning</strong></div>
									<div class="roster-title" style="text-align: center;"><strong><?php the_title();?></strong></div>

									<table style="width: 100%;">
										<tbody>
											<tr>
												<th style="text-align: left;">Week Day</th>
												<!-- <th style="text-align: left;">Staff Member</th> -->
												<th style="text-align: left;">Time Start</th>
												<th style="text-align: left;">Time Finish</th>
											</tr>
											<tr>
												<td><?php echo $roster_week_day_1;?></td>
												<td><?php echo $roster_time_start_1;?></td>
												<td><?php echo $roster_time_finish_1;?></td>
												<td><?php if ($roster_clients_1) {
													echo get_the_title($roster_clients_1);
												} ?></td>
											</tr>
											<tr>
												<td><?php echo $roster_week_day_2;?></td>
												<td><?php echo $roster_time_start_2;?></td>
												<td><?php echo $roster_time_finish_2;?></td>
												<td><?php if ($roster_clients_2) {
													echo get_the_title($roster_clients_2);
												} ?></td>
											</tr>
											<tr>
												<td><?php echo $roster_week_day_3;?></td>
												<td><?php echo $roster_time_start_3;?></td>
												<td><?php echo $roster_time_finish_3;?></td>
												<td><?php if ($roster_clients_3) {
													echo get_the_title($roster_clients_3);
												} ?></td>
											</tr>
											<tr>
												<td><?php echo $roster_week_day_4;?></td>
												<td><?php echo $roster_time_start_4;?></td>
												<td><?php echo $roster_time_finish_4;?></td>
												<td><?php if ($roster_clients_4) {
													echo get_the_title($roster_clients_4);
												} ?></td>
											</tr>
											<tr>
												<td><?php echo $roster_week_day_5;?></td>
												<td><?php echo $roster_time_start_5;?></td>
												<td><?php echo $roster_time_finish_5;?></td>
												<td><?php if ($roster_clients_5) {
													echo get_the_title($roster_clients_5);
												} ?></td>
											</tr>
											<tr>
												<td><?php echo $roster_week_day_6;?></td>
												<td><?php echo $roster_time_start_6;?></td>
												<td><?php echo $roster_time_finish_6;?></td>
												<td><?php if ($roster_clients_6) {
													echo get_the_title($roster_clients_6);
												} ?></td>
											</tr>
											<tr>
												<td><?php echo $roster_week_day_7; ?></td>
												<td><?php echo $roster_time_start_7; ?></td>
												<td><?php echo $roster_time_finish_7; ?></td>
												<td><?php if ($roster_clients_7) {
													echo get_the_title($roster_clients_7);
												} ?></td>
											</tr>
											<tr>
												<td><?php echo $roster_week_day_8; ?></td>
												<td><?php echo $roster_time_start_8; ?></td>
												<td><?php echo $roster_time_finish_8; ?></td>
												<td><?php if ($roster_clients_8) {
													echo get_the_title($roster_clients_8);
												} ?></td>
											</tr>
											<tr>
												<td><?php echo $roster_week_day_9;?></td>
												<td><?php echo $roster_time_start_9;?></td>
												<td><?php echo $roster_time_finish_9;?></td>
												<td><?php if ($roster_clients_9) {
													echo get_the_title($roster_clients_9);
												} ?></td>
											</tr>
											<tr>
												<td><?php echo $roster_week_day_10;?></td>
												<td><?php echo $roster_time_start_10;?></td>
												<td><?php echo $roster_time_finish_10;?></td>
												<td><?php if ($roster_clients_10) {
													echo get_the_title($roster_clients_10);
												} ?></td>
											</tr>

										</tbody>
									</table>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12">
									<div class="update-form-btn text-center">
										<input type="hidden" class="post-id-field" id="post-id-field" value="<?php echo $get_post_val; ?>">
										<button type="submit" id="edit_roster_update_post" class="btn custom-btn">Update Post</button>
										<div class="roster-print-btn">
											<?php echo do_shortcode('[print-me print_only="div.roster-edit-wrapper" target="div.roster-edit-wrapper" title="Print"]'); ?>
										</div>
									</div>
								</div>
							</div>
						</div>
						
						<div class="edit-roster-sucess-message"></div>

					<?php endwhile; ?>
					<!-- end of the loop -->

					<!-- pagination here -->

					<?php wp_reset_postdata(); ?>

					<?php else : ?>
						<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
					<?php endif; ?>	

			<?php }  // manager user condition end
			} ?>	
			<?php } else { // user condition end	?>
	
				<div class="error-login-wrapper text-center">
					<h4>Please login to visit this page.</h4>
					<a href="<?php echo $redux_demo['header-login']; ?>" class="btn custom-btn">Login</a>						
				</div>
			<?php } // Not Login (else condition) user condition end ?>			
				
		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();
