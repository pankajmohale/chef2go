<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package _s
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<div class="row">
			<div id="main" class="col-xs-12 col-sm-9 site-main" role="main">

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					
				<?php while ( have_posts() ) : the_post(); ?>
				<?php $current_user = wp_get_current_user();
						foreach ($current_user->roles as $key => $value) {
							if ($value == 'manager') { ?>
							<div class="myarea-page-links pull-right">
								<a href="<?php echo site_url(); ?>/manager-myarea/manage-employee/" class="btn custom-btn">Back to manage employee</a>
							</div>					
					<?php	}
						}
					?>
				<div class="entry-header">
					<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
				</div><!-- .entry-header -->
				<div class="entry-content">
					<?php
						the_content();
					?>
				</div><!-- .entry-content -->

				<div class="single-employee-body">
					<div class="row">
						<div class="col-sm-4">
							<strong>Seeking work as : </strong>
						</div>
						<div class="col-sm-8">
							<?php 
								$term_list = wp_get_post_terms(get_the_id(), 'staff_type_of_recruitment', array("fields" => "all"));
								$term_list_count = count($term_list);
								$i = 0;
								foreach($term_list as $term_single) {
									if(++$i === $term_list_count) {
										echo $term_single->name;
									}else{
										echo $term_single->name.', ';
									}
								}
							?>
						</div>
					</div>
					
					<div class="row">
						<div class="col-sm-4">
							<strong>Hospitality Position : </strong>
						</div>
						<div class="col-sm-8">
						<?php 
							$position_type_list = wp_get_post_terms(get_the_id(), 'position_type', array("fields" => "all"));
							$position_type_list_count = count($position_type_list);
							$i = 0;
							foreach($position_type_list as $position_type_single) {
								if(++$i === $position_type_list_count) {
									echo $position_type_single->name;
								}else{
									echo $position_type_single->name.', ';
								}
							}
						?>
						</div>
					</div>

					<div class="row">
						<div class="col-sm-4">
							<strong>Freelance Hourly Rate : </strong>
						</div>
						<div class="col-sm-8">
						<?php 
							$_hourly_rate = get_post_meta(get_the_id(),'staff_hourly_rate',true);
						?>
						<?php echo $_hourly_rate; ?>
						</div>
					</div>

					<div class="row">
						<div class="col-sm-4">
							<strong>Can work in remote locations : </strong>
						</div>
						<div class="col-sm-8">
						<?php 
						$_remote_locations = get_post_meta(get_the_id(),'staff_work_in_remote_locations',true);
						?>
						<?php echo $_remote_locations; ?>
						</div>
					</div>

					<div class="row">
						<div class="col-sm-4">
							<strong>Residency : </strong>
						</div>
						<div class="col-sm-8">
						<?php 
							$specialising_in_list = wp_get_post_terms(get_the_id(), 'location_residency', array("fields" => "all"));
							$specialising_in_list_count = count($specialising_in_list);
							$i = 0;
							foreach($specialising_in_list as $specialising_in_single) {
								if(++$i === $specialising_in_list_count) {
									echo $specialising_in_single->name;
								}else{
									echo $specialising_in_single->name.', ';
								}
							}
						?>
						</div>
					</div>	
					<div class="row">
						<div class="col-sm-4">
							<strong>Citizen Status : </strong>
						</div>
						<div class="col-sm-8">
						<?php 
							$citizenship_visa_list = wp_get_post_terms(get_the_id(), 'staff_citizenship', array("fields" => "all"));
							$citizenship_visa_list_count = count($specialising_in_list);
							$i = 0;
							foreach($citizenship_visa_list as $citizenship_visa_single) {
								if(++$i === $citizenship_visa_list_count) {
									echo $citizenship_visa_single->name;
								}else{
									echo $citizenship_visa_single->name.', ';
								}
							}
						?>
						</div>
					</div>					
				</div>
				<div class="added-roster-display ">
					<div class="row">
						<div class="col-xs-12">
							<div class="section-title text-center">
								<h2>Allocated Roster List</h2>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="row">
							<?php 
								$_user_id = get_post_meta(get_the_id(),'employee_user_id',true);
							?>
							<?php 
							// the query
							$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
							$args = array(
								'post_type' => 'rosters',
								'posts_per_page' => 4,
								'meta_query' => array(
									array(
										'key'     => 'roster_member_list',
										'value'   => $_user_id,
										'compare' => 'LIKE',
									),
								),
								'paged' => $paged,
							);
							$the_query = new WP_Query( $args ); ?>

							<?php if ( $the_query->have_posts() ) : ?>

								<!-- pagination here -->

								<!-- the loop -->
								<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
									<div class="col-md-6">
										<div class="employee-roster-wrapper equalheight manage-employee-single-roster">
										<span class="corner-top-left"></span>
										<span class="corner-top-right"></span>
										<span class="corner-bottom-left"></span>
										<span class="corner-bottom-right"></span>
											<div class="roster-title">
												<div class="row">
													<div class="col-sm-12">
														<div class="text-center">
															<div class="rosterheading"><h3><?php the_title(); ?></h3></div>
															<!-- <div class="roster-client-name"><h6><?php // echo get_the_title( $roster_clients ); ?></h6></div> -->
														</div>
													</div>										
												</div>
											</div>
											<div class="roster-details">
												<?php 
													for ($rostermeta = 1; $rostermeta <= 10; $rostermeta++) { 
														$daymetakey = 'roster_week_day_'.$rostermeta;
														$startmetakey = 'roster_time_start_'.$rostermeta;
														$endmetakey = 'roster_time_finish_'.$rostermeta;
														$clientsmetakey = 'roster_clients_'.$rostermeta;

														$daymetakey = get_post_meta( get_the_ID(), $daymetakey, true );
														$startmetakey = get_post_meta( get_the_ID(), $startmetakey, true );
														$endmetakey = get_post_meta( get_the_ID(), $endmetakey, true );
														$roster_clients = get_post_meta( get_the_ID(), $clientsmetakey, true ); 
													if ($startmetakey && $startmetakey && $endmetakey) {
													?>
													<div class="row">
														<div class="roster-timeing clearfix">
															<div class="col-sm-3 text-left"><?php echo $daymetakey; ?></div>
															<div class="col-sm-4 text-left"><?php echo get_the_title( $roster_clients ); ?></div>
															<div class="col-sm-5 text-right"><?php echo $startmetakey; ?> to <?php echo $endmetakey; ?></div>											
														</div>
													</div>
												<?php	} 
													} /* loop end */
												?>										
											</div>
										</div>
									</div>
								<?php endwhile; ?>
								<!-- end of the loop -->

								<!-- pagination here -->
								<?php if (function_exists("pagination")) {
								    pagination($the_query->max_num_pages);
								} ?>

								<?php wp_reset_postdata(); ?>

							<?php else : ?>
								<div class="col-sm-12">
									<h4><?php _e( 'Sorry, no rosters added your criteria.' ); ?></h4>
								</div>
							<?php endif; ?>
							</div>
						</div>
					</div>
				</div>

				<?php 
				endwhile; // End of the loop.
				?>
			</article><!-- #post-## -->


			</div><!-- #main -->
			<div id="sidebar" class="sidebar col-xs-12 col-sm-3">
				<?php get_sidebar(); ?>
			</div>			
		</div>
	</div><!-- #primary -->

<?php
get_footer();
