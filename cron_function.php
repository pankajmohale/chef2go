<?php
/**
 * Tell WordPress we are doing the CRON task.
 *
 * @var bool
 */
define('DOING_CRON', true);

if ( !defined('ABSPATH') ) {
	/** Set up WordPress environment */
	require_once( dirname( __FILE__ ) . '/wp-load.php' );
}

add_filter( 'wp_mail_from', function() {
    return 'wordpress@stagingrgba.com';
} );

//wp_mail( 'pankaj.mohale@prodigitas.com', 'Automatic email', 'Automatic scheduled email from chefs2go.');
	if(date('D') === 'Mon'){
		$weeklymailsubject = 'Weekly Email';
		$weeklymailbody = 'Good Morning<br/>Thanks for your good work this week, this is just a friendly reminder that all timesheets must be signed and submitted by Monday at 6pm so you can be paid by Thursday.<br/>All timesheets must be signed by a supervisor from the venue you were working at for it to be valid. If not signed it will be sent back to you to get signed off so we can process it the following week. If you miss the Tuesday 6pm cut off, you will go into the next week pay run.<br/><br/>Thanks for working with us and getting your signed timesheet in on time so we here at Chefs2Go can get you paid in a timely manner.<br/>Kind Regards, Bill';
		// get the chefs
		$chef_query = new WP_User_Query(
			array(
				'role'	 =>	'chef_employee',
			)
		);
		$chefs = $chef_query->get_results();
		
		// get the staff
		$staff_employee_query = new WP_User_Query(
			array(
				'role'	 =>	'staff_employee',
			)
		);
		$staffs = $staff_employee_query->get_results();
		
		// store them all as users
		$users = array_merge( $staffs, $chefs );
		
		// User Loop
		if ( ! empty( $users ) ) {
			foreach ( $users as $user ) { 
				$user_email_array[] = $user->user_email;
			}
			foreach ($user_email_array as $email_array) {
				$to = $email_array;
				$subject = $weeklymailsubject;
				$body = $weeklymailbody;
				$headers = array('Content-Type: text/html; charset=UTF-8');
			
				wp_mail( $to, $subject, $body, $headers );
			}
		} else {
			echo 'No users found.';
		}
	}
	
	
	// get the Recruiter
	$monthlymailsubject = 'Renewal Notifications';
	$monthlymailbody = 'Thanks for choosing Chefs2Go to find your perfect job, we hope you have been successful in finding the next step in your career.<br/>Your profile has been running for a month now and is about to expire from the list of candidates looking for work. If you have not found the job of your dreams yet and would like to keep looking with Chefs2Go you must log back on to your Chefs2Go account and click (looking for work) to re-activate your profile. This will place you back on the top of candidate list and back looking for work for another month.<br/>Once you have found your dream job please log back on to your Chefs2Go account and click (found work), this will remove you from the candidate list until the next time you are looking to move your career forward. When you simply wish to update your resume and profile click, (looking for work) to start finding your next job.<br/>Kind Regards, Bill';
	
	
	$recruiter_query = new WP_User_Query(
		array(
			'role'	 =>	'recruiter',
		)
	);
	$recruiter = $recruiter_query->get_results();

	// User Loop
	// Loop for get user registered date and add 28 days in that date.
	// for loop for check current date is same as expiredate if yes then it will send mail to
	// that user.
	if ( ! empty( $recruiter ) ) {
		foreach ( $recruiter as $user ) {
			$registered = $user->user_registered;
			$user_email = $user->user_email;
			$recruiter_id = $user->id;
			$type_of_subscription = get_user_meta( $recruiter_id, 'type_of_subscription', true );
			$registered_expire_date = date( "d M Y", strtotime( $registered.'+28 day' ) );
			$registered_expire_date_year = date( "d M Y", strtotime( $registered.'+330 day' ) );
			
			if (date( "d M Y") === $registered_expire_date && $type_of_subscription === 'Per Month Subscription') {
				$to = $user_email;
				$subject = $monthlymailsubject;
				$body = $monthlymailbody;
				$headers = array('Content-Type: text/html; charset=UTF-8');
			
				wp_mail( $to, $subject, $body, $headers );
			} elseif (date( "d M Y") === $registered_expire_date_year && $type_of_subscription === 'Per Year Subscription') {
				$to = $user_email;
				$subject = $monthlymailsubject;
				$body = $monthlymailbody;
				$headers = array('Content-Type: text/html; charset=UTF-8');
			
				wp_mail( $to, $subject, $body, $headers );
			}
		}
	} else {
		echo 'No recruiter found.';
	}