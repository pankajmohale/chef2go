<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'chefs2go');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '=bV@9g8V~qy_>D:zy$.r(h&=DK]vTek~Ly/EL$RHi!{y<Q^EV-/!t?0(}$N>YL[C');
define('SECURE_AUTH_KEY',  'oSH*i+<qbdh(v%6Jm&!Y9E+R;ir/waSW$=tH2q<5y1upBGfV-i-WpG!mG)m} !bE');
define('LOGGED_IN_KEY',    'Yt6POB}`-&|Y5cX$7pI7]g%Wx<>NWmq>LpA1+*&T~^>O:A6q/(Qy.vsin)w<8L53');
define('NONCE_KEY',        'EJ9d7peRn$j2`gZRYgNvPNkx8->4qY-R`2WS^<t(nDK?+KC.r5]w8(| ;`,o[}IP');
define('AUTH_SALT',        'jQ`aLr(e]-c8%CPAr=AAE~yZ=j#n{J7H#CGKfgQmR!MkU)()i~[1nR?SO7CdpD*`');
define('SECURE_AUTH_SALT', '*vFQdU<`n~PvRj_s/<Y638[y&`f*0ce2-wPKPi=oWMg+@A4uT{u9%cy`67lv2L(Q');
define('LOGGED_IN_SALT',   'IH6uza5s!qtwzT}/MX}+jDx!N&UA]l3e8+pBGSEGdV)T[(Q^>F)<Hl@og!urW@P2');
define('NONCE_SALT',       '(D-I?DD}U{@ZR2]TG`cA2$FW^/@:CL/yNfSH`%xZd:i#wG/QkugWW,~UPdLN gj5');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
